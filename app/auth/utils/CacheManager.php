<?php

class CacheManager
{
    private $lifetime;

    public function __construct($lifetime)
    {
        $this->setLifeTime($lifetime);
    }

    public function setLifeTime($lifetime)
    {
        $this->lifetime = $lifetime;
    }

    public function save($key, $value)
    {
        apc_store($key, $value, $this->lifetime);
    }

    public function get($key)
    {
        return apc_fetch($key);
    }

    public function exists($key)
    {
        return apc_exists($key);
    }

    public function delete($key)
    {
        return apc_delete($key);
    }
}