<?php

require_once(__DIR__."/../../library/PHPMailer/smtp.php");
require_once(__DIR__."/../../library/PHPMailer/PHPMailer.php");

class MailManager extends Phalcon\Mvc\User\Component {

    private $mailer;

    public function __construct()
    {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'mail.iclmsandbox.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'iclm';
        $mail->Password = '1@Qwerty';
        $mail->Port = 25;
        $mail->FromName = 'ICLMManager';
        $mail->From = 'system@ICLMmanager.com';
        $mail->isHTML(true);
        $mail->SMTPOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];

        $this->mailer = $mail;
    }

    public function send($to,$subject, $body)
    {
        $this->mailer->addAddress($to);
        $this->mailer->Subject = $subject;
        $this->mailer->Body = $body;

        if (! $this->mailer->send())
        {
            return false;
        }
    }
}