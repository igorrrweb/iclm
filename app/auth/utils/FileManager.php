<?php

class FileManager extends Phalcon\Mvc\User\Component {

    const UPLOAD_PATH = "../public/uploads/" ;

    private static function setFilePath($modelName)
    {
        $path =  self::UPLOAD_PATH . $modelName . "/";

        if (!file_exists($path))
        {
            return false;
        }

        $scandir = scandir($path);

        if (!isset($scandir[2]))
        {
            mkdir($path . "1", 0777);
        }

        return $path . max($scandir) . "/";
    }

    private static function getFilePath($modelName)
    {
        $path =  self::UPLOAD_PATH . $modelName . "/";

        if (!file_exists($path))
        {
            return false;
        }

        $lastFolder = max(scandir($path));
        return $path . $lastFolder . "/";
    }

    public static function getFolderID($modelName)
    {
        return max(scandir(self::UPLOAD_PATH . $modelName));
    }

    public static function hasFiles($modelName,$fileID)
    {
        $path = self::getFilePath($modelName);
        if (!$path)
        {
            return false;
        }

        $file = glob($path . (int)$fileID . ".*");
        return isset($file[0]);
    }

    public static function getFile($modelName,$fileID)
    {
        $file = glob(self::getFilePath($modelName) . $fileID . ".*");

        if ($file)
        {
            return substr($file[0],9);
        } else {
            return false;
        }

    }

    public static function getFiles($modelName,$fileID)
    {
        $allFiles = scandir(self::getFilePath($modelName) . $fileID . "/");
        $files = array_values(array_diff($allFiles, ['.', '..']));

        foreach ($files as $key=>$file)
        {
            $files[$key] = substr(self::getFilePath($modelName),9) . $fileID . "/" . $file;
        }

        return $files;
    }

    public static function upload($modelName,$file,$fileID, $multiple = false)
    {
        if (is_int($fileID / 1000)){
            $newDir = max(scandir("../uploads/" . $modelName)) + 1;
            mkdir($newDir, 0777);
        }

        $fileDir = self::setFilePath($modelName);

        $ext = strtolower($file->getExtension());
        $fileName = $fileID;

        if ($multiple)
        {
            $fileDir = $fileDir . "/" . $fileID . "/";
            $fileName = $file->getKey();
            if (!is_dir($fileDir))
            {
                mkdir($fileDir, 0777);
            }

        }

        if (isset(glob($fileDir . $fileName . ".*")[0]))
        {
            unlink(glob($fileDir . $fileName . ".*")[0]);
        }

        return $file->moveTo($fileDir . $fileName . "." . $ext);
    }

    public static function delete($modelName,$fileID)
    {
        $file = glob(self::getFilePath($modelName) . $fileID . ".*");

        if (isset($file[0]))
        {
            unlink($file[0]);
        } else {
            return false;
        }
    }
}