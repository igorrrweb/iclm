<?php

class ActiveForm extends \Phalcon\Mvc\User\Component {

   private $model;
   private $modelName;
   private $inputs = [];

   public function __construct($model)
   {
       $this->model = $model;
       $this->modelName = $model->getModelName();
       $this->buildInputs();
   }

   protected function buildInputs()
   {
       $labels = $this->model->labels($this->session->lang);


       foreach ($this->model->getAttributes(true) as $attribute => $dataType)
       {

              $inputName = $this->modelName. "[". $attribute ."]";
              $attribute == "image" ? $dataType = 5 : "";
              $attribute == "product_id" || $attribute == "category_id" ? $dataType = 3 : "";
              $value =  $this->model->$attribute;
               $label = "<label for='" . $inputName . "'>". $labels[$attribute] . "</label>";

               switch ($dataType) {
                   case 2:
                       $this->inputs[] = $label.$this->tag->textField(array($inputName, $this->model->$attribute, "class" => "input-lg", "value" => $value));
                       break;
                   case 0:
                       $this->inputs[] = $label.$this->tag->numericField(array($inputName, $this->model->$attribute, "class" => "input-lg", "value" => $value));
                       break;
                   case 4:
                       $this->inputs[] = $label."<div class='iconInput'><span class='fr'><i class='fa fa-calendar'></i></span>" .
                                          $this->tag->textField(array($inputName, $this->model->$attribute, "class" => "input-lg datepicker", "value" => $value)) .
                                         "</div>";
                       break;
                   case 5:
                       $this->inputs[] = $label."<a class='file-input-wrapper btn form-control file2 btn btn-primary'><i class='fa fa-file'></i>Upload
                                         ".$this->tag->fileField(array($inputName, $this->model->$attribute, "class" => "input-lg file2", "value" => $value)) .
                                         "</a>";
                       break;
                   case 3:
                       $related = $this->model->getRelatedByFk($attribute);
                       $this->inputs[] = $label.$this->tag->select(array($inputName,$related[0], "using" => array("id",$related[1]), "class" => "select2", "value" => $value));
                       break;
                   default:
                       $this->inputs[] = $label.$this->tag->textField(array($inputName, $this->model->$attribute, "class" => "input-lg", "value" => $value));
                       break;
                }
           }


   }


   public function getInputs()
   {
        return $this->inputs;
   }
}