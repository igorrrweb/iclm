<?php

class ACL extends Phalcon\Mvc\User\Component {
    private $resource;

    private function __construct()
    {
        $this->resource = $this->router->getControllerName() != "" ? $this->router->getControllerName() : "index";
    }

    public static function getInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }

    public function checkUserPermission()
    {
        $role = 0;

        if (unserialize($this->session->get("user"))) {
            $role = unserialize($this->session->get("user"))->role_id;
        }

        $resource = SystemResource::findFirst([
                "conditions" => "name = ?1",
                "bind" => ["1" => (string)$this->resource]
            ]);

        if (!$resource)
        {
            $this->response->redirect("/404");
            return false;
        }

        $permission = $resource->getSystemRoleResources()->toArray();

        if (!$permission || $role < $permission[0]['role'])
        {
            $this->response->redirect("/index");
            return false;
        }
    }



}