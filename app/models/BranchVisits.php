<?php

class BranchVisits extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $customer_id;

    /**
     *
     * @var integer
     */
    public $branch_id;

    /**
     *
     * @var string
     */
    public $enter;


    /**
     *
     * @var string
     */
    public $exit;

    /**
     *
     * @var integer
     */
    public $duration;

    /**
     *
     * @var integer
     */
    public $abandon;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'customer_id' => 'customer_id',
            'branch_id' => 'branch_id',
            'beacon_id' => 'beacon_id',
            'enter' => 'enter',
            'exit' => 'exit',
            'duration' => 'duration',
            'abandon' => 'abandon',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public static function getCurrentVisitors()
    {
        return self::count(["conditions" => " enter < '" . date("Y-m-d H:i:s") . "' AND exit IS NULL"]);
    }

    public static function getWeeklyVisitors()
    {
        $phql = "SELECT COUNT(*) as count, DAYNAME(enter) as day
                FROM branch_visits
                WHERE date_format(enter, '%Y-%m-%d') BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY) AND date_format(now(),'%Y-%m-%d')
                GROUP BY DAYOFWEEK(enter) ORDER BY date_format(enter,'%w') ASC LIMIT 7";
        $model = new BranchVisits();
        $res =  new Phalcon\Mvc\Model\Resultset\Simple(null, $model, $model->getReadConnection()->query($phql));

        return self::setWeekDaysResponse($res->toArray());
    }

    public static function getVisitorsByDays($segmentID,$branchID,$period)
    {
        $phql = "SELECT COUNT(*) AS visitors FROM BranchVisits bv JOIN SegmentCustomer sc ON bv.customer_id = sc.customer_id
                 WHERE bv.branch_id = " . (int)$branchID . " AND exit BETWEEN  '" . date("Y-m-d", time() - 86400 * $period) . "'
                 AND '" . date("Y-m-d") . "' AND sc.segment_id = " . (int)$segmentID;

        return self::getManager()->executeQuery($phql)[0]->visitors == null ? 0 : self::getManager()->executeQuery($phql)[0]->visitors;
    }

    public static function getAverageMemberVisits($segmentID,$branchID,$period)
    {
        $phql = "SELECT COUNT(DISTINCT bv.customer_id) as customer FROM BranchVisits bv JOIN SegmentCustomer sc ON
                 sc.customer_id = bv.customer_id WHERE sc.segment_id = " . (int)$segmentID . " AND bv.branch_id =
                 " . (int)$branchID . " AND bv.enter BETWEEN '" . date("Y-m-d", time() - 86400 * $period) . "' AND
                 '" . date("Y-m-d") ."'";

        return self::getManager()->executeQuery($phql)[0]->customer > 0 ?
               100 / self::getManager()->executeQuery($phql)[0]->customer : 0;
    }
}
