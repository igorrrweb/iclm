<?php

class CouponTranslation extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $coupon_id;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $sub_title;

    /**
     *
     * @var string
     */
    public $details;

    /**
     *
     * @var string
     */
    public $disclaimer;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'coupon_id' => 'coupon_id', 
            'lang' => 'lang',
            'title' => 'title', 
            'sub_title' => 'sub_title', 
            'details' => 'details', 
            'disclaimer' => 'disclaimer', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        ];
    }

}
