<?php

class GdCities extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $city_id;

    /**
     *
     * @var string
     */
    public $country_slug;

    /**
     *
     * @var string
     */
    public $region_slug;

    /**
     *
     * @var string
     */
    public $city;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'gd_cities';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return GdCities[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return GdCities
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
