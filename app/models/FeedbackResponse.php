<?php

class FeedbackResponse extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $customer_id;

    /**
     *
     * @var integer
     */
    public $feedback_id;

    /**
     *
     * @var integer
     */
    public $question_id;

    /**
     *
     * @var string
     */
    public $response;

    /**
     *
     * @var integer
     */
    public $is_read;

    /**
     *
     * @var integer
     */
    public $is_archived;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'customer_id' => 'customer_id', 
            'feedback_id' => 'feedback_id', 
            'question_id' => 'question_id', 
            'response' => 'response',
            'is_read' => 'is_read',
            'is_archived' => 'is_archived',
            'updated_at' => 'updated_at', 
            'created_at' => 'created_at'
        ];
    }

    public function initialize()
    {
        parent::initialize();
        $this->belongsTo("customer_id","Customer","id");
        $this->belongsTo("feedback_id","Feedback","id");
        $this->belongsTo("question_id","FeedbackQuestion","id");
    }

    public function afterFetch()
    {
        $this->created_at = self::toICLMDate($this->created_at);
    }
}
