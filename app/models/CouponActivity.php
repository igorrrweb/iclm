<?php

class CouponActivity extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $coupon_id;

    /**
     *
     * @var integer
     */
    public $customer_id;

    /**
     *
     * @var integer
     */
    public $share_count;

    /**
     *
     * @var string
     */
    public $offered;

    /**
     *
     * @var string
     */
    public $presented;

    /**
     *
     * @var string
     */
    public $declined;

    /**
     *
     * @var string
     */
    public $pending;

    /**
     *
     * @var string
     */
    public $redeemed;

    /**
     *
     * @var string
     */
    public $viewed;

    /**
     *
     * @var integer
     */
    public $shared_by;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'coupon_id' => 'coupon_id',
            'customer_id' => 'customer_id',
            'share_count' => 'share_count',
            'offered' => 'offered',
            'presented' => 'presented',
            'declined' => 'declined',
            'pending' => 'pending',
            'redeemed' => 'redeemed',
            'viewed' => 'viewed',
            'shared_by' => 'shared_by',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public static function getDailyRedeemed()
    {

        return self::count("redeemed BETWEEN '" . date("Y-m-d") . " 00:00:00' AND '" . date("Y-m-d") . " 23:59:59'");
    }

    // last 30 days
    public static function getLastRedeemed()
    {
        return self::count("redeemed BETWEEN '" . date("Y-m-d H:i:s", time() - 2592000) . "' AND '" . date("Y-m-d H:i:s") . "'");
    }

    public static function getDataByHour($column, $couponID)
    {
        $data = [];

        $phql = "SELECT HOUR(" . $column . ") as hour ,count(" . $column . ") as count FROM CouponActivity
                 WHERE coupon_id = ".(int)$couponID." AND " . $column ." IS NOT NULL GROUP BY HOUR(presented)";

        $rows = self::getManager()->executeQuery($phql);

        foreach ($rows as $row)
        {
            $data[$row->hour] = $row->count;
        }

        return $data;
    }

    public static function getDataByWeek($column, $couponID)
    {
        $data = [];

        $phql = "SELECT DAYOFWEEK(" . $column . ") AS day ,COUNT(*) AS count FROM CouponActivity
                 WHERE coupon_id = " . (int)$couponID . " AND " . $column . " IS NOT NULL GROUP BY
                 DAYOFWEEK(".$column.")";

        $rows =  self::getManager()->executeQuery($phql);

        foreach ($rows as $row)
        {
            $data[$row->day] = $row->count;
        }

        return $data;
    }

    public static function getDataBySegment($segmentID, $couponID, $column)
    {
        $phql = "SELECT COUNT(*) AS count FROM CouponActivity cac JOIN CouponSegment cse ON cac.coupon_id = cse.coupon_id
                 WHERE cse.segment_id = " . (int)$segmentID . " AND cse.coupon_id = " . (int)$couponID . " AND " . $column .
                 " IS NOT NULL";

        return self::getManager()->executeQuery($phql)[0]->count;
    }

    public static function getWeeklyRedeemed()
    {
        $phql = "SELECT COUNT(*) as count, DAYNAME(redeemed) as day
                 FROM coupon_activity
                 WHERE date_format(redeemed, '%Y-%m-%d') BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY) AND date_format(now(),'%Y-%m-%d')
                 GROUP BY DAYOFWEEK(redeemed) ORDER BY DAY(redeemed) DESC LIMIT 7";
        $model = new CouponActivity();
        $res =  new Phalcon\Mvc\Model\Resultset\Simple(null, $model, $model->getReadConnection()->query($phql));
        return self::setWeekDaysResponse($res->toArray());
    }

    public static function getRedeemedByWeek($week)
    {
        return self::count("redeemed BETWEEN '" . date("Y-m-d H:m:s", time() - 86400 * ($week * 7)) . "' AND '" .
                            date("Y-m-d H:m:s", time() - 86400 * ($week - 1) * 7) . "'");
    }

    public static function getDataByDate($couponID,$date, $column)
    {
        return self::count("coupon_id =" . (int)$couponID . " AND " . $column . " = '" . $date . "'");
    }

    public static function getLabel($dateFrom, $dateTo, $id, $flag, $column ){
        $chartData = array();
        $chartData['labels'] = array();
        $chartData['data'] = array();
        $dtz = new \DateTimeZone('UTC');
        $datatime_now = new \DateTime($dateFrom,$dtz);
        $datatime_exp = new \DateTime($dateTo,$dtz);
        $diffInterval = $datatime_now->diff($datatime_exp);
        $days = $diffInterval->days;
        $month = $diffInterval->m;
        $years = $diffInterval->y;
        $invert = $diffInterval->invert;
        if($diffInterval->invert === 0){
            $from = $dateFrom;
            $to = $dateTo;
        } else {
            $from = $dateTo;
            $to = $dateFrom;
        }
        $start_date = new \DateTime($from,$dtz);
        $finish_date = new \DateTime($to,$dtz);
        if($days==0){
            $interval = 'HOUR';
            $date_format = '%H';
            for($i=0; $i<=23; $i++){
                $chartData['labels'][]=$start_date->format('H');
                $h_date = $start_date->format('Y-m-d H:i:s');
                $query_res = self::getStatistic($column, $flag, $id, $h_date, null, $date_format, $interval, 1);
                if(!$query_res){
                    $chartData['data'][] = 0;
                } else {
                    foreach($query_res as $row){
                        $chartData['data'][] = (int)$row['count'];
                    }
                }
                $start_date->modify('+1 hours');
            }
        }
        if(($days > 0)&&($days <=7 )){
            $interval = 'DAY';
            $date_format = '%d';
            $st = $start_date->format('z');
            for($i=$st; $i<=$finish_date->format('z'); $i++){
                $chartData['labels'][]=$start_date->format('d/m');
                $d_date = $start_date->format('Y-m-d 00:00:00');
                $query_res = self::getStatistic($column, $flag, $id, $d_date, null, $date_format, $interval, 1);
                if(!$query_res){
                    $chartData['data'][] = 0;
                } else {
                    foreach($query_res as $row){
                        $chartData['data'][] = (int)$row['count'];
                    }
                }
                $start_date->modify('+1 day');
            }
        }
        if(($days>7) && ($days<=365)){
            $interval = 'DAY';
            $date_format = '%Y';
            $num_start_week = $start_date->format('W');
            $num_finish_week = $finish_date->format('W');
            $day_of_week_from = $start_date->format('N');
            $day_of_week_to = $finish_date->format('N');
            $between_arr = array();
            if(($finish_date->format('Y') - $start_date->format('o'))>0 ) {
                $Y = $start_date->format('Y');
                $dateCalculation = new \DateTime($Y."-12-30", $dtz);
                $max_week = $dateCalculation->format('W') == '01' ? 52 : 53;
                $max_i = $max_week - $num_start_week + $num_finish_week;
            } else {
                $max_i = $num_finish_week-$num_start_week;
            }
            for($i = 1; $i<=$max_i; $i++){
                if($i==1){
                    $first_from = $start_date->format('Y-m-d 00:00:00');
                    if($day_of_week_from > 3){
                        $start_date->modify("next Monday");
                        $start_date->modify("next Sunday");
                        $first_next_sunday = $start_date->format('Y-m-d 00:00:00');
                    } else {
                        $first_next_sunday = $start_date->modify("next Sunday")->format('Y-m-d 00:00:00');
                    }
                    $between_arr[] = array('from'=>$first_from, 'to'=>$first_next_sunday);
                }
                if($i>1 && $i<$max_i){
                    $between_arr[] = array(
                        'from'=>$start_date->modify("next Monday")->format('Y-m-d 00:00:00'),
                        'to'=>$start_date->modify("next Sunday")->format('Y-m-d 00:00:00')
                    );
                }
                if($i==$max_i){
                    $between_arr[] = array(
                        'from'=>$start_date->modify("next Monday")->format('Y-m-d 00:00:00'),
                        'to'=>$finish_date->format('Y-m-d 00:00:00')
                    );
                }
            }
            foreach ($between_arr as $key => $between){
                $between_label_from = new \DateTime($between['from'],$dtz);
                $between_label_to = new \DateTime($between['to'],$dtz);
                $chartData['labels'][]=$between_label_from->format('d/m').'-'.$between_label_to->format('d/m');
                $query_res = self::getStatistic($column, $flag, $id, $between['from'], $between['to'], $date_format, $interval, 1);
                if(!$query_res){
                    $chartData['data'][] = 0;
                } else {
                    foreach($query_res as $row){
                        $chartData['data'][] = (int)$row['count'];
                    }
                }
            }
        }
        if($days>365){
            $date_format = '%Y';
            $interval = 'YEAR';
            $chartData['labels'] = array();
            $chartData['data'] = array();
            $num_start_year = $start_date->format('Y');
            $num_finish_year = $finish_date->format('Y');
            for($i=$num_start_year; $i<=$num_finish_year; $i++){
                $y_date = $start_date->format('Y-01-01 00:00:00');
                $chartData['labels'][]=$start_date->format('Y');
                $query_res = self::getStatistic($column, $flag, $id, $y_date, null, $date_format, $interval, 1);
                if(!$query_res){
                    $chartData['data'][] = 0;
                } else {
                    foreach($query_res as $row){
                        $chartData['data'][] = (int)$row['count'];
                    }
                }
                $start_date->modify('+1 year');
            }
        }
        return $chartData;
    }

    public static function getStatistic($column, $flag, $id, $from, $to=null, $date_format, $interval, $interval_value){
        $dtz = new \DateTimeZone('UTC');
        $date_add = new \DateTime($from,$dtz);
        $mod_str = "+".$interval_value." ".$interval;

        $date_add->modify($mod_str);
        $end_date = $date_add->format('Y-m-d 00:00:00');
        $to = $to == null ? $end_date : $to;


        $sql = "SELECT date_format($column, '$date_format') as date, COUNT(*) AS count
                FROM CouponActivity WHERE coupon_id = " . $id .
                " AND $column BETWEEN '" . $from . "' AND '" . $to . "' GROUP BY date_format($column, '$date_format')";

        return self::getManager()->createQuery($sql)->execute()->toArray();
    }

    public static function getDataByDateRange($couponID, $startDate, $endDate, $column)
    {
        $startDate = self::toSQLDate($startDate);
        $endDate = self::toSQLDate($endDate);

        $data_arr = self::getLabel($startDate, $endDate, $couponID, 'coupons', $column);


        $data = ["dates" => [], "values" => []];

        $data["dates"] = $data_arr['labels'];
        $data["values"] = $data_arr['data'];

        return $data;
    }


}
