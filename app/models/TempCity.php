<?php

class TempCity extends BaseModel
{

    /**
     *
     * @var string
     */
    public $city;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'temp_city';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TempCity[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TempCity
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
