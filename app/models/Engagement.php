<?php

class Engagement extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $welcome_img;

    /**
     *
     * @var string
     */
    public $welcome_msg;

    /**
     *
     * @var string
     */
    public $thankyou_msg;

    /**
     *
     * @var string
     */
    public $start_date;

    /**
     *
     * @var string
     */
    public $end_date;

    /**
     *
     * @var string
     */
     public $maxed_out;

    /**
     *
     * @var integer
     */
    public $never_expires;

    /**
     *
     * @var integer
     */
    public $reward;

    /**
     *
     * @var integer
     */
    public $coupon_id;

    /**
     *
     * @var integer
     */
    public $total_units;

    /**
     *
     * @var integer
     */
    public $started;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var integer
     */
    public $presented;

    /**
     *
     * @var integer
     */
    public $completed;

    /**
     *
     * @var string
     */
    public $trigger;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'type' => 'type',
            'name' => 'name',
            'welcome_img' => 'welcome_img',
            'welcome_msg' => 'welcome_msg',
            'thankyou_msg' => 'thankyou_msg',
            'start_date' => 'start_date',
            'end_date' => 'end_date',
            'maxed_out' => 'maxed_out',
            'never_expires' => 'never_expires',
            'reward' => 'reward',
            'coupon_id' => 'coupon_id',
            'total_units' => 'total_units',
            'started' => 'started',
            'active'=> 'active',
            'presented' => 'presented',
            'completed' => 'completed',
            'trigger' => 'trigger',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        $this->hasMany("id","Fliptile","engagement_id");
        $this->hasMany("id","Poll","engagement_id");
        $this->hasMany("id","Punchcard","engagement_id");
        $this->hasMany("id","EngagementSegment","engagement_id");
        $this->hasOne("id", "Coupon","engagement_id");
        $this->hasMany("id", "EngagementTranslation","engagement_id");
    }

    public function afterFetch()
    {
        $this->start_date = self::toICLMDate($this->start_date, true);
        $this->end_date = $this->end_date ? self::toICLMDate($this->end_date, true) : null;
    }

    public function beforeValidation()
    {
        $this->start_date = $this->toSQLDate($this->start_date, true);
        $this->end_date = $this->end_date ? $this->toSQLDate($this->end_date, true) : null;
    }

    public function getDuration()
    {
        return $this->toTime($this->end_date) - $this->toTime($this->start_date);
    }

    public function getStatus()
    {
        $active = true;

        if (time() < $this->toTime($this->start_date) )
        {
            $active = false;
        }
        if(time() > $this->toTime($this->end_date))
        {
            $active = false;
        }
        if($this->reward === 0)
        {
            $active = false;
        }
        if($this->never_expires == 1){
            $active = true;
        }

        return $active;
    }

    public function getRedemptionPercent()
    {
        if ($this->presented == 0)
        {
            return 0;
        }

        return $this->completed / $this->presented * 100;
    }

    public static function getActiveEngagements()
    {
        return self::count("start_date < '" . date("Y-m-d") . "' AND end_date > '" . date("Y-m-d") . "'");
    }

    public static function getAboutToExpire()
    {
        return self::count("end_date BETWEEN '" . date("Y-m-d H:i:s") . "' AND '" . date("Y-m-d H:i:s", time() + 259200) .
                           "' AND completed > 0.95 * total_units");
    }

    public static function getAvailableEngagements()
    {
        $data = [];

        for ($i = 0; $i < 7; $i++)
        {
            $date = $i > 0 ? date("y-m-d",strtotime("-" . $i . "day")) : date("y-m-d",time());

            $phql = "SELECT DAYNAME('" . $date . "') as day,COUNT(*) as count FROM Engagement
                     WHERE ('" . $date . "' >= maxed_out OR maxed_out is null)
                     AND '" . $date . "' BETWEEN start_date AND end_date";

            $result = self::getManager()->executeQuery($phql)[0];
            $data[Lang::translate($result->day)] = (int)$result->count;
        }

        return $data;
    }

    public function setInactiveStatus()
    {
        $this->active = 0;
        if($this->never_expires == 1){
            $this->active = 1;
        }
        $this->save();
    }

    public static function getType(){
        return self::getManager()->executeQuery("SELECT DISTINCT type FROM Engagement");
    }
}
