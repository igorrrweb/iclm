<?php

class SystemSettings extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $setting;

    /**
     *
     * @var string
     */
    public $value;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function initialize(){}

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'setting' => 'setting', 
            'value' => 'value', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        ];
    }

}
