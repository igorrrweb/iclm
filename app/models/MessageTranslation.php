<?php


class MessageTranslation extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $message_id;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $content;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id'=>'id',
            'message_id' => 'message_id',
            'lang' => 'lang',
            'title' => 'title',
            'content' => 'content',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }
}