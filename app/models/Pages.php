<?php

class Pages extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $body_text;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var string
     */
    public $content_type;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'lang' => 'lang', 
            'title' => 'title', 
            'body_text' => 'body_text', 
            'active' => 'active', 
            'content_type' => 'content_type', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        ];
    }

}
