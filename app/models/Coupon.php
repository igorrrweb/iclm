<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Coupon extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $pos_coupon_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $start_date;

    /**
     *
     * @var string
     */
    public $expiry_date;

    /**
     *
     * @var string
     */
    public $maxed_out;

    /**
     *
     * @var integer
     */
    public $max_coupons;

    /**
     *
     * @var integer
     */
    public $public;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var integer
     */
    public $deactivated_manually;

    /**
     *
     * @var integer
     */
    public $shared;

    /**
     *
     * @var integer
     */
    public $promoted;


    /**
     *
     * @var integer
     */
    public $enabled;

    /**
     *
     * @var integer
     */
    public $product_id;

    /**
     *
     * @var integer
     */
    public $category_id;

    /**
     *
     * @var integer
     */
    public $total_pending;

    /**
     *
     * @var integer
     */
    public $total_redeemed;

    /**
     *
     * @var string
     */
    public $segment;

    /**
     *
     * @var string
     */
    public $coupon_type;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $sub_title;

    /**
     *
     * @var string
     */
    public $details;

    /**
     *
     * @var string
     */
    public $disclaimer;

    /**
     *
     * @var binary
     */
    public $image;

    /**
     *
     * @var integer
     */
    public $engagement_type;

    /**
     *
     * @var integer
     */
    public $engagement_id;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'coupon_type' => 'coupon_type',
            'pos_coupon_id' => 'pos_coupon_id',
            'name' => 'name',
            'title' => 'title',
            'sub_title' => 'sub_title',
            'details' => 'details',
            'img_uri' => 'img_uri',
            'max_coupons' => 'max_coupons',
            'disclaimer' => 'disclaimer',
            'product_id' => 'product_id',
            'category_id' => 'category_id',
            'start_date' => 'start_date',
            'expiry_date' => 'expiry_date',
            'maxed_out' => 'maxed_out',
            'engagement_type' => 'engagement_type',
            'engagement_id' => 'engagement_id',
            'active' => 'active',
            'deactivated_manually' => 'deactivated_manually',
            'public' => 'public',
            'shared' => 'shared',
            'promoted' => 'promoted',
            'rank' => 'rank',
            'total_pending' => 'total_pending',
            'total_redeemed' => 'total_redeemed',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        $this->belongsTo("product_id", "Product", "id");
        $this->belongsTo("category_id", "Category", "id");
        $this->belongsTo("coupon_type","CouponType","id");
        $this->hasManyToMany("id","CouponSegment","coupon_id", "segment_id", "Segment","id");
        $this->hasMany("id","CouponSegment","coupon_id");
        $this->hasMany("id","CouponActivity","coupon_id");
        $this->hasMany("id","CouponTranslation","coupon_id");
    }

    public function delete()
    {
        parent::delete();
        FileManager::delete("coupon",$this->id);
        $this->getRelated("CouponTranslation")->delete();
        $this->getRelated("CouponSegment")->delete();
    }

    public function afterFetch()
    {
        $this->start_date = date("d/m/Y H:i:s",strtotime($this->start_date));
        $this->expiry_date = date("d/m/Y H:i:s",strtotime($this->expiry_date));
    }

    public function beforeValidation()
    {
        $this->product_id == "" ? $this->product_id = null : "";
        $this->category_id == "" ? $this->category_id = null : "";
        $this->start_date = $this->toSQLDate($this->start_date);
        $this->expiry_date = $this->toSQLDate($this->expiry_date);
    }

    public function afterValidationOnCreate()
    {
        $this->total_pending = 0;
    }

    public static function orderByMembers($sortDirection, $searchCriteria)
    {
        $condition = $searchCriteria != "" ? "WHERE c.name = '" . $searchCriteria . "'" : "";

        $sql = "SELECT * FROM coupon c JOIN coupon_segment cs ON c.id = cs.coupon_id JOIN segment_customer sc
                ON cs.segment_id = sc.segment_id " . $condition . " GROUP BY cs.coupon_id ORDER BY COUNT(DISTINCT sc.customer_id)" .
               $sortDirection;

        $coupon = new Coupon;

        return new Resultset(null, $coupon, $coupon->getReadConnection()->query($sql));
    }

    public function getAudience()
	{
        if ($this->public == 1)
        {
            return Customer::count();
        }

        $phql = "SELECT COUNT(DISTINCT sc.customer_id) as audience FROM Coupon c JOIN CouponSegment cs ON c.id = cs.coupon_id
                 JOIN SegmentCustomer sc ON sc.segment_id = cs.segment_id where c.id = " . (int)$this->id;


        return self::getManager()->executeQuery($phql)[0]->audience;
	}

    public function getStatus()
    {
        $active = true;

        if (time() < $this->toTime($this->start_date)|| time() > $this->toTime($this->expiry_date) || $this->active == "0")
        {
            $active = false;
        }

        return $active;
    }

    public function getAbandonedPercent()
    {
        if ($this->total_pending == 0)
        {
            return 0;
        }

        return ($this->total_pending - $this->total_redeemed) / $this->total_pending * 100;
    }

	public function getRedeemedPercent()
	{
        $this->max_coupons = $this->max_coupons == null ? 0 : (int)$this->max_coupons;
        if ($this->max_coupons == 0)
        {
            return 0;
        }
        $this->total_redeemed = $this->total_redeemed == null ? 0 : (int)$this->total_redeemed;
        $result = $this->total_redeemed / $this->max_coupons * 100;
        settype($result, 'float');
        return $result;
	}

    public function getInterestPercent()
    {
        if ($this->getTotalPresented() == 0)
        {
            return 0;
        }

        return $this->total_pending / $this->getTotalPresented() * 100;
    }

    public static function getActiveCoupons()
    {
        //return self::count(["conditions" => "'" . date("y-m-d H:i:s") . "' < expiry_date AND active = 1"]);
        return self::count(["conditions" => "active = 1 AND coupon_type IN ('1','2','3')"]);
    }

    public static function getTotalCoupons()
    {
        //return self::count(["conditions" => "'" . date("y-m-d H:i:s") . "' < expiry_date AND active = 1"]);
        return self::count(["conditions" => " coupon_type IN ('1','2','3') "]);
    }

    public static function getWeeklyActiveCoupons()
    {
        /*return self::count(["conditions" => "expiry_date BETWEEN '" . date("Y-m-d H:i:s") . "' AND '" .
                                             date("Y-m-d H:i:s", time() + 172800) . "'"]);*/
        return self::count(["conditions" => "expiry_date BETWEEN '" . date("Y-m-d H:i:s") . "' AND '" .
                                             date("Y-m-d H:i:s", time() + 172800) . "' AND coupon_type IN ('1','2','3')"]);
    }

    public static function getAboutToExpire()
    {
        return self::count("expiry_date BETWEEN '" . date("Y-m-d H:i:s") . "' AND '" . date("Y-m-d H:i:s", time() + 259200) .
                            "' OR total_redeemed > 0.95 * max_coupons");
    }

    public static function getCouponsByWeek($week)
    {
        if ($week > 1)
        {
            return self::count("created_at BETWEEN '" . date("Y-m-d H:m:s", time() - 86400 * ($week * 7))
                                . "' AND '" .  date("Y-m-d H:m:s", time() - 604800 * ($week - 1)) . "'");
        } else {
            return self::count("created_at > '" . date("Y-m-d H:m:s", time() - 604800) . "'");
        }

    }

    public function getTotalPresented()
    {
        return CouponActivity::count("presented IS NOT NULL AND coupon_id = " . (int)$this->id );
    }

    public static function getAvailableCoupons()
    {
        $data = [];

        for ($i = 0; $i < 7; $i++)
        {
            $date = $i > 0 ? date("y-m-d",strtotime("-" . $i . "day")) : date("y-m-d",time());

            $phql = "SELECT DAYNAME('" . $date . "') as day,COUNT(*) as count FROM Coupon
                     WHERE ('" . $date . "' >= maxed_out OR maxed_out is null)
                     AND '" . $date . "' BETWEEN start_date AND expiry_date";

            $result = self::getManager()->executeQuery($phql)[0];
            $data[Lang::translate($result->day)] = (int)$result->count;
        }

        return $data;
    }

    public  function getExpiredCoupons(){
        $start_date = new DateTime();
        $date_today = $start_date->format('Y-m-d 23:59:59');
        $start_date->modify("last Sunday");
        $date_from = $start_date->format('Y-m-d 00:00:00');
        return self::count("expiry_date BETWEEN '".$date_from."' AND '".$date_today."'");
    }

    public function getReachedTheLimit(){
        $start_date = new DateTime();
        $date_today = $start_date->format('Y-m-d 23:59:59');
        $start_date->modify("last Sunday");
        $date_from = $start_date->format('Y-m-d 00:00:00');
        return self::count("max_coupons = total_redeemed AND updated_at BETWEEN '".$date_from."' AND '".$date_today."'");
    }

    public function getDeactivatedManually(){
        $start_date = new DateTime();
        $date_today = $start_date->format('Y-m-d 23:59:59');
        $start_date->modify("last Sunday");
        $date_from = $start_date->format('Y-m-d 00:00:00');

        /*return self::count("active = 0 AND (updated_at BETWEEN '".$date_from."' AND '".$date_today."')
            AND (expiry_date >'". $date_today."' OR  max_coupons != total_redeemed)");*/
        return self::count("active = 0 AND deactivated_manually = 1 AND (updated_at BETWEEN '".$date_from."' AND '".$date_today."')");
    }

    public function getDeactivatedThisWeek(){
        $start_date = new DateTime();
        $date_today = $start_date->format('Y-m-d 23:59:59');
        $start_date->modify("last Sunday");
        $date_from = $start_date->format('Y-m-d 00:00:00');
        /*return self::count("active = 0 AND expiry_date >'". date("Y-m-d H:m:s", strtotime(date("Y-m-d H:m:s")."-1 week"))."'");*/
        return self::count("active = 0 AND deactivated_manually = 1 AND (updated_at BETWEEN '".$date_from."' AND '".$date_today."')");
    }
}
