<?php

class Beacon extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $uuid;

    /**
     *
     * @var integer
     */
    public $major;

    /**
     *
     * @var integer
     */
    public $minor;

    /**
     *
     * @var string
     */
    public $mac;

    /**
     *
     * @var string
     */
    public $color;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $icon;

    /**
     *
     * @var string
     */
    public $in_store_location;

    /**
     *
     * @var integer
     */
    public $battery_life_expectancy_in_days;

    /**
     *
     * @var integer
     */
    public $context_id;

    /**
     *
     * @var string
     */
    public $tags;

    /**
     *
     * @var integer
     */
    public $branch_id;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function initialize()
    {
        $this->belongsTo("branch_id","Branch","branch_no");
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'uuid' => 'uuid',
            'major' => 'major',
            'minor' => 'minor',
            'mac' => 'mac',
            'color' => 'color',
            'name' => 'name',
            'icon' => 'icon',
            'battery_life_expectancy_in_days' => 'battery_life_expectancy_in_days',
            'context_id' => 'context_id',
            'tags' => 'tags',
            'branch_id' => 'branch_id',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }
}
