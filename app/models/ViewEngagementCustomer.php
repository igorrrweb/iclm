<?php

class ViewEngagementCustomer extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $segment_id;

    /**
     *
     * @var integer
     */
    public $customer_id;

    /**
     *
     * @var integer
     */
    public $engagement_id;

    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var string
     */
    public $start_date;

    /**
     *
     * @var string
     */
    public $end_date;

    /**
     *
     * @var integer
     */
    public $never_expires;

    /**
     *
     * @var integer
     */
    public $total_units;

    /**
     *
     * @var integer
     */
    public $started;

    /**
     *
     * @var integer
     */
    public $presented;

    /**
     *
     * @var integer
     */
    public $completed;

    /**
     *
     * @var string
     */
    public $trigger;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'view_engagement_customer';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ViewEngagementCustomer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ViewEngagementCustomer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
