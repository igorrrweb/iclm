<?php

class EngagementTranslation extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $engagement_id;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $welcome_msg;

    /**
     *
     * @var string
     */
    public $thankyou_msg;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'engagement_id' => 'engagement_id', 
            'lang' => 'lang', 
            'welcome_msg' => 'welcome_msg', 
            'thankyou_msg' => 'thankyou_msg', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        ];
    }

}
