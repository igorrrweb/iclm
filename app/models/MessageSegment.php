<?php

class MessageSegment extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $message_id;

    /**
     *
     * @var integer
     */
    public $segment_id;

    public function initialize()
    {
        $this->belongsTo("message_id","Message","id");
        $this->belongsTo("segment_id","Segment","id");
    }

    public function columnMap()
    {
        return [
            'message_id' => 'message_id',
            'segment_id' => 'segment_id'
        ];
    }

}
