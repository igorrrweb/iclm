<?php

class StatsBranchVisits extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $customer_id;

    /**
     *
     * @var string
     */
    public $total_visits;

    /**
     *
     * @var integer
     */
    public $visit_week;

    /**
     *
     * @var integer
     */
    public $visit_month;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'stats_branch_visits';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return StatsBranchVisits[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return StatsBranchVisits
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
