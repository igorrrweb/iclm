<?php

class FeedbackSegment extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $feedback_id;

    /**
     *
     * @var integer
     */
    public $segment_id;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'feedback_id' => 'feedback_id', 
            'segment_id' => 'segment_id'
        ];
    }

}
