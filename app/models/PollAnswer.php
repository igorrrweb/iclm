<?php

class PollAnswer extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $poll_id;

    /**
     *
     * @var string
     */
    public $answer;

    /**
     *
     * @var string
     */
    public $image;

    /**
     *
     * @var integer
     */
    public $votes;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function getSource()
    {
       return 'engagement_poll_answer';
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'poll_id' => 'poll_id',
            'answer' => 'answer',
            'image' => 'image',
            'votes' => 'votes',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        $this->belongsTo("poll_id","Poll","id");
    }
}
