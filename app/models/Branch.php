<?php

class Branch extends BaseModel
{
    protected $loggable = true;
    public $longitude;
    public $latitude;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $branch_no;

    /**
     *
     * @var string
     */
    public $branch_name;

    /**
     *
     * @var string
     */
    public $street_address;

    /**
     *
     * @var string
     */
    public $zip_code;

    /**
     *
     * @var integer
     */
    public $city_id;

    /**
     *
     * @var integer
     */
    public $manager;

    /**
     *
     * @var string
     */
    public $manager_mobile;

    /**
     *
     * @var string
     */
    public $branch_phone;

    /**
     *
     * @var string
     */
    public $location;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;


    public function afterFetch()
    {
        $this->latitude = ltrim(strstr($this->location,","),",");
        $this->longitude = strstr($this->location,",",true);
    }

    public function beforeValidation()
    {
        //parent::beforeValidation();
        //$this->active = 1;
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'branch_no' => 'branch_no',
            'branch_name' => 'branch_name',
            'street_address' => 'street_address',
            'zip_code' => 'zip_code',
            'city_id' => 'city_id',
            'manager' => 'manager',
            'manager_mobile' => 'manager_mobile',
            'branch_phone' => 'branch_phone',
            'location' => 'location',
            'active' => 'active',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        $this->hasMany("id","BranchOpeningHour","branch_id");
        $this->belongsTo("city_id","City","id");
        $this->hasMany("id","BranchTranslation","branch_id");
    }
}
