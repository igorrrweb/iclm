<?php

class ViewSegmentCoupon extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $coupon_id;

    /**
     *
     * @var integer
     */
    public $segment_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var integer
     */
    public $public;

    /**
     *
     * @var integer
     */
    public $shared;

    /**
     *
     * @var integer
     */
    public $promoted;

    /**
     *
     * @var integer
     */
    public $product_id;

    /**
     *
     * @var integer
     */
    public $category_id;

    /**
     *
     * @var string
     */
    public $start_date;

    /**
     *
     * @var string
     */
    public $expiry_date;

    /**
     *
     * @var integer
     */
    public $total_pending;

    /**
     *
     * @var integer
     */
    public $total_redeemed;

    /**
     *
     * @var integer
     */
    public $coupon_type;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $sub_title;

    /**
     *
     * @var string
     */
    public $details;

    /**
     *
     * @var string
     */
    public $disclaimer;

    /**
     *
     * @var integer
     */
    public $max_coupons;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'view_segment_coupon';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ViewSegmentCoupon[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ViewSegmentCoupon
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
