<?php

class Message extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $content;

    /**
     *
     * @var string
     */
    public $image;

    /**
     *
     * @var string
     */
    public $trigger;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var string
     */
    public $start_date;

    /**
     *
     * @var string
     */
    public $end_date;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     *
     * @var string
     */
    public $created_at;

    public function initialize()
    {
        $this->hasManyToMany("id","MessageSegment","message_id", "segment_id", "Segment","id");
        $this->hasMany("id","MessageSegment","message_id");
        $this->hasMany("id", "MessageTranslation","message_id");
    }

    public function afterFetch()
    {
        $this->start_date = self::toICLMDate($this->start_date);
        $this->end_date = self::toICLMDate($this->end_date);
    }

    public function beforeValidation()
    {
        $this->updated_at = date('Y-m-d H:i:s');
        $this->start_date = self::toSQLDate($this->start_date);
        $this->end_date = self::toSQLDate($this->end_date);
    }

    public function columnMap()
    {
        return [
            "id" => "id",
            "title" => "title",
            "content" => "content",
            "image" => "image",
            "trigger" => "trigger",
            "active" => "active",
            "start_date" => "start_date",
            "end_date" => "end_date",
            "updated_at" => "updated_at",
            "created_at" => "created_at"
        ];
    }

    public function getDuration()
    {
        return self::toTime($this->end_date) - self::toTime($this->start_date);
    }

    public function getStatus()
    {
        $active = true;

        if (time() < strtotime($this->start_date) || time() > strtotime($this->end_date) || $this->active == "0")
        {
            $active = false;
        }

        return $active;
    }

    public function getAudience()
    {
        $phql =   "SELECT COUNT(DISTINCT sc.customer_id) as audience FROM Message m JOIN MessageSegment ms ON m.id =
                   ms.message_id JOIN SegmentCustomer sc ON sc.segment_id = ms.segment_id where m.id = " . (int)$this->id;

        return self::getManager()->executeQuery($phql)[0]->audience;
    }
}
