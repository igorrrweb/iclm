<?php

class ViewCouponCustomer extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $coupon_id;

    /**
     *
     * @var integer
     */
    public $segment_id;

    /**
     *
     * @var integer
     */
    public $customer_id;

    /**
     *
     * @var string
     */
    public $pos_coupon_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $sub_title;

    /**
     *
     * @var string
     */
    public $details;

    /**
     *
     * @var string
     */
    public $disclaimer;

    /**
     *
     * @var string
     */
    public $start_date;

    /**
     *
     * @var string
     */
    public $expiry_date;

    /**
     *
     * @var integer
     */
    public $coupon_type;

    /**
     *
     * @var integer
     */
    public $rank;

    /**
     *
     * @var integer
     */
    public $total_pending;

    /**
     *
     * @var integer
     */
    public $total_redeemed;

    /**
     *
     * @var integer
     */
    public $max_coupons;

    /**
     *
     * @var integer
     */
    public $shared_coupon;

    /**
     *
     * @var integer
     */
    public $public;

    /**
     *
     * @var integer
     */
    public $promoted;

    /**
     *
     * @var integer
     */
    public $product_id;

    /**
     *
     * @var integer
     */
    public $category_id;

    /**
     *
     * @var string
     */
    public $offered;

    /**
     *
     * @var string
     */
    public $presented;

    /**
     *
     * @var string
     */
    public $redeemed;

    /**
     *
     * @var integer
     */
    public $shared;

    /**
     *
     * @var string
     */
    public $declined;

    /**
     *
     * @var string
     */
    public $pending;

    /**
     *
     * @var string
     */
    public $viewed;

    /**
     *
     * @var integer
     */
    public $shared_by;

    /**
     *
     * @var integer
     */
    public $parent_category;

    /**
     *
     * @var string
     */
    public $img_uri;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'view_coupon_customer';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ViewCouponCustomer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ViewCouponCustomer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
