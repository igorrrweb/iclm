<?php

class EngagementPoll extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $engagement_id;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $question;

    /**
     *
     * @var integer
     */
    public $randomize_answer;

    /**
     *
     * @var integer
     */
    public $multiple;

    /**
     *
     * @var integer
     */
    public $other;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'EngagementPollAnswer', 'poll_id', array('alias' => 'EngagementPollAnswer'));
        $this->belongsTo('engagement_id', 'Engagement', 'id', array('alias' => 'Engagement'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'engagement_poll';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return EngagementPoll[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return EngagementPoll
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
