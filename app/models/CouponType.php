<?php

class CouponType extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $coupon_type;


    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'coupon_type' => 'coupon_type',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }
}