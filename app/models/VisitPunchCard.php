<?php

class VisitPunchCard extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $visit_increase;

    /**
     *
     * @var integer
     */
    public $time_period;

    /**
     *
     * @var string
     */
    public $punch_rule;

    /**
     *
     * @var integer
     */
    public $visit_duration;

    /**
     *
     * @var integer
     */
    public $visit_interval;

    /**
     *
     * @var string
     */
    public $reward;

    /**
     *
     * @var string
     */
    public $reward_type;

    /**
     *
     * @var string
     */
    public $reward_value;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'visit_punch_card';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VisitPunchCard[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VisitPunchCard
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
