<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Segment extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var integer
     */
    public $branch_group;

    /**
     *
     * @var string
     */
    public $active;

    /**
     *
     * @var string
     */
    public $criteria;

    /**
     *
     * @var integer
     */
    public $retention;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'active' => 'active',
            'retention' => 'retention',
            'criteria' => 'criteria',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        $this->hasMany("id","CouponSegment","segment_id");
        $this->hasManyToMany("id","SegmentCustomer","segment_id","customer_id","Customer","id");
        $this->hasManyToMany("id","EngagementSegment","segment_id","engagement_id","Engagement","id");
        $this->hasManyToMany("id","CouponSegment","segment_id","coupon_id","Coupon","id");
        $this->hasMany("id", "SegmentCustomer", "segment_id");
        $this->hasMany("id", "EngagementSegment", "segment_id");
        $this->hasMany("id", "CouponSegment", "segment_id");
        $this->belongsTo("branch_group", "Branch", "id");
        $this->setCriteria();
    }

    public function delete()
    {
        parent::delete();
        $this->getRelated("SegmentCustomer")->delete();
        $this->getRelated("EngagementSegment")->delete();
        $this->getRelated("CouponSegment")->delete();
    }

    public function afterFetch()
    {
        $this->created_at = self::toICLMDate($this->created_at);
    }

    public function getMembers()
    {
        return count($this->getRelated("SegmentCustomer"));
    }

    public function getAmountSpentByPeriod($period)
    {
        switch ($period)
        {
            default:
                $range = "'" . $period . " 00:00:00' AND '" . $period . " 23:59:59'";
                break;
        }

        $phql = "SELECT SUM(amount) as amount FROM Invoice i JOIN SegmentCustomer sc ON sc.customer_id = i.customer_id
                 WHERE sc.segment_id = " . $this->id . " AND i.invoice_timestamp BETWEEN " . $range;

        return self::getManager()->executeQuery($phql)[0]->amount == null ? 0 :
        self::getManager()->executeQuery($phql)[0]->amount;
    }

    public function getCouponRedemption()
    {
        $coupons = $this->getRelated("Coupon");
        $percentage = 0;

        foreach ($coupons as $coupon)
        {
            $percentage += $coupon->total_redeemed / $coupon->max_coupons;
        }

        return round($percentage,2);
    }

    public function getAvailableCoupons()
    {
        $sum = 0;
        foreach ($this->getRelated("Coupon", ["active = 1 AND expiry_date > " . date("Y-m-d")]) as $coupon)
        {
            $sum += $coupon->max_coupons - $coupon->total_redeemed;
        }

        return $sum;
    }

    public function getAvailableEngagements()
    {
        $sum = 0;
        foreach ($this->getRelated("Engagement", ["never_expires = 1 AND end_date > " . date("Y-m-d")]) as $engagement)
        {
            $sum += $engagement->total_units - $engagement->completed;
        }

        return $sum;
    }

    public function getRedemptionRateByBranch($branchID, $period)
    {
        $phql = "SELECT SUM(c.total_redeemed) / SUM(c.max_coupons) AS redemption FROM Coupon c JOIN CouponSegment cs
                 ON c.id = cs.coupon_id JOIN CouponActivity ca on c.id = ca.coupon_id JOIN SegmentCustomer sc ON sc.segment_id =
                 cs.segment_id JOIN BranchVisits bv ON bv.customer_id  = sc.customer_id WHERE cs.segment_id = " . $this->id .
                 " AND ca.presented BETWEEN '" . date("Y-m-d", time() - 86400 * $period) ."' AND '" . date("Y-m-d") . "' AND
                 ca.presented BETWEEN bv.enter and bv.exit AND bv.branch_id = " . (int)$branchID;

        return self::getManager()->executeQuery($phql)[0]->redemption == null ? 0 :
               self::getManager()->executeQuery($phql)[0]->redemption * 100;
    }


    public function getLabel($dateFrom, $dateTo, $id, $flag ){
        $chartData = array();
        $chartData['labels'] = array();
        $chartData['data'] = array();
        $dtz = new \DateTimeZone('UTC');
        $datatime_now = new \DateTime($dateFrom,$dtz);
        $datatime_exp = new \DateTime($dateTo,$dtz);
        $diffInterval = $datatime_now->diff($datatime_exp);
        $days = $diffInterval->days;
        $month = $diffInterval->m;
        $years = $diffInterval->y;
        $invert = $diffInterval->invert;
        if($diffInterval->invert === 0){
            $from = $dateFrom;
            $to = $dateTo;
        } else {
            $from = $dateTo;
            $to = $dateFrom;
        }
        $start_date = new \DateTime($from,$dtz);
        $finish_date = new \DateTime($to,$dtz);
        if($days==0){
            $interval = 'HOUR';
            $date_format = '%H';
            for($i=0; $i<=23; $i++){
                $chartData['labels'][]=$start_date->format('H');
                $h_date = $start_date->format('Y-m-d H:i:s');
                $query_res = $this->getStatistic($flag, $id, $h_date, null, $date_format, $interval, 1);
                if(!$query_res){
                    $chartData['data'][] = 0;
                } else {
                    foreach($query_res as $row){
                       $chartData['data'][] = (int)$row['count'];
                    }
                }
                $start_date->modify('+1 hours');
            }
        }
        if(($days > 0)&&($days <=7 )){
            $interval = 'DAY';
            $date_format = '%d';
            $st = $start_date->format('z');
            for($i=$st; $i<=$finish_date->format('z'); $i++){
                $chartData['labels'][]=$start_date->format('d/m');
                $d_date = $start_date->format('Y-m-d 00:00:00');
                $query_res = $this->getStatistic($flag, $id, $d_date, null, $date_format, $interval, 1);
                if(!$query_res){
                    $chartData['data'][] = 0;
                } else {
                    foreach($query_res as $row){
                        $chartData['data'][] = (int)$row['count'];
                    }
                }
                $start_date->modify('+1 day');
            }
        }
        if(($days>7) && ($days<=365)){
            $interval = 'DAY';
            $date_format = '%Y';
            $num_start_week = $start_date->format('W');
            $num_finish_week = $finish_date->format('W');
            $day_of_week_from = $start_date->format('N');
            $day_of_week_to = $finish_date->format('N');
            $between_arr = array();
            if(($finish_date->format('Y') - $start_date->format('o'))>0 ) {
                $Y = $start_date->format('Y');
                $dateCalculation = new \DateTime($Y."-12-30", $dtz);
                $max_week = $dateCalculation->format('W') == '01' ? 52 : 53;
                $max_i = $max_week - $num_start_week + $num_finish_week;
            } else {
                $max_i = $num_finish_week-$num_start_week;
            }
            for($i = 1; $i<=$max_i; $i++){
                if($i==1){
                    $first_from = $start_date->format('Y-m-d 00:00:00');
                    if($day_of_week_from > 3){
                        $start_date->modify("next Monday");
                        $start_date->modify("next Sunday");
                        $first_next_sunday = $start_date->format('Y-m-d 00:00:00');
                    } else {
                        $first_next_sunday = $start_date->modify("next Sunday")->format('Y-m-d 00:00:00');
                    }
                    $between_arr[] = array('from'=>$first_from, 'to'=>$first_next_sunday);
                }
                if($i>1 && $i<$max_i){
                    $between_arr[] = array(
                        'from'=>$start_date->modify("next Monday")->format('Y-m-d 00:00:00'),
                        'to'=>$start_date->modify("next Sunday")->format('Y-m-d 00:00:00')
                    );
                }
                if($i==$max_i){
                    $between_arr[] = array(
                        'from'=>$start_date->modify("next Monday")->format('Y-m-d 00:00:00'),
                        'to'=>$finish_date->format('Y-m-d 00:00:00')
                    );
                }
            }
            foreach ($between_arr as $key => $between){
                $between_label_from = new \DateTime($between['from'],$dtz);
                $between_label_to = new \DateTime($between['to'],$dtz);
                $chartData['labels'][]=$between_label_from->format('d/m').'-'.$between_label_to->format('d/m');
                $query_res = $this->getStatistic($flag, $id, $between['from'], $between['to'], $date_format, $interval, 1);
                if(!$query_res){
                    $chartData['data'][] = 0;
                } else {
                    foreach($query_res as $row){
                        $chartData['data'][] = (int)$row['count'];
                    }
                }
            }
        }
        if($days>365){
            $date_format = '%Y';
            $interval = 'YEAR';
            $chartData['labels'] = array();
            $chartData['data'] = array();
            $num_start_year = $start_date->format('Y');
            $num_finish_year = $finish_date->format('Y');
            for($i=$num_start_year; $i<=$num_finish_year; $i++){
                $y_date = $start_date->format('Y-01-01 00:00:00');
                $chartData['labels'][]=$start_date->format('Y');
                $query_res = $this->getStatistic($flag, $id, $y_date, null, $date_format, $interval, 1);
                if(!$query_res){
                    $chartData['data'][] = 0;
                } else {
                    foreach($query_res as $row){
                        $chartData['data'][] = (int)$row['count'];
                    }
                }
                $start_date->modify('+1 year');
            }
        }
        return $chartData;
    }

    public function getStatistic($flag, $id, $from, $to=null, $date_format, $interval, $interval_value){
        $dtz = new \DateTimeZone('UTC');
        $date_add = new \DateTime($from,$dtz);
        $mod_str = "+".$interval_value." ".$interval;
        $date_add->modify($mod_str);
        $end_date = $date_add->format('Y-m-d 00:00:00');
        $to = $to == null ? $end_date : $to;
        switch ($flag){
            case 'branches':
                $sql = "SELECT COUNT(*) AS count
                        FROM BranchVisits bv JOIN SegmentCustomer sc ON bv.customer_id = sc.customer_id
                        WHERE exit BETWEEN '$from' AND '$to' AND sc.segment_id = " . (int)$id;
                break;
            case 'coupons':
                $sql = "SELECT  COUNT(*) as count, date_format(redeemed, '$date_format') as date
                  FROM CouponActivity ca
                  JOIN CouponSegment cs ON cs.coupon_id = ca.coupon_id
                  WHERE cs.segment_id = '$id' AND (redeemed BETWEEN '$from' AND '$to' )
                  GROUP BY date_format(redeemed, '$date_format')";
                break;
            case 'engagements':
                $sql = "SELECT count(*) as count FROM EngagementSegment es
                  join Engagement e on e.id=es.engagement_id
                  where es.segment_id='$id'
                  and ( (e.start_date BETWEEN '$from' AND '$to') or (e.end_date BETWEEN '$from' AND '$to') )";
                break;
        }

        return self::getManager()->createQuery($sql)->execute()->toArray();
    }

    public function getSegmentCouponsRedemption($startDate, $endDate, $flag)
    {
        $startDate = $this->toSQLDate($startDate);
        $endDate = $this->toSQLDate($endDate);
        $data = $this->getLabel($startDate, $endDate, $this->id, $flag );
        return $data;
    }

    public function getEngagementsCompletedByBranch($branchID, $period)
    {

    }

    private function setCriteria()
    {
        $this->criteria = [
            "profileRules" => [
                "gender" => "",
                "marital_status" => "",
                "birth_date" => "",
                "wedding_anniversary_date" => "",
                "join_date" => "",
                "age" => [
                    "min" => "",
                    "max" => ""
                ],
                "city_id" => []
            ],
            "branchVisitsRules" => [
                "enter" => [
                    "min" => "",
                    "max" => ""
                ],
                "freq" => [
                    "min" => "",
                    "max" => "",
                    "period" => ""
                ],
                "abandon" => "",
            ],
            "transactionsRules" => [
                "last_month" => [
                    "min" => "",
                    "max" => ""
                ],
                "last_year" => [
                    "min" => "",
                    "max" => ""
                ],
                "products_purchased" => [
                    "operator" => "",
                    "ids" => array()
                ],
                "products_not_purchased" => [
                    "operator" => "",
                    "ids" => []
                ],
                "category_purchased" => [
                    "operator" => "",
                    "ids" => []
                ],
                "category_not_purchased" => [
                    "operator" => "",
                    "ids" => []
                ]
            ]
        ];
    }

    public function modifyCriteria($rules)
    {
        foreach ($rules as $key=>$rule)
        {
            if (is_array($rule))
            {
                foreach ($rule as $key2=>$val)
                {
                    if (is_array($val))
                    {
                        foreach ($val as $key3=>$val2)
                        {
                            if (!is_array($val2))
                            {
                                $rules[$key][$key2][$key3] = $val2 == "" ? false : $val2;
                            }
                        }
                    } else {
                        $rules[$key][$key2] = $val == "" ? false : $val;
                    }
                }
            } else {
                $rules[$key] = $rule == "" ? false : $val;
            }
        }
        $this->criteria = json_encode($rules);
    }
}

