<?php

class SegmentStats extends BaseModel
{

    public static function getValueByPeriod($id)
    {
        $phql = "SELECT SUM(inv.amount) AS value FROM SegmentCustomer sec JOIN Invoice inv ON
                 sec.customer_id = inv.customer_id WHERE sec.segment_id = " . (int)$id . " GROUP BY
                 WEEKDAY(inv.invoice_timestamp) ORDER BY DAYOFWEEK(inv.invoice_timestamp)";

        return self::getStats($phql);
    }

    public static function getVisitsPerBranch($id)
    {
        $phql = "SELECT b.branch_name, COUNT(*) AS visits FROM SegmentCustomer sec JOIN
                 BranchVisits brv ON sec.customer_id = brv.customer_id JOIN Branch b ON b.id =
                 brv.branch_id WHERE sec.segment_id = " . (int)$id . " GROUP BY brv.branch_id";

        return self::getStats($phql);
    }

    public static function getBranchTotalValue($id)
    {
        $phql = "SELECT b.branch_name,SUM(amount) as value FROM Invoice inv JOIN Branch b on b.id = inv.branch_id
                 JOIN SegmentCustomer sec ON sec.customer_id = inv.customer_id WHERE sec.segment_id = " . (int)$id . "
                 GROUP BY branch_id";

        return self::getStats($phql);
    }

    public static function getEngagementsCompleted($id)
    {
        $phql = "SELECT eng.type, COUNT(*) as completed FROM Engagement eng JOIN EngagementSegment ens on eng.id =
                 ens.engagement_id WHERE ens.segment_id = " . (int)$id . " AND eng.completed = 1 GROUP BY eng.type";

        return self::getStats($phql);
    }

    public static function getEngagementsCompletedByType($id,$type)
    {
        $phql = "SELECT COUNT(*) as completed FROM Engagement eng JOIN EngagementSegment ens on eng.id = ens.engagement_id
                 WHERE ens.segment_id = " . (int)$id . " AND eng.completed = 1 AND eng.type = '" . $type . "' GROUP BY eng.type";

        $data = self::getStats($phql, false);
        return $data[0]->completed;
    }

    protected static function getStats($phql, $multiple = true)
    {
        $data = [];

        if (!$multiple)
        {
            $data = self::getManager()->executeQuery($phql);
        } else {
            foreach (self::getManager()->executeQuery($phql) as $row)
            {
                $data[] = $row;
            }
        }
        return $data;
    }

}