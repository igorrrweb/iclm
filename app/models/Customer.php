<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Customer extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $parent_id;


    /**
     *
     * @var string
     */
    public $firstname;

    /**
     *
     * @var string
     */
    public $lastname;

    /**
     *
     * @var integer
     */
    public $lc_number;

    /**
     *
     * @var string
     */
    public $last_digits_cc;

    /**
     *
     * @var integer
     */
    public $cashback;

    /**
     *
     * @var string
     */
    public $gender;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $phone;

    /**
     *
     * @var string
     */
    public $mobile;

    /**
     *
     * @var string
     */
    public $marital_status;

    /**
     *
     * @var string
     */
    public $birth_date;

    /**
     *
     * @var string
     */
    public $wedding_anniversary_date;

    /**
     *
     * @var string
     */
    public $join_date;

    /**
     *
     * @var string
     */
    public $address;

    /**
     *
     * @var integer
     */
    public $city_id;

    /**
     *
     * @var integer
     */
    public $zip_code;

    /**
     *
     * @var string
     */
    public $membership_expiry;

    /**
     *
     * @var string
     */
    public $device_token;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'parent_id' => 'parent_id',
            'firstname' => 'firstname',
            'lastname' => 'lastname',
            'lc_number' => 'lc_number',
            'last_digits_cc' => 'last_digits_cc',
            'cashback' => 'cashback',
            'gender' => 'gender',
            'email' => 'email',
            'phone' => 'phone',
            'mobile' => 'mobile',
            'marital_status' => 'marital_status',
            'birth_date' => 'birth_date',
            'wedding_anniversary_date' => 'wedding_anniversary_date',
            'join_date' => 'join_date',
            'address' => 'address',
            'city_id' => 'city_id',
            'zip_code' => 'zip_code',
            'membership_expiry' => 'membership_expiry',
            'device_token' => 'device_token',
            'token' => 'token',
            'mobile_device' => 'mobile_device',
            'profile_img' => 'profile_img',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        $this->skipAttributes(["cashback"]);
        $this->hasManyToMany("id", "SegmentCustomer", "customer_id","segment_id","Segment","id");
        $this->hasMany("id","Invoice","customer_id");
        parent::initialize();
    }


    public function beforeValidation()
    {
        $this->join_date = $this->join_date != '' ? $this->toSQLDate($this->join_date) : date("Y-m-d H:i:s");
        $this->birth_date = $this->toSQLDate($this->birth_date);
        $this->wedding_anniversary_date = $this->toSQLDate($this->wedding_anniversary_date);
    }

    public function afterFetch()
    {
        $this->join_date = strtotime($this->join_date) > 0 ? self::toICLMDate($this->join_date) : "";
        $this->birth_date = strtotime($this->birth_date) > 0 ? self::toICLMDate($this->birth_date): "";
        $this->wedding_anniversary_date = strtotime($this->wedding_anniversary_date) > 0 ?
        self::toICLMDate($this->wedding_anniversary_date) : "";
        $this->created_at = strtotime($this->created_at) > 0 ? self::toICLMDate($this->created_at) : "";
        strlen($this->id) < 9 ? $this->id = str_pad($this->id, 9, "0", STR_PAD_LEFT) : "";
    }

    public function getStatus()
    {
        $status = "Active";

        $sql = "SELECT amount FROM invoice WHERE customer_id = " . $this->id . " AND invoice_timestamp > '" . date("Y/m/d h:m:s",time() - 7776000) . "'
                GROUP BY MONTH(invoice_timestamp) ORDER BY invoice_timestamp DESC";
        $invoice = new Invoice();


        // last three months invoices (0 to 4 possible value returns)
        $invoices = new Resultset(null, $invoice, $invoice->getReadConnection()->query($sql));

        $lastInvoice = Invoice::findFirst(["columns" => "invoice_timestamp","conditions" =>
                                            "customer_id = " . $this->id, "order" => "id DESC"]);
        if (!$lastInvoice) {
            $status = "Inactive";
        } else {
            if (time() - strtotime($lastInvoice->invoice_timestamp) >= 15552000) {
                $status = "Inactive";
            } elseif (time() - strtotime($lastInvoice->invoice_timestamp) >= 5184000 ||
                      isset($invoices[2]) && $invoices[0]->amount < $invoices[1]->amount &&
                      $invoices[1]->amount < $invoices[2]->amount) {
                $status = "ChurnAlert";
            }
        }

        return $status;
    }

    public function getAccountType()
    {
        $invitedCustomers = $this->findFirst(["conditions" =>"parent_id = " . $this->id, "columns" => "id"]);
        $type = "Personal";

        if ($invitedCustomers)
        {
            $type = "BANLeader";
        } elseif ($this->parent_id)
        {
            $type = "BANMember";
        }

        return $type;
    }

    public function getLastVisit()
    {
        $lastVisit = BranchVisits::findFirst(["conditions" => "customer_id=".$this->id ,"columns" => "enter",
                                               "order" => "enter DESC"]);

        if ($lastVisit)
        {
            return self::toICLMDate($lastVisit->enter);
        } else {
            return "";
        }

    }

    public static function getNewCustomers()
    {
        return self::count("join_date BETWEEN '" . date("Y-m-d H:i:s", time() - 172800) . "' AND '" . date("Y-m-d H:i:s") . "'");

    }

    public static function getNewMembersWeekly()
    {
        $phql = "SELECT DAYNAME(join_date) as day ,COUNT(*) AS count
                 FROM customer
                 WHERE date_format(ifnull(join_date,created_at), '%Y-%m-%d') BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY) AND date_format(now(),'%Y-%m-%d')
                 GROUP BY DAYOFWEEK(ifnull(join_date,created_at)) ORDER BY DAY(ifnull(join_date,created_at)) LIMIT 6";

        $model = new Customer();
        $res =  new Phalcon\Mvc\Model\Resultset\Simple(null, $model, $model->getReadConnection()->query($phql));

        return self::setWeekDaysResponse($res->toArray());
    }

    public static function getTotalChurn()
    {
        $data = [];
        $date = date("y-m-d",strtotime("-3 Months"));

        $phql = "SELECT customer_id, COUNT(month(enter)) as count,
                AVG(TIMEDIFF(bv.exit,bv.enter)) as duration
                FROM BranchVisits bv
                WHERE enter >= '" . $date . "'
                GROUP BY customer_id, month(enter) ORDER BY customer_id,enter ASC";

        $rows = self::getManager()->executeQuery($phql)->toArray();

        foreach ($rows as $key=>$val)
        {
            if ($key > 0)
            {
                if ($val["customer_id"] ==  $rows[$key - 1]["customer_id"])
                {
                    if ($val["count"] < $rows[$key - 1]["count"] || $val["duration"] < $rows[$key - 1]["duration"])
                    {
                        $churn = true;
                    }
                } else {
                    if (isset($churn) && $churn)
                    {
                        $data[] = $rows[$key - 1]["customer_id"];
                    }

                    $churn = false;
                }

                if ($key == count($rows) - 1)
                {
                    if (isset($churn) && $churn)
                    {
                        $data[] = $rows[$key - 1]["customer_id"];
                    }
                }
            }
        }


        $phql = "SELECT customer_id, COUNT(*) as abandon FROM BranchVisits
                 WHERE abandon = 1 AND enter >= '" . $date . "'
                 GROUP BY customer_id, month(enter)";

        foreach (self::getManager()->executeQuery($phql) as $val)
        {
            $data[] = $val["customer_id"];
        }

        $phql = "SELECT customer_id, AVG(amount) as amount FROM Invoice
                 WHERE invoice_timestamp >= '" . $date . "'
                 GROUP BY customer_id, month(invoice_timestamp)
                 ORDER BY customer_id,invoice_timestamp ASC";

        $rows = self::getManager()->executeQuery($phql)->toArray();

        foreach ($rows as $key=>$val)
        {
            if ($key > 0)
            {
                if ($val["customer_id"] ==  $rows[$key - 1]["customer_id"])
                {
                    if ($val["amount"] < $rows[$key - 1]["amount"])
                    {
                        $churn = true;
                    }
                } else {
                    if (isset($churn) && $churn)
                    {
                        $data[] = $rows[$key - 1]["customer_id"];
                    }

                    $churn = false;
                }

                if ($key == count($rows) - 1)
                {
                    if (isset($churn) && $churn)
                    {
                        $data[] = $rows[$key - 1]["customer_id"];
                    }
                }
            }
        }

        return count(array_unique($data));
    }
}

