<?php

class City extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $city;

    /**
     *
     * @var string
     */
    public $destrict;

    /**
     *
     * @var integer
     */
    public $population;

    /**
     *
     * @var integer
     */
    public $population_density;

    /**
     *
     * @var double
     */
    public $population_growth;

    /**
     *
     * @var integer
     */
    public $eco_rank;

    public static function find($parameters = NULL)
    {
        if ($parameters != NULL)
        {
            return parent::find(["cache" => ["key" => "cities"]]);
        }

        return parent::find([$parameters, "cache" => ["key" => "cities"]]);
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'city' => 'city',
            'destrict' => 'destrict',
            'population' => 'population',
            'population_density' => 'population_density',
            'population_growth' => 'population_growth',
            'eco_rank' => 'eco_rank',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

}
