<?php

class GdRegions extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $region_id;

    /**
     *
     * @var string
     */
    public $country_slug;

    /**
     *
     * @var string
     */
    public $region;

    /**
     *
     * @var string
     */
    public $region_short;

    /**
     *
     * @var string
     */
    public $region_slug;

    /**
     *
     * @var integer
     */
    public $weight;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'gd_regions';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return GdRegions[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return GdRegions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
