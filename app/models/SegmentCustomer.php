<?php

class SegmentCustomer extends BaseModel
{

    public $segment_id;

    /**
     *
     * @var integer
     */
    public $customer_id;

    public function columnMap()
    {
        return [
            'segment_id' => 'segment_id',
            'customer_id' => 'customer_id'
        ];
    }

    public function initialize()
    {
        $this->belongsTo("segment_id", "Segment", "id");
        $this->belongsTo("customer_id", "Customer", "id");
    }

}
