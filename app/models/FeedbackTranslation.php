<?php

class FeedbackTranslation extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $question_id;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'question_id' => 'question_id', 
            'lang' => 'lang', 
            'title' => 'title', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        ];
    }

}
