<?php

class Poll extends Engagement
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $engagement_id;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $question;

    /**
     *
     * @var integer
     */
    public $randomize_answer;

    /**
     *
     * @var integer
     */
    public $multiple;

    /**
     *
     * @var integer
     */
    public $other;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function getSource()
    {
        return "engagement_poll";
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'engagement_id' => 'engagement_id',
            'lang' => 'lang',
            'question' => 'question',
            'randomize_answer' => 'randomize_answer',
            'multiple' => 'multiple',
            'other' => 'other',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        parent::initialize();
        $this->belongsTo("engagement_id","Engagement", "id");
        $this->hasMany("id","PollAnswer","poll_id");
    }

}
