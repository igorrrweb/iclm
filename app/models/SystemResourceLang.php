<?php

class SystemResourceLang extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $resource_id;

    /**
     *
     * @var string
     */
    public $name_he;

    /**
     *
     * @var string
     */
    public $name_en;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'system_resource_lang';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemResourceLang[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemResourceLang
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
