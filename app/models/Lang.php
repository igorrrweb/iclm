<?php

class Lang extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $keyword;

    /**
     *
     * @var string
     */
    public $he;

    /**
     *
     * @var string
     */
    public $en;

    /**
     *
     * @var string
     */
    public $ar;

    /**
     *
     * @var string
     */
    public $ru;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;



    public function initialize()
    {
          parent::initialize();
          $this->skipAttributesOnCreate(["ru","ar"]);

    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'keyword' => 'keyword',
            'he' => 'he',
            'en' => 'en',
            'ar' => 'ar',
            'ru' => 'ru',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public static function translate($keyword)
    {
        $currentLang = Phalcon\DI::getDefault()['session']->lang ? Phalcon\DI::getDefault()['session']->lang :
                       Phalcon\DI::getDefault()['settings']['defaultLang'];

        $lang = self::findFirst(["columns" => $currentLang, "conditions" => "keyword='" . $keyword . "'",
                                 "cache" => ["key" => $currentLang . "_" . $keyword]]);

        return $lang ? $lang->$currentLang : "";
    }
}

