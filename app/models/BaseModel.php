<?php

class BaseModel extends Phalcon\Mvc\Model
{
    protected $loggable = false;

    protected function initialize()
    {
        //parent::initialize();
        //$this->skipAttributesOnUpdate(["created_at"]);
    }

    public function setLogging($setting)
    {
        $this->loggable = (bool)$setting;
    }

    protected function beforeValidationOnCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
        $this->updated_at = date("Y-m-d H:i:s");
    }

    public function beforeValidationOnUpdate()
    {
        //Set the modification date
        $this->created_at = self::toSQLDate($this->created_at);
        $this->updated_at = date('Y-m-d H:i:s');
    }

    protected function afterCreate()
    {
        if ($this->loggable === true)
        {
            $log = new UserLogs();
            $log->user_id = unserialize(Phalcon\DI::getDefault()['session']->user)->id;
            $log->username = unserialize(Phalcon\DI::getDefault()['session']->user)->username;
            $log->action = "Created " . get_class($this) . " #" .  $this->id ;
            $log->save();
        }
    }

    protected function afterUpdate()
    {
        if ($this->loggable === true)
        {
            $log = new UserLogs();
            $log->user_id = unserialize(Phalcon\DI::getDefault()['session']->user)->id;
            $log->username = unserialize(Phalcon\DI::getDefault()['session']->user)->username;
            $log->action = "Updated " .  get_class($this) . " #" .  $this->id ;
            $log->save();
        }
    }

    public static function getDaysOfWeek()
    {
        return ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    }

    protected static function setWeekDaysResponse($data){
        $response = array();
        $weekDays = self::getDaysOfWeek();
        $modelRows = array();
        if($data){
            foreach($data as $arr){
                $modelRows[$arr['day']]=$arr['count'];
            }
        }
        foreach($weekDays as $weekDay){
            $response[Lang::translate($weekDay)] = array_key_exists($weekDay,$modelRows) ? (int)$modelRows[$weekDay] : 0;
        }
        return $response;
    }

    protected static function mergeWeekDays($data)
    {
        $daysBefore = array_reverse(array_slice(self::getDaysOfWeek(),date("w") + 1, 7 - date("w")));
        $daysAfter = array_reverse(array_slice(self::getDaysOfWeek(),0, date("w") + 1));
        $orderedWeek = array_merge($daysAfter,$daysBefore);

        $mergedData = [];

        foreach ($orderedWeek as $day)
        {
            $dayData = false;

            if (count($data) === 0)
            {
                $mergedData[Lang::translate($day)] = 0;
            } else {
                foreach ($data as $row)
                {
                    if ($row->day == $day)
                    {
                        $dayData = true;
                        $mergedData[Lang::translate($day)] = (int)$row['count'];
                    }
                }

                if (!$dayData)
                {
                    $mergedData[Lang::translate($day)] = 0;
                }
            }
        }

        return $mergedData;
    }

    protected static function getManager()
    {
        return Phalcon\DI::getDefault()->get("modelsManager");
    }

    public static function toICLMDate($date, $addTime = false)
    {
        $time = $addTime ? " H:i:s" : "";

        return date(Phalcon\DI::getDefault()->get("settings")['dateFormat'] . $time, strtotime($date));
    }

    public static function toSQLDate($date)
    {
        if (strstr($date, "/"))
        {
            $date = str_replace("/","-", $date);
        }

        return date("Y-m-d H:i:s",strtotime($date));
    }

    // Used for converting our ICLM timestamp ("/") to seconds

    public static function toTime($timestamp)
    {
        $dateObj = new \DateTime(str_replace("/","-",$timestamp));
        return $dateObj->getTimestamp();
    }
}