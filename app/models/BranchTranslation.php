<?php


class BranchTranslation extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $branch_id;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $branch_name;

    /**
     *
     * @var string
     */
    public $street_address;

    /**
     *
     * @var string
     */
    public $manager;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id'=>'id',
            'branch_id' => 'branch_id',
            'lang' => 'lang',
            'branch_name' => 'branch_name',
            'street_address' => 'street_address',
            'manager' => 'manager',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }
}