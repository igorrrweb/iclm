<?php

class EngagementSegment extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $engagement_id;

    /**
     *
     * @var integer
     */
    public $segment_id;

    public function columnMap()
    {
        return [
            'engagement_id' => 'engagement_id',
            'segment_id' => 'segment_id'
        ];
    }

}
