<?php

class FeedbackQuestion extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $feedback_id;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'feedback_id' => 'feedback_id',
            'lang' => 'lang',
            'type' => 'type', 
            'title' => 'title', 
            'updated_at' => 'updated_at', 
            'created_at' => 'created_at'
        ];
    }

    public function initialize()
    {
        $this->hasMany("id","FeedbackRating","id");
        $this->hasMany("id","FeedbackTranslation","question_id");
    }
}
