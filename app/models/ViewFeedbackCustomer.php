<?php

class ViewFeedbackCustomer extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $feedback_id;

    /**
     *
     * @var integer
     */
    public $segment_id;

    /**
     *
     * @var integer
     */
    public $customer_id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $sub_title;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'view_feedback_customer';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ViewFeedbackCustomer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ViewFeedbackCustomer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
