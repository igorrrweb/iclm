<?php

class UserLogs extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $username;

    /**
     *
     * @var string
     */
    public $action;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'user_id' => 'user_id',
            'username' => 'username',
            'action' => 'action', 
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        $this->belongsTo("user_id","SystemUser","id");
    }
}
