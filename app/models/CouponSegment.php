<?php

class CouponSegment extends BaseModel
{
    public function initialize()
    {
        $this->belongsTo("segment_id", "Segment", "id");
        $this->belongsTo("coupon_id", "Coupon", "id");
    }

    /**
     *
     * @var integer
     */
    public $coupon_id;

    /**
     *
     * @var integer
     */
    public $segment_id;

    public function columnMap()
    {
        return [
            'coupon_id' => 'coupon_id',
            'segment_id' => 'segment_id'
        ];
    }
}
