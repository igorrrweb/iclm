<?php

class EngagementPunchcard extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $duration;

    /**
     *
     * @var integer
     */
    public $punch1_2;

    /**
     *
     * @var integer
     */
    public $punch3_5;

    /**
     *
     * @var integer
     */
    public $punch6_8;

    /**
     *
     * @var integer
     */
    public $punch8;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'engagement_punchcard';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return EngagementPunchcard[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return EngagementPunchcard
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
