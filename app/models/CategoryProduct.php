<?php

class CategoryProduct extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $category_id;

    /**
     *
     * @var integer
     */
    public $product_id;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'category_id' => 'category_id', 
            'product_id' => 'product_id'
        ];
    }
    public function initialize()
    {
        $this->belongsTo("category_id","Category","id");
        $this->belongsTo("product_id","Product","id");
    }

}
