<?php

class Category extends BaseModel
{
    protected $loggable = true;

    public function initialize()
    {
        $this->hasMany("id","Coupon","category_id");
        $this->hasManyToMany("id","CategoryProduct","category_id","product_id","Product","id");
        $this->hasMany('id', 'Category', 'parent_category', ['alias' => 'parent']);
    }
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $category;

    /**
     *
     * @var integer
     */
    public $parent_category;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'category' => 'category',
            'parent_category' => 'parent_category',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

}
