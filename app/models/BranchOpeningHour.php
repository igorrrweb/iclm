<?php

class BranchOpeningHour extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $branch_id;

    /**
     *
     * @var string
     */
    public $day;

    /**
     *
     * @var string
     */
    public $open_hour;

    /**
     *
     * @var string
     */
    public $close_hour;

    /**
     *
     * @var integer
     */
    public $shabat_grace;

    /**
     *
     * @var integer
     */
    public $is_closed;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'branch_id' => 'branch_id', 
            'day' => 'day', 
            'open_hour' => 'open_hour', 
            'close_hour' => 'close_hour', 
            'shabat_grace' => 'shabat_grace',
            'is_closed' => 'is_closed',
            'created_at' => 'created_at', 
            'updated_at' => 'updated_at'
        ];
    }

    public function afterValidation()
    {
        $this->open_hour == "" ? $this->open_hour = null : "";
        $this->close_hour == "" ? $this->close_hour = null : "";
    }
}
