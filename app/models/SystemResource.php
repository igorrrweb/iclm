<?php

class SystemResource extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'name' => 'name'
        ];
    }

    public function initialize()
    {
        $this->hasMany("id","SystemRoleResources","resource_id");
    }
}
