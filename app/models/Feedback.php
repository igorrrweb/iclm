<?php

class Feedback extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $sub_title;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var string
     */
    public $trigger;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'title' => 'title', 
            'sub_title' => 'sub_title', 
            'active' => 'active',
            'trigger' => 'trigger',
            'updated_at' => 'updated_at', 
            'created_at' => 'created_at'
        ];
    }

    public function initialize()
    {
        $this->hasManyToMany("id","FeedbackSegment","feedback_id","segment_id","Segment","id");
        $this->hasMany("id","FeedbackSegment","feedback_id");
        $this->hasMany("id","FeedbackQuestion","feedback_id");
        $this->hasMany("id","FeedbackResponse","feedback_id");
        $this->hasManyToMany("id","FeedbackQuestion", "feedback_id", "id", "FeedbackTranslation","question_id");
    }

}
