<?php

class FeedbackRating extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $value1;

    /**
     *
     * @var string
     */
    public $value2;

    /**
     *
     * @var string
     */
    public $value3;

    /**
     *
     * @var string
     */
    public $value4;

    /**
     *
     * @var string
     */
    public $value5;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'value1' => 'value1', 
            'value2' => 'value2', 
            'value3' => 'value3', 
            'value4' => 'value4', 
            'value5' => 'value5', 
            'updated_at' => 'updated_at', 
            'created_at' => 'created_at'
        ];
    }

}
