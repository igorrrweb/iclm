<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class SystemUser extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $role_id;

    /**
     *
     * @var string
     */
    public $first_name;

    /**
     *
     * @var string
     */
    public $last_name;

    /**
     *
     * @var string
     */
    public $lang;

    /**
     *
     * @var string
     */
    public $username;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var integer
     */
    public $branch_id;

    /**
     *
     * @var string
     */
    public $phone;

    /**
     *
     * @var string
     */
    public $mobile;

    /**
     *
     * @var string
     */
    public $birth_date;

    /**
     *
     * @var string
     */
    public $last_login;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var string
     */
    public $token;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;


    public function initialize()
    {
        parent::initialize();
        $this->hasMany("id","UserLogs","user_id");
        $this->belongsTo("role_id","SystemRole","id");
        $this->skipAttributesOnCreate(["token"]);
    }

    protected function beforeCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
        $this->updated_at = date("Y-m-d H:i:s");
    }

    public function beforeValidationOnCreate()
    {
        parent::beforeValidationOnCreate();
        $this->branch_id = 1;
        $this->role_id = 1;
        $this->lang = "he";
    }

    public function afterFetch()
    {
        $this->created_at = self::toICLMDate($this->created_at, true);
    }

    /**
     * Validations and business logic
     */
    public function validation()
    {
        $this->validate(
            new Email([
                    'field'    => 'email',
                    'required' => true,
                ]
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return [
            'id' => 'id', 
            'role_id' => 'role_id', 
            'first_name' => 'first_name', 
            'last_name' => 'last_name', 
            'lang' => 'lang',
            'username' => 'username', 
            'password' => 'password', 
            'email' => 'email', 
            'branch_id' => 'branch_id', 
            'phone' => 'phone', 
            'mobile' => 'mobile', 
            'birth_date' => 'birth_date', 
            'last_login' => 'last_login', 
            'active' => 'active',
            'token' => 'token',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }
}
