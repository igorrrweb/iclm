<?php

class Product extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $barcode;

    /**
     *
     * @var string
     */
    public $image;

    /**
     *
     * @var double
     */
    public $price;

    /**
     *
     * @var string
     */
    public $unit;

    /**
     *
     * @var double
     */
    public $uom;

    /**
     *
     * @var string
     */
    public $promotion;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'vendor' => 'vendor',
            'name' => 'name',
            'description' => 'description',
            'barcode' => 'barcode',
            'image' => 'image',
            'price' => 'price',
            'unit' => 'unit',
            'uom' => 'uom',
            'promotion' => 'promotion',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        $this->hasMany("id","Coupon","product_id");
        $this->hasMany("id","CategoryProduct","product_id");
        $this->hasManyToMany("id","CategoryProduct","product_id", "category_id","Category","id");
    }
}
