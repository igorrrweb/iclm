<?php

class SystemRoleResources extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $resource_id;

    /**
     *
     * @var integer
     */
    public $role;

    public function initialize(){}

    public function columnMap()
    {
        return [
            'resource_id' => 'resource_id',
            'role' => 'role'
        ];
    }

}
