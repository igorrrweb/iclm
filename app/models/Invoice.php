<?php

class Invoice extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $customer_id;

    /**
     *
     * @var integer
     */
    public $branch_id;

    /**
     *
     * @var string
     */
    public $invoice_timestamp;

    /**
     *
     * @var integer
     */
    public $cashier_id;

    /**
     *
     * @var integer
     */
    public $amount;

    /**
     *
     * @var integer
     */
    public $amount_saved;

    /**
     *
     * @var integer
     */
    public $cashback_used;

    /**
     *
     * @var integer
     */
    public $cashback_gained;

    /**
     *
     * @var integer
     */
    public $coupons;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function columnMap()
    {
        return [
            'id' => 'id',
            'customer_id' => 'customer_id',
            'branch_id' => 'branch_id',
            'invoice_timestamp' => 'invoice_timestamp',
            'cashier_id' => 'cashier_id',
            'amount' => 'amount',
            'amount_saved' => 'amount_saved',
            'cashback_used' => 'cashback_used',
            'cashback_gained' => 'cashback_gained',
            'coupons' => 'coupons',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

}
