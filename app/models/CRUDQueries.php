<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;


class CRUDQueries {

    public static function getSegments($criteria)
    {
        $columns = array('s.id', 's.name','members', 's.active', 's.created_at', 'actions');

        $having = !empty($criteria['search']['value'])
            ? " having ((LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or ( id like '%".addslashes($criteria['search']['value'])."%')
            or ( members like '%".addslashes($criteria['search']['value'])."%')
            or ( date_format(created_at,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%') ) " : '';

        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }

        $sql = "SELECT s.id as id, s.name as name, COUNT( sc.segment_id ) as members,
                s.active as active, date_format(s.created_at,'%Y-%m-%d %H:%i:%s') as created_at, s.id as actions
                FROM segment s
                left join segment_customer sc ON s.id = sc.segment_id
                GROUP BY s.id ". $having . $orders ;

        $model = new Segment();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();



    }

    public static function getSegmentsFeedback($criteria)
    {
        $columns = array('active_segment', 'name','audience');

        $activeSegment = $criteria['getParam'] ? $criteria['getParam'] : "";



        $having = !empty($criteria['search']['value'])
            ? " having ((LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or ( id like '%".addslashes($criteria['search']['value'])."%')
            or ( audience like '%".addslashes($criteria['search']['value'])."%')
            ) " : '';

        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by  ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $sql = "SELECT  s.id as id,
                        s.name as name,
                        COUNT( sc.segment_id ) as audience,
                        if(s.id in (select fs.segment_id from feedback f
                                    left join feedback_segment fs on f.id=fs.feedback_id
                                    where  f.id='{$activeSegment}'),1,0) as active_segment
                FROM segment s
                left join segment_customer sc ON s.id = sc.segment_id
                GROUP BY s.id ". $having . $orders ;

        $model = new Segment();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getSegmentsEngagement($criteria)
    {
        $columns = array('active_segment', 'name','audience');

        $activeSegment = $criteria['getParam'] ? $criteria['getParam'] : "";

        $having = !empty($criteria['search']['value'])
            ? " having ((LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or ( id like '%".addslashes($criteria['search']['value'])."%')
            or ( audience like '%".addslashes($criteria['search']['value'])."%')
            ) " : '';

        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by  ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $sql = "SELECT s.id as id, s.name as name, COUNT( sc.segment_id ) as audience,
                if(s.id in (select es.segment_id from engagement e
                            left join engagement_segment es on e.id=es.engagement_id
                            where  e.id='{$activeSegment}'),1,0) as active_segment
                FROM segment s
                left join segment_customer sc ON s.id = sc.segment_id
                group by s.id ". $having . $orders ;

        $model = new Segment();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getSegmentsMessage($criteria){
        $columns = array('active_segment', 'name','audience');

        $activeSegment = $criteria['getParam'] ? $criteria['getParam'] : "";

        $having = !empty($criteria['search']['value'])
            ? " having ((LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or ( id like '%".addslashes($criteria['search']['value'])."%')
            or ( audience like '%".addslashes($criteria['search']['value'])."%')
            ) " : '';

        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by  ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $sql = "SELECT s.id as id, s.name as name, COUNT( sc.segment_id ) as audience,
                if(s.id in (select ms.segment_id from message m
                            left join message_segment ms on m.id=ms.message_id
                            where  m.id='{$activeSegment}'),1,0) as active_segment
                FROM segment s
                left join segment_customer sc ON s.id = sc.segment_id
                group by s.id ". $having . $orders ;

        $model = new Segment();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getCoupons($criteria)
    {
        $columns = array('id','name','barcode','product','audience','coupon_type','redeemed','active','actions');
        $having = !empty($criteria['search']['value'])
            ? " having ( (id like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (barcode like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(product) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (audience like '%".addslashes($criteria['search']['value'])."%')
            or (coupon_type like '%".addslashes($criteria['search']['value'])."%')
            or (redeemed like '%".addslashes($criteria['search']['value'])."%') )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $sql = "SELECT  c.id as id,
                        c.name as name,
                        c.active as active,
                        c.coupon_type as coupon_type,
                        c.total_redeemed as total_redeemed,
                        if(p.name,concat(ca.category,'/',p.name),ca.category) as product,
                        COALESCE(ROUND(c.total_redeemed / c.max_coupons * 100,2),0) as redeemed,
                        COUNT(DISTINCT scs.customer_id) as audience,
                        ifnull(p.barcode,'') as barcode,
                        c.id as actions
                FROM coupon c
                left JOIN coupon_segment cs ON c.id = cs.coupon_id
                left JOIN segment_customer scs ON cs.segment_id = scs.segment_id
                LEFT JOIN category ca ON ca.id = c.category_id
                LEFT JOIN product p ON p.id = c.product_id
                GROUP BY c.id ".$having. $orders;

        $model = new Coupon();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();

    }

    public static function getReportCoupons($criteria)
    {
        $columns = array('c.id','c.name','category','audience','c.coupon_type','c.total_redeemed','percentage_of_goal_reached');
        $having = !empty($criteria['search']['value'])
            ? " having (
            (id like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (LCASE(category) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (audience like '%".addslashes($criteria['search']['value'])."%')
            or (coupon_type like '%".addslashes($criteria['search']['value'])."%')
            or (redeemed like '%".addslashes($criteria['search']['value'])."%')
            or (percentage_of_goal_reached like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        if(($criteria['category'] != '') && ($criteria['product'] != '')){
            $where = " WHERE ca.id='".$criteria['category']."' AND p.id='".$criteria['product']."' ";
        } elseif($criteria['category'] != ''){
            $where = " WHERE ca.id='".$criteria['category']."' ";
        } elseif($criteria['product'] != ''){
            $where = " WHERE p.id='".$criteria['product']."' ";
        } else {
            $where = "";
        }
        $sql = "SELECT  c.id as id,
                        c.name as name,
                        if(p.name,concat(ca.category,'/',p.name),ca.category) as category,
                        COUNT(DISTINCT scs.customer_id) as audience,
                        c.coupon_type as coupon_type,
                        c.total_redeemed as redeemed,
                        COALESCE(ROUND(c.total_redeemed / c.max_coupons * 100,2),0) as percentage_of_goal_reached

                FROM coupon c
                left JOIN coupon_segment cs ON c.id = cs.coupon_id
                left JOIN segment_customer scs ON cs.segment_id = scs.segment_id
                LEFT JOIN category ca ON ca.id = c.category_id
                LEFT JOIN product p ON p.id = c.product_id " .$where.
                " GROUP BY c.id ".$having. $orders;

        $model = new Coupon();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();

    }

    public static function getFeedbacks($criteria)
    {
        $columns = array('id','title','audience','status','actions');

        $having = !empty($criteria['search']['value'])
            ? " having ( (id like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(title) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (audience like '%".addslashes($criteria['search']['value'])."%')
            or (status like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }

        $sql = "SELECT  f.id as id,
                        f.title as title,
                        COUNT(DISTINCT sc.customer_id) as audience,
                        f.active as status,
                        f.id as actions
                FROM feedback f
                JOIN feedback_segment fs ON f.id = fs.feedback_id
                JOIN segment_customer sc ON fs.segment_id = sc.segment_id
                GROUP BY f.id" . $having . $orders;

        $model = new Feedback();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();

    }

    public static function getCustomerStatus($id){
        $status = "Active";
        $sql = "SELECT amount FROM invoice WHERE customer_id = " . $id . " AND invoice_timestamp > '" . date("Y/m/d h:m:s",time() - 7776000) . "'
            GROUP BY MONTH(invoice_timestamp) ORDER BY invoice_timestamp DESC";
        $invoice = new Invoice();
        $invoices = new Resultset(null, $invoice, $invoice->getReadConnection()->query($sql));
        $lastInvoice = Invoice::findFirst(["columns" => "invoice_timestamp","conditions" =>
            "customer_id = " . $id, "order" => "id DESC"]);
        if (!$lastInvoice) {
            $status = "Inactive";
        } else {
            if (time() - strtotime($lastInvoice->invoice_timestamp) >= 15552000) {
                $status = "Inactive";
            } elseif (time() - strtotime($lastInvoice->invoice_timestamp) >= 5184000 ||
                isset($invoices[2]) && $invoices[0]->amount < $invoices[1]->amount &&
                $invoices[1]->amount < $invoices[2]->amount) {
                $status = "ChurnAlert";
            }
        }
        return $status;
    }

    public static function getCustomers($criteria)
    {
        $columns = array("id", "name", "join_date", "last_visit", "status", "actions");

        $having = !empty($criteria['search']['value'])
            ? " having ( (id like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (date_format(join_date,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            or (date_format(last_visit,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            or (status like '%".strtolower(addslashes($criteria['search']['value']))."%')
             )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $sql = "SELECT c.id as id,
                       concat(c.firstname,' ',c.lastname) as name,
                       ifnull(c.join_date, c.created_at) as join_date,
                       ifnull( ifnull( date_format(max(bv.enter), '%Y-%m-%d %H:%i'), date_format(max(bv.exit), '%Y-%m-%d %H:%i') ),
                                date_format(max(i.invoice_timestamp), '%Y-%m-%d %H:%i') ) as last_visit,
                       'null' as status,
                       c.id as actions
                FROM customer c
                LEFT JOIN invoice i ON c.id = i.customer_id
                LEFT JOIN branch_visits bv ON c.id = bv.customer_id
                GROUP BY c.id " . $having . $orders;

        $model = new Customer();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        $customers = $res->toArray();
        if($customers){
            foreach($customers as &$customer){
                $customer['status'] = self::getCustomerStatus($customer['id']);
            }
        }
        return $customers;
    }

    public static function getBeacon($criteria){
        $columns = array('id', 'name', 'branch', 'in_store_location', 'last_health_check', 'actions');

        $having = !empty($criteria['search']['value'])
            ? " having ( (id like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (LCASE(branch) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (in_store_location like '%".addslashes($criteria['search']['value'])."%')
            or (date_format(last_health_check,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')

             )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $currentLang = Phalcon\DI::getDefault()['session']->lang ? Phalcon\DI::getDefault()['session']->lang :
            Phalcon\DI::getDefault()['settings']['defaultLang'];

        $sql = "SELECT  b.id as id,
                        b.name as name,
                        br.branch_name as branch,
                        '' as in_store_location,
                        ifnull(date_format(b.updated_at,'%Y-%m-%d %H:%i'), (select {$currentLang} from lang where keyword = 'NotChecked' limit 1)) as last_health_check,
                        b.id as actions
                        FROM beacon b
                LEFT JOIN branch br ON  b.branch_id = br.branch_no
                GROUP BY b.id" . $having . $orders ;

        $model = new Beacon();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getEngagements($criteria, $type)
    {
        $columns = array('id', 'name', 'duration', 'end_date', 'redeemed', 'status', 'actions');
        $having = !empty($criteria['search']['value'])
            ? " having ( (id like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (duration like '%".addslashes($criteria['search']['value'])."%')
            or (date_format(end_date,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            or (redeemed like '%".addslashes($criteria['search']['value'])."%')
            or (status like '%".addslashes($criteria['search']['value'])."%')
             )" : '';

        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }

        $currentLang = Phalcon\DI::getDefault()['session']->lang ? Phalcon\DI::getDefault()['session']->lang :
            Phalcon\DI::getDefault()['settings']['defaultLang'];


        $sql = "SELECT id as id, name as name,
                case
                    when (TIMESTAMPDIFF(SECOND,start_date,end_date) > 86400) and (never_expires=0)
                        then concat(round((TIMESTAMPDIFF(SECOND,start_date,end_date) / 86400)), ' ', (select {$currentLang} from lang where keyword = 'Days' limit 1))
                    when (TIMESTAMPDIFF(SECOND,start_date,end_date) > 3600) and (never_expires=0)
                        then concat(round((TIMESTAMPDIFF(SECOND,start_date,end_date) / 3600)), ' ', (select {$currentLang} from lang where keyword = 'Hours' limit 1))
                    when never_expires = 1
                        then '&infin;'
                    else concat(round((TIMESTAMPDIFF(SECOND,start_date,end_date) / 60)), ' ', (select {$currentLang} from lang where keyword = 'Minutes' limit 1))
                end as duration,
                if(never_expires = 1, (select {$currentLang} from lang where keyword = 'NeverExpires' limit 1), end_date) as end_date,
                if(round(completed/presented * 100), concat(round(completed/presented * 100),'','%'), '0%'  ) as redeemed,
                case
                    when never_expires = 1 then 1
                    when (unix_timestamp() < unix_timestamp(start_date)) and (never_expires = 0) then 0
                    when (unix_timestamp() > unix_timestamp(end_date)) and (never_expires = 0) then 0
                    when (reward = 0) and (never_expires = 0) then 0
                    else 1
                end as status,
                 id as actions
                 FROM engagement where type = '{$type}' GROUP BY id ". $having . $orders;

        $engagement = new Engagement();
        $res =  new Resultset(null, $engagement, $engagement->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getMessages($criteria)
    {
        $columns = array('id', 'title', 'duration', 'end_date', 'audience', 'active', 'actions');
        $having = !empty($criteria['search']['value'])
            ? " having (
            (id like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(title) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (duration like '%".addslashes($criteria['search']['value'])."%')
            or (date_format(end_date,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            or (audience like '%".addslashes($criteria['search']['value'])."%')
            or (active like '%".addslashes($criteria['search']['value'])."%')
             )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $currentLang = Phalcon\DI::getDefault()['session']->lang ? Phalcon\DI::getDefault()['session']->lang :
            Phalcon\DI::getDefault()['settings']['defaultLang'];

        $sql = "SELECT  m.id as id,
                        m.title as title,
                        case
                            when TIMESTAMPDIFF(SECOND,m.start_date,m.end_date) > 86400
                                then concat(round((TIMESTAMPDIFF(SECOND,m.start_date,m.end_date) / 86400)), ' ', (select {$currentLang} from lang where keyword = 'Days' limit 1))
                            when TIMESTAMPDIFF(SECOND,m.start_date,m.end_date) > 3600
                                then concat(round((TIMESTAMPDIFF(SECOND,m.start_date,m.end_date) / 3600)), ' ', (select {$currentLang} from lang where keyword = 'Hours' limit 1))
                            else concat(round((TIMESTAMPDIFF(SECOND,m.start_date,m.end_date) / 60)), ' ', (select {$currentLang} from lang where keyword = 'Minutes' limit 1))
                        end as duration,
                        m.content as content,
                        m.image as image,
                        m.trigger,
                        m.active as active,
                        m.start_date as start_date,
                        m.end_date as end_date,
                        m.updated_at as updated_at,
                        m.created_at as created_at,
                        ms.segment_id as segment_id,
                        sc.customer_id as customer_id,
                        COUNT(DISTINCT sc.customer_id) as audience,
                        m.id as actions
                FROM message m
                left JOIN message_segment ms ON ms.message_id = m.id
                left JOIN segment_customer sc ON ms.segment_id = sc.segment_id
                GROUP BY m.id ". $having . $orders;

        $engagement = new Message();
        $res =  new Resultset(null, $engagement, $engagement->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getResponses($criteria)
    {
        $columns = array("question_id", "feedback_id", "customer_id", "title", "created_at",'actions');
        $having = !empty($criteria['search']['value'])
            ? " having ( (question_id like '%".addslashes($criteria['search']['value'])."%')
            or (feedback_id like '%".addslashes($criteria['search']['value'])."%')
            or (customer_id like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(title) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (date_format(created_at,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }

        $sql = "SELECT  fr.question_id as question_id,
                        f.id as feedback_id,
                        c.id as customer_id,
                        f.title as title,
                        fr.created_at as created_at,
                        fr.feedback_id as actions,
                        fr.is_read as is_read
                FROM feedback_response fr
                JOIN feedback f ON fr.feedback_id = f.id
                JOIN customer c ON fr.customer_id = c.id where fr.is_archived = 0 " . $having . $orders;

        $model = new FeedbackResponse();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getSystemUser($criteria){
        $columns = array("id", "username", "role", "added", "last_login", "status", "actions");
        $having = !empty($criteria['search']['value'])
            ? " having (
            (id like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(username) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (LCASE(role) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (date_format(added,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            or (date_format(last_login,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            or (status like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $currentLang = Phalcon\DI::getDefault()['session']->lang ? Phalcon\DI::getDefault()['session']->lang :
            Phalcon\DI::getDefault()['settings']['defaultLang'];

        $sql = "SELECT  su.id as id,
                        su.username as username,
                        sr.name as role,
                        su.created_at as added,
                        IFNULL(su.last_login, (select {$currentLang} from lang where keyword = 'NotLoggedInYet' limit 1) ) as last_login,
                        su.active as status,
                        su.id as actions
                FROM system_user su
                LEFT JOIN system_role sr ON sr.id = su.role_id
                GROUP BY su.id ". $having . $orders;

        $model = new SystemUser();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getBranches($criteria){
        $columns = array( "branch_number", "branch_name", "city", "manager", "phone_number", "active", "actions");

        $having = !empty($criteria['search']['value'])
            ? " having (
            (branch_number like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(branch_name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (LCASE(branch_name_lang) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (city like '%".addslashes($criteria['search']['value'])."%')
            or (LCASE(manager) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (phone_number like '%".addslashes($criteria['search']['value'])."%')
            or (active like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $currentLang = Phalcon\DI::getDefault()['session']->lang ? Phalcon\DI::getDefault()['session']->lang :
            Phalcon\DI::getDefault()['settings']['defaultLang'];

        $sql = "SELECT  b.branch_no as branch_number,
                        b.branch_name as branch_name,
                        (select bt.branch_name from branch_translation bt where bt.branch_id = b.id and lang = '{$currentLang}') as branch_name_lang,
                        c.city as city,
                        b.manager as manager,
                        b.branch_phone as phone_number,
                        b.active as active,
                        b.id as actions
                FROM branch b
                LEFT JOIN city c ON b.city_id = c.id
                GROUP BY b.id " . $having . $orders;

        $model = new Branch();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getSegmentReports($criteria)
    {
        $columns = array( "name", "audience", "redeemed", "available_coupons", "available_engagements");

        $having = !empty($criteria['search']['value'])
            ? " having (
            (LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (audience like '%".addslashes($criteria['search']['value'])."%')
            or (redeemed like '%".addslashes($criteria['search']['value'])."%')
            or (available_coupons like '%".addslashes($criteria['search']['value'])."%')
            or (available_engagements like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $sql = "SELECT  s.id as id,
                        s.name as name,
                        IFNULL(scs.audience,0) as audience,
                        css.redeemed as redeemed,
                        COALESCE(css.ac,0) as available_coupons,
                        COALESCE(ess.ae,0) as available_engagements
                FROM segment s
                LEFT JOIN ( SELECT *,
                                  COUNT(DISTINCT customer_id) as audience
                          FROM segment_customer
                          GROUP BY segment_id ) scs ON scs.segment_id = s.id
                LEFT JOIN  ( SELECT *,
                                    c.total_redeemed / c.max_coupons as redeemed,
                                    SUM(c.max_coupons) - SUM(c.total_redeemed) as ac
                            FROM coupon c
                            JOIN coupon_segment cs ON c.id = cs.coupon_id
                            GROUP BY cs.segment_id ) css ON css.segment_id = s.id
                LEFT JOIN ( SELECT e.*,
                                  SUM(e.total_units) - SUM(e.completed) as ae,
                                  es.*
                            FROM engagement e
                            JOIN engagement_segment es ON e.id = es.engagement_id
                            GROUP BY es.segment_id ) ess ON ess.segment_id = s.id
                GROUP BY s.id " . $having . $orders;

        $engagement = new Segment();
        $res =  new Resultset(null, $engagement, $engagement->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function getCustomer($lc_number,$user_id){
        $conditions = "";
        if ($lc_number && $user_id){
            $conditions = " where c.lc_number like '%".$lc_number."%' AND c.id like '%".$user_id."%' ";
        }elseif($lc_number){
            $conditions = " where c.lc_number like '%".$lc_number."%' ";
        }elseif($user_id){
            $conditions = " where c.id like '%".$user_id."%' ";
        }else{
            $conditions = "";
        }
        return $conditions;
    }

    public static function getPurchaseHistory($criteria){
        $columns = array( "date","duration","amount","redeemed" );

        $having = !empty($criteria['search']['value'])
            ? " having (amount is not null)
            and (
                (date_format(date,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
                or (duration like '%".addslashes($criteria['search']['value'])."%')
                or (amount like '%".addslashes($criteria['search']['value'])."%')
                or (redeemed like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }

        $where = self::getCustomer($criteria['lc'],$criteria['uid']);
        if(empty($where)){
            return [];
        }
        $sql = "SELECT c.id,
                CONCAT(' ',c.firstname,c.lastname) as customer_name,
                date_format(bv.enter, '%d/%m/%Y') as date,
                timestampdiff(MINUTE, bv.enter, bv.exit) as duration,
                i.amount as amount,
                count(ca.redeemed) as redeemed
                FROM customer c
                left join branch_visits bv on bv.customer_id = c.id
                left join invoice i on i.customer_id = bv.customer_id and date(i.invoice_timestamp) between date(bv.enter) and date(bv.exit)
                left join invoice_product p on p.invoice_id = i.id
                left join coupon_activity ca on ca.customer_id=bv.customer_id and date(ca.redeemed) between date(bv.enter) and date(bv.exit)
                " . $where . " group by date(bv.enter) " . $having . $orders;

        $model = new Customer();
        $res =  new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }

    private static function query($modelName, $sql)
    {
        $model = new $modelName;

        return new Resultset(null, $model, $model->getReadConnection()->query($sql));
    }


    public static function getCampaigns($criteria){

        $columns = array( "name", "audience", "campaign_type", "expiry_date", "redeemed_completed", "status", "actions" );

        $having = !empty($criteria['search']['value'])
            ? " having (
            (LCASE(name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (audience like '%".addslashes($criteria['search']['value'])."%')
            or (campaign_type like '%".addslashes($criteria['search']['value'])."%')
            or (date_format(expiry_date,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            or (redeemed_completed like '%".addslashes($criteria['search']['value'])."%')
            or (status like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $additionWhere = $criteria['campaign_type'] != '' ? " and campaign_type='".$criteria['campaign_type']."' " : '';
        $sql = "select t.*, t.id as actions from
                (select c.id, c.name as name, 'coupon' as campaign_type,
                count(sc.segment_id) as audience, c.expiry_date as expiry_date,
                COALESCE(ROUND(c.total_redeemed / c.max_coupons * 100,2),0) as redeemed_completed,
                c.active as status, 0 as never_expires, c.created_at
                from segment_customer sc
                left join segment s on s.id = sc.segment_id
                left join coupon_segment cs on cs.segment_id = s.id
                left join coupon c on c.id = cs.coupon_id
                where c.name is not null
                group by c.name
                union all
                select e.id, e.name as name, e.type as campaign_type, count(sc.customer_id) as audience, e.end_date as expiry_date,
                COALESCE(ROUND(e.completed / e.presented * 100,2),0) as redeemed_completed,
                e.active as status, e.never_expires as never_expires, e.created_at
                from segment_customer sc
                left join segment s on s.id = sc.segment_id
                left join engagement_segment es on es.segment_id = s.id
                left join engagement e on e.id = es.engagement_id
                where e.name is not null
                group by e.name
                union all
                select f.id, f.title as name, 'feedback' as campaign_type, count(sc.customer_id) as audience, null as expiry_date,
                '0.00' as redeemed_completed, f.active as status, 0 as never_expires, f.created_at
                from segment_customer sc
                left join segment s on s.id = sc.segment_id
                left join feedback_segment fs on fs.segment_id = s.id
                left join feedback f on f.id = fs.feedback_id
                where f.title is not null
                group by f.title) t WHERE ((t.expiry_date > subdate(now(), interval 3 day)) or t.never_expires=1) ". $additionWhere . $having . $orders;

        $model = new SegmentCustomer;
        $res = new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }
    public static function getChurnalert(){
        $columns = array( "customer_name", "phone_number", "join_date", "churn_alert_date", "retention", "status", "actions" );

        $having = !empty($criteria['search']['value'])
            ? " having (
            (LCASE(customer_name) like '%".strtolower(addslashes($criteria['search']['value']))."%')
            or (phone_number like '%".addslashes($criteria['search']['value'])."%')
            or (date_format(join_date,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            or (date_format(churn_alert_date,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            or (retention like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }

        $sql = "select id,
                concat_ws(' ', firstname, lastname) as customer_name,
                ifnull(phone, mobile) as phone_number,
                ifnull(join_date,created_at) as join_date,
                date_format(now(), '%d/%m/%Y') as churn_alert_date,
                date_format(now(), '%d/%m/%Y') as retention,
                id as status,
                id as actions
                from customer where id in (
                select t3.cid from(
                select c.id as cid, 100-(t2.duration2 *100 / t1.duration1) as percentage
                from customer c
                left join (select customer_id, avg(timestampdiff(MINUTE, enter, `exit`)) as duration1 from branch_visits where date_format(enter,'%Y-%m-%d') between subdate(current_date(),interval 6 month) and subdate(current_date(),interval 3 month)  group by customer_id) t1 on t1.customer_id = c.id
                left join (select customer_id, avg(timestampdiff(MINUTE, enter, `exit`)) as duration2 from branch_visits where date_format(enter,'%Y-%m-%d') between subdate(current_date(),interval 3 month) and current_date()  group by customer_id) t2 on t2.customer_id = c.id
                group by c.id
                having percentage >= 15
                union
                select c.id as cid,
                100-(t2.duration2 *100 / t1.duration1) as percentage
                from customer c
                left join (select customer_id, count(*) as duration1 from branch_visits where date_format(enter,'%Y-%m-%d') between subdate(current_date(),interval 6 month) and subdate(current_date(),interval 3 month)
                            group by customer_id) t1 on t1.customer_id = c.id

                left join (select customer_id, count(*) as duration2 from branch_visits where date_format(enter,'%Y-%m-%d') between subdate(current_date(),interval 3 month) and current_date()
                            group by customer_id) t2 on t2.customer_id = c.id
                group by c.id
                having percentage >= 15
                union
                select c.id as cid,
                100-(t2.amount2 *100 / t1.amount1) as percentage
                from customer c
                left join(select customer_id, avg(amount) as amount1 from invoice
                where date_format(invoice_timestamp,'%Y-%m-%d') between subdate(current_date(),interval 6 month) and subdate(current_date(),interval 3 month)
                group by customer_id) t1 on t1.customer_id = c.id
                left join(select customer_id, avg(amount) as amount2 from invoice
                where date_format(invoice_timestamp,'%Y-%m-%d') between subdate(current_date(),interval 3 month) and current_date()
                group by customer_id) t2 on t2.customer_id = c.id
                group by c.id
                having percentage >= 15
                union
                select c.id as cid, t.left_store as percentage from customer c
                join (select bv.customer_id as customer_id , count(*) as left_store from branch_visits bv
                where bv.customer_id NOT IN (
                    select i.customer_id
                    from invoice i
                    where i.invoice_timestamp
                    between subdate(current_date(),interval 6 month) and current_date() group by i.customer_id
                ) and enter between subdate(current_date(),interval 6 month) and current_date() group by bv.customer_id
                ) t on t.customer_id = c.id group by c.id having percentage >= 2) t3 group by t3.cid)".$having . $orders;

        $model = new Customer;
        $res = new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }



    public static function getRetention($criteria){
        $columns = array( "id", "churn_alert_date", "activity" );

        $having = !empty($criteria['search']['value'])
            ? " having (
            (id like '%".addslashes($criteria['search']['value'])."%')
            or (date_format(churn_alert_date,'%Y-%m-%d %H:%i') like '%".addslashes($criteria['search']['value'])."%')
            )" : '';
        $orders = '';
        if(!empty($criteria['order'])){
            $orders .= " order by ";
            foreach($criteria['order'] as $order){
                $orders .= $columns[ $order['column'] ]." ". $order['dir']." ";
            }
        }
        $currentLang = Phalcon\DI::getDefault()['session']->lang ? Phalcon\DI::getDefault()['session']->lang :
            Phalcon\DI::getDefault()['settings']['defaultLang'];

        $sql = "select  cu.id as id,
                        date_format(now(), '%d/%m/%Y') as churn_alert_date,
                        ifnull(cu.phone, cu.mobile) as phone_number,
                        'test' as customer_name,
                        '' as action,
                        date_format(now(), '%d/%m/%Y') as date,
                        '' as activity,
                        (select {$currentLang} from lang where keyword = 'Action' limit 1) as action,
                        (select {$currentLang} from lang where keyword = 'Feedback' limit 1) as feedback,
                        (select {$currentLang} from lang where keyword = 'Coupon' limit 1) as coupon,
                        (select {$currentLang} from lang where keyword = 'FlipTiles' limit 1) as flip_tiles,
                        (select {$currentLang} from lang where keyword = 'PhoneCall' limit 1) as phone_call

                from branch_visits bv
                left join customer cu on cu.id=bv.customer_id GROUP BY cu.id " . $having . $orders;


        $model = new BranchVisits();
        $res = new Resultset(null, $model, $model->getReadConnection()->query($sql));
        return $res->toArray();
    }

    public static function setAdditionFields($criteria){

        $additionFields = array();
        if($criteria['filterValueUserId'] && $criteria['filterValueLcNumber'] && $criteria['filterValueSegmentName']){
            $joinedTable = "left join customer c on c.id = bv.customer_id
                            left join segment_customer sc on sc.customer_id = bv.customer_id ";
            $field = " and c.lc_number='".$criteria['filterValueLcNumber']."' and c.id='".$criteria['filterValueUserId']."' and sc.segment_id='".$criteria['filterValueSegmentName']."' ";
        } elseif ($criteria['filterValueUserId'] && $criteria['filterValueLcNumber']){
            $joinedTable = "left join customer c on c.id = bv.customer_id ";
            $field = " and c.lc_number='".$criteria['filterValueLcNumber']."' and c.id='".$criteria['filterValueUserId']."' ";
        } elseif ($criteria['filterValueLcNumber'] && $criteria['filterValueSegmentName']){
            $joinedTable = "left join customer c on c.id = bv.customer_id
                            left join segment_customer sc on sc.customer_id = bv.customer_id ";
            $field = " and c.lc_number='".$criteria['filterValueLcNumber']."' and sc.segment_id='".$criteria['filterValueSegmentName']."' ";
        } elseif ($criteria['filterValueUserId'] &&  $criteria['filterValueSegmentName']){
            $joinedTable = "left join customer c on c.id = bv.customer_id
                            left join segment_customer sc on sc.customer_id = bv.customer_id ";
            $field = " and c.id='".$criteria['filterValueUserId']."' and sc.segment_id='".$criteria['filterValueSegmentName']."' ";
        } elseif ($criteria['filterValueUserId']){
            $joinedTable = ' left join customer c on c.id = bv.customer_id ';
            $field = " and c.id='".$criteria['filterValueUserId']."' ";
        } elseif ($criteria['filterValueLcNumber']){
            $joinedTable = ' left join customer c on c.id = bv.customer_id ';
            $field = " and c.lc_number='".$criteria['filterValueLcNumber']."' ";
        } elseif ($criteria['filterValueSegmentName']){
            $joinedTable = ' left join segment_customer sc on sc.customer_id = bv.customer_id ';
            $field = " and sc.segment_id='".$criteria['filterValueSegmentName']."' ";
        } else {
            $joinedTable = '';
            $field = '';
        }

        $additionFields['joinedTable'] = $joinedTable;
        $additionFields['filter'] = $field;
        return $additionFields;
    }


    public static function getUsersVisitsWithPurchase($criteria){

        $additionFields = self::setAdditionFields($criteria);

        $sql = "SELECT count(bv.customer_id) as count_customers FROM branch_visits bv
                inner join invoice i on i.customer_id = bv.customer_id ".
                $additionFields['joinedTable']
                ." where date(bv.enter) between '".$criteria['startDate']."' and '".$criteria['endDate']."'
                and date(i.invoice_timestamp) between date(bv.enter) and date(bv.exit) ". $additionFields['filter'];

        $branch_visits = new BranchVisits();
        return new Resultset(null, $branch_visits, $branch_visits->getReadConnection()->query($sql));
    }

    public static function getVisitsWithoutPurchase($criteria){
        $additionFields = self::setAdditionFields($criteria);

        $sql = "select count(bv.customer_id)-(SELECT count(bv.customer_id) as count_customers FROM branch_visits bv
                inner join invoice i on i.customer_id = bv.customer_id ".
                $additionFields['joinedTable']
                ." where date(bv.enter) between '".$criteria['startDate']."' and '".$criteria['endDate']."'
                and date(i.invoice_timestamp) between date(bv.enter) and date(bv.exit) ". $additionFields['filter'].") as count_customers, bv.customer_id
                from branch_visits bv ".
                $additionFields['joinedTable']
                ." where (bv.enter is not null and bv.exit is not null)
                and date(bv.enter) between '".$criteria['startDate']."' and '".$criteria['endDate']."'" . $additionFields['filter'];

        $branch_visits = new BranchVisits();
        return new Resultset(null, $branch_visits, $branch_visits->getReadConnection()->query($sql));
    }

    public static function getPassedByWithoutEntering($criteria){
        $additionFields = self::setAdditionFields($criteria);

        $sql = "select count(bv.customer_id) as count_customers
                from branch_visits bv ".$additionFields['joinedTable']
                ." where bv.enter is null or bv.exit is null
                and (date(bv.enter) between '".$criteria['startDate']."' and '".$criteria['endDate']."') " . $additionFields['filter'];

        $branch_visits = new BranchVisits();
        return new Resultset(null, $branch_visits, $branch_visits->getReadConnection()->query($sql));
    }

    public static function getAverageTimePerVisits($criteria){
        $additionFields = self::setAdditionFields($criteria);
        $startDate = new \DateTime($criteria['startDate']);
        $endDate = new \DateTime($criteria['endDate']);
        $interval = $startDate->diff($endDate);
        $days = $interval->format('%a');
        $secondStart = new \DateTime($criteria['startDate']);
        $secondStart->modify('-'.$days.' day');
        $sDateStart = $secondStart->format('Y-m-d');


        $branch_visits = new BranchVisits();
        $sql = "select ifnull(round(avg(t.duration),0),0) as duration from
                (select bv.customer_id, avg(timestampdiff(MINUTE,bv.enter,bv.exit)) as duration
                from branch_visits bv " .$additionFields['joinedTable']
            ." where bv.enter is not null and bv.exit is not null
                and (date(bv.enter) between '".$sDateStart."' and '".$criteria['startDate']."')". $additionFields['filter']
            ." group by bv.customer_id) t";

        $response1 = new Resultset(null, $branch_visits, $branch_visits->getReadConnection()->query($sql));

        $sql = "select ifnull(round(avg(t.duration),0),0) as duration from
                (select bv.customer_id, avg(timestampdiff(MINUTE,bv.enter,bv.exit)) as duration
                from branch_visits bv " .$additionFields['joinedTable']
                ." where bv.enter is not null and bv.exit is not null
                and (date(bv.enter) between '".$criteria['startDate']."' and '".$criteria['endDate']."')". $additionFields['filter']
                ." group by bv.customer_id) t";
        $response2 = new Resultset(null, $branch_visits, $branch_visits->getReadConnection()->query($sql));


        return [ $response1, $response2 ];
    }

    public static function getVisitedOn($criteria){
        $additionFields = self::setAdditionFields($criteria);

        $sql = "select count(date_format(bv.enter, '%W')) as count_visits,
                date_format(bv.enter, '%W') as dayweek
                from branch_visits bv ".$additionFields['joinedTable']
                ." where ((bv.enter is not null and bv.exit is not null)  and (date(bv.enter) between '".$criteria['startDate']."' and '".$criteria['endDate']."'))" .$additionFields['filter']
                ." group by date_format(bv.enter,'%w')";

        $branch_visits = new BranchVisits();
        return new Resultset(null, $branch_visits, $branch_visits->getReadConnection()->query($sql));
    }

    public static function getPassedByOn($criteria){
        $additionFields = self::setAdditionFields($criteria);
        $sql = "select count(date_format(bv.enter, '%W')) as count_visits,
                date_format(bv.enter, '%W') as dayweek
                from branch_visits bv ".$additionFields['joinedTable']
                ." where ((bv.enter is null and date(bv.exit) between '".$criteria['startDate']."' and '".$criteria['endDate']."')
                or (bv.exit is null and date(bv.enter) between '".$criteria['startDate']."' and '".$criteria['endDate']."')) ".$additionFields['filter']
                ." group by date_format(bv.enter,'%w')";

        $branch_visits = new BranchVisits();
        return new Resultset(null, $branch_visits, $branch_visits->getReadConnection()->query($sql));
    }

    public static function getSearchResponsesByCustomerIdOrTitle($searchField,$criteria)
    {

        if ($criteria['conditions'] != null)
        {
            $criteria['conditions'] = "WHERE " . $criteria['conditions'];
        }

        $sql = "SELECT DISTINCT {$searchField} FROM feedback_response fr JOIN feedback f ON fr.feedback_id = f.id JOIN customer c ON fr.customer_id = c.id " . $criteria['conditions']. " LIMIT 5 ";

        $response = new FeedbackResponse();
        $a = new Resultset(null, $response, $response->getReadConnection()->query($sql));
        return $a->toArray();
    }

    public static function getSearchCustomers($searchField,$criteria)
    {
        if ($criteria['conditions'] != null)
        {
            $criteria['conditions'] = "WHERE " . $criteria['conditions'];
        }
        $sql = "SELECT DISTINCT {$searchField}
                FROM customer c
                LEFT JOIN invoice i ON c.id = i.customer_id
                LEFT JOIN branch_visits bv ON c.id = bv.customer_id
                " . $criteria['conditions']. " LIMIT 5 ";

        $customer = new Customer;
        $a = new Resultset(null, $customer, $customer->getReadConnection()->query($sql));
        return $a->toArray();
    }

    public static function getSearchCouponsReports($searchField,$criteria){
        if ($criteria['conditions'] != null)
        {
            $criteria['conditions'] = "WHERE " . $criteria['conditions'];
        }
        $sql = "SELECT  DISTINCT {$searchField} FROM coupon c
        left JOIN coupon_segment cs ON c.id = cs.coupon_id
        left JOIN segment_customer scs ON cs.segment_id = scs.segment_id
        LEFT JOIN category ca ON ca.id = c.category_id
        LEFT JOIN product p ON p.id = c.product_id
        " . $criteria['conditions']. "
        GROUP BY c.id ORDER BY c.created_at ASC LIMIT 5";

        $coupon = new Coupon;
        $a =  new Resultset(null, $coupon, $coupon->getReadConnection()->query($sql));
        return $a->toArray();
    }



}
