<?php

class Punchcard extends BaseModel
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $duration;

    /**
     *
     * @var integer
     */
    public $punch1_2;

    /**
     *
     * @var integer
     */
    public $punch3_5;

    /**
     *
     * @var integer
     */
    public $punch6_8;

    /**
     *
     * @var integer
     */
    public $punch8;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function getSource()
    {
        return "engagement_punchcard";
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'duration' => 'duration',
            'punch1_2' => 'punch1_2',
            'punch3_5' => 'punch3_5',
            'punch6_8' => 'punch6_8',
            'punch8' => 'punch8',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        parent::initialize();
        $this->hasOne("id","Engagement","id");
    }

}
