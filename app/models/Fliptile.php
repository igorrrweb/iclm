<?php

class Fliptile extends Engagement
{
    protected $loggable = true;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $engagement_id;

    /**
     *
     * @var string
     */
    public $image;

    /**
     *
     * @var string
     */
    public $price_before;

    /**
     *
     * @var string
     */
    public $price_after;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    public function getSource()
    {
        return "engagement_fliptile";
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'engagement_id' => 'engagement_id',
            'image' => 'image',
            'price_before' => 'price_before',
            'price_after' => 'price_after',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at'
        ];
    }

    public function initialize()
    {
        parent::initialize();
        $this->belongsTo("engagement_id","Engagement","id");
    }
}
