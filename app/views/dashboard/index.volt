{{ partial("reusables/morrisMacro", []) }}

<div class="row">
    {{ info_panel( "panel-primary", "fa-comments", (feedbacks is defined ? feedbacks : 0), "FeedBack",  baseUrl~"userfeedbacks") }}
    {{ info_panel( "panel-green", "fa-tasks", (currentVisitors is defined ? currentVisitors : 0), "CurrentVisitors", "" ) }}
    {{ info_panel( "panel-yellow", "fa-shopping-cart", (aboutExpireCampaigns is defined ? aboutExpireCampaigns : 0), "AboutExpireCampaigns", baseUrl~"campaigns" ) }}
    {{ info_panel( "panel-red", "fa-support", (churn is defined ? churn : 0), "ChurnAlert", baseUrl~"churnalert" ) }}
</div>
<div class="row">
    <div class="col-md-8">
        {{ bar_chart( "morris-bar-chart", "WeeklyStoreVisitors", visitorsToday, '' ) }}
    </div>
    <div class="col-md-4">
        {{ bar_chart( "morris-bar-chart1", "CouponsRedeemedToday", couponsRedeemedToday, '' ) }}
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        {{ bar_chart("morris-bar-chart-2", "AvailableCoupons", availableCoupons, '' ) }}
    </div>
    <div class="col-md-4">
        {{ bar_chart("morris-bar-chart-3", "AvailableEngagements", availableEngagements, '' ) }}
    </div>
    <div class="col-md-4">
        {{ bar_chart("morris-bar-chart-4", "NewLCMembers", weeklyNewMembers, '' ) }}
    </div>
</div>