{{ partial("fliptile/macroses", []) }}
<form action="" id="userEngagements" method="post" enctype="multipart/form-data" class="crudForm"
      xmlns="http://www.w3.org/1999/html">
    <div id="rootwizard" class="panel panel-default">
        <div class="panel-heading">{{ "CreateANewChallenge" | t }}</div>
        <div class="panel-body">
            <ul class="step-tabs-nav nav nav-tabs" role="tablist">
                {{ step( 'active', 'engagementGeneralDetails', 'engagementGeneralDetails', 'fa-list', 'Step', 1, 'GeneralDetails' ) }}
                {{ step( 'optional', 'engagementAudience', 'engagementAudience', 'fa-users', 'Step', 2, 'Audience' ) }}
                {{ step( 'optional', 'engagementReward', 'engagementReward', 'fa-gift', 'Step', 3, 'Reward') }}
                {{ step( 'optional', 'fliptileData', 'fliptileData', 'fa-check-square-o', 'Step', 4, 'Content' ) }}
                {{ step( 'optional', 'engagementPreview', 'engagementPreview', 'fa-eye', 'Step', 5, 'Preview' ) }}
            </ul>
            <div class="tab-content">
                {{ engagementGeneralDetails( engagement, img, baseUrl, langs, engagementLang  ) }}
                {{ engagementAudience() }}
                {{ engagementReward( coupon, allowShare, couponImg, baseUrl, langs, couponLang, engagement ) }}
                {{ fliptileData( images, fliptiles ) }}
                {{ engagementPreview( img ) }}
                {{ pagerwizard( baseUrl, 'fliptile' ) }}
            </div>
        </div>
    </div>
</form>
{{ addMoreAnswersMockUp() }}
