{{ partial("branch/macroses", []) }}
<form action="" method="post" class="crudForm {% if router.getActionName() is 'view' %} readOnly{% endif %}">
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i> {{ "GeneralInformation" | t }}</div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist">
                {% for label,lang in langs %}
                    <li role="presentation" class="langTab fr {% if loop.index0 is 0 %}active{% endif %}" data-lang="{{ lang }}">
                        <a href="#branch_{{lang}}" aria-controls="branch_{{lang}}" role="tab" data-toggle="tab" >{{ label | t }}</a>
                    </li>
                {% endfor %}
            </ul>
            <div class="tab-content">
                {% for i in langs %}
                    {{ langtabpanel( i, branch, branchLang, cities ) }}
                {% endfor %}
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-sign-in marginLeft"></i> {{ "OpeningHours" | t }}</div>
        <div class="panel-body">
            {% for i in days %}
                <div class="branch-hours">
                    <label>{{ i | t }}</label>

                    <div class="form-inline">
                        <div class="form-group fr">
                            <input type="time"
                                   class="form-control {% if i is 'Saturday' %} normal {% else %} week-day {% endif %}
                                        {% if opening[i] is defined and opening[i]['is_closed'] is not 1 %} required {% endif %}"
                                   name="Opening[{{ i | lower }}][open_hour]"
                                   {% if opening[i] is defined %}value="{{ opening[i]['open_hour'] }}"{% endif %}>


                        </div>
                        <span class="formSpan fr">{{ "To" | t }}</span>

                        <div class="form-group fr">
                            <input type="time"
                                   class="form-control {% if i is 'Saturday' %} normal {% else %} week-day {% endif %}
                                        {% if opening[i] is defined and opening[i]['is_closed'] is not 1 and opening[i]['shabat_grace'] == 0 %} required {% endif %}"
                                   name="Opening[{{ i | lower }}][close_hour]"
                                    {% if i is "Saturday" %}
                                {% if opening[i]['shabat_grace'] > 0 %} value="" {% else %} value="{{ opening[i]['close_hour'] }}" {% endif %}
                            {% else %}
                                {% if opening[i] is defined %} value="{{ opening[i]['close_hour'] }}"{% endif %}
                                    {% endif %}>


                        </div>
                        <div class="checkbox-inline fr">
                            <label class="fl marginRight"><input type="checkbox" class="icheck closed"
                                                                 name="Opening[{{ i | lower }}][is_closed]"
                                                                 {% if opening[i] is defined and opening[i]['is_closed'] is 1 %}checked{% endif %}> {{ 'Closed' | t }}
                            </label>
                        </div>
                        {% if i is "Saturday" %}
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <span class="marginRight marginLeft">{{ "Or" | t }}</span>
                                        <input type="radio" class="icheck radio-check" name="Opening[{{ i | lower}}][shabat_radio]"
                                               value="1"
                                                {% if opening[i]['is_closed'] != 1 %}
                                            {% if opening[i]['shabat_grace'] > 0 %} checked {% endif %}
                                                {% endif %}>
                                        <span class="marginRight marginLeft">{{ "Opens" | t }}:</span>

                                        <div class="form-group" style="width:90px;">

                                            <input type="text" class="form-control shabat-grace form-control {% if opening[i]['shabat_grace'] > 0 %} required {% endif %}"
                                                   style="float: none; width:90px;"
                                                   name="Opening[{{ i | lower }}][shabat_grace]"
                                                   {% if opening[i] is defined
                                                   %}value="{% if opening[i]['shabat_grace'] > 0 %}{{ opening[i]['shabat_grace'] }}{% endif %}" {% endif %}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span>{{ "MinutesAfterShabatExit" | t }}</span>
                                        <span class="formSpan">{{ "To" | t }}</span>

                                        <div class="form-group">
                                            <input type="time" class="form-control shabat-grace {% if opening[i]['shabat_grace'] > 0 %} required {% endif %}"
                                                   name="Opening[{{ i | lower }}][shabat_close_hour]"
                                                    {% if opening[i]['shabat_grace'] < 1 %} value="" {% else %}
                                                {% if opening[i] is defined %}value="{{ opening[i]['close_hour'] }}"{% endif %}
                                                    {% endif %}>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        {% endif %}
                    </div>
                </div>
            {% endfor %}
        </div>
    </div>
    {{ btns( baseUrl, 'branch' ) }}
</form>
