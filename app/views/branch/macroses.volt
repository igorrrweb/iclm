{%- macro langtabpanel( i, branch, branchLang, cities ) %}
<div role="tabpanel" id="branch_{{i}}" class="tab-pane langTabData {% if i is 'he' %} active {% endif %}" {% if router.getActionName() is 'view' %} readOnly {% endif %}>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group"></div>
            <div class="form-group">
                <div class="form-inline select2-wraper">
                    <div class="form-group fr">
                        <label for="Branch[branch_no]">{{ "BranchNumber" | t }}</label>

                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input class="form-control" data-class="branch-no"
                                       value="{%  if branch.branch_no is defined %}{{ branch.branch_no }}{% endif %}"
                                       type="text"
                                       name="Branch[branch_no]">

                                <p class="errorText"></p></div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group fr">
                        <label for="Branch[branch_name]">{{ "BranchName" | t }}*</label>

                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input class="form-control branch_name"
                                       data-required-once="branch_name"
                                       value="{% if branchLang[i] is defined %}{{ branchLang[i]['branch_name'] }}{% endif %}"
                                       type="text"
                                       name="branchLang[{{ i }}][branch_name]">
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group fr">
                        <label for="Branch[city_id]">{{ "City" | t }}</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select class="select2" data-class="branch-city"
                                        name="Branch[city_id]">
                                    {% for city in cities %}
                                        <option {% if branch.city_id == city.id %}selected{% endif %}
                                                value="{{ city.id }}">{{ city.city }}</option>
                                    {% endfor %}
                                </select></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-inline">
                    <div class="form-group fr">
                        <label for="Branch[street_address]">{{ "BranchAddress" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input class="form-control street_address"
                                       data-required-once="street_address"
                                       value="{% if branchLang[i] is defined %}{{ branchLang[i]['street_address'] }}{% endif %}"
                                       type="text"
                                       name="branchLang[{{ i }}][street_address]">
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group fr">
                        <label for="Branch[zip_code]">{{ "Zipcode" | t }}</label>

                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input class="form-control" data-class="branch-zip"
                                       value="{{ branch.zip_code }}"
                                       type="text"
                                       name="Branch[zip_code]">
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group fr">
                        <label for="Branch[branch_phone]">{{ "PhoneNumber" | t }}*</label>

                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input class="form-control"
                                       data-required-once="branch_phone"
                                       data-class="branch-phone"
                                       value="{{ branch.branch_phone }}"
                                       type="text"
                                       name="Branch[branch_phone]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-inline">
                    <div class="form-group fr">
                        <label for="Branch[manager]">{{ "ManagerName" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input class="form-control manager-name"
                                       data-required-once="manager"
                                       value="{% if branchLang[i] is defined %}{{ branchLang[i]['manager'] }}{% endif %}"
                                       type="text"
                                       name="branchLang[{{ i }}][manager]">
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group fr">
                        <label for="Branch[mobile_manager]">{{ "ManagerMobile" | t }}</label>

                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input class="form-control"
                                       data-class="branch-mobile"
                                       value="{{ branch.manager_mobile }}"
                                       type="text"
                                       name="Branch[mobile_manager]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-inline">
                    <div class="form-group fr">
                        <label for="Branch[latitude]">{{ "Latitude" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input class="form-control"
                                       data-required-once="latitude"
                                       data-class="branch-lat"
                                       value="{{ branch.latitude }}" type="text"
                                       name="Branch[latitude]">
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group fr">
                        <label for="Branch[longitude]">{{ "Longitude" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input class="form-control"
                                       data-required-once="longitude"
                                       data-class="branch-lon"
                                       value="{{ branch.longitude }}" type="text"
                                       name="Branch[longitude]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{%- endmacro %}

{%- macro btns( baseUrl, path ) %}
{% if router.getActionName() is "view" %}
    <a href="{{ baseUrl }}{{ path }}" class="btn btn-primary fl backToList" >{{ "BackToList" | t }}</a>
{% else %}
    <input type="submit" value="{{ 'Save' | t }}" class="btn btn-primary fl">
    <a href="{{ baseUrl }}{{ path }}" class="btn btn-primary backToList">{{ "Abort" | t }}</a>
{% endif %}
{%- endmacro %}