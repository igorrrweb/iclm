{%- macro step( step_class, step_href, aria_controls, i_class, step_title, step_number, step_body ) %}
<li role="presentation" class="fr {{ step_class }}">
    <a href="#{{ step_href }}" aria-controls="{{ aria_controls }}" role="tab" data-toggle="tab">
        <i class="fr fa {{ i_class }}"></i> {{ step_title | t }} {{ step_number }}
        <p>{{ step_body | t }}</p>
    </a>
</li>
{%- endmacro %}

{%- macro messageGeneralDetails( langs, messageLang, img, message ) %}
    <div role="tabpanel" id="messageGeneralDetails" class="tab-pane active">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <div class="sectionInfo">
                        <p class="h4">{{ "GeneralDetails" | t }}</p>
                        <p>{{ "MessageGeneralInfoText" | t }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="mainForm">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                {% for label,lang in langs %}
                                    <li role="presentation"
                                        class="langTab fr {% if loop.index0 is 0 %}active{% endif %}"
                                        data-lang="{{ lang }}">
                                        <a href="#{{ lang }}" aria-controls="offered" role="tab"
                                           data-toggle="tab">{{ label | t }}</a></li>
                                {% endfor %}
                            </ul>
                            <div class="tab-content">
                                {% for i in langs %}
                                    <div role="tabpanel" id="{{ i }}"
                                         class="tab-pane langTabData {% if router.getActionName() is 'view' %} readOnly {% endif %} {% if i is 'he' %}active{% endif %}">
                                        <div class="form-group"></div>
                                        <div class="form-inline">
                                            <div class="form-group fr">
                                                <label for="Message[title]">{{ "Title" | t }}{% if i is 'he' %}*{% endif %}</label>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label for="" class="sr-only"></label>
                                                        <input  class="form-control {% if i is 'he' %} required {% endif %}"
                                                                id="messageTitle"
                                                                data-lang="{{ i }}"
                                                                value="{% if messageLang[i] is defined %}{{ messageLang[i]['title'] }}{% endif %}"
                                                                type="text"
                                                                name="MessageTranslation[{{ i }}][title]">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                            <div class="form-group fr">
                                                <label for="Message[content]">{{ "Content" | t }}{% if i is 'he' %}*{% endif %}</label>

                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <textarea rows="1"
                                                                class="form-control {% if i is 'he' %} required {% endif %}"
                                                                id="messageContent"
                                                                name="MessageTranslation[{{ i }}][content]">{% if messageLang[i] is defined %}{{ messageLang[i]['content'] }}{% endif %}</textarea>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {% endfor %}
                            </div>
                        </div>
                    </div>
                    <div class="row {% if router.getActionName() is 'view' %}readOnly{% endif %}">
                        <div class="col-md-12">
                            <div class="form-group"></div>
                            <div class="form-group">
                                <label for="messageImage">{{ "Image" | t }}*</label>
                                <div class="form-group">
                                    <img class="previewImage img-thumbnail"
                                         src="{% if img is not empty %}{{ img }}{% endif %}"
                                         class="">
                                </div>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <input type="file"
                                               class="{% if img is not defined %}required{% endif %} imageUpload"
                                               name="messageImage" id="imageUpload" data-text="{{ "UploadFile" | t }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row {% if router.getActionName() is 'view' %}readOnly{% endif %}">
                        <div class="col-md-12">

                            <div class="form-inline">
                                <div class="form-group fr">
                                    <label for="Message[start_date]">{{ "StartDate" | t }}*</label>

                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <input
                                                    class="form-control minVal datepicker required"
                                                    type="text"
                                                    name="Message[start_date]"
                                                    value="{{ message.start_date }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                <div class="form-group fr">
                                    <label for="Message[end_date]">{{ "EndDate" | t }}*</label>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <input
                                                    class="form-control maxVal datepicker required"
                                                    type="text"
                                                    name="Message[end_date]"
                                                    value="{{ message.end_date }} ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
{%- endmacro %}

{%- macro messageAudience(  ) %}
    <div role="tabpanel" id="messageAudience" class="audience tab-pane ">
        <div class="row">
            <div class="col-md-2">
                <p class="errorText hide" id="segmentError"> - {{ "PleaseChooseASegment" | t }}</p>
                <div class="sectionInfo">
                    <p class="h4">{{ "Audience" | t }}</p>
                    <p>{{ "AudienceInfoText" | t }}</p>
                </div>
            </div>
            <div class="col-md-10">
                <div class="form-group"></div>
                <div class="form-group">
                    <table id="segments" width="100%"
                           class="table table-striped table-bordered table-hover " {% if
                           router.getActionName() is
                           'view' %}readOnly{% endif %}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ "SegmentName" | t }}</th>
                        <th>{{ "Audience" | t }}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
{%- endmacro %}

{%- macro messageTrigger( message ) %}
    <div role="tabpanel" id="messageTrigger" class="tab-pane {% if router.getActionName() is 'view' %}readOnly{% endif %}">
        <div class="row">
            <div class="col-md-2">
                <div class="sectionInfo fr">
                    <p class="h4">{{ "MessageTrigger" | t }}</p>
                    <p>{{ "MessageTriggerText" | t }}</p>
                </div>
            </div>
            <div class="col-md-10">
                <div class="mainForm">
                    <div class="radio">
                        <label><input type="radio" class="icheck" name="Message[trigger]" value="on_branch_entry"
                                      {% if message.trigger is "on_branch_entry"  or message.trigger is  "" %}checked{% endif %}> {{ 'OnBranchEntry' | t }}</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="icheck" name="Message[trigger]" value="on_branch_exit_after_purchase"
                                    {% if message.trigger is "on_branch_exit_after_purchase" %} checked{% endif %}> {{ 'OnBranchExitAfterPurchase' | t }}</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="icheck" name="Message[trigger]" value="when_not_in_branch"
                                    {% if message.trigger is "when_not_in_branch" %} checked{% endif %}> {{ 'WhenNotInBranch' | t }}</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="icheck" name="Message[trigger]" value="on_branch_exit_no_purchase"
                                    {% if message.trigger is "on_branch_exit_no_purchase" %} checked{% endif %}> {{ 'OnBranchExitNoPurchase' | t }}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
{%- endmacro %}

{%- macro messagePreview(  ) %}
    <div role="tabpanel" id="messagePreview" class="tab-pane">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group"></div>
                <div class="form-group">
                    <div class="panel panel-warning preview">
                        <div class="panel-heading">
                            <p class="previewTitle"></p>
                            <i id="messageIcon" class="fa fa-check-circle-o"></i>
                        </div>
                        <div class="panel-body">
                            <p id="fixedMessage">{{ "ThanksForParticipating" | t }}</p>
                            <p class="previewContent"></p>
                        </div>
                        <div class="panel-footer">
                            <i id="closeIcon" class="fa fa-times-circle-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {% if router.getActionName() != "view" %}
                    <input class="btn btn-primary fr" type="submit" value="{{ 'Save' | t }}">
                {% endif %}
            </div>
        </div>

    </div>
{%- endmacro %}

{%- macro pagerwizard( baseUrl, path ) %}
    <ul class="pager wizard">
        <li class="previous fr"><a href="#">{{ "Previous" | t }}</a></li>
        <li class="next fl"><a href="#">{{ "Next" | t }}</a></li>
        <li>
            <p>
                <a href="{{ baseUrl }}{{ path }}" class="btn btn-primary backToList">{{ "Abort" | t }}</a>
            </p>
        </li>
    </ul>
{%- endmacro %}
