{{ partial("message/macroses", []) }}
<form action="" method="post" enctype="multipart/form-data" class="crudForm readOnly">
    <div id="rootwizard" class="panel panel-default">
        <div class="panel-heading">{{ "CreateANewMessage" | t }}</div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist">
                {{ step( 'active', 'messageGeneralDetails', 'messageGeneralDetails', 'fa-list', 'Step', 1, 'GeneralDetails' ) }}
                {{ step( 'optional', 'messageAudience', 'messageAudience', 'fa-users', 'Step', 2, 'Audience' ) }}
                {{ step( 'optional', 'messageTrigger', 'messageTrigger', 'fa-wrench', 'Step', 3, 'Trigger' ) }}
                {{ step( 'optional', 'messagePreview', 'messagePreview', 'fa-eye', 'Step', 4, 'Preview' ) }}
            </ul>
            <div class="tab-content">
                {{ messageGeneralDetails( langs, messageLang, img, message ) }}
                {{ messageAudience() }}
                {{ messageTrigger( message ) }}
                {{ messagePreview(  ) }}
                {{ pagerwizard( baseUrl, 'message' ) }}
            </div>
        </div>
    </div>
</form>

