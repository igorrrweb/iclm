{# paginator #}
<div class="segmentList">
    <div class="refreshTable fr"></div>
    <table id="segments" class="table table-bordered clr "{% if router.getActionName() is 'view' %}readOnly{% endif %}">
        <thead>
        <tr>
            <th></th>
            <th>{{ "SegmentName" | t }}</th>
            <th>{{ "Audience" | t }}</th>
        </tr>
        </thead>
        <tbody>
        {% for segment in paginator.items %}
            <tr>
                <td>
                    <input type="checkbox" class="icheck" name="Segments[]" value="{{ segment.id }}"
                            {% if activeSegments is not null %}
                                {% for activeSegment in activeSegments %}
                                    {% if segment.id is activeSegment.segment_id %}checked{% endif %}
                                {% endfor %}
                            {% endif %}>
                </td>
                <td>{{ segment.name }}</td>
                <td>{{ segment.getMembers() }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

</div>
<p class="hide jsTranslation">{{ "NoMatches" | t }}</p>