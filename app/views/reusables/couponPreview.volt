<div id="couponPreview" class="preview">
    <div class="previewHeader">
        <span class="fl close"><i class="fa fa-close"></i></span>
        <img src="{{baseUrl}}img/MHLogo.png">
    </div>
    <div class="previewBody">
        <p id="previewTitle" class="h1">{{ coupon.title }}</p>
        <p id="previewSubTitle" class="h3">{{ coupon.sub_title }}</p>
        <figure>
            <img id="couponPreviewImage" {%  if img is defined %}src="{% if img is not empty %}{{baseUrl}}{{ img }}{% endif %}"{% endif %}>
        </figure>
        <p id="previewDetails" class="h3">{{ coupon.details }}</p>
        <p id="previewDisclaimer" class="h4">{{ coupon.disclaimer }}</p>
    </div>
    <div class="clr"></div>
    <div class="previewFooter">
        <img src="{{baseUrl}}img/couponPreviewFooter.png">
    </div>
</div>