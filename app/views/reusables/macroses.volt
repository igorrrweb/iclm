{%- macro datatable( attributes ) %}
    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
        <thead>
        <tr>
            {% for attribute in attributes %}
                <th>{{ attribute | t}}</th>
            {% endfor %}

        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
{%- endmacro %}

