{%- macro info_panel(panelClass,faClass,i,title,uri) %}
    <div class="col-lg-3 col-md-6">
        <div class="panel {{panelClass}}">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3 hbr">
                        <i class="fa {{faClass}} fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{i}}</div>
                        <div>{{title | t}}</div>
                    </div>
                </div>
            </div>
            <a href="{{uri}}">
                <div class="panel-footer">
                    <span class="pull-left">{{ "ViewDetails" | t }}</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
{%- endmacro %}

{%- macro bar_chart( MorrisBarChartId, BarChartTitle, dataValues, dataTitles ) %}
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ BarChartTitle | t }}
        </div>
        <div class="panel-body">
            <div class="barChart" id="{{MorrisBarChartId}}" data-values='{{ dataValues }}' data-title='{{ dataTitles }}'></div>
        </div>
    </div>
{%- endmacro %}