{{ partial("feedback/macroses", []) }}
<form action="" method="post" class="crudForm {% if router.getActionName() is 'view' %} readOnly{% endif %}">
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-edit marginLeft"></i> {{ "CreateNewFeedbackForm" | t }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="Feedback[title]">{{ "Title" | t }}*</label>
                                <input id="feedbackTitle" class="form-control required" value="{{ feedback.title }}"
                                       type="text"
                                       name="Feedback[title]">
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group">
                                <label for="Feedback[sub_title]">{{ "SubTitle" | t }}*</label>
                                <input id="feedbackSubTitle" class="form-control required"
                                       value="{{ feedback.sub_title }}"
                                       type="text"
                                       name="Feedback[sub_title]">
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group">
                                <label><input type="radio" class="icheck" name="Feedback[active]" value="1" {% if
                                    (feedback.active== "1")
                                    or feedback.active is not defined %} checked{% endif %}> {{ 'Yes' | t }}</label>
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group">
                                <label><input type="radio" class="icheck" name="Feedback[active]" value="0" {% if
                                    (feedback.active== "0") %} checked{% endif %}> {{ 'No' | t }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ "Trigger" | t }}</label>
                        <div class="radio">
                            <label><input type="radio" class="icheck" name="Feedback[trigger]" value="on_branch_enter"
                                        {% if (feedback.trigger== "on_branch_enter") or feedback.trigger == "" %}
                                checked{% endif %}> {{ 'OnBranchEntry' | t }}</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" class="icheck" name="Feedback[trigger]" value="not_in_branch" {%
                                if (feedback.trigger== "not_in_branch") %} checked{% endif %}> {{
                                'WhenNotInBranch' | t }}</label>
                        </div>


                        <div class="radio">
                            <label><input type="radio" class="icheck" name="Feedback[trigger]"
                                          value="on_branch_leave"
                                        {% if (feedback.trigger== "on_branch_leave") %} checked{%
                                endif %}> {{ 'OnBranchExitAfterPurchase' | t }}</label>
                        </div>


                        <div class="radio">
                            <label><input type="radio" class="icheck" name="Feedback[trigger]"
                                          value="on_branch_leave_empty"
                                        {% if (feedback.trigger== "on_branch_leave_empty") %}
                                checked{% endif %}> {{ 'OnBranchExitNoPurchase' | t }}</label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-question marginLeft"></i> {{ "Questions" | t }}</div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist">
                {% for label,lang in langs %}
                    <li role="presentation"  class="langTab fr {% if loop.index0 is 0 %}active{% endif %}" data-lang="{{ lang }}">
                        <a href="#{{lang}}" aria-controls="offered" role="tab" data-toggle="tab">{{ label | t }}</a></li>
                {% endfor %}
            </ul>

            <div class="tab-content">
                {% for a in langs %}
                    {{ questions( a,questions,ratings ) }}
                {% endfor %}
            </div>
        </div>
    </div>
    <div class="audience panel panel-default">
        <div class="panel-heading"><i class="fa fa-users marginLeft"></i> {{ "Audience" | t }}</div>
        <div class="panel-body">
            <table id="segments"  width="100%" class="table table-striped table-bordered table-hover " {% if router.getActionName() is 'view' %}readOnly{% endif %}">
            <thead>
            <tr>
                <th></th>
                <th>{{ "SegmentName" | t }}</th>
                <th>{{ "Audience" | t }}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
            <p class="hide jsTranslation">{{ "NoMatches" | t }}</p>
            <div class="clearfix"></div>
            <p class="hide error" id="segmentError"> - {{ "PleaseChooseASegment" | t }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {% if router.getActionName() is "view" %}
                    <a href="{{ baseUrl }}feedback" class="btn btn-primary fl backToList" >{{ "BackToList" | t }}</a>
                {% else %}
                    <input type="submit" value="{{ 'Save' | t }}" class="btn btn-primary fl">
                    <a href="{{ baseUrl }}feedback" class="btn btn-primary backToList">{{ "Abort" | t }}</a>
                {% endif %}
                <button type="button" class="btn btn-primary feedback-preview" data-toggle="modal" data-target="#feedbackPreview">{{ "Preview" | t }}</button>
                {#<a id="previewBtn" class="btn btn-primary fancy fl" href="#feedbackPreview">{{ "Preview" | t }}</a>#}
            </div>
        </div>
    </div>

</form>
<!-- Modal -->
<div class="modal fade" id="feedbackPreview" tabindex="-1" role="dialog" aria-labelledby="feedbackPreviewTitle">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="feedbackPreviewTitle"></h4>
            </div>
            <div class="modal-body">
                <p class="h4" id="feedbackPreviewSubTitle"></p>
                <ul id="feedbackPreviewQuestions"></ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <span class="hide" id="jsTranslate">["{{ "FreeText" | t}}","{{ "Yes" | t }}","{{ "No" | t }}"]</span>
            </div>
        </div>
    </div>
</div>

<div id="questionPrototype" class="hidden">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-inline">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for=""><span class="icon fr"></span></label>
                                <input type="text"
                                       class="form-control questionTitle"
                                       name="FeedbackQuestion[][title]">

                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group">
                        <label>{{ "QuestionType" | t }}</label>
                        <select class="questionType"
                                name="FeedbackQuestion[][type]">
                            <option selected value="free_text">{{ "FreeText" | t }}</option>
                            <option value="yes_no">{{ "YesNo" | t }}</option>
                            <option value="rating">{{ "Rating" | t }}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group rating hidden">
                <div class="form-inline">
                    <div class="form-group">
                        <ul>
                            <label>{{ "RatingValues" | t }}</label>
                            {% for i in 1..5 %}
                                <li class="feedbackRating fr">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <input type="text" class="form-control"
                                               name="FeedbackQuestion[][rating][value{{ i }}]"
                                               placeholder="{{ i }} {{ 'Stars' | t }}">
                                    </div>
                                </li>
                            {% endfor %}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="FeedbackQuestion[][lang]" value="">
    </div>

</div>