{%- macro questions( a,questions,ratings ) %}

    <div role="tabpanel" id="{{a}}" class="tab-pane langTabData{% if a is 'he' %} active{% endif %}">
        <div class="form-group"></div>

        {{ questions_row( a,questions,ratings ) }}

        <button type="button" class="btn btn-link addMoreQuestions">{{ "AddMoreQuestions" | t }}</button>
    </div>
{%- endmacro %}

{%- macro questions_row( a, questions, ratings ) %}
    {% set counter = 0 %}
    <?php if (isset($questions[$a])) : ?>
    <div class="form-group feedbackQuestions" data-lang="{{ a }}">
        {% for key,question in questions[a] %}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for=""><span class="icon fr">{{ loop.index }}</span></label>
                                <input type="text" value="{{ question['title'] }}"
                                       name="FeedbackQuestion[{{ a }}][{{ counter }}][title]"
                                       class="form-control questionTitle">
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group">
                                <label>{{ "QuestionType" | t }}</label>
                                <select class="select2 questionType"
                                        name="FeedbackQuestion[{{ a }}][{{ counter }}][type]">
                                    <option value="free_text" {% if question['type'] is "free_text" %}selected{% endif %}>{{ "FreeText" | t
                                        }}</option>
                                    <option value="yes_no" {% if question['type'] is "yes_no" %}selected{% endif %}>{{ "YesNo" | t
                                        }}</option>
                                    <option value="rating" {% if question['type'] is "rating" %}selected{% endif %}>{{ "Rating" | t
                                        }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group rating {% if question['type'] != 'rating' %}hidden{% endif %}">
                        <div class="form-inline">
                            <div class="form-group">
                                <ul>
                                    <label>{{ "RatingValues" | t }}</label>
                                    {% for i in 1..5 %}
                                        <li class="feedbackRating fr">
                                            <div class="form-group">
                                                <label for="" class="sr-only"></label>
                                                <input type="text" class="form-control" {% if ratings[question['id']]
                                                is defined %}
                                                    value="{{ ratings[question['id']]["value"~i] }}"{% endif %}
                                                       name="FeedbackQuestion[{{ a }}][{{ counter }}][rating][value{{ i }}]"
                                                       placeholder="{{ i }} {{ "Stars" | t }}"></div>
                                        </li>
                                    {% endfor %}
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="FeedbackQuestion[{{ a }}][{{ counter}}][lang]" value="{{ a }}">
            <input type="hidden" name="FeedbackQuestion[{{ a }}][{{ counter }}][id]" value="{{ question['id'] }}">

            {% set counter += 1 %}
        {% endfor %}
    </div>
    <?php else : ?>
    {% for num in 1..2 %}
        <div class="form-group feedbackQuestions" data-lang="{{ a }}">
            {{ clear_row( a, num, counter ) }}
        </div>
        {% set counter += 1  %}
    {% endfor %}
    <?php endif ?>
{%- endmacro %}

{%- macro clear_row( a, num,counter ) %}
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-inline">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for=""><span class="icon fr">{{ num }}</span></label>
                                <input type="text"
                                       class="form-control questionTitle"
                                       name="FeedbackQuestion[{{ a }}][{{ counter }}][title]">

                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group">
                        <label>{{ "QuestionType" | t }}</label>
                        <select class="select2 questionType"
                                name="FeedbackQuestion[{{ a }}][{{counter}}][type]">
                            <option value="free_text">{{ "FreeText" | t }}</option>
                            <option value="yes_no">{{ "YesNo" | t }}</option>
                            <option value="rating">{{ "Rating" | t }}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group rating hidden">
                <div class="form-inline">
                    <div class="form-group">
                        <ul>
                            <label>{{ "RatingValues" | t }}</label>
                            {% for i in 1..5 %}
                                <li class="feedbackRating fr">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <input type="text" class="form-control"
                                               name="FeedbackQuestion[{{ a }}][{{ counter }}][rating][value{{ i }}]"
                                               placeholder="{{ i }} {{ 'Stars' | t }}">
                                    </div>
                                </li>
                            {% endfor %}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="FeedbackQuestion[{{ a }}][{{ counter }}][lang]" value="{{ a }}">
    <input type="hidden" name="FeedbackQuestion[{{ a }}][{{ counter }}][id]" value="">
{%- endmacro %}