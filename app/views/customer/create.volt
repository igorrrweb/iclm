{{ partial("customer/macroses", []) }}
<form role="form" action="" method="post" class="crudForm {% if router.getActionName() is "view" %} readOnly{% endif %}" >
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i> {{ "PersonalInfo" | t }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="Customer[firstname]">{{ "FirstName" | t }}*</label>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    {{ text_field("Customer[firstname]",
                                    "class":"form-control required",
                                    "value": customer.firstname
                                    ) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[lastname]">{{ "LastName" | t }}*</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    {{ text_field("Customer[lastname]",
                                    "class":"form-control required",
                                    "value": customer.lastname
                                    ) }}
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group fr">
                            <label for="Customer[id]">{{ "IDNumber" | t }}*</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    {{ text_field("Customer[id]",
                                    "class":"form-control required",
                                    "value": customer.id
                                    ) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group">
                            <label for="Customer[birth_date]">{{ "Birthday" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    {{ text_field("Customer[birth_date]",
                                    "class":"form-control datepicker birth-date",
                                    "value": customer.birth_date
                                    ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i> {{ "GeneralInfo" | t }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>{{ "Gender" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label class="radio-inline">
                                        {{ radio_field("Customer[gender]",
                                        "class":"icheck",
                                        "value":"male",
                                        (((customer.gender == "male") or
                                        (customer.gender is not defined)) ? 'checked' : '')
                                        ) }} {{ 'Male' | t }}</label>


                                    <label class="radio-inline">
                                        {{ radio_field("Customer[gender]",
                                        "class":"icheck",
                                        "value":"female",
                                        ((customer.gender== "female") ? 'checked' : '')
                                        ) }} {{ 'Female' | t }}</label></div>
                            </div>


                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group">
                            <label>{{ "Married" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label class="radio-inline">
                                        {{ radio_field("Customer[marital_status]",
                                        "class":"icheck",
                                        "value":"married",
                                        ((customer.gender== "married") ? 'checked' : '')
                                        ) }} {{ 'Yes' | t }}</label>


                                    <label class="radio-inline">
                                        {{ radio_field("Customer[marital_status]",
                                        "class":"icheck",
                                        "value":"single",
                                        ( ((customer.gender== "single") or (customer.marital_status is not defined)) ? 'checked' : '')
                                        ) }} {{ 'No' | t }}</label></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="Customer[wedding_anniversary_date]">{{ "WeddingDay" | t }}</label>

                    <div class="form-inline">
                        <div class="form-group">
                            <label for="" class="sr-only"></label>
                            {{ text_field("Customer[wedding_anniversary_date]",
                            "class":"datepicker form-control wedding",
                            "value": customer.wedding_anniversary_date
                            ) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i> {{ "ContactInfo" | t }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group fr">
                            <label for="Customer[phone]">{{ "HomeNumber" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    {{ text_field("Customer[phone]",
                                    "class":"form-control",
                                    "value": customer.phone
                                    ) }}
                                </div>
                            </div>

                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[mobile]">{{ "Mobile" | t }}*</label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    {{ text_field("Customer[mobile]",
                                    "class":"form-control required",
                                    "value": customer.mobile
                                    ) }}
                                </div>
                            </div>

                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[email]">{{ "EmailAddress" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    {{ text_field("Customer[email]",
                                    "class":"form-control",
                                    "value": customer.email
                                    ) }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group fr">
                            <label for="Customer[address]">{{ "Address" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    {{ text_field("Customer[address]",
                                    "class":"form-control",
                                    "value": customer.address
                                    ) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[zip_code]">{{ "Zipcode" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    {{ text_field("Customer[zip_code]",
                                    "class":"form-control",
                                    "value": customer.zip_code
                                    ) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[city_id]">{{ "City" | t }}</label>
                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    <select class="select2" name="Customer[city_id]">
                                        <option value="0">-------</option>
                                        {% for city in cities %}
                                        <option {% if customer.city_id== city.id %}selected{% endif %}
                                                value="{{ city.id }}">{{
                                            city.city }}
                                        </option>
                                        {% endfor %}
                                    </select></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i>{{ "LCRelatedInfo" | t }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group fr">
                            <label for="Customer[lc_number]">{{ "LCNumber" | t }}*</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    {{ text_field("Customer[lc_number]",
                                    "class":"form-control required",
                                    "value": customer.lc_number
                                    ) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[last_digits_cc]">{{ "CCLastDigits" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    {{ text_field("Customer[last_digits_cc]",
                                    "class":"form-control",
                                    "value": customer.last_digits_cc
                                    ) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[join_date]">{{ "JoinDate" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    {{ text_field("Customer[join_date]",
                                    "class":"form-control datepicker join-date",
                                    "value": customer.join_date
                                    ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
    {{ buttons( baseUrl ) }}
</form>

