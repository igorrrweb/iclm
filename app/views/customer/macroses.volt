{%- macro buttons( baseUrl ) %}
    <div class="form-group">
    {% if router.getActionName() is "view" %}
        <a href="{{ baseUrl }}customer" class="btn btn-primary backToList fl">{{ "BackToList" | t }}</a>
    {% else %}
        <input type="submit" value="{{ 'Save' | t }}" class="btn btn-primary fl">
        <a href="{{ baseUrl }}customer" class="btn btn-primary backToList">{{ "Abort" | t }}</a>
    {% endif %}
    </div>
{%- endmacro %}