{%- macro step( step_class, step_href, aria_controls, i_class, step_title, step_number, step_body ) %}
<li role="presentation" class="fr {{ step_class }}">
    <a href="#{{ step_href }}" aria-controls="{{ aria_controls }}" role="tab" data-toggle="tab">
        <i class="fr fa {{ i_class }}"></i> {{ step_title | t }} {{ step_number }}
        <p>{{ step_body | t }}</p>
    </a>
</li>
{%- endmacro %}

{%- macro modal( id, labelledby, baseUrl, img_id, previewTitle, previewSubTitle, couponPreviewImage_id, img, previewDetails, previewDisclaimer ) %}
<div id="{{ id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="{{ labelledby }}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img id="{{ img_id }}" src="{{baseUrl}}img/MHLogo.png">
            </div>
            <div class="modal-body">

                <h1 id="previewTitle">{{ previewTitle }}</h1>
                <h3 id="previewSubTitle">{{ previewSubTitle }}</h3>
                <figure>
                    <img class="img img-responsive img-thumbnail" id="{{ couponPreviewImage_id }}"
                         {%  if img is defined %}src="{% if img is not empty %}{{ img }}{% endif %}"{% endif %}>
                </figure>
                <h3 id="previewDetails">{{ previewDetails }}</h3>
                <h4 id="previewDisclaimer">{{ previewDisclaimer }}</h4>

            </div>
            <div class="modal-footer previewFooter">
                <img src="{{baseUrl}}img/couponPreviewFooter.png">
            </div>
        </div>
    </div>
</div>
{%- endmacro %}

{%- macro table( table_id, table_heads ) %}
<table id="{{ table_id }}" width="100%" class="table table-striped table-bordered table-hover "
{% if router.getActionName() is 'view' %}readOnly{% endif %}">
    <thead>
    <tr>
        <th></th>
        {%  for th in table_heads  %}
        <th>{{ th | t }}</th>
        {% endfor %}
    </tr>
    </thead>
    <tbody></tbody>
</table>
{%- endmacro %}

{%- macro engagementGeneralDetails( engagement, img, baseUrl, langs, engagementLang  ) %}
<div id="engagementGeneralDetails" role="tabpanel" class="tab-pane active">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2">
                <h4>{{ "GeneralDetails" | t }}</h4>
                <p>{{ "EngagementGeneralInfoText" | t }}</p>
            </div>
            <div class="col-md-10">
                <div class="form-group"></div>
                <div class="form-group {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                    <label for="Engagement[name]">{{ "Name" | t }}*</label>
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="" class="sr-only"></label>
                            <input class="form-control required"
                                   id="engagementName"
                                   value="{{ engagement.name }}" type="text"
                                   name="Engagement[name]" data-column="name">
                        </div>
                    </div>
                </div>
                <div class="form-group {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                    <label for="engagementImage">{{ "Image" | t }}*</label>
                    <div class="form-inline">
                        <div class="form-group">
                            <img class="previewImage thumbnail" {% if img is not empty %} src="{{ img }}" {% endif %} >
                            <input class="{% if img == '' %}required{% endif %} input-lg imageUpload"
                                   type="file" id="engagementImageUpload"
                                   name="engagementImage" data-text="{{ " UploadFile" | t }}">
                        </div>
                    </div>
                </div>
                <div class="form-group {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                    <div class="inputGroup fr">
                        <label for="Engagement[start_date]">{{ "StartDate" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input
                                        class="form-control minVal datepicker required" type="text"
                                        name="Engagement[start_date]"
                                        value="{{ engagement.start_date }}">
                            </div>
                        </div>
                    </div>
                    <div class="inputGroup fr">
                        <label for="Engagement[end_date]">{{ "EndDate" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input
                                        class="form-control maxVal datepicker required" type="text"
                                        name="Engagement[end_date]" value="{{ engagement.end_date }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                    <ul class="nav nav-tabs" role="tablist">
                        {% for label,lang in langs %}
                            <li role="presentation"
                                class="langTab fr {% if loop.index0 is 0 %}active{% endif %}"
                                data-lang="{{ lang }}">
                                <a href="#engagementGeneralDetails_{{lang}}" aria-controls="engagementGeneralDetails_{{lang}}" role="tab" data-toggle="tab">{{
                                    label | t }}</a></li>
                        {% endfor %}
                    </ul>
                    <div class="tab-content">
                        {% for i in langs %}
                            <div role="tabpanel" id="engagementGeneralDetails_{{i}}" class="tab-pane langTabData clr {% if router.getActionName() is 'view' %} readOnly {% endif %} {% if i is 'he' %}active{% endif %}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group"></div>
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <div class="form-group fr">
                                                        <label for="Engagement[welcome_msg]">{{ "WelcomeMessage" | t }}{% if i is "he" %}*{% endif %}</label>
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <label for="" class="sr-only"></label>
                                                                <textarea
                                                                        rows="1"
                                                                        class="form-control {% if i is 'he' %}required{% endif %}"
                                                                        name="EngagementLang[{{ i }}][welcome_msg]">{% if engagementLang[i] is defined %}{{ engagementLang[i]['welcome_msg'] }}{% endif %}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group hidden-xs">
                                                        <div class="col-md-1"></div>
                                                    </div>
                                                    <div class="form-group fr">
                                                        <label for="Engagement[thankyou_msg]">{{ "ThankYouMessage" | t }}{% if i is "he" %}*{% endif %}</label>
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <label for="" class="sr-only"></label>
                                                                <textarea
                                                                        rows="1"
                                                                        class="form-control {% if i is 'he' %}required{% endif %}"
                                                                        name="EngagementLang[{{ i }}][thankyou_msg]">{% if engagementLang[i] is defined %}{{ engagementLang[i]['thankyou_msg'] }}{% endif %}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {% endfor %}
                    </div>
                </div>
                <div class="form-group {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                    <label>{{ "Trigger" | t }}</label>
                    <div class="radio">
                        <label><input type="radio" class="icheck" name="Engagement[trigger]"
                                      value="on_branch_enter"
                                    {% if (engagement.trigger== "on_branch_enter") or
                                    engagement.trigger == "" %} checked{% endif %}> {{ 'OnBranchEntry' | t }}</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="icheck"
                                      name="Engagement[trigger]"
                                      value="not_in_branch"
                                    {% if (engagement.trigger== "not_in_branch") %}
                            checked{% endif
                            %}> {{ 'WhenNotInBranch' | t }}</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="icheck"
                                      name="Engagement[trigger]"
                                      value="on_branch_leave"
                                    {% if (engagement.trigger== "on_branch_leave") %}
                            checked{% endif
                            %}> {{ 'OnBranchExitAfterPurchase' | t }}</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="icheck"
                                      name="Engagement[trigger]"
                                      value="on_branch_leave_empty"
                                    {% if (engagement.trigger==
                                    "on_branch_leave_empty") %} checked{%
                            endif %}> {{ 'OnBranchExitNoPurchase' | t }}</label>
                    </div>
                    <div class="checkbox fr {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                        <label><input type="checkbox" class="icheck" name="Engagement[never_expires]" value="1"
                                    {% if (engagement.never_expires is "1") %} checked{% endif %}> {{ 'NeverExpires' | t }}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{%- endmacro %}

{%- macro engagementAudience() %}
<div id="engagementAudience" class="audience tab-pane {% if router.getActionName() is 'view' %} readOnly {% endif %}">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group"></div>
            <div class="col-md-2">
                <div class="form-group">
                    <p class="errorText hide" id="segmentError"> - {{ "PleaseChooseASegment" | t }}</p>
                    <div class="sectionInfo">
                        <h4>{{ "Audience" | t }}</h4>
                        <p>{{ "AudienceInfoText" | t }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="form-group">
                    {{ table( 'segments', ['SegmentName','Audience'] )  }}
                </div>
            </div>
        </div>
    </div>
</div>
{%- endmacro %}


{%- macro engagementReward( coupon, allowShare, couponImg, baseUrl, langs, couponLang, engagement) %}
<div id="engagementReward" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group"></div>
            <div class="col-md-2">
                <div class="sectionInfo">
                    <p class="h4">{{ "ChallengeReward" | t }}</p>
                    <p>{{ "EngagementRewardText" | t }}</p>
                </div>
            </div>
            <div class="col-md-10">
                <div class="mainForm">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="errorText hide" id="couponError"> - {{
                                "IncompleteCouponText" | t }}</p>

                            <div id="rewardOnCompletion"
                                 class="checkbox {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                                <label><input type="checkbox" class="icheck" name="Engagement[reward]" id="EngagementReward"
                                              value="1" {% if engagement.reward == '1' %} checked="checked" {% endif %} > {{ 'RewardOnCompletion' | t }}</label>
                            </div>
                        </div>
                    </div>
                    <div id="engagementCoupon">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-inline">
                                        <div class="form-group fr {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                                            <label>{{ "Name" | t }}*</label>
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label for="" class="sr-only"></label>
                                                    <input
                                                            class="couponName form-control tabRequired"
                                                            {% if coupon.name is defined %}
                                                                value="{{ coupon.name }}" {% endif %}
                                                            type="text" name="Coupon[name]"
                                                            data-column="name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                        <div class="form-group fr {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                                            <label for="couponID">{{ "CouponNumber" | t }}*</label>

                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label for="" class="sr-only"></label>
                                                    <input id="couponID"
                                                           class="form-control tabRequired"
                                                           value="{{ coupon.pos_coupon_id }}"
                                                           type="text"
                                                           name="Coupon[pos_coupon_id]">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                        <div class="form-group fr {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                                            <label for="couponTotalUnits">{{ "TotalUnits" | t
                                                }}*</label>

                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label for="" class="sr-only"></label>
                                                    <input id="couponTotalUnits"
                                                           class="form-control tabRequired"
                                                           value="{{ coupon.max_coupons }}"
                                                           type="text"
                                                           name="Coupon[max_coupons]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="checkbox-inline fr {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                                        <label><input type="checkbox" value="1" class="icheck"
                                                      name="Coupon[active]" {% if
                                            coupon.active== "1" %} checked{% endif %}> {{
                                            'Enabled' | t }}</label>
                                    </div>
                                    {% if allowShare %}
                                        <div class="checkbox-inline fr {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                                            <label><input type="checkbox" value="1" class="icheck"
                                                          name="Coupon[promoted]" {% if
                                                coupon.promoted== "1" %} checked{% endif %}> {{
                                                'Promoted' | t }}</label>
                                        </div>
                                    {% endif %}
                                    <div class="checkbox-inline fr {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                                        <label><input type="checkbox" value="1" class="icheck"
                                                      name="Coupon[shared]" {% if
                                            coupon.shared== "1" %} checked{% endif %}> {{
                                            'Shareable' | t }}</label>
                                    </div>
                                    <div class="checkbox-inline fr {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                                        <label><input type="checkbox" value="1" class="icheck"
                                                      name="Coupon[public]" {% if
                                            coupon.public== "1" %} checked{% endif %}> {{
                                            'Public'
                                            | t }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {% if router.getActionName() is 'view' %} readOnly {% endif %}">
                                    <label for="couponImage">{{ "Image" | t }}*</label>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <img class="previewImage thumbnail"
                                                 src="{% if couponImg is not empty %}{{ couponImg }}{% endif %}">
                                            <input id="couponImageUpload" class="imageUpload"
                                                   type="file"
                                                   name="couponImage"
                                                   data-text="{{ " ChooseFile" | t }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <ul class="nav nav-tabs" role="tablist">
                                {% for label,lang in langs %}
                                    <li class="langTab fr {% if loop.index0 is 0 %}active{% endif %}" data-lang="{{ lang }}">
                                        <a href="#engagementReward_{{lang}}" aria-controls="engagementReward_{{lang}}" role="tab" data-toggle="tab">{{ label | t }}</a>
                                    </li>
                                {% endfor %}
                            </ul>
                            <div class="tab-content">
                                {% for i in langs %}
                                    <div role="tabpanel" id="engagementReward_{{i}}"  class="tab-pane langTabData {% if router.getActionName() is 'view' %} readOnly {% endif %} {% if i is 'he' %} active {% endif %}">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group"></div>
                                                    <div class="form-group">
                                                        <div class="form-inline">
                                                            <div class="form-group fr">
                                                                <label>{{ "Title" | t }}{% if i is 'he' %}*{%
                                                                    endif
                                                                    %}</label>

                                                                <div class="form-inline">
                                                                    <div class="form-group">
                                                                        <label for="" class="sr-only"></label>
                                                                        <input
                                                                                class="form-control {% if i is 'he' %} tabRequired{% endif %} couponTitle"
                                                                                {% if couponLang[i]['title'] is
                                                                                defined
                                                                                %}
                                                                                    value="{{
                                                                                    couponLang[i]['title'] }}" {% endif %}
                                                                                type="text"
                                                                                name="CouponLang[{{i}}][title]">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group hidden-xs">
                                                                <div class="col-md-1"></div>
                                                            </div>
                                                            <div class="form-group fr">
                                                                <label>{{ "SubTitle" | t }}</label>

                                                                <div class="form-inline">
                                                                    <div class="form-group">
                                                                        <label for="" class="sr-only"></label>
                                                                        <input
                                                                                class="form-control couponSubTitle"
                                                                                {% if couponLang[i]['sub_title']
                                                                                is
                                                                                defined
                                                                                %} value="{{
                                                                                couponLang[i]['sub_title'] }}" {% endif
                                                                                %}
                                                                                type="text"
                                                                                name="CouponLang[{{i}}][sub_title]">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="form-inline">
                                                            <div class="form-group fr ">
                                                                <label>{{ "Details" | t }}</label>

                                                                <div class="form-inline">
                                                                    <div class="form-group">
                                                                        <label for="" class="sr-only"></label>
                                                                                <textarea
                                                                                        rows="1"
                                                                                        class="form-control couponDetails"
                                                                                        name="CouponLang[{{i}}][details]">{% if couponLang[i]['details'] is defined %}{{ couponLang[i]['details'] }}{% endif %}</textarea>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group hidden-xs">
                                                                <div class="col-md-1"></div>
                                                            </div>
                                                            <div class="form-group fr">
                                                                <label>{{ "Disclaimer" | t }}</label>
                                                                <div class="form-inline">
                                                                    <div class="form-group">
                                                                        <label for="" class="sr-only"></label>
                                                                                <textarea
                                                                                        rows="1"
                                                                                        class="form-control couponDisclaimer"
                                                                                        name="CouponLang[{{i}}][disclaimer]">{% if couponLang[i]['disclaimer'] is defined %}{{ couponLang[i]['disclaimer'] }}{% endif %}</textarea>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {% endfor %}
                            </div>
                            <button id="couponPreviewButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#couponPreview">{{ "Preview" | t }}</button>
                            {{ modal( 'couponPreview', 'couponPreviewLabel', baseUrl,
                            'couponPreviewLabel', (coupon.title is defined ? coupon.title : ''),
                            (coupon.sub_title is defined ? coupon.sub_title : '' ),
                            'couponPreviewImage', (img is defined ? img : ''),
                            (coupon.details is defined ? coupon.details : ''),
                            (coupon.disclaimer ? coupon.disclaimer : '') ) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{%- endmacro %}


{%- macro punchcardData( punchcard ) %}
    <div id="punchcardData" class="tab-pane">
        <p class="h3">{{ "PunchcardCriteria" | t }}</p>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="Punchcard[duration]">{{ "EngagementDuration" | t }}</label>

                    <div class="form-inline">
                        <div class="form-group">
                            <input type="text" id="punchcardDuration"
                                   class="form-control required"
                                   name="Punchcard[duration]"
                                   value="{{ punchcard.duration }}">
                            <span>{{ "Days" | t }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>{{ "IncreaseVisitsBy" | t }}</label>
                    <div class="form-group">
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label>{{ "AverageOf" | t }}
                                        1-2 {{ "Visits" | t }} {{ "IncreaseBy" | t }}</label>
                                    <input type="text" class="form-control required"
                                           name="Punchcard[punch1_2]"
                                           value="{{ punchcard.punch1_2 }}">
                                    <span>{{ "Visits" | t }}</span>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label>{{ "AverageOf" | t }}
                                        3-5 {{ "Visits" | t }} {{ "IncreaseBy" | t }}</label>
                                    <input type="text"
                                           class="form-control required"
                                           name="Punchcard[punch3_5]"
                                           value="{{ punchcard.punch3_5 }}">
                                    <span>{{ "Visits" | t }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label>{{ "AverageOf" | t }}
                                        6-8 {{ "Visits" | t }} {{ "IncreaseBy" | t }}</label>
                                    <input type="text" class="form-control required"
                                           name="Punchcard[punch6_8]"
                                           value="{{ punchcard.punch6_8 }}">
                                    <span>{{ "Visits" | t }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label>{{ "AverageOf" | t }} {{ "MoreThan" | t }}
                                        8 {{ "Visits" | t }} {{ "IncreaseBy" | t }}</label>
                                    <input type="text" class="form-control required"
                                           name="Punchcard[punch8]"
                                           value="{{ punchcard.punch8 }}">
                                    <span>{{ "Visits" | t }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{%- endmacro %}


{%- macro engagementPreview( engagement, punchcard ) %}
    <div id="engagementPreview" class="tab-pane">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group"></div>
                <div class="form-group">
                    <div class="panel panel-warning">
                        <div id="punchcardPreviewHeader" class="panel-heading">{{ "Punchcard" | t }}</div>
                        <div id="punchcardPreviewBody" class="panel-body">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1">
                                    <p id="punchcardPreviewText">{{ "PunchcardPreviewText" | t }}</p>
                                </div>
                                <div class="clearfix"></div>
                                <h3 id="punchcardPreviewName">{{ engagement.name }}</h3>
                                <div class="h1" id="punchcardPreviewDuration">
                                    <span>{{ punchcard.duration }}</span>
                                    <p>{{ "Days" | t }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group topHr"><hr></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    {% for i in 1..4 %}
                                        <div class="punchcardPreviewNumbersFormGroup fr col-xs-6  col-md-3 ">
                                            <span class="circle">{{ i }}</span>
                                        </div>
                                    {% endfor %}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    {% for i in 5..8 %}
                                        <div class="punchcardPreviewNumbersFormGroup fr col-xs-6  col-md-3 ">
                                            <span class="circle">{{ i }}</span>
                                        </div>
                                    {% endfor %}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group"><hr></div>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </div>
                        <div id="punchcardPreviewFooter" class="panel-footer">
                            <a class="btn btn-success">
                                <i class="fa fa-clock-o fr marginLeft"></i> {{ "RemindMe" | t }}</a>
                            <p>{{ "Regulations" | t }}</p>
                        </div>
                    </div>
                    <div class="btns">
                        {% if router.getActionName() != "view" %}
                            <input id="save" class="btn btn-primary" type="submit" value="{{ 'Save' | t }}">
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
{%- endmacro %}

{%- macro btns( target ) %}
<div class="btns">
    <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#{{ target }}">{{ "Preview" | t }}</button>
    {% if router.getActionName() != "view" %}
        <input id="save" class="btn btn-primary" type="submit" value="{{ 'Save' | t }}">
    {% endif %}
</div>
{%- endmacro %}

{%- macro pagerwizard( baseUrl, path ) %}
<ul class="pager wizard">
    <li class="previous fr">
        <a href="#">{{ "Previous" | t }}</a>
    </li>
    <li class="next fl">
        <a href="#">{{ "Next" | t }}</a>
    </li>
    <li>
        <p>
            <a href="{{ baseUrl }}{{ path }}" class="btn btn-primary backToList">{{ "Abort" | t }}</a>
        </p>
    </li>
</ul>
{%- endmacro %}

{%- macro addMoreAnswersMockUp() %}
    <div class="addMoreAnswersMockUp hidden">
        <div class="answersListUnit form-group">
            <div class="form-group">
                <img class="thumbnail previewImage">
                <input class="imageUpload" type="file" name="answerImage[]"
                       data-text='{{ "ChooseFile" | t }}'>
            </div>
            <div class="form-inline">
                <div class="form-group">
                    <label class="icon fr"></label>
                    <input type="text" class="answersListUnitTitle form-control" name='PollAnswer[][]'>
                </div>
            </div>
        </div>
    </div>
{%- endmacro %}
