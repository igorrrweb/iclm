<!DOCTYPE html>
<html lang="{{ lang }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ this.settings['projectName'] }} - {{ (this.router.getControllerName() | capitalize) | t }}</title>

    {{ assets.outputCss('headerCss') }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {{ assets.outputJs('headerJs') }}
    <![endif]-->

</head>

<body class="{% if lang is 'he' %}rtl{% else %}ltr{% endif %}" id="{{ router.getControllerName() }}" data-lang="{{ lang }}">

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{baseUrl}}">{{ this.settings['projectName'] }} - {{ (this.router.getControllerName() | capitalize) | t }}</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                            </div>
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                            </div>
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                            </div>
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>Read All Messages</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-tasks">
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <span class="pull-right text-muted">40% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <span class="pull-right text-muted">20% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <span class="pull-right text-muted">60% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <span class="pull-right text-muted">80% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Tasks</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-tasks -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-comment fa-fw"></i> New Comment
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                <span class="pull-right text-muted small">12 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-envelope fa-fw"></i> Message Sent
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-tasks fa-fw"></i> New Task
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-alerts -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="{{baseUrl}}index/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown langOptions fl">
                {#<span id="language">{{ "Language" | t }}:</span>#}
                <a class="dropdown-toggle langToggle" data-toggle="dropdown">
                    <img src="{{baseUrl}}img/{{ lang }}.png">
                </a>
                <ul class="dropdown-menu langMenu">
                    <li {% if lang is "en" %}class="active"{% endif %}>
                        <a class="changeLang" data-lang="en">
                            <img src="{{baseUrl}}img/en.png">
                            <span class="marginRight">{{ "English" | t }}</span>
                        </a>
                    </li>
                    <li {% if lang is "he" %}class="active"{% endif %}>
                        <a class="changeLang" data-lang="he">
                            <img src="{{baseUrl}}img/he.png">
                            <span class="marginRight">{{ "Hebrew" | t }}</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li {% if dispatcher.getControllerName() is "index" %}class='active'{% endif %}>
                        <a href="{{baseUrl}}dashboard" {% if dispatcher.getControllerName() is "index" %}class='active'{% endif %}><i class="fa fa-dashboard fa-fw"></i><span> {{ "Dashboard" | t }}</span></a>
                    </li>
                    <li {% if dispatcher.getControllerName() is "segment" %}class='active'{% endif %}>
                        <a href="{{baseUrl}}segment" {% if dispatcher.getControllerName() is "segment" %}class='active'{% endif %}><i class="fa fa-sort-amount-asc fr"></i><span> {{ "Segments" | t }}</span></a>
                    </li>
                    <li {% if dispatcher.getControllerName() is "coupon" %}class='active'{% endif %}>
                        <a href="{{baseUrl}}coupon" {% if dispatcher.getControllerName() is "coupon" %}class='active'{% endif %}><i class="fa fa-tag fr"></i><span> {{ "Coupons" | t }}</span></a>
                    </li>
                    <li {% if dispatcher.getControllerName() is "customer" %}class='active'{% endif %}>
                        <a href="{{baseUrl}}customer" {% if dispatcher.getControllerName() is "customer" %}class='active'{% endif %}><i class="fa fa-users fr"></i><span> {{ "ClubMembers" | t }}</span></a>
                    </li>
                    <li id="engagementsTab">
                        <a id="engagementsHeader"><i class="fa fa-rocket fr"></i><span> {{ "UserEngagements" | t }}</span></a>
                        <ul class="nav nav-second-level" id="engagementsListTabs">
                            <li {% if dispatcher.getControllerName() is "poll" %}class='active'{% endif %}>
                                <a href="{{baseUrl}}poll" {% if dispatcher.getControllerName() is "poll" %}class='active'{% endif %}><i class="fa fa-check-square-o marginLeft"></i> {{ "Polls" | t }}</a>
                            </li>
                            <li {% if dispatcher.getControllerName() is "punchcard" %}class='active'{% endif %}>
                                <a href="{{baseUrl}}punchcard" {% if dispatcher.getControllerName() is "punchcard" %}class='active'{% endif %}><i class="fa fa-cubes marginLeft"></i> {{ "PunchCards" | t }}</a>
                            </li>
                            <li {% if dispatcher.getControllerName() is "fliptile" %}class='active'{% endif %}>
                                <a href="{{baseUrl}}fliptile" {% if dispatcher.getControllerName() is "fliptile" %}class='active'{% endif %}><i class="fa fa-th marginLeft"></i> {{ "Fliptiles" | t }}</a>
                            </li>
                            <li {% if dispatcher.getControllerName() is "message" %}class='active'{% endif %}>
                                <a href="{{baseUrl}}message" {% if dispatcher.getControllerName() is "message" %}class='active'{% endif %}><i class="fa fa-envelope-o marginLeft"></i> {{ "Messages" | t }}</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a id="reportsHeader"><i class="fa fa-list-alt fr"></i><span> {{ "Reports" | t }}</span></a>
                        <ul class="nav nav-second-level" id="reportTabs">
                            <li {% if dispatcher.getControllerName() is "reports" and dispatcher.getActionName() is "coupons" %}class='active'{% endif %}>
                                <a href="{{baseUrl}}reports/coupons" {% if dispatcher.getControllerName() is "reports" and dispatcher.getActionName() is "coupons" %}class='active'{% endif %}><i class="fa fa-tags marginLeft"></i> {{ "Coupons" | t }}</a>
                            </li>
                            <li {% if dispatcher.getControllerName() is "reports" and dispatcher.getActionName() is "segments" %}class='active'{% endif %}>
                                <a href="{{baseUrl}}reports/segments" {% if dispatcher.getControllerName() is "reports" and dispatcher.getActionName() is "segments" %}class='active'{% endif %}><i class="fa fa-list-alt marginLeft"></i> {{ "Segments" | t }}</a>
                            </li>
                            <li {% if dispatcher.getControllerName() is "reports" and dispatcher.getActionName() is "usersreports"  %}class='active'{% endif %}>
                                <a href="{{baseUrl}}reports/usersreports" {% if dispatcher.getControllerName() is "reports" and dispatcher.getActionName() is "usersreports"  %}class='active'{% endif %}><i class="fa fa-tags marginLeft"></i> {{ "UsersReports" | t }}</a>
                            </li>
                            <li {% if dispatcher.getControllerName() is "reports" and dispatcher.getActionName() is "history"  %}class='active'{% endif %}>
                                <a href="{{baseUrl}}reports/history" {% if dispatcher.getControllerName() is "reports" and dispatcher.getActionName() is "history"  %}class='active'{% endif %}><i class="fa fa-tags marginLeft"></i> {{ "PurchaseHistory" | t }}</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a id="feedbackHeader"><i class="fa fa-rss fr"></i><span> {{ "Feedback" | t }}</span></a>
                        <ul class="nav nav-second-level" id="feedbackTabs">
                            <li {% if dispatcher.getControllerName() is "feedback" %}class='active'{% endif %}>
                                <a href="{{baseUrl}}feedback" {% if dispatcher.getControllerName() is "feedback" %}class='active'{% endif %}><i class="fa fa-edit marginLeft"></i> {{ "FeedbackForms" | t }}</a>
                            </li>
                            <li {% if dispatcher.getControllerName() is "userfeedbacks" %}class='active'{% endif %}>
                                <a href="{{baseUrl}}userfeedbacks" {% if dispatcher.getControllerName() is "userfeedbacks" %}class='active'{% endif %}><i class="fa fa-users marginLeft"></i> {{ "UserFeedbacks" | t }}</a>
                            </li>
                        </ul>
                    <li {% if dispatcher.getControllerName() is "beacon" %}class='active'{% endif %}>
                        <a href="{{baseUrl}}beacon" {% if dispatcher.getControllerName() is "beacon" %}class='active'{% endif %}><i class="fa fa-wifi fr"></i><span> {{ "Beacons" | t }}</span></a>
                    </li>
                    <li {% if dispatcher.getControllerName() is "admin" %}class='active'{% endif %}>
                        <a href="{{baseUrl}}admin" {% if dispatcher.getControllerName() is "admin" %}class='active'{% endif %}><i class="fa fa-users fr"></i><span> {{ "Admin" | t }}</span></a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            {{ content() }}
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
<input type="hidden" readonly value="{{baseUrl}}" class="baseUrl">
<!-- /#wrapper -->
{{ assets.outputJs('footer') }}
</body>

</html>

