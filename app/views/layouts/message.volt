<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ (this.router.getControllerName() | capitalize) | t }}</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{baseUrl}}dashboard">
                    <i class="fa fa-dashboard"></i> {{ "Dashboard" | t }}
                </a>
            </li>
            <li>
                <a href="{{baseUrl}}{{ router.getControllerName() }}">
                    <i class="fa fa-envelope-o marginLeft"></i> {{ (this.router.getControllerName() | capitalize) | t }}
                </a>
            </li>
            <li class="active">
                {% set params = this.router.getParams() %}
                {% set paramStr = '' %}
                {% if params is not empty %}
                    {% set paramStr = implode('/',params) %}
                {% endif %}
                <a href="{{baseUrl}}{{ this.router.getControllerName() }}/{{ this.router.getActionName() }}{% if paramStr is not empty %}/{{paramStr}}{% endif %}">
                    {{ ("Breadcrumbs"~this.router.getActionName() | capitalize) | t}}
                </a>
            </li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group flash-message">
            {{ this.flashSession.output() }}
        </div>
    </div>
</div>
{{ content() }}
<span class="hidden" id="jsTranslate">["{{ "Confirm" | t }}","{{ "Cancel" | t }}"]</span>