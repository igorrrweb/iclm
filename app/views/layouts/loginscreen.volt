<!DOCTYPE html>
<html lang="{{ lang }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ICLM Admin</title>

    {{ assets.outputCss('headerCss') }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {{ assets.outputJs('headerJs') }}
    <![endif]-->

</head>

<body class="{% if lang is 'he' %}rtl{% else %}ltr{% endif %} page-body login-page login-form-fall" data-lang="{{ lang }}">
    <div class="container">
        {{ content() }}
    </div>
<input type="hidden" readonly value="{{baseUrl}}" class="baseUrl">

{{ assets.outputJs('footer') }}


</body>

</html>
