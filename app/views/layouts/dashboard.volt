<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ "Dashboard" | t }}</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{baseUrl}}dashboard">
                    <i class="fa fa-dashboard"></i> {{ "Dashboard" | t }}
                </a>
            </li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
{{ content() }}