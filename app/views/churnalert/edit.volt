<div id="crudIndex">
    <div class="row">
        <div class="col-md-12">
            <h3>{{"Customer's Purchase History"}}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <h3>{{'Ticket ID'}}:</h3>
            <h3>{{id}}</h3>
        </div>
        <div class="col-md-3 ">
            <h3>{{'CustomerName' | t}}:</h3>
            <h3>{{customerData['firstname']~' '~customerData['lastname']}}</h3>
        </div>
        <div class="col-md-3 ">
            <h3>{{'PhoneNumber' | t}}:</h3>
            <h3>{{customerData['mobile']}}</h3>
        </div>
        <div class="col-md-3">
            <h3>{{'TriggerDate' | t}}:</h3>
            <h3>{{date('d-m-Y')}}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form id="indexForm" action="" method="get" class="crudForm ">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                    <tr>
                        {% for attribute in attributes %}
                            <th>{{ attribute | t}}</th>
                        {% endfor %}

                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="add-activity col-md-2 col-md-offset-10">
                <div class="form-group">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        {{'AddActivity' | t}}
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{'AddActivity' | t}}</h4>
                </div>
                <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                    <tr>
                                        {% for attribute in attributes %}
                                            <th>{{ attribute | t}}</th>
                                        {% endfor %}

                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ "test" }}</td>
                                            <td>{{ date('d/m/Y') }}</td>
                                            <td>
                                                <select class='select2'>
                                                    <option value='' selected>{{ "Action" | t }}</option>
                                                    <option value='1'>{{ "Feedback" | t }}</option>
                                                    <option value='2'>{{ "Coupon" | t }}</option>
                                                    <option value='3'>{{ "FlipTiles" | t }}</option>
                                                    <option value='4'>{{ "PhoneCall" | t }}</option>
                                                    </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="hide">
    {% if history is defined %}
        <div id="historyList">
            <p class="h3">{{ "UserEventsHistory" | t }}</p>
            <ul id="eventsHistory"></ul>
            <ul class="fl pagination" id="historyPagination"></ul>
        </div>
    {% endif %}
    <span id="jsTranslate">["{{ "Confirm" | t }}","{{ "Cancel" | t }}"]</span>
</div>

