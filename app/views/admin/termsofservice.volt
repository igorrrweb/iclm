<div id="adminTos">
    <h3>{{ "TermsOfService" | t }}:</h3>
    <form action="" method="post">
        <ul class="nav nav-tabs" role="tablist">
            {% for label,lang in langs %}
                <li role="presentation" class="langTab fr {% if loop.index0 is 0 %}active{% endif %}" data-lang="{{ lang }}">
                    <a href="#adminTos_{{ lang }}" aria-controls="adminTos_{{ lang }}" role="tab" data-toggle="tab">{{ label | t }}</a>
                </li>
            {% endfor %}
        </ul>
        <div class="tab-content">
        {% for lang in langs %}
            <div role="tabpanel" id="adminTos_{{ lang }}" class="tab-pane langTabData {% if lang is 'he' %}active{% endif %}">
                <div class="form-group"></div>
                <textarea class="form-control tosText" name="tos[{{ lang }}]">{% if tos[lang] is defined %}{{ tos[lang]['body_text'] }}{% endif %}</textarea>
                <div class="form-group"></div>
            </div>
        {% endfor %}
        </div>
        <div id="btns">
            <input class="btn btn-primary" type="submit" value="{{ 'Save' | t }}">
            <button type="button" class="btn btn-primary previewBtn" data-toggle="modal" data-target="#myModal">{{ 'Preview' | t }}</button>
            <a href="{{baseUrl}}admin" class="btn btn-primary">{{ 'Abort' | t }}</a>
        </div>
    </form>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1 class="modal-title" id="myModalLabel">{{ "Regulations" | t }} / {{ "TermsOfService" | t }}</h1>
            </div>
            <div class="modal-body">
                <div class="previewBody"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
