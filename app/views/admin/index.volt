<div class="row">
    <div class="col-md-12">
        <div class="well tile"><a href="{{baseUrl}}systemuser">{{ 'ManageUsers' | t }}</a></div>
        <div class="well tile"><a href="{{baseUrl}}branch">{{ 'Branches' | t }}</a></div>
        <div class="well tile"><a href="{{baseUrl}}admin/settings">{{ 'Settings' | t }}</a></div>
        <div class="well tile"><a href="{{baseUrl}}admin/termsofservice">{{ 'TermsOfService' | t }}</a></div>
    </div>
</div>