<form class="crudForm" action="" method="post">
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-cog"></i> {{ "SupportedLanguages" | t }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            {% for label,lang in langs %}
                            <div class="checkbox-inline">
                                <label class="fr marginLeft marginRight"><input type="checkbox" class="icheck" name="Settings[supported_langs][]" value="{{ lang }}"
                                            {% if settings['supported_langs'] is defined %}
                                        {% for i in settings['supported_langs'] | json_decode %}
                                            {% if i is lang %} checked {% endif %}
                                        {% endfor %}
                                            {% endif %}> {{ label | t }}</label>

                            </div>
                            {% endfor %}
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div id="defaultLanguage" class="form-group fr">
                                <label>{{ "DefaultLanguage" | t }}</label>
                                <select name="Settings[default_lang]" class="select2 select2NoSearch">
                                    {% for label,lang in langs %}
                                        <option value="{{ lang }}"
                                                {% if settings['default_lang'] is defined and settings['default_lang'] is lang %}selected{% endif %}>
                                            {{ label | t }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-cog"></i> {{ "Preferences" | t }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group setting-form-group">
                                <label>{{ "AllowCouponShare" | t }}:</label>
                                <div class="form-inline">
                                    <div class="radio-inline">
                                        <label class="radio-inline"><input class="icheck"
                                                                           type="radio"
                                                                           name="Settings[coupon_share]"
                                                                           value="1"
                                                                           {% if settings['coupon_share'] is defined and settings['coupon_share']is 1 %}checked{% endif %}> {{ "Yes" | t }}
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label class="radio-inline"><input class="icheck"
                                                                           type="radio"
                                                                           name="Settings[coupon_share]"
                                                                           value="0"
                                                                           {% if settings['coupon_share'] is defined and settings['coupon_share'] is 0 %}checked{% endif %}> {{ "No" | t }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group fr">
                                <label>{{ "MaxAllowedShares" | t }}:</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <input type="text" class="form-control"
                                               name="Settings[max_allow_share]"
                                               {% if settings['max_allow_share'] is defined %}value="{{ settings['max_allow_share'] }}" {% endif %}>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group fr">
                        <div class="form-inline">
                            <div class="form-group setting-form-group">
                                <label>{{ "AllowCouponPush" | t }}:</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <div class="radio-inline fr">
                                            <label class="radio-inline fr"><input class="icheck" type="radio"
                                                                                  name="Settings[coupon_push]" value="1"
                                                                                  {% if settings['coupon_push'] is defined and settings['coupon_push'] is 1 %}checked{% endif %}> {{ "Yes" | t }}
                                            </label>
                                        </div>
                                        <div class="radio-inline fr">
                                            <label class="radio-inline fr"><input class="icheck" type="radio"
                                                                                  name="Settings[coupon_push]" value="0"
                                                                                  {% if settings['coupon_push'] is defined and settings['coupon_push'] is 0 %}checked{% endif %}> {{ "No" | t }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group fr">
                                <label>{{ "MaxAllowedPushes" | t }}:</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <input type="text" class="form-control"
                                               name="Settings[max_allow_push]"
                                               {% if settings['max_allow_push'] is defined %}value="{{ settings['max_allow_push'] }}" {% endif %}>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group timeRangeGroup fr">
                        <label>{{ "PushSendingHours" | t }}:</label>
                        <div class="form-inline">
                            <div class="form-group fr">
                                <input type="time" class="minVal form-control" name="Settings[min_push_hour]"
                                       {% if settings['min_push_hour'] is defined %}value="{{ settings['min_push_hour'] }}" {% endif %}>
                            </div>
                            <span class="formSpan fr">{{ "To" | t }}</span>

                            <div class="form-group fr">
                                <input type="time" class="maxVal form-control" name="Settings[max_push_hour]"
                                       {% if settings['max_push_hour'] is defined %}value="{{ settings['max_push_hour'] }}" {% endif %}>
                            </div>
                            <div class="form-group fr">
                                <label class="fl marginRight"><input type="checkbox" class="icheck" name="Settings[ignore_push]" value="1"
                                                                     {% if settings['ignore_push'] is defined and settings['ignore_push'] is 1 %}checked{% endif %}> {{ "IgnorePushText" | t }}</label>
                                <input type="hidden" name="Settings[ignore_push]" value="0">

                            </div>
                            <p class="errorText clr"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ "ContactusMailAddress" | t }}</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input type="text" class="form-control" name="Settings[contact_us]"
                                        {% if settings['contact_us'] is defined %}value="{{ settings['contact_us'] }}"{% endif %}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="inputGroup">
                        <label>{{ "BranchClosingReminder" | t }}</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input type="text" class="form-control"
                                        name="Settings[closing_reminder]"
                                        {% if settings['closing_reminder'] is defined %}value="{{ settings['closing_reminder'] }}" {% endif %}>
                                <span class="marginRight">{{ "MinutesBeforeBranchCloses" | t }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ "ICLMTimeFormat" | t }}</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <div class="radio-inline fr">
                                    <label><input type="radio" class="icheck" name="Settings[time_format]" value="d/m/Y"
                                                  {% if settings['time_format'] is defined and settings['time_format'] is "d/m/Y" %}checked{% endif %}>
                                        dd/mm/yyyy</label>
                                </div>
                                <div class="radio-inline fr">
                                    <label><input type="radio" class="icheck" name="Settings[time_format]" value="Y/m/d"
                                                  {% if settings['time_format'] is defined and settings['time_format'] is "Y/m/d" %}checked{% endif %}>
                                        yyyy/mm/dd</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="submit" value="{{ 'Save' | t }}" class="btn btn-primary fl">
    <a href="{{baseUrl}}admin" class="btn btn-primary">{{ "Abort" | t }}</a>
</form>