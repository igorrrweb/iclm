{# @var beacon #}
{% set values = [beacon['name'],beacon['color'],beacon['tags'],beacon['uuid'],beacon['id'],beacon['major'],beacon['minor'],beacon['mac']] %}
{%  set labels = ["BeaconName","BeaconColor","Tags","SecureUUID","ProximityUUID","Major","Minor",
"MacAddress","BroadcastingPower","MaximumRange","AdvertisingInterval","RemainingBatteryLife","BasicBatterySaving",
"SmartBatterySaving","FirmwareVersion","HardWareVersion"] %}

<div class="row">
    <div class="col-md-12">
        <h2>{{ beacon['name'] }}</h2>
        <div id="beaconDetails">
            <p class="h1"></p>
            <ul>
                {% for label in labels %}
                <li>
                    <div class="form-group">
                        <label class="control-label">{{ label | t }}</label>
                        <span class="pull-right">{% if values[loop.index0] is defined %}{{ values[loop.index0] }}{% else %}{{ "Unknown" | t }}{% endif %}</span>
                    </div>
                </li>
                {% endfor %}
            </ul>
            <a href="{{baseUrl}}beacon" class="btn btn-primary backToList">{{ "BackToList" | t }}</a>
        </div>
    </div>
</div>
