{{ partial("segment/macroses", []) }}
<form action="" method="post" id="segmentMainData" class="crudForm {% if retention is defined %}retention{% endif %}">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="Segment[name]">{{ "SegmentName" | t }}*</label>
                <div class="form-inline">
                    <div class="form-group">
                        <label for="" class="sr-only"></label>
                        {{ text_field("Segment[name]", "class":"form-control required", "value":segment.name, "autocomplete":"off") }}
                    </div>
                    <div class="checkbox-inline">
                        <label class="checkbox-inline">
                            {{ check_field("Segment[active]", "class":"icheck", "value":"1", (segment.active== "1" ? 'checked' : '') ) }} {{ 'Active' | t }}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-user"></i> {{ "GeneralInfo" | t }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>{{ "Gender" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="hidden" name="SegmentRule[profileRules][gender]" value="">
                                        {{ gender( segmentRule ) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label>{{ "Married" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="hidden" name="SegmentRule[profileRules][marital_status]" value="">
                                        {{ married( segmentRule ) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="SegmentRule[profileRules][birthdate]">{{ "Birthday" | t }} {{ "Within" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("SegmentRule[profileRules][birth_date]",
                                        "class":"form-control",
                                        "value":segmentRule['profileRules']['birth_date']) }}
                                        <span class="formSpan">{{ "Days" | t }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[wedding_anniversary]">{{ "WeddingAnniversary" | t }} {{ "Within" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("SegmentRule[profileRules][wedding_anniversary_date]",
                                        "class":"form-control",
                                        "value":segmentRule['profileRules']['wedding_anniversary_date']) }}
                                        <span class="formSpan">{{ "Days" | t }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[profileRules][join_date]">{{ "LCJoiningAnniversary" | t }} {{ "Within" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("SegmentRule[profileRules][join_date]",
                                        "class":"form-control",
                                        "value":segmentRule['profileRules']['join_date']) }}
                                        <span class="formSpan">{{ "Days" | t }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>{{ "Age" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("SegmentRule[profileRules][age][min]",
                                        "class":"form-control minVal",
                                        "value":segmentRule['profileRules']['age']['min']) }}
                                        <label for="" class="sr-only"></label>
                                        <span class="formSpan fr">{{ "To" | t }}</span>
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("SegmentRule[profileRules][age][max]",
                                        "class":"form-control maxVal",
                                        "value":segmentRule['profileRules']['age']['max']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[city_id][]">{{ "City" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <select class="select2"
                                                name="SegmentRule[profileRules][city_id][]"
                                                multiple>
                                            {% if cities is iterable %}
                                                {% for city in cities %}
                                                    <option
                                                            {% if (segmentRule['profileRules']['city_id'] is defined) and
                                                            (segmentRule['profileRules']['city_id'] is iterable) %}
                                                                {% for id in segmentRule['profileRules']['city_id'] %}
                                                                    {% if id is city.id %} selected {% endif %}
                                                                {% endfor %}
                                                            {% endif %}
                                                            value="{{ city.id }}">{{ city.city }}</option>
                                                {% endfor %}
                                            {% endif %}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-shopping-cart"></i> {{ "StoreVisitHabits" | t }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="retentionWrapper" class="checkbox">
                        <label>
                            {{ check_field("Segment[retention]",
                            "id":"retention",
                            "class":"icheck",
                            "value":"1",
                            ((segment.retention == "1") ? 'checked':'')) }} {{ 'Retention' | t }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ "BranchVisitFrequency" | t }}</label>
                        <div class="form-inline">
                            <div class="form-group">
                                {{ text_field("SegmentRule[branchVisitsRules][freq][min]",
                                "class":"form-control minVal",
                                "value":segmentRule['branchVisitsRules']['freq']['min'] ) }}
                                <span class="formSpan fr">{{ "To" | t }}</span>
                                {{ text_field("SegmentRule[branchVisitsRules][freq][max]",
                                "class":"form-control maxVal",
                                "value":segmentRule['branchVisitsRules']['freq']['max'] ) }}
                            </div>
                        </div>
                        <label class="radio-inline">
                            {{ radio_field("SegmentRule[branchVisitsRules][freq][period]",
                            "class":"icheck",
                            "value":"week",
                            ( (segmentRule['branchVisitsRules']['freq']['period'] is defined)
                            and ((segmentRule['branchVisitsRules']['freq']['period'] is "week") or
                            (segmentRule['branchVisitsRules']['freq']['period'] is "")) ? 'checked' : '' )
                            ) }} {{ 'Week' | t }}</label>
                        <label class="radio-inline">
                            {{ radio_field("SegmentRule[branchVisitsRules][freq][period]",
                            "class":"icheck",
                            "value":"month",
                            ( ((segmentRule['branchVisitsRules']['freq']['period'] is defined) and
                            (segmentRule['branchVisitsRules']['freq']['period'] is "month")) ? 'checked' : '' )
                            ) }}  {{ 'Month' | t }}</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ "BranchVisitHours" | t }}</label>

                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                {{ time_field(["SegmentRule[branchVisitsRules][enter][min]", "class":"form-control",
                                "value":segmentRule['branchVisitsRules']['enter']['min'] ]) }}
                                <span class="formSpan fr">{{ "To" | t }}</span>
                                <label for="" class="sr-only"></label>
                                {{ time_field(["SegmentRule[branchVisitsRules][enter][max]",
                                "class":"form-control",
                                "value":segmentRule['branchVisitsRules']['enter']['max'] ] ) }}
                            </div>
                        </div>
                        <div class="checkbox">
                            <input type="hidden" name="SegmentRule[branchVisitsRules][abandon]" value="">
                            <label>
                                {{ check_field("SegmentRule[branchVisitsRules][abandon]",
                                "class":"icheck",
                                "value":"1",
                                (segmentRule['branchVisitsRules']['abandon'] == "1" ? 'checked' : '')) }} {{ 'BranchAbandon' | t }}</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book"></i> {{ "PurchaseHistory" | t }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="SegmentRule[transactionsRules][products_purchased][ids]">{{ "ProductsPurchased" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <select class="select2"
                                                name="SegmentRule[transactionsRules][products_purchased][ids][]"
                                                multiple>
                                            {% if products is iterable %}
                                                {% for product in products %}
                                                    <option
                                                            {% if (segmentRule['transactionsRules']['products_purchased']["ids"] is defined) and
                                                            (segmentRule['transactionsRules']['products_purchased']["ids"] is iterable) %}
                                                                {% for id in segmentRule['transactionsRules']['products_purchased']['ids'] %}
                                                                    {% if id is product.id %}
                                                                        selected
                                                                    {% endif %}
                                                                {% endfor %}
                                                            {% endif %}
                                                            value="{{ product.id }}">{{ product.name }}</option>
                                                {% endfor %}
                                            {% endif %}
                                        </select>
                                    </div>
                                </div>
                                <div class="radio">

                                    <label class="radio-inline">
                                        {{ radio_field("SegmentRule[transactionsRules][products_purchased][operator]",
                                        "class":"icheck",
                                        "value":"and",
                                        ( ((segmentRule['transactionsRules']['products_purchased']['operator'] is defined ) and
                                        ((segmentRule['transactionsRules']['products_purchased']['operator'] == "and")
                                        or (segmentRule['transactionsRules']['products_purchased']['operator'] == ""))) ? 'checked' : '' )
                                        ) }} {{ 'And' | t }}</label>

                                    <label class="radio-inline">
                                        {{ radio_field("SegmentRule[transactionsRules][products_purchased][operator]",
                                        "class":"icheck",
                                        "value":"or",
                                        ( ((segmentRule['transactionsRules']['products_purchased']['operator'] is defined ) and
                                        (segmentRule['transactionsRules']['products_purchased']['operator'] == "or")) ? 'checked' : '' )
                                        ) }} {{ 'Or' | t }}</label>

                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[transactionsRules][products_not_purchased][ids]">{{
                                    "ProductsNotPurchased" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <select class="select2"
                                                    name="SegmentRule[transactionsRules][products_not_purchased][ids][]"
                                                    multiple>
                                                {% if products is iterable  %}
                                                    {% for product in products %}
                                                        <option
                                                                {% if segmentRule['transactionsRules']['products_not_purchased']["ids"] is defined and
                                                                segmentRule['transactionsRules']['products_not_purchased']["ids"] is iterable %}
                                                                    {% for id in segmentRule['transactionsRules']['products_not_purchased']['ids'] %}
                                                                        {% if id is product.id %}
                                                                            selected
                                                                        {% endif %}
                                                                    {% endfor %}
                                                                {% endif %}
                                                                value="{{ product.id }}">{{ product.name }}</option>
                                                    {% endfor %}
                                                {% endif %}
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="radio">
                                    <label class="radio-inline">
                                        {{ radio_field("SegmentRule[transactionsRules][products_not_purchased][operator]",
                                        "class":"icheck",
                                        "value":"and",
                                        ( ((segmentRule['transactionsRules']['products_not_purchased']['operator'] is defined) and
                                        ((segmentRule['transactionsRules']['products_not_purchased']['operator'] == "and")
                                        or (segmentRule['transactionsRules']['products_not_purchased']['operator'] == "") ) ) ? 'checked' : '' )
                                        ) }} {{ 'And' | t }}</label>
                                    <label class="radio-inline">
                                        {{ radio_field("SegmentRule[transactionsRules][products_not_purchased][operator]",
                                        "class":"icheck",
                                        "value":"or",
                                        ( ((segmentRule['transactionsRules']['products_not_purchased']['operator'] is defined) and
                                        (segmentRule['transactionsRules']['products_not_purchased']['operator'] == "or" )) ? 'checked' : '' )
                                        ) }} {{ 'Or' | t }}</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="SegmentRule[transactionsRules][category_purchased][ids]">{{ "CategoriesPurchased" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <select class="select2"
                                                    name="SegmentRule[transactionsRules][category_purchased][ids][]"
                                                    multiple>
                                                {% if categories is iterable %}
                                                    {% for category in categories %}
                                                        <option
                                                                {% if segmentRule['transactionsRules']['category_purchased']["ids"] is defined and
                                                                segmentRule['transactionsRules']['category_purchased']["ids"] is iterable %}
                                                                    {% for id in segmentRule['transactionsRules']['category_purchased']['ids'] %}
                                                                        {% if id is category.id %}
                                                                            selected
                                                                        {% endif %}
                                                                    {% endfor %}
                                                                {% endif %}
                                                                value="{{ category.id }}">{{ category.category }}</option>
                                                    {% endfor %}
                                                {% endif %}
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="radio">
                                    <label class="radio-inline">
                                        {{ radio_field("SegmentRule[transactionsRules][category_purchased][operator]",
                                        "class":"icheck",
                                        "value":"and",
                                        ( ((segmentRule['transactionsRules']['category_purchased']['operator'] is defined) and
                                        ((segmentRule['transactionsRules']['category_purchased']['operator'] == "and")
                                        or (segmentRule['transactionsRules']['category_purchased']['operator'] == "") ) ) ? 'checked' : '' )
                                        ) }} {{ 'And' | t }}</label>
                                    <label class="radio-inline">
                                        {{ radio_field("SegmentRule[transactionsRules][category_purchased][operator]",
                                        "class":"icheck",
                                        "value":"or",
                                        ( ((segmentRule['transactionsRules']['category_purchased']['operator'] is defined) and
                                        (segmentRule['transactionsRules']['category_purchased']['operator'] == "or" )) ? 'checked' : '' )
                                        ) }} {{ 'Or' | t }}</label>

                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[transactionsRules][products_not_purchased][ids][]">{{ "CategoriesNotPurchased" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <select class="select2"
                                                name="SegmentRule[transactionsRules][category_not_purchased][ids][]"
                                                multiple>
                                            {% if categories is iterable %}
                                                {% for category in categories %}
                                                    <option
                                                            {% if segmentRule['transactionsRules']['category_not_purchased']["ids"] is defined and
                                                            segmentRule['transactionsRules']['category_not_purchased']["ids"] is iterable %}
                                                                {% for id in segmentRule['transactionsRules']['category_not_purchased']['ids'] %}
                                                                    {% if id is category.id %}
                                                                        selected
                                                                    {% endif %}
                                                                {% endfor %}
                                                            {% endif %}
                                                            value="{{ category.id }}">{{ category.category }}</option>
                                                {% endfor %}
                                            {% endif %}
                                        </select>
                                    </div>

                                </div>
                                <div class="radio">
                                    <label class="radio-inline">
                                        {{ radio_field("SegmentRule[transactionsRules][category_not_purchased][operator]",
                                        "class":"icheck",
                                        "value":"and",
                                        ( ((segmentRule['transactionsRules']['category_not_purchased']['operator'] is defined) and
                                        ((segmentRule['transactionsRules']['category_not_purchased']['operator'] == "and")
                                        or (segmentRule['transactionsRules']['category_not_purchased']['operator'] == "") ) ) ? 'checked' : '' )
                                        ) }} {{ 'And' | t }}</label>
                                    <label class="radio-inline">
                                        {{ radio_field("SegmentRule[transactionsRules][category_not_purchased][operator]",
                                        "class":"icheck",
                                        "value":"or",
                                        ( ((segmentRule['transactionsRules']['category_not_purchased']['operator'] is defined) and
                                        (segmentRule['transactionsRules']['category_not_purchased']['operator'] == "or" )) ? 'checked' : '' )
                                        ) }}  {{ 'Or' | t }}</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ "AvgPurchasesMonth" | t }}</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                {{ text_field("SegmentRule[transactionsRules][last_month][min]",
                                "class":"form-control minVal",
                                "value":segmentRule['transactionsRules']['last_month']['min']
                                ) }}
                                <label for="" class="sr-only"></label>
                                <span class="formSpan fr">{{ "To" | t }}</span>
                                <label for="" class="sr-only"></label>
                                {{ text_field("SegmentRule[transactionsRules][last_month][max]",
                                "class":"form-control maxVal",
                                "value":segmentRule['transactionsRules']['last_month']['max']
                                ) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{ "AvgPurchasesYear" | t }}</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                {{ text_field("SegmentRule[transactionsRules][last_year][min]",
                                "class":"form-control minVal",
                                "value":segmentRule['transactionsRules']['last_year']['min']
                                ) }}
                                <label for="" class="sr-only"></label>
                                <span class="formSpan fr">{{ "To" | t }}</span>
                                <label for="" class="sr-only"></label>
                                {{ text_field("SegmentRule[transactionsRules][last_year][max]",
                                "class":"form-control maxVal",
                                "value":segmentRule['transactionsRules']['last_year']['max']
                                ) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    {% if router.getActionName() is "view" %}
        <a href="{{ baseUrl }}segment" class="btn btn-primary fl backToList">{{ "BackToList" | t }}</a>
    {% else %}
        <input type="submit" value="{{ 'Save' | t }}" class="btn btn-primary fl">
        <a href="{{ baseUrl }}segment" class="btn btn-primary backToList">{{ "Abort" | t }}</a>
    {% endif %}
</form>
