{%- macro gender( segmentRule ) %}
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][gender]" class="icheck", value="any"
                {% if((segmentRule['profileRules']['gender'] is "any") or (segmentRule['profileRules']['gender'] is empty))%}
               checked {% endif %}> {{ 'Both' | t }}</label>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][gender]" class="icheck", value="male"
                {% if(segmentRule['profileRules']['gender'] is "male")%}
        checked {% endif %}> {{ 'Male' | t }}</label>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][gender]" class="icheck", value="female"
                {% if(segmentRule['profileRules']['gender'] is "female")%}
        checked {% endif %}> {{ 'Female' | t }}</label>
{%- endmacro %}

{%- macro married( segmentRule ) %}
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][marital_status]" class="icheck", value="any"
                {% if((segmentRule['profileRules']['marital_status'] is "any") or (segmentRule['profileRules']['marital_status'] is empty))%}
        checked {% endif %}> {{ 'Both' | t }}</label>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][marital_status]" class="icheck", value="single"
                {% if(segmentRule['profileRules']['marital_status'] is "single")%}
        checked {% endif %}> {{ 'Single' | t }}</label>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][marital_status]" class="icheck", value="married"
                {% if(segmentRule['profileRules']['marital_status'] is "married")%}
        checked {% endif %}> {{ 'Married' | t }}</label>
{%- endmacro %}

