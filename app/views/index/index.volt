<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Please Sign In</h3>
            </div>
            <div class="panel-body">
                {{form("method":"post", "id":"form_login", "role":"form")}}
                    <fieldset>
                        <div class="form-group">
                            {{ text_field("username", "id":"username", "class":"form-control", "placeholder":("Username" | t), "autofocus":"autofocus", "autocomplete":"off")}}
                        </div>
                        <div class="form-group">
                            {{ password_field("password", "id":"password", "class":"form-control", "placeholder":("Password" | t), "value":"", "autocomplete":"off" )}}
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ check_field("rememberme", "id":"rememberme", "value":"1" )}}<span> Remember me </span>
                            </label>
                        </div>
                        <!-- Change this to a button or input when using this as a form -->
                        <button type="submit" class="btn btn-lg btn-success btn-block">{{ "Login" | t }}</button>

                        <p id="forgotPassword">{{ "ForgotPassword" | t }}?</p>
                        <div id="resetPassword" class="form-group">
                            {{ email_field("resetPassword", "class":"form-control", "id":"userEmail", "placeholder":("YourEmailAddress" | t) )}}
                            <a id="submitReset" class="btn btn-primary btn-block btn-login">
                                <i class="fa fa-envelope-o fr marginLeft"></i>
                                {{ "Send" | t }}
                            </a>
                            <p class="hide loginError">{{ "WrongEmailAddressText" | t }}</p>
                            <p class="hide loginError">{{ "PasswordResetMailSent" | t }}</p>
                        </div>
                    </fieldset>
                {{ end_form()}}
            </div>
        </div>
    </div>
</div>
