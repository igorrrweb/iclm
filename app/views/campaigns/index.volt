<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <select class="select2 campaign-type" name="Search[campaignType]">
                <option value="">{{ "CampaignType" | t }}: {{ "All" | t }}</option>
                {% for campaign in campaigns %}
                    <option value="{{ campaign['type'] | lower }}" {% if search['campaignType'] is defined and search['campaignType'] is campaign['type'] %}
                    selected{% endif %}>{{ campaign['type'] | t }}</option>
                {% endfor %}
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
            <thead>
            <tr>
                {% for attribute in attributes %}
                    <th>{{ attribute | t}}</th>
                {% endfor %}

            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        <!-- /.panel -->
    </div>
</div>