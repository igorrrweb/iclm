<form action="" method="post" class="crudForm">
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i>{{ "UserDetails" | t }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group fr">
                                <label for="SystemUser[first_name]">{{ "FirstName" | t }}*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        {{ text_field("SystemUser[first_name]", "class":"form-control required", "value":systemUser.first_name, "readonly":"readonly") }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group fr">
                                <label for="SystemUser[last_name]">{{ "LastName" | t }}*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        {{ text_field("SystemUser[last_name]", "class":"form-control required", "value":systemUser.last_name, "readonly":"readonly") }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group fr">
                                <label for="SystemUser[username]">{{ "Username" | t }}*</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("SystemUser[username]", "class":"form-control required", "value":systemUser.username, "readonly":"readonly") }}
                                    </div>
                                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                    <div class="checkbox-inline fr">
                                        <label for="SystemUser[active]">
                                            {{ hidden_field("SystemUser[active]", "value":"0", "readonly":"readonly") }}
                                            <input class="icheck" type="checkbox" name="SystemUser[active]" value="1"
                                                   {% if systemUser.active is "1" %}checked{% endif %} disabled> {{ "Enabled" | t }}*</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="SystemUser[password]">{{ "Password" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                {{ text_field("SystemUser[password]", "id":"userPassword", "class":"form-control required", "placeholder":"*****", "readonly":"readonly") }}
                            </div>
                            <div class="form-group system-user-form-group">
                                <label for="" class="sr-only"></label>
                                <a class="btn" disabled id="generatePassword">{{ "GeneratePassword" | t }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="SystemUser[password]">{{ "ConfirmPassword" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                {{ text_field("confirmPassword", "id":"confirmPassword", "class":"form-control required", "placeholder":"*****", "readonly":"readonly") }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="SystemUser[email]">{{ "Email" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                {{ text_field("SystemUser[email]", "class":"form-control required", "value":systemUser.email, "readonly":"readonly") }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="SystemUser[phone]">{{ "Phone" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                {{ text_field("SystemUser[phone]", "class":"form-control required", "value":systemUser.phone, "readonly":"readonly") }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {% if router.getActionName() is "view" %}
        <a href="{{ baseUrl }}systemuser" class="btn btn-primary fl backToList" >{{ "BackToList" | t }}</a>
    {% else %}
        <input type="submit" value="{{ 'Save' | t }}" class="btn btn-primary fl">
        <a href="{{ baseUrl }}systemuser" class="btn btn-primary backToList">{{ "Abort" | t }}</a>
    {% endif %}
</form>
