<form action="" method="post" class="crudForm">
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i>{{ "UserDetails" | t }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group fr">
                                <label for="SystemUser[first_name]">{{ "FirstName" | t }}*</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        {{ text_field("SystemUser[first_name]", "class":"form-control required", "value":systemUser.first_name) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group fr">
                                <label for="SystemUser[last_name]">{{ "LastName" | t }}*</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        {{ text_field("SystemUser[last_name]", "class":"form-control required", "value":systemUser.last_name) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group fr">
                                <label for="SystemUser[username]">{{ "Username" | t }}*</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("SystemUser[username]", "class":"form-control required", "value":systemUser.username) }}
                                    </div>
                                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                    <div class="checkbox-inline fr">
                                        <label for="SystemUser[active]">
                                            {{ hidden_field("SystemUser[active]", "value":"0") }}
                                            <input class="icheck" type="checkbox" name="SystemUser[active]" value="1"
                                                   {% if systemUser.active is "1" %}checked{% endif %}> {{ "Enabled" | t }}*</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">{{ "Change password" }}</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="SystemUser[email]">{{ "Email" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                {{ text_field("SystemUser[email]", "class":"form-control required", "value":systemUser.email) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="SystemUser[phone]">{{ "Phone" | t }}*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                {{ text_field("SystemUser[phone]", "class":"form-control required", "value":systemUser.phone) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {% if router.getActionName() is "view" %}
        <a href="{{ baseUrl }}systemuser" class="btn btn-primary fl backToList" >{{ "BackToList" | t }}</a>
    {% else %}
        <input type="submit" value="{{ 'Save' | t }}" class="btn btn-primary fl">
        <a href="{{ baseUrl }}systemuser" class="btn btn-primary backToList">{{ "Abort" | t }}</a>
    {% endif %}
</form>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form action="" class="changePassword">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Change password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 change-response"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="SystemUser[password]">{{ "Password" | t }}*</label>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    {{ text_field("password", "id":"password", "class":"form-control required", "placeholder":"*****") }}
                                </div>
                                <div class="form-group system-user-form-group">
                                    <label for="" class="sr-only"></label>
                                    <a class="btn" id="generatePassword">{{ "GeneratePassword" | t }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="confirmPassword">{{ "ConfirmPassword" | t }}*</label>
                            <div class="form-inline">
                                <div class="form-group">
                                    {{ text_field("confirmPassword", "id":"confirmPassword", "class":"form-control required", "placeholder":"*****") }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ hidden_field("systemUserId","value":systemUser.id) }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-changed-password">Save changes</button>
            </div>
        </div>
        </form>
    </div>
</div>
