{{ partial("coupon/macroses", []) }}
{% set updateVar = '' %}
{% if router.getActionName() is not 'update' %}{% set updateVar = ', "unique"' %}{% endif %}

<form action="" method="post" enctype="multipart/form-data" class="crudForm readOnly" role="form">
    <div class="panel panel-default {% if router.getActionName() is 'view' %} readOnly{% endif %}">
        <div class="panel-heading"><i class="fa fa-tag marginLeft"></i> Header</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="Coupon[name]">{{ "Name" | t }}*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("Coupon[name]",
                                        "class":"required form-control",
                                        "value": coupon.name,
                                        "id":"couponName"
                                        ) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="Coupon[pos_coupon_id]">{{ "CouponNumber" | t }}*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("Coupon[pos_coupon_id]",
                                        "class":"required form-control",
                                        "value": coupon.pos_coupon_id,
                                        "id":"couponID"
                                        ) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="Coupon[max_coupons]">{{ "TotalUnits" | t }}*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("Coupon[max_coupons]",
                                        "class":"required form-control",
                                        "value": coupon.max_coupons,
                                        "id":"couponTotalUnits"
                                        ) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="Coupon[start_date]">{{ "StartDate" | t }}*</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("Coupon[start_date]",
                                        "class":"form-control minVal required datepicker",
                                        "value": coupon.start_date,
                                        "id":"couponTotalUnits"
                                        ) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="Coupon[expiry_date]">{{ "EndDate" | t }}*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("Coupon[expiry_date]",
                                        "class":"form-control maxVal required datepicker",
                                        "value": coupon.expiry_date,
                                        "id":"couponTotalUnits"
                                        ) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="CouponSegment[]">{{ "Audience" | t }}</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <select multiple class="select2" name="CouponSegment[]">
                                            {% if segments is iterable %}
                                                {% for segment in segments %}
                                                    <option
                                                            {% if couponSegments is iterable %}
                                                                {% for couponSegment in couponSegments %}
                                                                    {% if segment.id is couponSegment.segment_id %}selected{% endif %}
                                                                {% endfor %}
                                                            {% endif %}
                                                            value="{{ segment.id }}">{{ segment.name }}
                                                    </option>
                                                {% endfor %}
                                            {% endif %}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="Coupon[coupon_type]">{{ "Type" | t }}</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <select class="select2 select2NoSearch" name="Coupon[coupon_type]">
                                    {% set types = ['Discount','BuyAndGet', 'Gift'] %}
                                    {% for i in types %}
                                        <option {% if coupon.coupon_type is loop.index %}selected{% endif %}
                                                value="{{ loop.index }}">{{ i | t }}
                                        </option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="Coupon[couponImage]">{{ "Image" | t }}</label>

                        <div class="form-group">
                            <img class="img-thumbnail previewImage" {% if img is not empty %}src="{{ img }}"{% endif %}>
                        </div>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                {{ file_field("couponImage",
                                "class":"imageUpload",
                                "id":"couponImageUpload",
                                "data-text": (img is not empty ? ('ChangeImage' | t) : ('ChooseImage' | t) ) ) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>{{ "Barcode" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        {{ text_field("Product[barcode]",
                                        "class":"form-control required",
                                        "value": (barcode.barcode is defined ? barcode.barcode : '')
                                        ) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Coupon[category_id]">{{ "Category" | t }}</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <textarea rows="1" name="" class="form-control c-control">{% if rootCategories is defined %}{{ rootCategories.category | trim }}{% endif %}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="checkbox-inline">
                        <input type="hidden" value="0" name="Coupon[active]">
                        <label>
                            {{ check_field("Coupon[active]",
                            "value":"1",
                            "class":"icheck",
                            (coupon.active== "1" ? 'checked' : '')) }} {{ 'Enabled' | t }}</label>
                    </div>
                    <div class="checkbox-inline">
                        <input type="hidden" value="0" name="Coupon[promoted]">
                        <label>
                            {{ check_field("Coupon[promoted]",
                            "value":"1",
                            "class":"icheck",
                            (coupon.promoted== "1" ? 'checked' : '')) }} {{ 'Promoted' | t }}</label>
                    </div>
                    {% if allowShare %}
                        <div class="checkbox-inline">
                            <input type="hidden" value="0" name="Coupon[shared]">
                            <label>
                                {{ check_field("Coupon[shared]",
                                "value":"1",
                                "class":"icheck",
                                (coupon.shared== "1" ? 'checked' : '')) }} {{ 'Shareable' | t }}</label>
                        </div>
                    {% endif %}
                    <div class="checkbox-inline">
                        <input type="hidden" value="0" name="Coupon[public]">
                        <label>
                            {{ check_field("Coupon[public]",
                            "value":"1",
                            "class":"icheck",
                            (coupon.public== "1" ? 'checked' : '')) }}  {{ 'Public' | t }}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-tags marginLeft"></i> Header</div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist">
                {% for label,lang in langs %}
                    <li role="presentation" class="langTab fr {% if loop.index0 is 0 %}active{% endif %}" data-lang="{{ lang }}">
                        <a href="#coupon_{{lang}}" aria-controls="coupon_{{lang}}" role="tab" data-toggle="tab">{{ label | t }}</a></li>
                {% endfor %}
            </ul>

            <div class="tab-content">
                {% for i in langs %}
                    {{ tabpanel( i, couponLang ) }}
                {% endfor %}
            </div>
        </div>
    </div>
    {{ buttons( baseUrl ) }}
</form>
{{ modal( coupon, img, baseUrl ) }}