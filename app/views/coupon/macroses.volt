{%- macro tabpanel( i, couponLang ) %}
    <div role="tabpanel" id="coupon_{{i}}" class="tab-pane langTabData
        {% if i is 'he' %}active{% endif %}
        {% if router.getActionName() is 'view' %} readOnly{% endif %}">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-group"></div>
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="CouponLang[{{ i }}][title]">{{ "Title" | t }}*</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <input class="form-control couponTitle"
                                           {% if couponLang[i] is defined
                                           %}value="{{ couponLang[i]['title'] }}"
                                            {% endif %}
                                           type="text"
                                           name="CouponLang[{{ i }}][title]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group">
                            <label for="CouponLang[{{ i }}][sub_title]">{{ "SubTitle" | t }}</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <input class="couponSubTitle form-control" type="text"
                                           name="CouponLang[{{ i }}][sub_title]"
                                           {% if couponLang[i] is defined
                                           %}value="{{ couponLang[i]['sub_title'] }}" {%
                                    endif
                                    %}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="CouponLang[{{ i }}][details]">{{ "Details" | t }}</label>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                                    <textarea class="form-control couponDetails"
                                                              name="CouponLang[{{ i }}][details]">
                                                        {% if couponLang[i] is defined %}{{ couponLang[i]['details'] }}{% endif %}
                                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group">
                            <label for="CouponLang[{{ i }}][disclaimer]">{{ "Disclaimer" | t }}</label>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                                    <textarea class="form-control couponDisclaimer"
                                                              name="CouponLang[{{ i }}][disclaimer]">
                                                        {% if couponLang[i] is defined %}{{  couponLang[i]['disclaimer'] }}{% endif %}
                                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{%- endmacro %}

{%- macro modal( coupon, img, baseUrl ) %}
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header previewHeader">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img class="modal-title" id="myModalLabel" src="{{baseUrl}}img/MHLogo.png">
            </div>
            <div class="modal-body">
                <div id="couponPreview" class="preview">


                    <h1 id="previewTitle">{{ coupon.title }}</h1>
                    <h3 id="previewSubTitle">{{ coupon.sub_title }}</h3>
                    <figure>
                        <img class="img-responsive" id="couponPreviewImage" {%  if img is defined %}src="{% if img is not empty %}{{ img }}{% endif %}"{% endif %}>
                    </figure>
                    <h3 id="previewDetails">{{ coupon.details }}</h3>
                    <h4 id="previewDisclaimer">{{ coupon.disclaimer }}</h4>


                </div>
            </div>
            <div class="modal-footer previewFooter">
                <img src="{{baseUrl}}img/couponPreviewFooter.png">
            </div>
        </div>
    </div>
</div>
{%- endmacro %}

{%- macro buttons( baseUrl ) %}
{% if router.getActionName() is "view" %}
    <a href="{{ baseUrl }}coupon" class="btn btn-primary fr backToList">{{ "BackToList" | t }}</a>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="couponPreviewButton">{{ "Preview" | t }}</button>
{% else %}
    <input class="btn btn-primary fl" type="submit" value="{{ 'Save' | t }}" >
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="couponPreviewButton">{{ "Preview" | t }}</button>
    <a href="{{ baseUrl }}coupon" class="btn btn-primary fr backToList">{{ "Abort" | t }}</a>
{% endif %}
{%- endmacro %}