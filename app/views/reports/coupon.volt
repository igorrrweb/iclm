{#<p class="reportTitle h1">{% if coupon %}{{ coupon.name }}{% endif %}</p>#}
<div id="couponSegmentReport">
    <div class="row">
        <div class="col-md-12">
            <h3>{{ "CouponStartDate" | t }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="fr active">
                    <a href="#coupon_tab" aria-controls="coupon_tab" role="tab" data-toggle="tab">Owerview</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" id="coupon_tab" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"></div>
                            <div class="form-group">
                                <label class="sr-only" for="select_start_date"></label>
                                <div class="input-group">
                                    <input type="text" class="form-control date-range-choices selectStartDate"
                                           id="select_start_date"
                                           aria-describedby="select_start_dateStatus"
                                            data-startdate="{{ start }}"
                                            data-enddate="{{ end }}"
                                            value="{{ start~' - '~end }}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        {% if dates != "false" %}
            <div class="chartContainer col-md-12" data-labels='{{ dates }}' data-values='{{ data }}' data-min="{{ minDate }}" data-max="{{ maxDate }}">
                {#<canvas class="reportChart"></canvas>#}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ "CouponStartDate" | t }}
                    </div>
                    <div class="panel-body">
                        <div class="flot-chart">
                            <div class="flot-chart-content" id="flot-line-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        {% else %}
            <p id="startDateNotifier" class="h1">{{ "NoStartDateText" | t }}</p>
        {% endif %}
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table id="dataTable" class="table table-striped table-bordered base-data">
                <thead>
                <tr>
                    <th></th>
                    <th>{{ "SegmentName" | t }}</th>
                    <th>% {{ "OfGoalReached" | t }}</th>
                    <th># {{ "Redeemed" | t }}</th>
                    <th>% {{ "Abandoned" | t }}</th>
                    <th># {{ "Presented" | t }}</th>
                    <th>% {{ "Redemption" | t }}</th>
                </tr>
                </thead>
                <tbody>
                {% for val in couponStats %}
                    <tr>
                        <td></td>
                        <td>{{ val['segment'] }}</td>
                        <td>{{ val['reached'] | roundShort }}%</td>
                        <td>{{ val['redeemed'] }}</td>
                        <td>{{ val['abandoned'] | roundShort }}%</td>
                        <td>{{ val['presented'] }}</td>
                        <td>{{ val['redemption'] | roundShort }}%</td>
                    </tr>
                {% endfor %}
                    <tr>
                        <td id="tableTotal">{{ "Total" | t }}</td>
                        <td>{{ couponStats|length }}</td>
                        <td>{{ total['reached'] | roundShort }}%</td>
                        <td>{{ total['redeemed'] }}</td>
                        <td>{{ total['abandoned'] | roundShort }}%</td>
                        <td>{{ total['presented'] }}</td>
                        <td>{{ "Min" | t }}: {{ total['minRedemption'] | roundShort }}% {{ "Max" | t }}: {{ total['maxRedemption'] | roundShort }}%</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
