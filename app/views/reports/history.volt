{{ partial("reusables/macroses", []) }}
<div id="reportsHistory">
    <form id="indexForm" action="{{baseUrl}}reports/history" method="post">
        <div class="row">
            <div class="col-md-12">
                <h3>{{"Customer's Purchase History"}}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3 class="pull-left">{{'CustomerName' | t}} {% if customerData is not empty %}{{ customerData['firstname'] }} {{ customerData['lastname'] }}{% endif %}</h3>
                <h3 class="pull-right">{{'CustomerId' | t}} {% if customerData is not empty %}{{customerData['id']}}{% endif %}</h3></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="" class="">{{ "LC NUMBER" }}: </label>
                            <div class="form-inline">
                                <div class="form-group autocomplete-parent">
                                    <label for="" class="sr-only"></label>
                                    <input type="number" class="form-control"
                                           name="filterValueLcNumber"
                                           value="{% if filterValueLcNumber is not empty %}{{ filterValueLcNumber }}{% endif %}">
                                    <ul class="autoComplete" data-message="{{'NoMatches' | t}}"></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                        <div class="form-group">
                            <label for="reportFilter2" class="">{{ "USER ID" }}: </label>
                            <div class="form-inline">
                                <div class="form-group autocomplete-parent">
                                    <label for="" class="sr-only"></label>
                                    <input class="form-control" type="number" name="filterValueUserId"
                                           value="{% if filterValueUserId is not empty %}{{ filterValueUserId }}{% endif %}">
                                    <ul class="autoComplete" data-message="{{'NoMatches' | t}}"></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                        <div class="form-group">
                            <label for=""> </label>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <button type="submit" class="btn btn-primary get-report">{{ "Apply" }}</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                {{ datatable( attributes ) }}
            </div>
        </div>

        
    </form>

</div>