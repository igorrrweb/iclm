{{ partial("reusables/morrisMacro", []) }}
{{ partial("reusables/macroses", []) }}
{% set labels = ["ThisWeek" | t, "WeekAgo" | t, "TwoWeeksAgo" | t, "ThreeWeeksAgo" | t ] %}
{% set deactivatedLabels = ["ExpiredCoupons" | t, "ReachedTheLimit" | t, "DeactivatedManually" | t ] %}
<div id="couponReportsList">
    <div class="row">
        <div class="col-md-3">
            {{ bar_chart( 'CouponsAddedWeek', "CouponsAddedWeek", couponsByWeek, (labels | json_encode) ) }}
        </div>
        <div class="col-md-3">
            {{ bar_chart( 'CouponsDeactivatedWeek', "CouponsDeactivatedWeek", (deactivated | json_encode), (deactivatedLabels | json_encode) ) }}
        </div>
        <div class="col-md-3">
            {{ bar_chart( 'CouponsRedeemedWeek', "CouponsRedeemedWeek", redeemedWeekly, (labels | json_encode) ) }}
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ "ActiveCoupons" | t }}
                </div>
                <div class="panel-body">
                    <div id="morris-donut-chart" data-activecoupons="{{ activeCoupons }}" data-totalcoupons="{{ totalCoupons }}"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="" class="sr-only"></label>
                    <select class="select2 category" name="Search[categoryID]">
                        <option value="">{{ "Category" | t }}: {{ "All" | t }}</option>
                        {% for category in categories %}
                            <option value="{{ category.id }}" {% if search['categoryID'] is defined and search['categoryID'] is category.id %}
                            selected {% endif %}>{{ category.category }}</option>
                        {% endfor %}
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="" class="sr-only"></label>
                    <select class="select2 product" name="Search[productID]">
                        <option value="">{{ "Product" | t }}: {{ "All" | t }}</option>
                        {% for product in products %}
                            <option value="{{ product.id }}" {% if search['productID'] is defined and search['productID'] is product.id %}selected{% endif %}>
                                {{ product.name }}
                            </option>
                        {% endfor %}
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {{ datatable( attributes ) }}
        </div>
    </div>
</div>