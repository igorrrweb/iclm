{%- macro usersreports_table( title ) %}
    <table class="table table-striped table-bordered table-hover">
        <thead></thead>
        <tbody>
        <tr>
            <td class="text-center">{{ title }}</td>
        </tr>
        </tbody>
        <tfoot></tfoot>
    </table>
{%- endmacro %}

