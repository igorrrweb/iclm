{{ partial("reports/macroses", []) }}
<div id="usersreports">
    <form id="indexForm" action="{{baseUrl}}reports/usersreports" method="post">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ "FilterBy" | t }}</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="">{{ "LC NUMBER" }}: </label>
                                    <div class="form-inline">
                                        <div class="form-group autocomplete-parent">
                                            <label for="" class="sr-only"></label>
                                            <input type="number" class="form-control"
                                                   name="filterValueLcNumber"
                                                   value="{% if filterValueLcNumber is not empty %}{{ filterValueLcNumber }}{% endif %}">
                                            <ul class="autoComplete" data-message="{{'NoMatches' | t}}"></ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group hidden-xs">
                                    <div class="col-md-1"></div>
                                </div>
                                <div class="form-group">
                                    <label for="reportFilter2" class="">{{ "USER ID" }}: </label>
                                    <div class="form-inline">
                                        <div class="form-group autocomplete-parent">
                                            <label for="" class="sr-only"></label>
                                            <input class="form-control" type="number" name="filterValueUserId"
                                                   value="{% if filterValueUserId is not empty %}{{ filterValueUserId }}{% endif %}">
                                            <ul class="autoComplete" data-message="{{'NoMatches' | t}}"></ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                <div class="form-group">
                                    <label for="reportFilter2" class="">{{ "Segment Name" }}: </label>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <select name="filterValueSegmentName" class="select2 select2-offscreen"
                                                    id="">
                                                <option {% if filterValueSegmentName is empty %} {{ 'selected' }} {% endif %}
                                                        value="">{{ "PleaseChooseASegment" | t }}</option>
                                                {% for segment in segments %}
                                                    <option {% if (filterValueSegmentName is segment['id']) %} {{ 'selected' }} {% endif %}
                                                            value="{{ segment['id'] }}">{{ segment['name'] }}</option>
                                                {% endfor %}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                <div class="form-group">
                                    <label for="">{{ "Time Range" }}</label>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <input id="select_start_date" type="text"
                                                   class="form-control dateChoices selectStartDate"></div>
                                    </div>
                                </div>
                                <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                <div class="form-group">
                                    <label for=""> </label>

                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <button type="submit"
                                                    class="btn btn-primary get-report">{{ "Apply" }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sx-12 col-md-12">
                <h1>{{ "Define Reports Time Range" }}: </h1>
            </div>
            <div class="col-sx-12 col-md-12">
                <hr class="users-hr">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="col-xs-12 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ "Visits with purchase" }}</div>
                        <div class="panel-body">
                            {{ usersreports_table( visitsWithPurchase ) }}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ "Visits without purchase" }}</div>
                        <div class="panel-body">
                            {{ usersreports_table( visitsWithoutPurchase ) }}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ "Passed By without entering" }}</div>
                        <div class="panel-body">
                            {{ usersreports_table( passedByWithoutEntering ) }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-md-12">
                <div class="col-xs-12 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ "Average Time Per Visits" }}</div>
                        <div class="panel-body">
                            {{ usersreports_table( averageTimePerVisits~' Mins '~avgSign ) }}
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ "Visited on" }}</div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <thead></thead>
                                <tbody>
                                {% set weekdays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"] %}
                                {% for weekday in weekdays %}
                                    <tr>
                                        <td>{{ weekday }}</td>
                                        <td align="center">
                                            {% for visited in visitedOn %}
                                                {% if visited['dayweek'] == weekday %}
                                                    <span>{{ visited['count_visits'] }}</span>
                                                {% endif %}
                                            {% endfor %}
                                        </td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ "Passed by on" }}</div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <thead></thead>
                                <tbody>
                                {% set weekdays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"] %}
                                {% for weekday in weekdays %}
                                    <tr>
                                        <td>{{ weekday }}</td>
                                        <td align="center">
                                            {% for passed in passedByOn %}
                                                {% if passed['dayweek'] == weekday %}
                                                    {{ passed['count_visits'] }}
                                                {% endif %}
                                            {% endfor %}
                                        </td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <input type="hidden" name="startDate" class="startDate" value="{{ start | date }}">
        <input type="hidden" name="endDate" class="endDate" value="{{ end | date }}">
    </form>
</div>

