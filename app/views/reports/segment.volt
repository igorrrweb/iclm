<div id="segmentReport">
    <div class="row">
        <div class="col-md-12">
            <h1 class="reportTitle">{{ segment.name }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="">{{ "SelectDateRange" | t }}</label>
                <input id="select_start_date" type="text"
                       class="form-control dateChoices selectStartDate"
                       data-target="branches"
                       value=""
                       data-startdate="{{ start }}"
                       data-enddate="{{ finish }}">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="chartContainer" data-labels="[0,0]" data-values="[0,0]" data-legend="">
                {#<canvas class="reportChart"></canvas>#}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 id="chartTitle">
                            <span class="chart-title-span">{{ "CouponRedemption" | t }}</span> Start Date: <span class='date-in-title' id='start-date'></span> End Date: <span class='date-in-title' id='end-date'></span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="flot-chart">
                            <div class="flot-chart-content" id="flot-line-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <ul class="nav nav-tabs segment-tab" role="tablist">
                        <li role="presentation" class="active fr">
                            <a href="#branches" aria-controls="branches" role="tab" data-toggle="tab"
                               class="segmentReportTab">{{ "Branches" | t }}</a>
                        </li>
                        <li role="presentation" class="fr">
                            <a href="#engagements" aria-controls="engagements" role="tab" data-toggle="tab"
                               class="segmentReportTab">{{ "Engagements" | t }}</a>
                        </li>
                        <li role="presentation" class="fr">
                            <a href="#coupons" aria-controls="coupons" role="tab" data-toggle="tab"
                               class="segmentReportTab">{{ "Coupons" | t }}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="branches" role="tabpanel" class="tab-pane active">
                            <div class="form-group"></div>
                            <table class="table table-bordered segmentReportTable">
                                <thead>
                                <tr>
                                    <th>{{ "Branch" | t }}</th>
                                    <th>{{ "TotalVisits" | t }}</th>
                                    <th>{{ "AverageDailyVisits" | t }}</th>
                                    <th>{{ "AverageMemberVisits" | t }}</th>
                                    <th>{{ "CouponRedemption" | t }}</th>
                                    <th>{{ "EngagementsCompleted" | t }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {% if (branches | length) > 0 %}
                                {% for branch in branches %}
                                    <tr>
                                        {% set dailyVisitors = BranchVisits.getVisitorsByDays(segment.id,branch.id,30) %}
                                        <td>{{ branch.branch_name }}</td>
                                        <td>{{ dailyVisitors }}</td>
                                        <td>{{ (dailyVisitors / 30) | roundShort }}%</td>
                                        <td>{{ (BranchVisits.getAverageMemberVisits(segment.id,branch.id,30)) | roundShort }}
                                            %
                                        </td>
                                        <td>{{ segment.getRedemptionRateByBranch(branch.id,30) }}%</td>
                                        <td></td>
                                    </tr>
                                {% endfor %}
                                {% else %}
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                {% endif %}
                                </tbody>
                            </table>
                        </div>
                        <div id="engagements" role="tabpanel" class="tab-pane">
                            <div class="form-group"></div>
                            <table class="table table-bordered segmentReportTable">
                                <thead>
                                <tr>
                                    <th>{{ "Type" | t }}</th>
                                    <th>{{ "EngagementName" | t }}</th>
                                    <th># {{ "Completed" | t }}</th>
                                    <th>% {{ "Redemption" | t }}</th>
                                    <th>{{ "CouponRedemption" | t }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {% if (engagements | length) > 0 %}
                                {% for engagement in engagements %}
                                    <tr>
                                        {% set coupon = engagement.getRelated("Coupon") %}
                                        <td>{{ engagement.type | t }}</td>
                                        <td>{{ engagement.name }}</td>
                                        <td>{{ engagement.completed }}</td>
                                        <td>{{ engagement.getRedemptionPercent() | roundShort }}%</td>
                                        <td>{% if coupon %} {{ coupon.getRedeemedPercent() | roundShort }} {% else %} {{ '0' }} {% endif %}
                                            %
                                        </td>
                                    </tr>
                                {% endfor %}
                                {% else %}
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                {% endif %}
                                </tbody>
                            </table>
                        </div>
                        <div id="coupons" role="tabpanel" class="tab-pane">
                            <div class="form-group"></div>
                            <table class="table table-bordered segmentReportTable">
                                <thead>
                                <tr>
                                    <th>{{ "Coupon" | t }}</th>
                                    <th>{{ "Type" | t }}</th>
                                    <th>% {{ "OfGoalReached" | t }}</th>
                                    <th># {{ "Presented" | t }}</th>
                                    <th># {{ "Pending" | t }}</th>
                                    <th># {{ "Redeemed" | t }}</th>
                                    <th>% {{ "Abandoned" | t }}</th>
                                    <th>% {{ "Interest" | t }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {% if (coupons | length) > 0 %}
                                {% for coupon in coupons %}
                                    <tr>
                                        <td>{{ coupon.name }}</td>
                                        <td>{{ coupon.getRelated("CouponType").coupon_type | t }}</td>
                                        <td>{{ coupon.getRedeemedPercent() | roundShort }}%</td>
                                        <td>{{ coupon.getTotalPresented() }}</td>
                                        <td>{{ coupon.total_pending }}</td>
                                        <td>{{ coupon.total_redeemed }}</td>
                                        <td>{{ coupon.getAbandonedPercent() | roundShort }}%</td>
                                        <td>{{ coupon.getInterestPercent() | roundShort }}%</td>
                                    </tr>
                                {% endfor %}
                                {% else %}
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                {% endif %}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




</div>
