<div class="row">
    <div class="col-md-12">
        <h1>{{ "FeedbackFormName" | t }}: {{ form }}</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="form-inline">
                <div class="form-group">
                    <label for="" class="sr-only"></label>
                    <p class="form-control-static">{{ "MemberID" | t }}: {{ user.id }}</p>
                </div>
                <div class="form-group"><div class="hidden-xs col-md-2"></div></div>
                <div class="form-group">
                    <label for="" class="sr-only"></label>
                    <p class="form-control-static">{{ "MemberName" | t }}: {{ user.firstname }} {{ user.lastname }}</p>
                </div>
                <div class="form-group"><div class="hidden-xs col-md-2"></div></div>
                <div class="form-group">
                    <label for="" class="sr-only"></label>
                    <p class="form-control-static">{{ "Submitted" | t }}: {{ submitted }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <h1>{{ "Feedback" | t }}:</h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <ol>
                {% for feedbackResponse in responses %}
                <li>
                    <div class="form-inline">
                        <label>{{ "Question" | t }}:</label>
                        <p class="form-control-static">{{
                        feedbackResponse.getRelated("FeedbackQuestion").title }}</p>
                    </div>
                    <div class="form-inline">
                        <label>{{ "Answer" | t }}:</label>
                        <p class="form-control-static">{{ feedbackResponse.response }}</p>
                    </div>
                </li>
                {% endfor %}
            </ol>
        </div>
    </div>
</div>
<div id="row">
    <div class="col-md-12">
        <div class="form-group">
            <a href="{{baseUrl}}userfeedbacks" class="btn btn-primary backToList">{{ "BackToList" | t }}</a>
        </div>
    </div>
</div>