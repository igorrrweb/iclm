<?php
use \Phalcon\Mvc\View;


class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setMainView('loginscreen');
        parent::initialize();
        $this->assetsHeaderCss->addCss('css/main.css');
        $this->assetsFooter->addJs('js/login.js');
    }

    public function redirectAction($destroySession=false) {
        if($destroySession) {
            session_regenerate_id(true);
            session_destroy();
        }

        $this->response->redirect(self::getDI()['url']->getBaseUri()."dashboard");
        exit;
    }
    public function getTokenPathAction(){
        return dirname(__FILE__)."/../../public/tokens";
    }


    public function IndexAction()
    {

        if ($this->user)
        {
            $this->response->redirect(self::getDI()['url']->getBaseUri()."dashboard");
        }

        $request = new \Phalcon\Http\Request();
        $username = $request->getPost("username");

        if($this->cookies->has('remember-me')) {
            $cookie_username = $this->cookies->get('remember-me')->getValue();
            $user = SystemUser::findFirst(["username = :username:", "bind" => ["username" => $cookie_username]]);
            $this->session->set("user", serialize($user));
            $user->last_login = date("Y-m-d H:i:s");
            $user->created_at = SystemUser::toSQLDate($user->created_at);
            $user->setLogging(false);
            $user->save();
            $this->response->redirect(self::getDI()['url']->getBaseUri()."dashboard");
        } else {

            if ($username != "")
            {
                $user = SystemUser::findFirst(["username = :username:", "bind" => ["username" => $username]]);

                if (!$user)
                {
                    die("false");
                } elseif ($user->password != md5($request->getPost("password"))){
                    die("false");
                } else {
                    $this->session->set("user", serialize($user));
                    $user->last_login = date("Y-m-d H:i:s");
                    $user->created_at = SystemUser::toSQLDate($user->created_at);
                    $user->setLogging(false);
                    $user->save();
                    if($request->getPost('rememberme')) {
                        $this->cookies->set('remember-me', $username, time() + 3600 * 24 * 90, '/');
                    }
                    else {
                        if($this->cookies->has('remember-me')) {
                            $this->cookies->get('remember-me')->delete();
                        }
                    }
                    echo self::getDI()['url']->getBaseUri()."dashboard";
                }
                $this->view->disable();
            }
            $this->view->setTemplateAfter('loginscreen');
        }

    }


    public function forgotPasswordAction()
    {
        $criteria = ["conditions" => "email = ?1", "bind" => [1 => $this->request->getPost("userEmail")]];
        $user = SystemUser::findFirst($criteria);

        if ($user)
        {
            $user->token = bin2hex(openssl_random_pseudo_bytes(16));
            $user->created_at = SystemUser::toSQLDate($user->created_at);
            $user->setLogging(false);
            $user->save();

            $mail = new MailManager();
            $mail->send($user->email, "Password Reset",$this->view->getRender("mail","resetPassword", ["token" => $user->token]));
            echo "true";
        } else {
            echo "false";
        }

        $this->view->disable();
    }

    public function resetPasswordAction()
    {
        $user = SystemUser::findFirst(["conditions" => "token = ?1", "bind" => [1 => $this->request->get("token")]]);

        if (!$user)
        {
            $this->response->redirect(self::getDI()['url']->getBaseUri()."index");
        }

        if ($this->request->getPost("passwordReset") != "")
        {
            $user->password = $this->request->getPost("passwordReset");
            $user->created_at = SystemUser::toSQLDate($user->created_at);
            $user->save();
            $this->response->redirect(self::getDI()['url']->getBaseUri()."index");
        }

        $this->view->setTemplateAfter("loginscreen");
    }

    public function logoutAction()
    {
        if($this->cookies->has('remember-me')) {
            $this->cookies->get('remember-me')->delete();
        }
        $this->session->remove("user");
        $this->response->redirect(self::getDI()['url']->getBaseUri()."index");
    }

    public function switchLangAction()
    {
        if ($this->user)
        {
            $this->user->lang = $this->request->getPost("lang");
            $this->user->save();
            $this->session->set("user",serialize($this->user));
        }

        $this->session->set("lang",$this->request->getPost("lang"));
        $this->view->disable();
    }

    public function notFoundAction()
    {
        $this->view->setMainView("index");
    }
}

