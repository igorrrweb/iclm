<?php

use \DataTables\DataTable;

class ReportsController extends ControllerBase {

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss
            ->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css',false,false)
            ->addCss('//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js',false,false)
            ->addJs('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js',false,false)
            ->addJs('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js',false,false)
            ->addJs('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('//cdn.jsdelivr.net/momentjs/latest/moment.min.js',false,false)
            ->addJs('//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js',false,false)
            ->addJs('js/funcs.js');

        $this->modelName = 'Message';
    }

    public function indexAction()
    {
        $this->response->redirect("/reports/coupons");
    }

    public function segmentAction($id)
    {
        $this->assetsFooter
            ->addJs('bower_components/highcharts/highcharts.js')
            ->addJs('js/report-segment.js');
        $segment = Segment::findFirst((int)$id);

        $dateObj = new \DateTime();
        $finish = $dateObj->format('d/m/Y');
        $start = $dateObj->modify('-1 month')->format('d/m/Y');
        $this->view->setVars(
            [
                "segment" => $segment,
                "start" => $start ,
                "finish" => $finish,
                "coupons" => $segment->getRelated("Coupon"),
                "engagements" => $segment->getRelated("Engagement"),
                "branches" => Branch::find(),
                "BranchVisits" => new BranchVisits()
            ]
        );

    }
    private function getSegmentsAttributesAction()
    {
        return  [
            "Name",
            "Members",
            "CouponRedemption",
            "AvailableCoupons",
            "AvailableEngagements"
        ];
    }

    public function getSegmentsDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getSegmentReports($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function segmentsAction()
    {
        $this->assetsFooter
            ->addJs('js/report-segments.js');
        $this->view->setVars(
        [
            "attributes" => $this->getSegmentsAttributesAction(),
        ]);
    }

    private function getCouponsAttributesAction()
    {
        return  [
            "ID",
            "Name",
            "ProductCategory",
            "Audience",
            "CouponType",
            "Redeemed",
            "PercentageOfGoalReached"
        ];
    }

    public function getCouponsDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getReportCoupons($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function couponsAction()
    {
        $this->assetsFooter
            ->addJs('bower_components/raphael/raphael-min.js')
            ->addJs('bower_components/morrisjs/morris.min.js')
            ->addJs('bower_components/highcharts/highcharts.js')
            ->addJs('js/report-coupons.js');

        $redeemedWeekly = [];
        $couponsByWeek = [];
        $deactivated = [];

        $coupon = new Coupon();

        for ($i = 1; $i < 5;$i++)
        {
            $redeemedWeekly[] = CouponActivity::getRedeemedByWeek($i);
            $couponsByWeek[] = $coupon->getCouponsByWeek($i);
        }

        $deactivated[] = $coupon->getExpiredCoupons();
        $deactivated[] = $coupon->getReachedTheLimit();
        $deactivated[] = $coupon->getDeactivatedManually();
        $deactivatedThisWeek = $coupon->getDeactivatedThisWeek();

        $this->view->setVars(
            [
                "coupon" => $coupon,
                "couponsByWeek" => json_encode($couponsByWeek),
                "redeemedWeekly" => json_encode($redeemedWeekly),
                "activeCoupons" => Coupon::getActiveCoupons(),
                "totalCoupons" => Coupon::getTotalCoupons(),
                "categories" => Category::find(),
                "products" => Product::find(),
                "couponsRedeemedToday" => CouponActivity::getDailyRedeemed(),
                "totalCustomers" => Customer::count(),
                "deactivated" => $deactivated,
                "deactivatedThisWeek" => $deactivatedThisWeek,
                "attributes" => $this->getCouponsAttributesAction(),
            ]
        );

    }

    public function couponAction($id)
    {
        $this->assetsFooter
            ->addJs('bower_components/highcharts/highcharts.js')
            ->addJs('js/report-coupon.js');

        $data = [];
        $coupon = Coupon::findFirst((int)$id);
        $couponStats = [];
        $total = ['maxRedemption' => 0, 'minRedemption' => 0,'reached' => 0,'redeemed' => 0,
                  'abandoned' => 0, 'presented' => 0];

        if($coupon){
            foreach ($coupon->getRelated("Segment")->toArray() as $key=>$segment)
            {
                $redeemed = CouponActivity::getDataBySegment($segment["id"],(int)$id,"redeemed");
                $_presented = CouponActivity::getDataBySegment($segment["id"],(int)$id,"presented");
                $presented = $_presented != 0 ? $_presented : 1;

                $couponStats[$key]["segment"] = $segment["name"];
                $couponStats[$key]["reached"] = $redeemed / $coupon->max_coupons * 100 . "%";
                $couponStats[$key]["redeemed"] = $redeemed;

                $couponStats[$key]["abandoned"] =  (CouponActivity::getDataBySegment($segment["id"],(int)$id,"pending") -
                                                    $redeemed) / $presented * 100;

                $couponStats[$key]["presented"] = $presented;
                $couponStats[$key]["redemption"] = $redeemed / $presented * 100;

                $total["reached"] += $couponStats[$key]["reached"];
                $total["redeemed"] += $redeemed;
                $total["abandoned"] +=  $couponStats[$key]["abandoned"];
                $total["presented"] += $presented;

                $couponStats[$key]["redemption"] > $total['maxRedemption'] ?  $total['maxRedemption'] =
                $couponStats[$key]["redemption"] : "";

                $couponStats[$key]["redemption"] < $total['minRedemption'] || $total['minRedemption'] ===  0 ?
                $total['minRedemption'] = $couponStats[$key]["redemption"] : "";
            }

            switch (true)
            {
                case time() > BaseModel::toTime($coupon->expiry_date):
                    $dates = [substr($coupon->start_date,0,10),substr($coupon->expiry_date,0,10)];
                    break;
                case time() > BaseModel::toTime($coupon->start_date):
                    $startDate = time() - 2592000 > BaseModel::toTime($coupon->start_date) ?
                                 date("d/m/Y", time() - 2592000) : substr($coupon->start_date,0,10);

                    $dates = [$startDate,date("d/m/Y", time() - 86400)];
                    break;
                case time() < BaseModel::toTime($coupon->start_date):
                    $dates = [substr($coupon->start_date,0,10),substr($coupon->expiry_date,0,10)];
                    break;
            }
        }

        $data["presented"] = CouponActivity::getDataByDateRange((int)$id, $dates[0], $dates[1], "presented");
        $data["redeemed"] = CouponActivity::getDataByDateRange((int)$id, $dates[0], $dates[1], "redeemed");

        $this->view->setVars(
            [
                "id"=>$id,
                "coupon" => $coupon,
                "data" => json_encode($data),
                "start"=>$dates[0],
                "end"=>$dates[1],
                "dates" => json_encode(array_unique(array_merge($data['presented']['dates'],$data['redeemed']['dates']))),
                "segments" => $coupon ? $coupon->getRelated("Segment")->toArray() : [],
                "couponStats" => $couponStats,
                "total" => $total,
                "minDate" => (strtotime(date('d-m-Y')) - BaseModel::toTime($dates[0])) / 86400,
                "maxDate" => (BaseModel::toTime($dates[1]) - strtotime(date('d-m-Y'))) / 86400
            ]
        );
    }

    public function getCouponDataAction($id)
    {
        $this->view->disable();

        if ($this->request->getPost("dates"))
        {
            $dates = json_decode($this->request->getPost("dates"));
            $data = [
                "presented" => CouponActivity::getDataByDateRange((int)$id, $dates[0],$dates[1], "presented"),
                "redeemed" =>  CouponActivity::getDataByDateRange((int)$id, $dates[0],$dates[1], "redeemed")
            ];

            echo json_encode($data);
        }
    }

    public function getSegmentDataAction($id)
    {
        $this->view->disable();

        $segment = Segment::findFirst((int)$id);

        if ($this->request->getPost("dates"))
        {
            $dates = json_decode($this->request->getPost("dates"));
            if(count($dates)==1){
                $first_date = new DateTime($dates[0]);
                $dates[0] = $first_date->format('Y-m-01');
                $last_date = new DateTime();
                array_push($dates, $last_date->format('Y-m-t'));
            }
            $data = $segment->getSegmentCouponsRedemption($dates[0],$dates[1], $this->request->getPost("flag"));

            echo json_encode($data);
        }
    }

    public function getBranchesDataAction($id){
        $this->view->disable();
        $segmentCustomer = SegmentCustomer::findBySegment_id((int)$id);

        if ($this->request->getPost("dates")){
            $dates = json_decode($this->request->getPost("dates"));
            if(count($dates)==1){
                $first_date = new DateTime($dates[0]);
                $dates[0] = $first_date->format('Y-m-01');
                $last_date = new DateTime();
                array_push($dates, $last_date->format('Y-m-t'));
            }
            $data = $segmentCustomer->getStatisticsData($dates[0],$dates[1]);
        }

        echo json_encode($data);
    }

    public function getEngagementsDataAction($id){
        $this->view->disable();
        $data = 'getEngagementsData';
        echo json_encode($data);
    }

    public function usersReportsAction(){
        $this->assetsFooter
            ->addJs('js/report-usersreports.js');

        $criteria = array();
        $criteria['filterValueUserId'] = '';
        $criteria['filterValueLcNumber'] = '';
        $dateObj = new \DateTime();
        $endDate = $dateObj->format('d/m/Y');
        $startDate = $dateObj->modify('-1 month')->format('d/m/Y');

        if($this->request->getPost()){
            $startDate = $this->request->getPost('startDate');
            $endDate = $this->request->getPost('endDate');
            $criteria['filterValueUserId'] = $this->request->getPost('filterValueUserId');
            $criteria['filterValueLcNumber'] = $this->request->getPost('filterValueLcNumber');
            $criteria['filterValueSegmentName'] = $this->request->getPost('filterValueSegmentName');
        }

        $criteria['startDate'] = $startDate;
        $dateObj = new \DateTime(str_replace("/","-",$criteria['startDate']));
        $criteria['startDate'] = $dateObj->format('Y-m-d');

        $criteria['endDate'] = $endDate;
        $dateObj = new \DateTime(str_replace("/","-",$criteria['endDate']));
        $criteria['endDate'] = $dateObj->format('Y-m-d');
        $avgTimePerVisits1 = CRUDQueries::getAverageTimePerVisits($criteria)[0]->toArray();
        $avgTimePerVisits2 = CRUDQueries::getAverageTimePerVisits($criteria)[1]->toArray();

        $sign = $avgTimePerVisits1[0]['duration']<$avgTimePerVisits2[0]['duration']
            ? '<span class="glyphicon glyphicon-arrow-up text-success" aria-hidden="true"></span>'
            : '<span class="glyphicon glyphicon-arrow-down text-danger" aria-hidden="true"></span>';

        $data = [
            'filterValueLcNumber'       => $this->request->hasPost('filterValueLcNumber') ? $this->request->getPost('filterValueLcNumber') : '',
            'filterValueUserId'         => $this->request->hasPost('filterValueUserId') ? $this->request->getPost('filterValueUserId') : '',
            'filterValueSegmentName'    => $this->request->hasPost('filterValueSegmentName') ? $this->request->getPost('filterValueSegmentName') : '',
            'start'                     => $startDate,
            'end'                       => $endDate,
            'segments'                  => Segment::find()->toArray(),
            'visitsWithPurchase'        => CRUDQueries::getUsersVisitsWithPurchase($criteria)->toArray()[0]['count_customers'],
            'visitsWithoutPurchase'     => CRUDQueries::getVisitsWithoutPurchase($criteria)->toArray()[0]['count_customers'],
            'passedByWithoutEntering'   => CRUDQueries::getPassedByWithoutEntering($criteria)->toArray()[0]['count_customers'],
            'averageTimePerVisits'      => $avgTimePerVisits2[0]['duration'],
            'avgSign'                   => $sign,
            'visitedOn'                 => CRUDQueries::getVisitedOn($criteria)->toArray(),
            'passedByOn'                => CRUDQueries::getPassedByOn($criteria)->toArray()
        ];

        $this->view->setVars($data);
    }

    private function getHistoryAttributesAction()
    {
        return  [
            "Date",
            "Duration",
            "Amount",
            "CouponsRedeemed"
        ];
    }

    public function getHistoryDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getPurchaseHistory($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function historyAction($id=null){
        $this->assetsFooter
            ->addJs('js/report-history.js');

        $this->modelName = "BranchVisits";
        $filterValueUserId = $id ? $id : ($this->request->hasPost('filterValueUserId') ? $this->request->getPost('filterValueUserId') : '');
        $customerData = $filterValueUserId ? Customer::findFirst($filterValueUserId)->toArray() : '';
        $this->view->setVars(
            [
                'customerData'          => $customerData,
                'filterValueLcNumber'   => $this->request->hasPost('filterValueLcNumber') ? $this->request->getPost('filterValueLcNumber') : '',
                'filterValueUserId'     => $filterValueUserId,
                "attributes"            => $this->getHistoryAttributesAction(),
            ]);


    }

    public function autoCompleteAction()
    {
        $this->view->disable();
        $criteria = array();
        $column = $this->request->hasPost('filterValueUserId') ? 'id' :
            ($this->request->hasPost('filterValueLcNumber') ? 'lc_number' : false);
        $value = $this->request->hasPost('filterValueUserId') ? $this->request->getPost('filterValueUserId') :
            ($this->request->hasPost('filterValueLcNumber') ? $this->request->getPost('filterValueLcNumber') : false);
        $addOnCondition = '';
        $extraColumn = $this->request->getPost("extracolumn");
        $extraValue = null !== $this->request->getPost("extravalue") ? $this->request->getPost("extravalue") : false;
        if($extraValue){
            switch($extraColumn){
                case 'UserId':
                    $addOnCondition = " AND id LIKE '%" . addslashes($extraValue) . "%' ";
                    break;
                case 'LcNumber':
                    $addOnCondition = " AND lc_number LIKE '%" . addslashes($extraValue) . "%' ";
                    break;
                default:
                    $addOnCondition = '';
                    break;
            }
        }
        $criteria['columns'] = $column.' as user ';
        $criteria['conditions'] = $column." LIKE '%" . addslashes($value) . "%' ".$addOnCondition;
        $criteria['limit'] = 5;

        $searchedColumn = Customer::find($criteria)->toArray();

        echo json_encode($searchedColumn);

    }


}