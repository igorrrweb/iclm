<?php

use \DataTables\DataTable;

class BeaconController extends ControllerBase {


    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('js/beacon.js');
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }

    private function getAttributesAction()
    {
        return  [
            "ID",
            "Name",
            "Branch",
            "InStoreLocation",
            "LastHealthCheck",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getBeacon($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }
    public function viewAction($id)
    {
        $beacon = Beacon::findFirst((int)$id)->toArray();
        $beacon["tags"] = $beacon["tags"] == "[]" ? "---" : implode(json_decode($beacon["tags"]));
        $this->view->beacon = $beacon;
        $this->view->pick("beacon/view");
    }



}