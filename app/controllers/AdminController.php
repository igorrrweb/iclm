<?php

class AdminController extends ControllerBase {

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('js/validators.js')
            ->addJs('js/funcs.js')
            ->addJs('js/admin.js');

        //$this->modelName = '';
    }

    public function indexAction()
    {

    }

    public function termsofserviceAction()
    {
        $tos = [];
        foreach (Pages::find("content_type = 'tos'")->toArray() as $val)
        {
            $tos[$val['lang']] = $val;
        }

        if ($this->request->getPost("tos"))
        {
            $i = 0;
            foreach ($this->request->getPost("tos") as $lang => $val)
            {
                if (isset($tos[$lang]))
                {
                    $tosModel = Pages::findFirst((int)$tos[$lang]["id"]);
                    $tosModel->body_text = $val;
                    $tosModel->save();
                } else {
                    $newTos = new Pages();
                    $newTos->content_type = "tos";
                    $newTos->body_text = $val;
                    $newTos->lang = $lang;
                    $newTos->save();
                }

                $i++;
            }

            $this->response->redirect(self::getDI()['url']->getBaseUri()."/admin/termsofservice");
        }

        $this->view->tos = $tos;
        $this->view->langs =  self::getDI()['settings']['availableLangs'];
    }

    public function settingsAction()
    {
        $settings = [];

        foreach (SystemSettings::find()->toArray() as $row)
        {
            $settings[$row['setting']] = $row['value'];
        }

        if ($this->request->getPost("Settings"))
        {
            $post = $this->request->getPost("Settings");
            $post["supported_langs"] = json_encode($post["supported_langs"]);

            foreach ($post as $setting => $value)
            {
                if (isset($settings[$setting]))
                {
                    $sysSettings = SystemSettings::findFirstBySetting($setting);
                } else {
                    $sysSettings = new SystemSettings();
                    $sysSettings->setting = $setting;
                }

                $sysSettings->value = $value;
                $sysSettings->save();
            }

            $this->response->redirect(self::getDI()['url']->getBaseUri()."admin");
        }

        $this->view->settings = $settings;
        $this->view->langs =  self::getDI()['settings']['availableLangs'];
    }
}