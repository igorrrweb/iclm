<?php

use \DataTables\DataTable;

class SystemuserController extends ControllerBase {

    private $systemUser;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('js/funcs.js')
            ->addJs('js/validators.js')
            ->addJs('js/systemuser.js');

        $this->modelName = "SystemUser";
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }

    private function getAttributesAction()
    {
        return  [
            "ID",
            "Username",
            "Role",
            "Added",
            "LastLogin",
            "Status",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getSystemUser($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function createAction()
    {
        $this->loadModels();

        if ($this->request->getPost("SystemUser"))
        {
            $this->saveModel(true);
        }
    }

    public function updateAction($id)
    {
        $this->loadModels($id);

        if ($this->request->getPost("SystemUser"))
        {
            $this->saveModel();
        }
    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }

    public function saveModel($flag=false)
    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();

            $this->systemUser->setTransaction($transactions);
            $values = $this->request->getPost("SystemUser");
            if($flag){
                $values['password'] =  md5($values['password']);
            }

            if (!$this->systemUser->save($values)){
                foreach ($this->systemUser->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }

            if($transactions->commit()){
                $this->flashSession->success("Your information was stored correctly!");
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $this->flashSession->error($e->getMessage());
            $this->response->redirect($this->theBaseUrl.$this->linkName);
        }

    }

    public function deleteAction(){
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $this->view->disable();
            $id = $this->request->getPost('id');
            $model = $this->modelName;
            $instance = array();
            $instance['status'] = false;
            $modelObj = $model::findFirst((int)$id);
            $modelObj->setTransaction($transactions);
            if (!$modelObj->delete()){
                foreach ($modelObj->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }
            if($transactions->commit()){
                $instance['status'] = true;
            }
            echo json_encode($instance);
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $instance['error'][] = $e->getMessage();
            echo json_encode($instance);
        }
    }

    public function getEventHistoryAction($userID)
    {
        $this->view->disable();

        $page = $this->request->get("page") ?  $this->request->get("page") : 1;

        $paginator = new \Phalcon\Paginator\Adapter\Model(
            [
                "data" => SystemUser::findFirst((int)$userID)->getRelated("UserLogs"),
                "limit" => 10,
                "page" =>  $page
            ]
        );

        echo json_encode($paginator->getPaginate());
    }

    protected function loadModels($id = null)
    {
        !$this->request->getPost() ? parent::loadModels() : "";
        $this->view->systemUser = $this->systemUser = $id === null ? new SystemUser : SystemUser::findFirst((int)$id);
    }

    public function validateNameAction(){
        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        $result = $model::findFirst(
            array(
                "conditions"=>"username = ?1",
                "bind" => array(
                    1 => $this->request->getPost('name')
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }

    public function activateAction()
    {
        $this->view->disable();

        $model = $this->modelName;

        $updatedModel = $model::findFirst((int)$this->request->getPost("item-id"));

        $updatedModel->active = $this->request->getPost("active");

        $updatedModel->created_at = BaseModel::toSQLDate($updatedModel->created_at);

        $updatedModel->save();
    }

    public function changePasswordAction(){
        $response = 'false';
        $this->view->disable();
        $changePassword = $this->request->getPost();
        $systemUser = SystemUser::findFirst(array($changePassword['systemUserId']));
        $systemUser->password = md5($changePassword['password']);
        if($systemUser->save()){
            $response = 'true';
        }
        echo json_encode($response);
    }

}