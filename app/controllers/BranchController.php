
<?php

use \DataTables\DataTable;

class BranchController extends ControllerBase
{
    private $branch;
    private $opening = [];

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('js/funcs.js')
            ->addJs('js/validators.js')
            ->addJs('js/branch.js');

        $this->modelName = 'Branch';
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }

    private function getAttributesAction()
    {
        return  [
            "BranchNumber",
            "BranchName",
            "City",
            "Manager",
            "PhoneNumber",
            "Active",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getBranches($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function createAction()
    {
        $this->loadModels();

        if ($this->request->getPost("Branch")) {
            $this->saveModel();
        }
    }

    public function updateAction($id)
    {
        $this->loadModels($id);

        if ($this->request->getPost("Branch")) {
            $this->saveModel();
        }
    }

    public function saveModel()
    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();


            $currentLang = $this->session->get("lang");
            $arr = $this->request->getPost("Branch");


            if($this->request->getPost("branchLang")[$currentLang]['branch_name']){
                $arr["branch_name"] = $this->request->getPost("branchLang")[$currentLang]['branch_name'];
            } else {
                foreach($this->request->getPost("branchLang") as $branchLang){
                    if($branchLang['branch_name']){
                        $arr["branch_name"] = $branchLang['branch_name'];
                        break;
                    }
                }
            }

            if($this->request->getPost("branchLang")[$currentLang]['street_address']){
                $arr["street_address"] = $this->request->getPost("branchLang")[$currentLang]['street_address'];
            } else {
                foreach($this->request->getPost("branchLang") as $street_address){
                    if($street_address['street_address']){
                        $arr["street_address"] = $street_address['street_address'];
                    }
                }
            }

            if($this->request->getPost("branchLang")[$currentLang]['manager']){
                $arr["manager"] = $this->request->getPost("branchLang")[$currentLang]['manager'];
            } else {
                foreach($this->request->getPost("branchLang") as $manager){
                    if($manager['manager']){
                        $arr["manager"] = $manager['manager'];
                    }
                }
            }

            $arr["location"] = $arr["longitude"] . "," . $arr["latitude"];
            $this->branch->setTransaction($transactions);
            if (!$this->branch->save($arr)) {
                foreach ($this->branch->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }

            foreach ($this->request->getPost("branchLang") as $lang => $value)
            {
                $translation = "";
                foreach ($this->branchTranslation as $key => $val)
                {
                    if ($val->lang == $lang)
                    {
                        $translation = $this->branchTranslation[$key];
                    }
                }

                if($translation == ""){
                    $translation = new BranchTranslation();
                }
                $translation->setTransaction($transactions);
                $translation->branch_id = $this->branch->id;
                $translation->lang = $lang;
                $translation->branch_name = $value['branch_name'];
                $translation->street_address = $value['street_address'];
                $translation->manager = $value['manager'];
                if(!$translation->save()){
                    foreach ($translation->getMessages() as $message) {
                        $transactions->rollback($message->getMessage());
                    }
                }
            }

            foreach ($this->request->getPost("Opening") as $day => $val)
            {
                $is_opening = BranchOpeningHour::findFirst(array("branch_id = '".$this->branch->id."' AND day ='". $day ."'"));

                $opening = $is_opening ? $is_opening : new BranchOpeningHour();
                $opening->setTransaction($transactions);
                $opening->branch_id = $this->branch->id;
                $opening->day = strtolower($day);
                $opening->open_hour = isset($val['is_closed']) ? null :  $val['open_hour'];
                $opening->close_hour = isset($val['is_closed']) ? null : $val['close_hour'];
                $opening->shabat_grace = isset($val['is_closed']) ? null : isset($val['shabat_radio']) && isset($val['shabat_grace']) ? (int)$val['shabat_grace'] : null;
                $opening->is_closed = isset($val['is_closed']) ?  1 : 0;
                if(isset($val['shabat_radio']) && !isset($val['is_closed'])){
                    $opening->open_hour = null;
                    $opening->close_hour = $val['shabat_close_hour'];
                }

                if(!$opening->save()){
                    foreach ($opening->getMessages() as $message) {
                        $transactions->rollback($message->getMessage());
                    }
                }
            }


            if($transactions->commit()){
                $this->flashSession->success("Your information was stored correctly!");
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $this->flashSession->error($e->getMessage());
            $this->response->redirect($this->theBaseUrl.$this->linkName);
        }
    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }

    public function deleteAction()    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $this->view->disable();
            $id = $this->request->getPost('id');
            $model = $this->modelName;
            $instance = array();
            $instance['status'] = false;
            $modelObj = $model::findFirst((int)$id);
            $modelObj->setTransaction($transactions);
            if (!$modelObj->delete()){
                foreach ($modelObj->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }
            if($transactions->commit()){
                $instance['status'] = true;
            }
            echo json_encode($instance);
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $instance['error'][] = $e->getMessage();
            echo json_encode($instance);
        }
    }

    protected function loadModels($id = null)
    {
        !$this->request->getPost() ? parent::loadModels() : "";
        $this->branch = $this->view->branch = $id === null ? new Branch : Branch::findFirst((int)$id);
        $this->view->cities = City::find();
        $this->view->days = BaseModel::getDaysOfWeek();
        $this->view->langs =  self::getDI()['settings']['availableLangs'];
        if ($id !== null){
            foreach ($this->branch->getRelated("BranchOpeningHour") as $opening)
            {
                $this->opening[ucfirst($opening->day)] = $opening->toArray();
            }
            $this->view->opening = $this->opening;
            $this->branchTranslation = $this->branch->getRelated("BranchTranslation");
            $langs = [];
            foreach ($this->branchTranslation as $val)
            {
                $langs[$val->lang] = $val->toArray();
            }
            $this->view->branchLang = $langs;
        } else {
            $this->view->branchLang = $this->view->langs;
            $opening = array();
            foreach($this->branch->getDaysOfWeek() as $day){
                $opening[$day]['branch_id'] = '';
                $opening[$day]['open_hour'] = '';
                $opening[$day]['close_hour'] = '';
                $opening[$day]['shabat_grace'] = '';
                $opening[$day]['is_closed'] = 0;
                $this->view->opening = $opening;
            }
        }
    }

    public function autoCompleteAction()
    {
        $criteria = array();

        $column = $this->request->getPost("column");
        $value = $this->request->getPost("value");
        $extraColumn = null !== $this->request->getPost("extracolumn") ? $this->request->getPost("extracolumn") : '';
        $extraValue = null !== $this->request->getPost("extravalue") ? $this->request->getPost("extravalue") : '';
        $addOnCondition = $extraColumn ? " AND ".$extraColumn."='".addslashes($extraValue)."' " : '';
        $criteria['columns'] = $column;
        $criteria['conditions'] = $column." LIKE '%" . addslashes($value) . "%' ".$addOnCondition;
        $criteria['limit'] = 5;

        $searchedColumn = Branch::find($criteria)->toArray();

        echo json_encode($searchedColumn);

        $this->view->disable();
    }

    public function validateNoAction(){

        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        $result = $model::findFirst(
            array(
                "conditions"=>"branch_no = ?1",
                "bind" => array(
                    1 => $this->request->getPost('name')
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }

    public function activateAction()
    {
        $this->view->disable();

        $updatedModel = Branch::findFirst((int)$this->request->getPost("item-id"));

        $updatedModel->active = (int)$this->request->getPost("active");

        $updatedModel->created_at = BaseModel::toSQLDate($updatedModel->created_at);

        $updatedModel->save();


    }

}