<?php

use \Phalcon\Mvc\View;
use \DataTables\DataTable;

class ChurnalertController extends ControllerBase
{

    public function initialize(){
        parent::initialize();
        $this->assetsHeaderCss
            ->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css',false,false)
            ->addCss('//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js',false,false)
            ->addJs('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js',false,false)
            ->addJs('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js',false,false)
            ->addJs('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('//cdn.jsdelivr.net/momentjs/latest/moment.min.js',false,false)
            ->addJs('//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js',false,false)
            ->addJs('js/funcs.js');

        $this->modelName = "SegmentCustomer";
    }


    private function getAttributesAction()
    {
        return  [
            "CustomerName",
            "PhoneNumber",
            "ClubJoinDate",
            "ChurnAlertDate",
            "Retention",
            "PurchaseHistory",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getChurnalert($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function indexAction(){
        $this->assetsFooter
            ->addJs('js/churnalert.js');
        $campaigns_type = array();
        array_push($campaigns_type, ['type' =>'ReadTickets'],['type' =>'UnreadTickets'],['type' =>'RemovedTickets']);
        $this->view->setVars(
            [
                "campaigns" => $campaigns_type,
                "attributes" => $this->getAttributesAction(),
            ]);
    }

    private function getEditAttributesAction()
    {
        return  [
            "ActionID",
            "Date",
            "Activity"
        ];
    }

    public function getEditDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getRetention($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function getAddTmp(){
        return "<select>
                    <option value=''>".Lang::translate('Action')."</option>
                    <option value='Feedback'>".Lang::translate('Feedback')."</option>
                    <option value='Coupon'>".Lang::translate('Coupon')."</option>
                    <option value='FlipTiles'>".Lang::translate('FlipTiles')."</option>
                    <option value='PhoneCall'>".Lang::translate('PhoneCall')."</option>
                </select>";

    }

    public function editAction($id){
        $this->assetsFooter
            ->addJs('js/churnalert-edit.js');
        $this->modelName = "BranchVisits";

        $this->view->setVars(
            [
                "id"    => $id,
                "customerData" => Customer::findFirst($id)->toArray(),
                "attributes" => $this->getEditAttributesAction(),
                "getAddTmp" => $this->getAddTmp()
            ]);
    }



}