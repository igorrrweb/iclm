<?php

class DashboardController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('bower_components/highcharts/highcharts.js')
            ->addJs('js/dashboard.js');
    }

    public function indexAction()
    {
        $frontCache =new Phalcon\Cache\Frontend\Data([
            "lifetime" => 172800
        ]);

        $cache = new Phalcon\Cache\Backend\Redis($frontCache);

        //$cache = new CacheManager(172800);

        if (!$cache->exists("churn"))
        {
            $cache->save("churn", Customer::getTotalChurn());
        }

        $this->view->setVars(
            [
                "currentVisitors" => BranchVisits::getCurrentVisitors(),
                "visitorsToday" => json_encode(BranchVisits::getWeeklyVisitors()),
                "couponsRedeemedToday" => json_encode(CouponActivity::getWeeklyRedeemed()),
                "availableCoupons" => json_encode(Coupon::getAvailableCoupons()),
                "availableEngagements" => json_encode(Engagement::getAvailableEngagements()),
                "churn" => $cache->get("churn"),
                "weeklyNewMembers" => json_encode(Customer::getNewMembersWeekly()),
                "feedbacks" => FeedbackResponse::count("is_read = 0 AND is_archived = 0"),
                "aboutExpireCampaigns" => Coupon::getAboutToExpire() + Engagement::getAboutToExpire(),
            ]
        );
    }
}

