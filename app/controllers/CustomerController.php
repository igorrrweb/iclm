<?php

use \DataTables\DataTable;

class CustomerController extends ControllerBase {

    private $customer;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('js/validators.js')
            ->addJs('js/customer.js');

        $this->modelName = 'Customer';
        $this->linkName = "customer";
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }

    private function getAttributesAction()
    {
        return  [
            "PersonID",
            "Name",
            "JoinDate",
            "LastVisit",
            "Status",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getCustomers($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function createAction()
    {

        $this->loadModels();

        if ($this->request->getPost("Customer"))
        {
            $this->saveModel();
        }
    }

    public function updateAction($id)
    {
        $this->loadModels($id);

        if ($this->request->getPost("Customer"))
        {
            $this->saveModel();
        }
    }

    public function saveModel()
    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $values = $this->request->getPost("Customer");
            $this->customer->setTransaction($transactions);
            if (!$this->customer->save($values)){
                foreach ($this->customer->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }

            if($transactions->commit()){
                $this->flashSession->success("Your information was stored correctly!");
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $this->flashSession->error($e->getMessage());
            $this->response->redirect($this->theBaseUrl.$this->linkName);
        }
    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }

    public function deleteAction()    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $this->view->disable();
            $id = $this->request->getPost('id');
            $model = $this->modelName;
            $instance = array();
            $instance['status'] = false;
            $modelObj = $model::findFirst((int)$id);
            $modelObj->setTransaction($transactions);
            if (!$modelObj->delete()){
                foreach ($modelObj->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }
            if($transactions->commit()){
                $instance['status'] = true;
            }
            echo json_encode($instance);
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $instance['error'][] = $e->getMessage();
            echo json_encode($instance);
        }
    }

    protected function loadModels($id = null)
    {
        !$this->request->getPost() ? parent::loadModels() : "";
        $this->view->customer = $this->customer = $id === null ? new Customer : Customer::findFirst((int)$id);
        $this->view->cities = City::find();
    }

    public function validateIdAction(){
        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        $result = $model::findFirst(
            array(
                "conditions"=>"id = ?1",
                "bind" => array(
                    1 => $this->request->getPost('name')
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }
    public function validatePhoneAction(){
        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        $result = $model::findFirst(
            array(
                "conditions"=>"phone = ?1",
                "bind" => array(
                    1 => $this->request->getPost('name')
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }

    public function validateMobileAction(){
        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        $result = $model::findFirst(
            array(
                "conditions"=>"mobile = ?1",
                "bind" => array(
                    1 => $this->request->getPost('name')
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }

    public function autoCompleteAction()
    {
        $criteria = array();

        $column = (string)$this->request->getPost("column");
        $value = $this->request->getPost("value");
        $extraColumn = null !== $this->request->getPost("extracolumn") ? $this->request->getPost("extracolumn") : '';
        $extraValue = null !== $this->request->getPost("extravalue") ? $this->request->getPost("extravalue") : '';
        switch($extraColumn){
            case 'id':
                $addOnCondition = " AND c.id LIKE '%" . addslashes($extraValue) . "%' ";
                break;
            case 'name':
                $addOnCondition = "AND (CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . addslashes($extraValue) . "%' OR
                                               c.firstname LIKE '%" . addslashes($extraValue) . "%' OR c.lastname
                                               LIKE '%" . addslashes($extraValue) . "%')";
                break;
            default:
                $addOnCondition = '';
                break;
        }

        switch($column){
            case "name":
                $searchField = "CONCAT(c.firstname, ' ', c.lastname) as name ";
                $criteria['conditions'] = "(CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . addslashes($value) . "%' OR
                                               c.firstname LIKE '%" . addslashes($value) . "%' OR c.lastname
                                               LIKE '%" . addslashes($value) . "%') ".$addOnCondition;
                break;
            case "id":
                $searchField = " c.id";
                $criteria['conditions'] = " c.id LIKE '%" . addslashes($value) . "%' ".$addOnCondition;
                break;
        }

        $searchedColumn = CRUDQueries::getSearchCustomers($searchField,$criteria);

        echo json_encode($searchedColumn);

        $this->view->disable();
    }

}