<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    // user model instance
    protected $user;
    public  $theBaseUrl;
    public  $assetsHeaderCss;
    public  $assetsHeaderJs;
    public  $assetsFooter;
    public  $modelName;
    public  $linkName;

    public function initialize(){

        $this->assetsHeaderCss = $this->assets->collection('headerCss')
            ->addCss('bower_components/bootstrap/dist/css/bootstrap.min.css')
            ->addCss('bower_components/metisMenu/dist/metisMenu.min.css')
            ->addCss('css/sb-admin-2.css')
            ->addCss('bower_components/font-awesome/css/font-awesome.min.css')
            ->addJs('bower_components/select2/select2.css')
            ->addCss('css/bootstrap-switch.min.css')
            ->addCss('css/jquery-confirm.min.css');

        $this->assetsHeaderJs = $this->assets->collection('headerJs')
            ->addJs('//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', false, false)
            ->addJs('//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', false, false);

        $this->assetsFooter = $this->assets->collection('footer')
            ->addJs('bower_components/jquery/dist/jquery.min.js')
            ->addJs('bower_components/bootstrap/dist/js/bootstrap.min.js')
            ->addJs('bower_components/metisMenu/dist/metisMenu.min.js')
            ->addJs('bower_components/jquery-validation/dist/jquery.validate.js')
            ->addJs('bower_components/jquery-validation/dist/additional-methods.js')
            ->addJs('bower_components/select2/select2.js')
            ->addJs('bower_components/bootstrap-switch.min.js')
            ->addJs('bower_components/jquery-confirm.min.js')
            ->addJs('js/lang.js')
            ->addJs('js/sb-admin-2.js');


        $this->modelName = ucfirst((string)$this->dispatcher->getControllerName());
        $this->linkName = $this->dispatcher->getControllerName();

        $acl = ACL::getInstance();
        $acl->checkUserPermission();

        if ($this->session->get("user")){
            $this->user = unserialize($this->session->get("user"));
            $this->view->username = $this->user->username;
        }

        if (!$this->session->get("lang") && $this->user)
        {
            $this->session->set("lang", $this->user->lang);
        }
        $this->theBaseUrl = self::getDI()['url']->getBaseUri();

        $this->view->baseUrl = $this->theBaseUrl;
        $this->view->lang = $this->session->get("lang") ? $this->session->get("lang") : self::getDI()['settings']['defaultLang'];
        $this->linkName = $this->dispatcher->getControllerName();

    }

    protected function loadModels()
    {
        self::getDI()['session']->set('lastUrl', $this->request->getHTTPReferer());

    }
}
