<?php

use \DataTables\DataTable;

class PunchcardController extends UsersengagementsController
{
    private $punchcard;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('js/funcs.js')
            ->addJs('js/validators.js')
            ->addJs('js/punchcard.js');


        $this->linkName = "punchcard";
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }

    private function getAttributesAction()
    {
        return  [
            "ID",
            "Name",
            "Duration",
            "EndDate",
            "Redeemed",
            "Status",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getEngagements($this->request->getPost(),$this->linkName);
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function createAction()
    {
        $this->loadModels();

        if ($this->request->getPost("Punchcard"))
        {
            $this->engagement->type = "punchcard";

            $this->engagement->never_expires = isset($this->request->getPost("Engagement")["never_expires"]) ? 1 : 0;

            if ($this->engagement->save($this->request->getPost("Engagement")))
            {
                $this->punchcard->id = $this->engagement->id;
                $this->punchcard->save($this->request->getPost("Punchcard"));

                parent::create();
            }
        }
    }

    public function updateAction($id)
    {
        $this->loadModels($id);

        if ($this->request->getPost("Punchcard"))
        {
            $this->engagement->never_expires = isset($this->request->getPost("Engagement")["never_expires"]) ? 1 : 0;

            if ($this->engagement->save($this->request->getPost("Engagement")))
            {
                $this->punchcard->save($this->request->getPost("Punchcard"));

                parent::update();
            }
        }
    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }

    protected function loadModels($id = null)
    {
        parent::loadModels($id);

        $this->engagement->type = "punchcard";
        $this->couponType = 9;
        $this->punchcard = $this->view->punchcard = ($id === null) ? new Punchcard() : Punchcard::findFirst((int)$id);
        $this->engagement = $this->view->engagement = $id === null ? new Engagement() : Engagement::findFirst($id);

        $langs = [];
        if(null != $this->coupon->toArray()['id']){
            $this->couponTranslation = $this->engagement->getRelated("Coupon")->getRelated("CouponTranslation");
            foreach ($this->couponTranslation->toArray() as $val)
            {
                $langs[$val['lang']] = $val;
            }
        } else {
            $langs = $this->view->langs;
        }
        $this->view->couponLang = $langs;
    }
}