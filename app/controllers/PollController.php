<?php

use \DataTables\DataTable;

class PollController extends UsersengagementsController {

    private $polls;
    private $pollAnswers;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('js/funcs.js')
            ->addJs('js/validators.js')
            ->addJs('js/poll.js');

        $this->linkName = "poll";
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }

    private function getAttributesAction()
    {
        return  [
            "ID",
            "Name",
            "Duration",
            "EndDate",
            "Redeemed",
            "Status",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getEngagements($this->request->getPost(),$this->linkName);
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }



    public function createAction()
    {
        $this->loadModels();

        if ($this->request->getPost("Poll"))
        {
            $this->saveModel();
        }
    }

    public function updateAction($id)
    {
        $this->loadModels($id);

        if ($this->request->getPost("Poll"))
        {
            $this->saveModel($id);
        }
    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }

    private function saveModel($id = null)
    {
        $this->engagement->type = "poll";
        $this->engagement->never_expires = isset($this->request->getPost("Engagement")["never_expires"]) ? 1 : 0;
        $this->engagement->end_date = isset($this->request->getPost("Engagement")["end_date"]) ? $this->request->getPost("Engagement")["end_date"] : null;
        $this->engagement->save($this->request->getPost("Engagement"));

        foreach ($this->request->getPost("Poll") as $lang=>$poll)
        {
            if ($poll['question'] != "")
            {
                if (isset($this->polls[$lang]))
                {

                    $this->polls[$lang]->save($poll);


                } else {
                    $newPoll = new Poll();

                    $newPoll->engagement_id = $this->engagement->id;
                    $newPoll->lang = $lang;
                    $newPoll->save($poll);


                    $this->polls[$newPoll->lang] = $newPoll;
                }
            }
        }

        $id === null ? parent::create(true) : parent::update(true);

        foreach ($this->request->getPost("PollAnswer") as $lang=>$answer)
        {
            foreach ($answer as $key=>$val)
            {
                if (!ctype_space($val) && $val != "")
                {
                    $answerModel = isset($this->pollAnswers[$lang][$key]) ? $this->pollAnswers[$lang][$key] : new PollAnswer();

                    $answerModel->answer = $val;
                    $answerModel->poll_id = $this->polls[$lang]->id;
                    $answerModel->save();

                }
            }
        }

    }

    protected function loadModels($id = null)
    {
        parent::loadModels($id);
        $this->engagement->type = "poll";
        $this->couponType = 6;
        $polls = array();
        $langs = array();
        $images = array();
        $this->pollAnswers = array();
        if ($id !== null) {

            foreach ($this->engagement->getRelated("Poll") as $poll) {
                $this->polls[$poll->lang] = $poll;
                $polls[$poll->lang] = $this->polls[$poll->lang]->toArray();
                $this->pollAnswers[$poll->lang] = $poll->getRelated("PollAnswer");
            }

            $images = FileManager::getFiles("poll", $this->engagement->id);

            $this->view->img = $images[count($images) - 1];

            unset($images[count($images) - 1]);



            if (null != $this->coupon->toArray()['id']) {
                $this->couponTranslation = $this->engagement->getRelated("Coupon")->getRelated("CouponTranslation");
                foreach ($this->couponTranslation->toArray() as $val) {
                    $langs[$val['lang']] = $val;
                }
            }
        } else {
            $langs = $this->view->langs;
        }

        $this->view->images = $images;
        $this->view->polls = $polls;
        $this->view->pollAnswers = $this->pollAnswers;
        $this->view->couponLang = $langs;
    }



}