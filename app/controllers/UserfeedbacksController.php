<?php

use \DataTables\DataTable;

class UserfeedbacksController extends ControllerBase {

    private $responses;
    private $response;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('js/userfeedbacks.js');

        $this->modelName = "FeedbackResponse";

        if ($this->dispatcher->getActionName() != "index" && !$this->request->isAjax())
        {
            $this->view->responses = $this->responses = FeedbackResponse::find([
                "conditions" => "feedback_id = ?1 AND customer_id = ?2",
                "bind" => ["1" => $this->dispatcher->getParams()[0], "2" => $this->dispatcher->getParams()[1]]]);

            $this->response = FeedbackResponse::findFirst([
                "conditions" => "customer_id = ?1 AND question_id = ?2",
                "bind" => ["1" => $this->dispatcher->getParams()[0], "2" => $this->dispatcher->getParams()[1]]]);

            //$this->view->disable();
        }
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }
    private function getAttributesAction()
    {
        return  [
            "QuestionID",
            "FormID",
            "MemberID",
            "FormName",
            "SubmissionDate",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getResponses($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function viewSubmittedAction()
    {
        $this->view->user = $this->responses[0]->getRelated("Customer");
        $this->view->form = $this->responses[0]->getRelated("Feedback")->title;
        $this->view->submitted = $this->responses[0]->created_at;
    }

    public function archiveAction()
    {
        $this->response = FeedbackResponse::findFirst([
            "conditions" => "customer_id = ?1 AND question_id = ?2",
            "bind" => ["1" => $this->dispatcher->getParams()[0], "2" => $this->dispatcher->getParams()[1]]]);
        $this->response->is_archived = 1;
        $this->response->save();
    }

    public function readAction()
    {
        $this->response = FeedbackResponse::findFirst([
            "conditions" => "customer_id = ?1 AND question_id = ?2",
            "bind" => ["1" => $this->dispatcher->getParams()[0], "2" => $this->dispatcher->getParams()[1]]]);
        $this->response->is_read = 1;
        $this->response->save();
    }

    public function unreadAction()
    {
        $this->response = FeedbackResponse::findFirst([
            "conditions" => "customer_id = ?1 AND question_id = ?2",
            "bind" => ["1" => $this->dispatcher->getParams()[0], "2" => $this->dispatcher->getParams()[1]]]);
        $this->response->is_read = 0;
        $this->response->save();
    }
    public function autoCompleteAction()
    {

        $column = (string)$this->request->getPost("column");
        $value = $this->request->getPost("value");
        switch($column){
            case "title":
                $searchField = " f.title ";
                break;
            case "customer_id":
                $searchField = " fr.customer_id";
        }

        $condition['conditions'] = $searchField." LIKE '%" . addslashes($value) . "%' and fr.is_archived = 0 ";
        $searchedColumn = CRUDQueries::getSearchResponsesByCustomerIdOrTitle($searchField,$condition);



        echo json_encode($searchedColumn);

        $this->view->disable();
    }
}