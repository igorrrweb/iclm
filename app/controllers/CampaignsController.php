<?php

use \Phalcon\Mvc\View;
use \DataTables\DataTable;

class CampaignsController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss
            ->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css',false,false)
            ->addCss('//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js',false,false)
            ->addJs('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js',false,false)
            ->addJs('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js',false,false)
            ->addJs('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js',false,false)
            ->addJs('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('//cdn.jsdelivr.net/momentjs/latest/moment.min.js',false,false)
            ->addJs('//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js',false,false)
            ->addJs('js/funcs.js')
            ->addJs('js/campaigns.js');
        $this->modelName = "SegmentCustomer";
    }

    public function indexAction()
    {
        $campaigns_type = Engagement::getType()->toArray();
        array_push($campaigns_type, ['type' =>'Coupon'],['type' =>'Feedback']);

        $this->view->setVars(
            [
                "campaigns" => $campaigns_type,
                "attributes" => $this->getAttributesAction(),
            ]);
    }


    private function getAttributesAction()
    {
        return  [
            "Name",
            "Audience",
            "CampaignType",
            "ExpiryDate",
            "RedeemedCompleted",
            "Status",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getCampaigns($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    protected function getStrategy()
    {
        $strategyName = "CampaignsStrategy";
        $link = $this->linkName == "" ? strtolower($this->modelName) : $this->linkName;
        return new $strategyName($this->request->get(), $link);
    }

    public function autoCompleteAction()
    {
        if(ctype_digit($this->request->getPost("value"))){
            $column = 'id';
        } else {
            $column = (string)$this->request->getPost("column");
        }
        $response = [];
        $models = ['Coupon', 'Engagement', 'Feedback'];
        foreach($models as $model){
            if($model=='Feedback'){
                $column = $column == 'name' ? 'title' : $column;
            }
            $searchedColumn = $model::find([
                "conditions" => $column . " LIKE " . "'" . (string)$this->request->getPost("value") . "%'",
                "limit" => 5,
                "columns" => $column
            ])->toArray();

         if($searchedColumn){
              foreach($searchedColumn as $val){
                  array_push($response, $val);
              }

            }
        }
        echo json_encode($response);
        $this->view->disable();
    }


}