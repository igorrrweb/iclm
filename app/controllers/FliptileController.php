<?php

use \DataTables\DataTable;

class FliptileController extends UsersengagementsController {

    private $fliptiles = [];
    private $couponTranslation;


    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('js/funcs.js')
            ->addJs('js/validators.js')
            ->addJs('js/fliptile.js');

        $this->linkName = "fliptile";
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }

    private function getAttributesAction()
    {
        return  [
            "ID",
            "Name",
            "Duration",
            "EndDate",
            "Redeemed",
            "Status",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getEngagements($this->request->getPost(),$this->linkName);
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function createAction()
    {

        $this->loadModels();

        if ($this->request->getPost("Fliptile"))
        {
            $this->engagement->type = "fliptile";
            $this->engagement->never_expires = isset($this->request->getPost("Engagement")["never_expires"]) ? 1 : 0;

            if ($this->engagement->save($this->request->getPost("Engagement")))
            {
                foreach ($this->request->getPost("Fliptile") as $val)
                {
                    $fliptile = new Fliptile();
                    $fliptile->engagement_id = $this->engagement->id;
                    $fliptile->price_before = $val["price_before"];
                    $fliptile->price_after = $val["price_after"];
                    $fliptile->save();
                }
                parent::create(true);
            }
        }

    }

    public function updateAction($id)
    {
        $this->loadModels($id);

        if ($this->request->getPost("Fliptile"))
        {
            $this->engagement->type = "fliptile";

            $this->engagement->never_expires = isset($this->request->getPost("Engagement")["never_expires"]) ? 1 : 0;

            if ($this->engagement->save($this->request->getPost("Engagement")))
            {
                $i = 0;

                foreach ($this->request->getPost("Fliptile") as $val)
                {
                    $this->fliptiles[$i]->price_before = $val["price_before"];
                    $this->fliptiles[$i]->price_after = $val["price_after"];
                    $this->fliptiles[$i]->save();
                    $i++;
                }
                parent::update(true);
            }
        }
    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }




    protected function loadModels($id = null)
    {
        parent::loadModels($id);
        $this->engagement->type = "flip_tile";
        $this->couponType = 7;
        $this->fliptiles =  $id !== null ? $this->engagement->getRelated("Fliptile") : null;
        $this->view->fliptiles = $this->fliptiles != null ? $this->fliptiles->toArray() : false;
        $this->view->langs =  self::getDI()['settings']['availableLangs'];
        $langs = [];
        $images = [];
        if ($id !== null){
            $this->view->img = FileManager::getFile("fliptile",$this->engagement->id . "/engagementImage");
            for ($i = 1; $i <= 6; $i++)
            {
                $images[$i] = FileManager::getFile("fliptile",$this->engagement->id . "/image" . $i);
            }
        }
        if ($id !== null){
            $this->couponTranslation = $this->engagement->getRelated('Coupon') ? $this->engagement->getRelated('Coupon')->getRelated("CouponTranslation") : null;
            if($this->couponTranslation){
                foreach ($this->couponTranslation->toArray() as $val)
                {
                    $langs[$val['lang']] = $val;
                }
            } else {
                $langs = $this->view->langs;
            }
        } else {
            $langs = $this->view->langs;
        }
        $this->view->couponLang = $langs;
        $this->view->images = $images ? $images : false;

    }

}