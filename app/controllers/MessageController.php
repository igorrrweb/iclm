<?php

use \DataTables\DataTable;

class MessageController extends ControllerBase
{
    private $message;
    private $messageSegments;
    private $img;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/jquery.bootstrap.wizard.min.js')
            ->addJs('bower_components/bootstrap-filestyle.min.js')
            ->addJs('js/funcs.js')
            ->addJs('js/validators.js')
            ->addJs('js/message.js');

        $this->modelName = 'Message';
        $this->linkName = 'message';
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }

    private function getAttributesAction()
    {
        return  [
            "ID",
            "Name",
            "Duration",
            "EndDate",
            "Audience",
            "Status",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getMessages($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function createAction()
    {
        $this->loadModels();

        if ($this->request->getPost("Message"))
        {
            try{
                $manager = $this->getDI()->getTransactions();
                $transactions = $manager->get();
                $this->message->active = 1;
                $arr = array();
                $arr = $this->request->getPost("Message");
                $arr['title'] = $this->request->getPost("MessageTranslation")['he']['title'];
                $arr['content'] = $this->request->getPost("MessageTranslation")['he']['content'];
                $this->message->setTransaction($transactions);
                if (!$this->message->save($arr)) {
                    foreach ($this->message->getMessages() as $message) {
                        $transactions->rollback($message->getMessage());
                    }
                }

                if ($this->request->hasFiles() && $this->request->getUploadedFiles()[0]->getName() != "") {
                    foreach ($this->request->getUploadedFiles() as $file) {
                        FileManager::upload("message",$file, $this->message->id);
                    }
                }

                foreach ($this->request->getPost("Segments") as $val)
                {
                    $messageSegment = new MessageSegment;
                    $messageSegment->setTransaction($transactions);
                    $messageSegment->segment_id = $val;
                    $messageSegment->message_id = $this->message->id;
                    if(!$messageSegment->save()){
                        foreach ($messageSegment->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }
                }

                foreach ($this->request->getPost("MessageTranslation") as $lang => $value)
                {
                    $translation = new MessageTranslation();
                    $translation->setTransaction($transactions);
                    $translation->message_id = $this->message->id;
                    $translation->lang = $lang;
                    $translation->title = $value['title'];
                    $translation->content = $value['content'];
                    if(!$translation->save()){
                        foreach ($translation->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }
                }
                if($transactions->commit()){
                    $this->flashSession->success("Your information was stored correctly!");
                    $this->response->redirect($this->theBaseUrl.$this->linkName);
                }
            } catch (Phalcon\Mvc\Transaction\Failed $e){
                $this->flashSession->error($e->getMessage());
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        }


    }

    public function updateAction($id)
    {
        $this->loadModels($id);

        if ($this->request->getPost("Message"))
        {
            try{
                $manager = $this->getDI()->getTransactions();
                $transactions = $manager->get();
                $this->message->active = 1;
                $arr = array();
                $arr = $this->request->getPost("Message");
                $arr['title'] = $this->request->getPost("MessageTranslation")['he']['title'];
                $arr['content'] = $this->request->getPost("MessageTranslation")['he']['content'];
                $this->message->setTransaction($transactions);
                if (!$this->message->save($arr)) {
                    foreach ($this->message->getMessages() as $message) {
                        $transactions->rollback($message->getMessage());
                    }
                }
                if ($this->request->hasFiles() && $this->request->getUploadedFiles()[0]->getName() != "") {
                    foreach ($this->request->getUploadedFiles() as $file) {
                        FileManager::upload("message", $file, $this->message->id);
                    }
                }

                foreach ($this->messageSegments as $messageSegment) {
                    $deleteItem = true;

                    foreach ($this->request->getPost("Segments") as $val) {
                        if ($val == $messageSegment->segment_id) {
                            $deleteItem = false;
                        }
                    }

                    if ($deleteItem) {
                        $messageSegment->delete();
                    }
                }

                foreach ($this->request->getPost("Segments") as $val)
                {
                    $messageSegment = new MessageSegment;
                    $messageSegment->setTransaction($transactions);
                    $messageSegment->segment_id = $val;
                    $messageSegment->message_id = $this->message->id;
                    if(!$messageSegment->save()){
                        foreach ($messageSegment->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }
                }

                foreach ($this->request->getPost("MessageTranslation") as $lang => $value){
                    $translation = "";
                    foreach ($this->messageTranslation as $key => $val)
                    {
                        if ($val->lang == $lang)
                        {
                            $translation = $this->messageTranslation[$key];
                        }
                    }
                    if($translation == ""){
                        $translation = new MessageTranslation();
                    }
                    $translation->setTransaction($transactions);
                    $translation->message_id = $this->message->id;
                    $translation->lang = $lang;
                    $translation->title = $value['title'];
                    $translation->content = $value['content'];
                    if(!$translation->save()){
                        foreach ($translation->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }
                }
                if($transactions->commit()){
                        $this->flashSession->success("Your information was stored correctly!");
                        $this->response->redirect($this->theBaseUrl.$this->linkName);
                }
            } catch (Phalcon\Mvc\Transaction\Failed $e){
                $this->flashSession->error($e->getMessage());
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        }

    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }

    public function deleteAction()    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $this->view->disable();
            $id = $this->request->getPost('id');
            $model = $this->modelName;
            $instance = array();
            $instance['status'] = false;
            $modelObj = $model::findFirst((int)$id);
            $modelObj->setTransaction($transactions);
            if (!$modelObj->delete()){
                foreach ($modelObj->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }
            if($transactions->commit()){
                $instance['status'] = true;
            }
            echo json_encode($instance);
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $instance['error'][] = $e->getMessage();
            echo json_encode($instance);
        }
    }


    protected function loadModels($id = null)
    {
        !$this->request->getPost() ? parent::loadModels() : "";
        $this->message = ($id === null) ? new Message : Message::findFirst((int)$id);
        $this->view->message = $this->message;
        $this->messageSegments = ($id !== null ?  $this->message->getRelated("MessageSegment") : null);
        $this->view->activeSegments = $this->messageSegments;
        $this->img = $id !== null ? FileManager::getFile("message",$this->message->id) : '';
        $this->view->img = $this->img;
        $this->view->langs =  self::getDI()['settings']['availableLangs'];

        $langs = [];
        if ($id !== null){
            $this->messageTranslation = $this->message->getRelated("MessageTranslation");

            foreach ($this->messageTranslation->toArray() as $val)
            {
                $langs[$val['lang']] = $val;
            }


        } else {
            $langs = $this->view->langs;
        }
        $this->view->messageLang = $langs;
    }

    public function getSegmentAction(){
        $this->view->disable();
        $array = CRUDQueries::getSegmentsMessage($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }


    public function validateTitleAction(){
        $this->view->disable();
        $response = true;

        $result = MessageTranslation::findFirst(
            array(
                "conditions"=>"title = ?1 and lang = ?2",
                "bind" => array(
                    1 => $this->request->getPost('name'),
                    2 => $this->request->getPost('lang'),
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->message_id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }

    public function activateAction()
    {
        $this->view->disable();

        $model = $this->modelName;

        $updatedModel = $model::findFirst((int)$this->request->getPost("item-id"));

        $updatedModel->active = $this->request->getPost("active");

        $updatedModel->created_at = BaseModel::toSQLDate($updatedModel->created_at);

        $updatedModel->save();
    }
}