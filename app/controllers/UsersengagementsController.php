<?php

use \Phalcon\Mvc\View;
use \DataTables\DataTable;

class UsersengagementsController extends ControllerBase
{
    protected $engagement;
    protected $engagementType;
    protected $segments;
    protected $engagementSegments;
    protected $engagementTranslation;
    protected $coupon;
    protected $couponType;
    protected $img;
    protected $couponImg;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss
            ->addCss('css/main.css');
        $this->modelName = "Engagement";
    }

    protected function create($multiple = false)
    {
        try{
            if ($this->request->getPost("Segments"))
            {
                $manager = $this->getDI()->getTransactions();
                $transactions = $manager->get();
                foreach ($this->request->getPost("Segments") as $val)
                {
                    $engagmentSegment = new EngagementSegment();
                    $engagmentSegment->setTransaction($transactions);
                    $engagmentSegment->segment_id = $val;
                    $engagmentSegment->engagement_id = $this->engagement->id;
                    if (!$engagmentSegment->save()){
                        foreach ($engagmentSegment->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }
                }
            }

            foreach ($this->request->getPost("EngagementLang") as $lang => $value)
            {
                $translation = new EngagementTranslation();
                $translation->setTransaction($transactions);
                $translation->engagement_id = $this->engagement->id;
                $translation->lang = $lang;
                $translation->thankyou_msg = $value['thankyou_msg'];
                $translation->welcome_msg = $value['welcome_msg'];
                if (!$translation->save()){
                    foreach ($translation->getMessages() as $message) {
                        $transactions->rollback($message->getMessage());
                    }
                }
            }

            if (isset($this->request->getPost("Engagement")["reward"]) && $this->request->getPost("Engagement")["reward"] == 1) {
                $arr = [];
                $arr = $this->request->getPost("Coupon");
                $arr["engagement_id"] = $this->engagement->id;
                $arr["engagement_type"] = $this->engagement->type;
                $arr["coupon_type"] = $this->couponType;
                if($this->coupon->save($arr)){
                    foreach ($this->request->getPost("CouponLang") as $lang => $value)
                    {
                        $translation = new CouponTranslation();
                        $translation->setTransaction($transactions);
                        $translation->coupon_id = $this->coupon->id;
                        $translation->lang = $lang;
                        $translation->title = $value['title'];
                        $translation->sub_title = $value['sub_title'];
                        $translation->details = $value['details'];
                        $translation->disclaimer = $value['disclaimer'];
                        if (!$translation->save()){
                            foreach ($translation->getMessages() as $message) {
                                $transactions->rollback($message->getMessage());
                            }
                        }
                    }
                } else {
                    foreach ($this->coupon->getMessages() as $message) {
                        $transactions->rollback($message->getMessage());
                    }
                }


            }

            if ($this->request->hasFiles())
            {
                foreach ($this->request->getUploadedFiles() as $file)
                {
                    if ($file->getName() != "")
                    {
                        if ($file->getKey() != "couponImage" && $file->getName() != "")
                        {
                            FileManager::upload((string)$this->dispatcher->getControllerName(),$file, $this->engagement->id, $multiple);
                        } else {
                            FileManager::upload("coupon",$file, $this->coupon->id);
                        }
                    }
                }

            }
            if($transactions->commit()){
                $this->flashSession->success("Your information was stored correctly!");
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $this->flashSession->error($e->getMessage());
            $this->response->redirect($this->theBaseUrl.$this->linkName);
        }

    }

    protected function update($multiple = false)
    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            foreach ($this->request->getPost("EngagementLang") as $lang => $value)
            {
                if ($value["welcome_msg"] != "")
                {
                    $translation = "";
                    foreach ($this->engagementTranslation as $key => $val)
                    {
                        if ($val->lang == $lang)
                        {
                            $translation = $this->engagementTranslation[$key];
                        }
                    }
                    if($translation == ""){
                        $translation = new EngagementTranslation();
                    }
                    $translation->setTransaction($transactions);
                    $translation->engagement_id = $this->engagement->id;
                    $translation->lang = $lang;
                    $translation->welcome_msg = $value['welcome_msg'];
                    $translation->thankyou_msg = $value['thankyou_msg'];
                    if(!$translation->save()){
                        foreach ($translation->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }
                }
            }

            if ($this->request->getPost("Segments"))
            {
                foreach ($this->engagementSegments as $engagementSegment)
                {
                    $deleteItem = true;

                    foreach ($this->request->getPost("Segments") as $val)
                    {
                        if ($val == $engagementSegment->segment_id)
                        {
                            $deleteItem = false;
                        }
                    }

                    if ($deleteItem)
                    {
                        $engagementSegment->delete();
                    }
                }

                foreach ($this->request->getPost("Segments") as $val)
                {
                    $engagementSegment = new EngagementSegment;
                    $engagementSegment->setTransaction($transactions);
                    $engagementSegment->segment_id = $val;
                    $engagementSegment->engagement_id = $this->engagement->id;
                    if(!$engagementSegment->save()){
                        foreach ($engagementSegment->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }
                }
            }

            if (isset($this->request->getPost("Engagement")["reward"]) && $this->request->getPost("Engagement")["reward"] == 1)
            {
                $arr = [];
                $arr = $this->request->getPost("Coupon");
                $arr["engagement_id"] = $this->engagement->id;
                $arr["engagement_type"] = $this->engagement->type;
                $arr["coupon_type"] = $this->couponType;
                if($this->coupon->save($arr)){
                    foreach ($this->request->getPost("CouponLang") as $lang => $value)
                    {
                        $translation = "";
                        foreach ($this->couponTranslation as $key => $val)
                        {
                            if ($val->lang == $lang)
                            {
                                $translation = $this->couponTranslation[$key];
                            }
                        }

                        if($translation == ""){
                            $translation = new CouponTranslation();
                        }
                        $translation->setTransaction($transactions);
                        $translation->coupon_id = $this->coupon->id;
                        $translation->lang = $lang;
                        $translation->title = $value['title'];
                        $translation->sub_title = $value['sub_title'];
                        $translation->details = $value['details'];
                        $translation->disclaimer = $value['disclaimer'];
                        if(!$translation->save()){
                            foreach ($translation->getMessages() as $message) {
                                $transactions->rollback($message->getMessage());
                            }
                        }
                    }
                }
            }

            if ($this->request->hasFiles())
            {
                foreach ($this->request->getUploadedFiles() as $file)
                {
                    if ($file->getName() != "")
                    {
                        if ($file->getKey() != "couponImage")
                        {
                            FileManager::upload((string)$this->dispatcher->getControllerName(),$file, $this->engagement->id, $multiple);
                        } else {
                            FileManager::upload("coupon",$file, $this->coupon->id);
                        }
                    }
                }
            }
            if($transactions->commit()){
                $this->flashSession->success("Your information was stored correctly!");
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $this->flashSession->error($e->getMessage());
            $this->response->redirect($this->theBaseUrl.$this->linkName);
        }

    }

    public function deleteAction()    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $this->view->disable();
            $id = $this->request->getPost('id');
            $model = $this->modelName;
            $instance = array();
            $instance['status'] = false;
            $modelObj = $model::findFirst((int)$id);
            $modelObj->setTransaction($transactions);
            if (!$modelObj->delete()){
                foreach ($modelObj->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }
            if($transactions->commit()){
                $instance['status'] = true;
            }
            echo json_encode($instance);
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $instance['error'][] = $e->getMessage();
            echo json_encode($instance);
        }
    }

    protected function loadModels($id = null)
    {
        !$this->request->getPost() ? parent::loadModels() : "";
        $this->engagement = ($id === null ? new Engagement() : Engagement::findFirst($id));
        $this->engagementSegments = ($id !== null ?  $this->engagement->getRelated("EngagementSegment") : null);
        $this->coupon = $id !== null && $this->engagement->getRelated("Coupon") ?
        $this->engagement->getRelated("Coupon") : new Coupon;
        $this->img = $id !== null ? FileManager::getFile((string)$this->dispatcher->getControllerName(),$this->engagement->id) : "";
        $this->couponImg = $id !== null && $this->engagement->getRelated("Coupon") ? FileManager::getFile("coupon",$this->coupon->id) : "";

        if ($id !== null){
            $this->engagementTranslation = $this->engagement->getRelated("EngagementTranslation");
            $langs = [];
            foreach ($this->engagementTranslation->toArray() as $val)
            {
                $langs[$val['lang']] = $val;
            }
        } else {
            $langs = self::getDI()['settings']['availableLangs'];
        }

        $this->view->setVars(
            [
                'engagement' => $this->engagement,
                'activeSegments' => $this->engagementSegments,
                'coupon' => $this->coupon,
                'img' => $this->img,
                'couponImg' => $this->couponImg,
                'allowShare' => self::getDI()['settings']['allowShares'],
                'langs' =>  self::getDI()['settings']['availableLangs'],
                'engagementLang' => $langs

            ]);
    }

    public function validateNameAction(){
        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        $result = $model::findFirst(
            array(
                "conditions"=>"name = ?1 and type = ?2",
                "bind" => array(
                    1 => $this->request->getPost('name'),
                    2 => $this->linkName
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }

    public function validateCouponNameAction(){
        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        if($this->request->getPost('id_param') != 'NaN' ){
            $engagement =  $model::findFirst($this->request->getPost('id_param'));
            $coupon = $engagement ? $engagement->getRelated("Coupon")->toArray() : false;
            if($coupon){
                if($this->request->getPost('name') != $coupon['name']){
                    $result = Coupon::findFirst(
                        array(
                            "conditions"=>"name = ?1",
                            "bind" => array(
                                1 => $this->request->getPost('name')
                            )
                        )
                    );
                    if($result){
                        $response = false;
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function getSegmentAction(){
        $this->view->disable();
        $array = CRUDQueries::getSegmentsEngagement($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function activateAction()
    {
        $this->view->disable();

        $model = $this->modelName;

        $updatedModel = $model::findFirst((int)$this->request->getPost("item-id"));

        $updatedModel->active = $this->request->getPost("active");

        $updatedModel->created_at = BaseModel::toSQLDate($updatedModel->created_at);

        $updatedModel->save();
    }


}