<?php
use \Phalcon\Mvc\View;
use \DataTables\DataTable;

class CouponController extends ControllerBase
{
    private $coupon;
    private $couponTranslation;
    private $couponSegments;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss
            ->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('bower_components/datetimepicker/jquery.datetimepicker.css')
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/datetimepicker/jquery.datetimepicker.js')
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('js/funcs.js')
            ->addJs('js/validators.js')
            ->addJs('js/coupon.js');

        $this->modelName = 'Coupon';
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }

    private function getAttributesAction()
    {
        return  [
            "ID",
            "Name",
            "Barcode",
            "Category",
            "CouponAudience",
            "CouponType",
            "Redeemed",
            "Status",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getCoupons($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function createAction()
    {
        $this->loadModels();

        $product_id = null;
        if($this->request->getPost('Product')['barcode']){
            $product_id = Product::findFirstByBarcode($this->request->getPost('Product')['barcode'])->id;
        }
        if ($this->request->getPost("Coupon"))
        {
            try{
                $manager = $this->getDI()->getTransactions();
                $transactions = $manager->get();

                $coupon = $this->request->getPost("Coupon");
                $coupon['product_id'] = $product_id;
                $this->coupon->setTransaction($transactions);
                if (!$this->coupon->save($coupon)){
                    foreach ($this->coupon->getMessages() as $message) {
                        $transactions->rollback($message->getMessage());
                    }
                }
                foreach ($this->request->getPost("CouponLang") as $lang => $value)
                {
                    $translation = new CouponTranslation();
                    $translation->setTransaction($transactions);
                    $translation->coupon_id = $this->coupon->id;
                    $translation->lang = $lang;
                    $translation->title = $value['title'];
                    $translation->sub_title = $value['sub_title'];
                    $translation->details = $value['details'];
                    $translation->disclaimer = $value['disclaimer'];
                    if (!$translation->save()){
                        foreach ($translation->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }
                }

                if ($this->request->getPost("CouponSegment"))
                {
                    foreach ($this->request->getPost("CouponSegment") as $segment)
                    {
                        $couponSegment = new CouponSegment();
                        $couponSegment->setTransaction($transactions);
                        $couponSegment->coupon_id = $this->coupon->id;
                        $couponSegment->segment_id = $segment;
                        if (!$couponSegment->save()){
                            foreach ($couponSegment->getMessages() as $message) {
                                $transactions->rollback($message->getMessage());
                            }
                        }
                    }
                }

                if ($this->request->hasFiles())
                {
                    if (FileManager::upload("coupon",$this->request->getUploadedFiles()[0],$this->coupon->id))
                    {
                        $this->coupon->img_uri = "/coupon/" . FileManager::getFolderID("coupon") . "/" .
                                                 $this->coupon->id . "." . strtolower($this->request->getUploadedFiles()[0]->getExtension());

                        if (!$this->coupon->save()){
                            foreach ($this->coupon->getMessages() as $message) {
                                $transactions->rollback($message->getMessage());
                            }
                        }
                    }
                }

                if($transactions->commit()){
                    $this->flashSession->success("Your information was stored correctly!");
                    $this->response->redirect($this->theBaseUrl.$this->linkName);
                }
            } catch (Phalcon\Mvc\Transaction\Failed $e){
                $this->flashSession->error($e->getMessage());
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        }
    }

    public function updateAction($id)
    {
        $this->loadModels($id);
        $product_id = null;
        if($this->request->getPost('Product')['barcode']){
            $product_id = Product::findFirstByBarcode($this->request->getPost('Product')['barcode'])->id;
        }
        if ($this->request->getPost("Coupon")) {
            try{
                $manager = $this->getDI()->getTransactions();
                $transactions = $manager->get();

                $coupon = $this->request->getPost("Coupon");
                $coupon['product_id'] = $product_id;
                $this->coupon->setTransaction($transactions);
                if (!$this->coupon->save($coupon)){
                    foreach ($this->coupon->getMessages() as $message) {
                        $transactions->rollback($message->getMessage());
                    }
                }

                foreach ($this->couponSegments as $couponSegment)
                {
                    $deleteItem = true;

                    foreach ($this->request->getPost("CouponSegment") as $val)
                    {
                        if ($val == $couponSegment->segment_id)
                        {
                            $deleteItem = false;
                        }
                    }

                    if ($deleteItem)
                    {
                        $couponSegment->delete();
                    }
                }

                foreach ($this->request->getPost("CouponLang") as $lang => $value)
                {
                    $translation = "";
                    foreach ($this->couponTranslation as $key => $val)
                    {
                        if ($val->lang == $lang)
                        {
                            $translation = $this->couponTranslation[$key];
                        }
                    }

                    if($translation == ""){
                        $translation = new CouponTranslation();
                    }
                    $translation->setTransaction($transactions);
                    $translation->coupon_id = $this->coupon->id;
                    $translation->lang = $lang;
                    $translation->title = $value['title'];
                    $translation->sub_title = $value['sub_title'];
                    $translation->details = $value['details'];
                    $translation->disclaimer = $value['disclaimer'];
                    if (!$translation->save()){
                        foreach ($translation->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }

                }

                if ($this->request->getPost("CouponSegment"))
                {
                    foreach ($this->request->getPost("CouponSegment") as $segment) {
                        $couponSegment = new CouponSegment();
                        $couponSegment->setTransaction($transactions);
                        $couponSegment->coupon_id = $this->coupon->id;
                        $couponSegment->segment_id = $segment;
                        if (!$couponSegment->save()){
                            foreach ($couponSegment->getMessages() as $message) {
                                $transactions->rollback($message->getMessage());
                            }
                        }
                    }
                }

                if ($this->request->hasFiles() && $this->request->getUploadedFiles()[0]->getName() != "") {

                    if (FileManager::upload("coupon",$this->request->getUploadedFiles()[0],$this->coupon->id))
                    {
                        $this->coupon->img_uri = "/coupon/" . FileManager::getFolderID("coupon") . "/"
                                                 . $this->coupon->id . "." . strtolower($this->request->getUploadedFiles()[0]->getExtension());
                        if(!$this->coupon->save()){
                            foreach ($this->coupon->getMessages() as $message) {
                                $transactions->rollback($message->getMessage());
                            }
                        }
                    }
                }

                if($transactions->commit()){
                    $this->flashSession->success("Your information was stored correctly!");
                    $this->response->redirect($this->theBaseUrl.$this->linkName);
                }
            } catch (Phalcon\Mvc\Transaction\Failed $e){
                $this->flashSession->error($e->getMessage());
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        }
    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }

    public function deleteAction()    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $this->view->disable();
            $id = $this->request->getPost('id');
            $model = $this->modelName;
            $instance = array();
            $instance['status'] = false;
            $modelObj = $model::findFirst((int)$id);
            $modelObj->setTransaction($transactions);
            if (!$modelObj->delete()){
                foreach ($modelObj->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }
            if($transactions->commit()){
                $instance['status'] = true;
            }
            echo json_encode($instance);
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $instance['error'][] = $e->getMessage();
            echo json_encode($instance);
        }
    }

    public function getProductsAction($id)
    {
        $this->view->disable();
        echo json_encode(Category::findFirst((int)$id)->getRelated("Product")->toArray());
    }

    public function getCategoryAction()
    {
        $barcode = $this->request->getPost("barcode");
        $this->view->disable();
        $category = array();
        $product = Product::findFirstByBarcode($barcode);
        if($product){
            $categoryProduct = $product->getCategoryProduct()->toArray();
            $category = Category::findFirst("id='".$categoryProduct[0]['category_id']."'")->toArray();
        }
        echo json_encode($category);
    }

    protected function loadModels($id = null)
    {
        !$this->request->getPost() ? parent::loadModels() : "";
        $this->view->coupon = $this->coupon = $id === null ? new Coupon : Coupon::findFirst((int)$id);
        $this->view->segments = Segment::find(["columns" => "id,name"]);
        $this->view->couponSegments = $this->couponSegments = $this->coupon->getRelated("CouponSegment");
        $this->view->products = $id !== null ? Category::findFirst($this->coupon->category_id)->getRelated("Product") :
        Category::findFirst()->getRelated("Product");
        $barcode = $id !== null ? $this->coupon->getProduct() : '';
        $this->view->barcode = $barcode;
        $this->view->img = $id !== null ? FileManager::getFile("coupon",(int)$id) : "";

        $this->view->rootCategories = $barcode->barcode ? CategoryProduct::findFirst(array(
            "conditions"=>"product_id = ?1",
            "bind"=>array(
                1=>$barcode->id)
        ))->getRelated('Category') : null;
        $this->view->allowShare = self::getDI()['settings']['allowShares'];
        $this->view->langs =  self::getDI()['settings']['availableLangs'];

        $this->view->category = $id !== null ? ($this->coupon->getRelated("Category") ? $this->coupon->getRelated("Category")->toArray(): null) : null;
        if ($id !== null){
            $this->couponTranslation = $this->coupon->getRelated("CouponTranslation");
            $langs = [];
            foreach ($this->couponTranslation->toArray() as $val)
            {
                $langs[$val['lang']] = $val;
            }
        } else {
            $langs = $this->view->langs;
        }
        $this->view->couponLang = $langs;
    }

    public function autoCompleteAction()
    {
        $criteria = array();

        if(ctype_digit($this->request->getPost("value"))){
            $searchField = 'c.id';
        } else {
            $searchField = 'c.name';
        }
        $criteria['conditions'] = " c.id > 0 ";
        $criteria['conditions'] .= null !== $this->request->getPost("categoryID") ? " AND category_id LIKE '%".$this->request->getPost("categoryID")."%'" : '';
        $criteria['conditions'] .= null !== $this->request->getPost("productID") ? " AND product_id LIKE '%".$this->request->getPost("productID")."%'" : '';
        $criteria['conditions'] .= " AND (c.id LIKE '%".addslashes($this->request->getPost("value"))."%' OR c.name LIKE '%".addslashes($this->request->getPost("value"))."%')";
        $criteria['conditions'] .= " AND coupon_type IN ('1','2','3')";

        $searchedColumn = CRUDQueries::getSearchCouponsReports($searchField,$criteria);

        echo json_encode($searchedColumn);

        $this->view->disable();
    }

    public function validateNameAction(){
        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        $result = $model::findFirst(
            array(
                "conditions"=>"name = ?1",
                "bind" => array(
                    1 => $this->request->getPost('name')
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }

    public function validatePosCouponIdAction(){
        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        $result = $model::findFirst(
            array(
                "conditions"=>"pos_coupon_id = ?1",
                "bind" => array(
                    1 => $this->request->getPost('name')
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }

    public function activateAction()
    {
        $this->view->disable();

        $model = $this->modelName;

        $updatedModel = $model::findFirst((int)$this->request->getPost("item-id"));

        $updatedModel->active = $this->request->getPost("active");

        $updatedModel->deactivated_manually = $this->request->getPost("active") == 1 ? 0 : 1;

        $updatedModel->created_at = BaseModel::toSQLDate($updatedModel->created_at);

        $updatedModel->save();
    }
}


