<?php

use \DataTables\DataTable;

class SegmentController extends ControllerBase {

    private $segment;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss
            ->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('js/funcs.js')
            ->addJs('js/segment.js');

        $this->modelName = 'Segment';
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }
    private function getAttributesAction()
    {
        return  [
            "ID",
            "Name",
            "Members",
            "Active",
            "CreatedAt",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getSegments($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function createAction()
    {
        $this->loadModels();
        if ($this->request->getPost("Segment"))
        {
            $this->saveModel();
        }

    }

    public function updateAction($id)
    {
        $this->loadModels($id);

        if ($this->segment->retention === "1")
        {
            $this->view->retention = true;
        }

        if ($this->request->getPost("Segment"))
        {
            $this->saveModel();
        }
    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }

    public function deleteAction()    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $this->view->disable();
            $id = $this->request->getPost('id');
            $model = $this->modelName;
            $instance = array();
            $instance['status'] = false;
            $modelObj = $model::findFirst((int)$id);
            $modelObj->setTransaction($transactions);
            if (!$modelObj->delete()){
                foreach ($modelObj->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }
            if($transactions->commit()){
                $instance['status'] = true;
            }
            echo json_encode($instance);
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $instance['error'][] = $e->getMessage();
            echo json_encode($instance);
        }
    }

    private function saveModel()
    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $this->segment->name = $this->request->getPost("Segment")["name"];
            if (isset($this->request->getPost("Segment")["active"]))
            {
                $this->segment->active = $this->request->getPost("Segment")["active"];
            }

            if (isset($this->request->getPost("Segment")["retention"]))
            {
                $this->segment->retention = $this->request->getPost("Segment")["retention"];
            }

            $this->segment->modifyCriteria($this->request->getPost("SegmentRule"));


            if($this->segment->created_at == '01/01/1970'){
                $this->segment->created_at = date('Y-m-d H:i:s');
            }
            $this->segment->updated_at = date('Y-m-d H:i:s');

            if (!$this->segment->save()){
                foreach ($this->segment->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }
            if($transactions->commit()){
                $this->flashSession->success("Your information was stored correctly!");
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $this->flashSession->error($e->getMessage());
            $this->response->redirect($this->theBaseUrl.$this->linkName);
        }
    }

    protected function loadModels($id = null)
    {
        !$this->request->getPost() ? parent::loadModels() : "";
        $this->segment = $this->view->segment = $id === null ? new Segment : Segment::findFirst((int)$id);
        $this->view->segmentRule = $id === null ? $this->segment->criteria : json_decode($this->segment->criteria,true);
        $this->view->cities = City::find();
        $this->view->products = Product::find();
        $this->view->categories = Category::find();
    }

    public function validateNameAction(){
        $this->view->disable();
        $response = true;
        $model = $this->modelName;
        $result = $model::findFirst(
            array(
                "conditions"=>"name = ?1",
                "bind" => array(
                    1 => $this->request->getPost('name')
                )
            )
        );
        if($result){
            if($this->request->getPost('id_param') != $result->id ){
                $response = false;
            }
        }
        echo json_encode($response);
    }

    public function activateAction()
    {
        $this->view->disable();

        $model = $this->modelName;

        $updatedModel = $model::findFirst((int)$this->request->getPost("item-id"));

        $updatedModel->active = $this->request->getPost("active");

        $updatedModel->created_at = BaseModel::toSQLDate($updatedModel->created_at);

        $updatedModel->save();
    }
}