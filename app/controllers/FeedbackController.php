<?php

use \DataTables\DataTable;

class FeedbackController extends ControllerBase
{

    private $feedback;
    private $questions;
    private $translate;

    public function initialize()
    {
        parent::initialize();
        $this->assetsHeaderCss
            ->addCss('//cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css',false,false)
            ->addCss('css/main.css');
        $this->assetsFooter
            ->addJs('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',false,false)
            ->addJs('//cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js',false,false)
            ->addJs('//cdn.datatables.net/plug-ins/1.10.11/api/sum().js',false,false)
            ->addJs('bower_components/accounting/accounting.js')
            ->addJs('js/funcs.js')
            ->addJs('js/validators.js')
            ->addJs('js/feedback.js');
    }

    public function IndexAction(){
        $this->view->setVars(
            [
                "attributes" => $this->getAttributesAction(),
            ]);

    }
    private function getAttributesAction()
    {
        return  [
            "ID",
            "title",
            "Audience",
            "Status",
            "Actions"
        ];
    }

    public function getDataTableAction(){
        $this->view->disable();
        $array = CRUDQueries::getFeedbacks($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }
    public function getSegmentAction(){
        $this->view->disable();
        $array = CRUDQueries::getSegmentsFeedback($this->request->getPost());
        $dataTable = new DataTable();
        $dataTable->fromArray($array)->sendResponse();
    }

    public function viewAction($id)
    {
        $this->loadModels($id);
    }


    public function createAction()
    {
        $this->loadModels();

        if ($this->request->getPost("Feedback")) {
            $this->saveModel();
        }

    }

    public function updateAction($id)
    {
        $this->loadModels($id);
        if ($this->request->getPost("Feedback")) {
            $this->saveModel();
        }
    }

    public function saveModel()
    {

        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();

            $this->feedback->setTransaction($transactions);
            if (!$this->feedback->save($this->request->getPost("Feedback"))){
                foreach ($this->feedback->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }

            foreach($this->request->getPost("FeedbackQuestion") as $langKey=>$FeedbackQuestion) {

                foreach ($FeedbackQuestion as $index => $feedbackQuestion) {

                    $question = !empty($feedbackQuestion['id']) ? FeedbackQuestion::findFirst($feedbackQuestion['id']) : new FeedbackQuestion();
                    $question->setTransaction($transactions);
                    if ($question->type == "rating" && $feedbackQuestion['type'] != "rating") {
                        $question->getRelated("FeedbackRating")[0]->delete();
                    }

                    $question->feedback_id = $this->feedback->id;
                    $question->title = $feedbackQuestion['title'];
                    $question->type = $feedbackQuestion['type'];
                    $question->lang = $feedbackQuestion['lang'];
                    if (!$question->save()) {
                        foreach ($question->getMessages() as $message) {
                            $transactions->rollback($message->getMessage());
                        }
                    }

                    if ($feedbackQuestion["type"] == "rating") {
                        $rating = isset($question->getRelated("FeedbackRating")[0]) ? $question->getRelated("FeedbackRating")[0] :
                            new FeedbackRating();
                        $rating->setTransaction($transactions);
                        $rating->id = $question->id;
                        if (!$rating->save($feedbackQuestion["rating"])) {
                            foreach ($rating->getMessages() as $message) {
                                $transactions->rollback($message->getMessage());
                            }
                        }
                    }

                    $translation = "";
                    if ($this->translate) {
                        foreach ($this->translate as $translate) {
                            if ($translate->lang == $langKey) {
                                if ($translate->question_id == $feedbackQuestion['id']) {
                                    $translation = $translate;
                                    $translation->setTransaction($transactions);
                                    $translation->question_id = $feedbackQuestion['id'];
                                    $translation->lang = $feedbackQuestion['lang'];
                                    $translation->title = $feedbackQuestion['title'];
                                    if (!$translation->save()) {
                                        foreach ($translation->getMessages() as $message) {
                                            $transactions->rollback($message->getMessage());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!$translation) {
                        $translation = new FeedbackTranslation();
                        $translation->setTransaction($transactions);
                        $translation->question_id = $feedbackQuestion['id'] ? $feedbackQuestion['id'] : $question->id;
                        $translation->lang = $langKey;
                        $translation->title = $feedbackQuestion['title'];
                        if (!$translation->save()) {
                            foreach ($translation->getMessages() as $message) {
                                $transactions->rollback($message->getMessage());
                            }
                        }
                    }
                }
            }

            foreach ($this->request->getPost("Segments") as $feedbackSegment)
            {
                $segment = new FeedbackSegment();
                $segment->setTransaction($transactions);
                $segment->feedback_id = $this->feedback->id;
                $segment->segment_id = $feedbackSegment;
                if(!$segment->save()){
                    foreach ($segment->getMessages() as $message) {
                        $transactions->rollback($message->getMessage());
                    }
                }
            }

            if($transactions->commit()){
                $this->flashSession->success("Your information was stored correctly!");
                $this->response->redirect($this->theBaseUrl.$this->linkName);
            }
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $this->flashSession->error($e->getMessage());
            $this->response->redirect($this->theBaseUrl.$this->linkName);
        }
    }

    public function deleteAction()    {
        try{
            $manager = $this->getDI()->getTransactions();
            $transactions = $manager->get();
            $this->view->disable();
            $id = $this->request->getPost('id');
            $model = $this->modelName;
            $instance = array();
            $instance['status'] = false;
            $modelObj = $model::findFirst((int)$id);
            $modelObj->setTransaction($transactions);
            if (!$modelObj->delete()){
                foreach ($modelObj->getMessages() as $message) {
                    $transactions->rollback($message->getMessage());
                }
            }
            if($transactions->commit()){
                $instance['status'] = true;
            }
            echo json_encode($instance);
        } catch (Phalcon\Mvc\Transaction\Failed $e){
            $instance['error'][] = $e->getMessage();
            echo json_encode($instance);
        }
    }


    protected function loadModels($id = null)
    {
        !$this->request->getPost() ? parent::loadModels() : "";
        $this->view->feedback = $this->feedback = $id === null ? new Feedback : Feedback::findFirst((int)$id);
        $this->view->activeSegments = $id !== null ? $this->feedback->getRelated("FeedbackSegment") : "";
        $this->view->langs = self::getDI()['settings']['availableLangs'];

        if ($id !== null) {
            $this->questions = $this->feedback->getRelated("FeedbackQuestion");
            $this->translate = $this->feedback->getRelated("FeedbackTranslation");
            $langs = [];
            foreach ($this->questions->toArray() as $key => $question) {
                foreach ($this->translate->toArray() as $translate) {
                    if ($question['id'] == $translate['question_id']) {
                        $question['title'] = $translate['title'];
                        $langs[$translate['lang']][] = $question;
                    }
                }
                $this->view->questions = $langs;
                $ratings = [];
                foreach ($this->questions as $question) {
                    if (isset($question->getRelated("FeedbackRating")[0])) {
                        $ratings[$question->id] = $question->getRelated("FeedbackRating")[0]->toArray();
                    }
                }
                $this->view->ratings = $ratings;
            }
        } else {
            $this->view->questions =  $this->view->langs;
            $this->view->ratings = [];
        }

    }

    public function activateAction()
    {
        $this->view->disable();

        $updatedModel = Feedback::findFirst((int)$this->request->getPost("item-id"));

        $updatedModel->active = $this->request->getPost("active");

        $updatedModel->created_at = BaseModel::toSQLDate($updatedModel->created_at);

        $updatedModel->save();
    }

}