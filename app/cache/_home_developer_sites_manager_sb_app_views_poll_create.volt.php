<?php echo $this->partial('poll/macroses', array()); ?>
<form action="" id="userEngagements" method="post" enctype="multipart/form-data" class="crudForm"
      xmlns="http://www.w3.org/1999/html">
    <div id="rootwizard" class="panel panel-default">
        <div class="panel-heading"><?php echo Lang::translate('CreateANewChallenge'); ?></div>
        <div class="panel-body">
            <ul class="step-tabs-nav nav nav-tabs" role="tablist">
                <?php echo $this->callMacro('step', array('active', 'engagementGeneralDetails', 'engagementGeneralDetails', 'fa-list', 'Step', 1, 'GeneralDetails')); ?>
                <?php echo $this->callMacro('step', array('optional', 'engagementAudience', 'engagementAudience', 'fa-users', 'Step', 2, 'Audience')); ?>
                <?php echo $this->callMacro('step', array('optional', 'engagementReward', 'engagementReward', 'fa-gift', 'Step', 3, 'Reward')); ?>
                <?php echo $this->callMacro('step', array('optional', 'pollData', 'engagementData', 'fa-check-square-o', 'Step', 4, 'Content')); ?>
                <?php echo $this->callMacro('step', array('optional', 'engagementPreview', 'engagementPreview', 'fa-eye', 'Step', 5, 'Preview')); ?>
            </ul>
            <div class="tab-content">
                <?php echo $this->callMacro('engagementGeneralDetails', array($engagement, $img, $baseUrl, $langs, $engagementLang)); ?>
                <?php echo $this->callMacro('engagementAudience', array()); ?>
                <?php echo $this->callMacro('engagementReward', array($coupon, $allowShare, $couponImg, $baseUrl, $langs, $couponLang, $engagement)); ?>
                <?php echo $this->callMacro('pollData', array($langs, $polls, $pollAnswers, $images, $baseUrl)); ?>
                <?php echo $this->callMacro('engagementPreview', array($img)); ?>
                <?php echo $this->callMacro('pagerwizard', array($baseUrl, 'poll')); ?>
            </div>
        </div>
    </div>
</form>
<?php echo $this->callMacro('addMoreAnswersMockUp', array()); ?>
