<?php echo $this->partial('segment/macroses', array()); ?>
<form action="" method="post" id="segmentMainData" class="crudForm <?php if (isset($retention)) { ?>retention<?php } ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="Segment[name]"><?php echo Lang::translate('SegmentName'); ?>*</label>
                <div class="form-inline">
                    <div class="form-group">
                        <label for="" class="sr-only"></label>
                        <?php echo $this->tag->textField(array('Segment[name]', 'class' => 'form-control required', 'value' => $segment->name, 'autocomplete' => 'off')); ?>
                    </div>
                    <div class="checkbox-inline">
                        <label class="checkbox-inline">
                            <?php echo $this->tag->checkField(array('Segment[active]', 'class' => 'icheck', 'value' => '1', (($segment->active == '1' ? 'checked' : '')))); ?> <?php echo Lang::translate('Active'); ?></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-user"></i> <?php echo Lang::translate('GeneralInfo'); ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label><?php echo Lang::translate('Gender'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="hidden" name="SegmentRule[profileRules][gender]" value="">
                                        <?php echo $this->callMacro('gender', array($segmentRule)); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label><?php echo Lang::translate('Married'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="hidden" name="SegmentRule[profileRules][marital_status]" value="">
                                        <?php echo $this->callMacro('married', array($segmentRule)); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="SegmentRule[profileRules][birthdate]"><?php echo Lang::translate('Birthday'); ?> <?php echo Lang::translate('Within'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('SegmentRule[profileRules][birth_date]', 'class' => 'form-control', 'value' => $segmentRule['profileRules']['birth_date'])); ?>
                                        <span class="formSpan"><?php echo Lang::translate('Days'); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[wedding_anniversary]"><?php echo Lang::translate('WeddingAnniversary'); ?> <?php echo Lang::translate('Within'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('SegmentRule[profileRules][wedding_anniversary_date]', 'class' => 'form-control', 'value' => $segmentRule['profileRules']['wedding_anniversary_date'])); ?>
                                        <span class="formSpan"><?php echo Lang::translate('Days'); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[profileRules][join_date]"><?php echo Lang::translate('LCJoiningAnniversary'); ?> <?php echo Lang::translate('Within'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('SegmentRule[profileRules][join_date]', 'class' => 'form-control', 'value' => $segmentRule['profileRules']['join_date'])); ?>
                                        <span class="formSpan"><?php echo Lang::translate('Days'); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label><?php echo Lang::translate('Age'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('SegmentRule[profileRules][age][min]', 'class' => 'form-control minVal', 'value' => $segmentRule['profileRules']['age']['min'])); ?>
                                        <label for="" class="sr-only"></label>
                                        <span class="formSpan fr"><?php echo Lang::translate('To'); ?></span>
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('SegmentRule[profileRules][age][max]', 'class' => 'form-control maxVal', 'value' => $segmentRule['profileRules']['age']['max'])); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[city_id][]"><?php echo Lang::translate('City'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <select class="select2"
                                                name="SegmentRule[profileRules][city_id][]"
                                                multiple>
                                            <?php if ((is_array($cities) || ($cities) instanceof Traversable)) { ?>
                                            <?php foreach ($cities as $city) { ?>
                                                <option
                                                        <?php if ((isset($segmentRule['profileRules']['city_id'])) && ((is_array($segmentRule['profileRules']['city_id']) || ($segmentRule['profileRules']['city_id']) instanceof Traversable))) { ?>
                                                            <?php foreach ($segmentRule['profileRules']['city_id'] as $id) { ?>
                                                                <?php if ($id == $city->id) { ?> selected <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        value="<?php echo $city->id; ?>"><?php echo $city->city; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-shopping-cart"></i> <?php echo Lang::translate('StoreVisitHabits'); ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="retentionWrapper" class="checkbox">
                        <label>
                            <?php echo $this->tag->checkField(array('Segment[retention]', 'id' => 'retention', 'class' => 'icheck', 'value' => '1', ((($segment->retention == '1') ? 'checked' : '')))); ?> <?php echo Lang::translate('Retention'); ?></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?php echo Lang::translate('BranchVisitFrequency'); ?></label>
                        <div class="form-inline">
                            <div class="form-group">
                                <?php echo $this->tag->textField(array('SegmentRule[branchVisitsRules][freq][min]', 'class' => 'form-control minVal', 'value' => $segmentRule['branchVisitsRules']['freq']['min'])); ?>
                                <span class="formSpan fr"><?php echo Lang::translate('To'); ?></span>
                                <?php echo $this->tag->textField(array('SegmentRule[branchVisitsRules][freq][max]', 'class' => 'form-control maxVal', 'value' => $segmentRule['branchVisitsRules']['freq']['max'])); ?>
                            </div>
                        </div>
                        <label class="radio-inline">
                            <?php echo $this->tag->radioField(array('SegmentRule[branchVisitsRules][freq][period]', 'class' => 'icheck', 'value' => 'week', (((isset($segmentRule['branchVisitsRules']['freq']['period'])) && (($segmentRule['branchVisitsRules']['freq']['period'] == 'week') || ($segmentRule['branchVisitsRules']['freq']['period'] == '')) ? 'checked' : '')))); ?> <?php echo Lang::translate('Week'); ?></label>
                        <label class="radio-inline">
                            <?php echo $this->tag->radioField(array('SegmentRule[branchVisitsRules][freq][period]', 'class' => 'icheck', 'value' => 'month', ((((isset($segmentRule['branchVisitsRules']['freq']['period'])) && ($segmentRule['branchVisitsRules']['freq']['period'] == 'month')) ? 'checked' : '')))); ?>  <?php echo Lang::translate('Month'); ?></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?php echo Lang::translate('BranchVisitHours'); ?></label>

                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <?php echo $this->tag->timeField(array('SegmentRule[branchVisitsRules][enter][min]', 'class' => 'form-control', 'value' => $segmentRule['branchVisitsRules']['enter']['min'])); ?>
                                <span class="formSpan fr"><?php echo Lang::translate('To'); ?></span>
                                <label for="" class="sr-only"></label>
                                <?php echo $this->tag->timeField(array('SegmentRule[branchVisitsRules][enter][max]', 'class' => 'form-control', 'value' => $segmentRule['branchVisitsRules']['enter']['max'])); ?>
                            </div>
                        </div>
                        <div class="checkbox">
                            <input type="hidden" name="SegmentRule[branchVisitsRules][abandon]" value="">
                            <label>
                                <?php echo $this->tag->checkField(array('SegmentRule[branchVisitsRules][abandon]', 'class' => 'icheck', 'value' => '1', (($segmentRule['branchVisitsRules']['abandon'] == '1' ? 'checked' : '')))); ?> <?php echo Lang::translate('BranchAbandon'); ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book"></i> <?php echo Lang::translate('PurchaseHistory'); ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="SegmentRule[transactionsRules][products_purchased][ids]"><?php echo Lang::translate('ProductsPurchased'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <select class="select2"
                                                name="SegmentRule[transactionsRules][products_purchased][ids][]"
                                                multiple>
                                            <?php if ((is_array($products) || ($products) instanceof Traversable)) { ?>
                                                <?php foreach ($products as $product) { ?>
                                                    <option
                                                            <?php if ((isset($segmentRule['transactionsRules']['products_purchased']['ids'])) && ((is_array($segmentRule['transactionsRules']['products_purchased']['ids']) || ($segmentRule['transactionsRules']['products_purchased']['ids']) instanceof Traversable))) { ?>
                                                                <?php foreach ($segmentRule['transactionsRules']['products_purchased']['ids'] as $id) { ?>
                                                                    <?php if ($id == $product->id) { ?>
                                                                        selected
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="radio">

                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('SegmentRule[transactionsRules][products_purchased][operator]', 'class' => 'icheck', 'value' => 'and', ((((isset($segmentRule['transactionsRules']['products_purchased']['operator'])) && (($segmentRule['transactionsRules']['products_purchased']['operator'] == 'and') || ($segmentRule['transactionsRules']['products_purchased']['operator'] == ''))) ? 'checked' : '')))); ?> <?php echo Lang::translate('And'); ?></label>

                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('SegmentRule[transactionsRules][products_purchased][operator]', 'class' => 'icheck', 'value' => 'or', ((((isset($segmentRule['transactionsRules']['products_purchased']['operator'])) && ($segmentRule['transactionsRules']['products_purchased']['operator'] == 'or')) ? 'checked' : '')))); ?> <?php echo Lang::translate('Or'); ?></label>

                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[transactionsRules][products_not_purchased][ids]"><?php echo Lang::translate('ProductsNotPurchased'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <select class="select2"
                                                    name="SegmentRule[transactionsRules][products_not_purchased][ids][]"
                                                    multiple>
                                                <?php if ((is_array($products) || ($products) instanceof Traversable)) { ?>
                                                    <?php foreach ($products as $product) { ?>
                                                        <option
                                                                <?php if (isset($segmentRule['transactionsRules']['products_not_purchased']['ids']) && (is_array($segmentRule['transactionsRules']['products_not_purchased']['ids']) || ($segmentRule['transactionsRules']['products_not_purchased']['ids']) instanceof Traversable)) { ?>
                                                                    <?php foreach ($segmentRule['transactionsRules']['products_not_purchased']['ids'] as $id) { ?>
                                                                        <?php if ($id == $product->id) { ?>
                                                                            selected
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('SegmentRule[transactionsRules][products_not_purchased][operator]', 'class' => 'icheck', 'value' => 'and', ((((isset($segmentRule['transactionsRules']['products_not_purchased']['operator'])) && (($segmentRule['transactionsRules']['products_not_purchased']['operator'] == 'and') || ($segmentRule['transactionsRules']['products_not_purchased']['operator'] == ''))) ? 'checked' : '')))); ?> <?php echo Lang::translate('And'); ?></label>
                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('SegmentRule[transactionsRules][products_not_purchased][operator]', 'class' => 'icheck', 'value' => 'or', ((((isset($segmentRule['transactionsRules']['products_not_purchased']['operator'])) && ($segmentRule['transactionsRules']['products_not_purchased']['operator'] == 'or')) ? 'checked' : '')))); ?> <?php echo Lang::translate('Or'); ?></label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="SegmentRule[transactionsRules][category_purchased][ids]"><?php echo Lang::translate('CategoriesPurchased'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <select class="select2"
                                                    name="SegmentRule[transactionsRules][category_purchased][ids][]"
                                                    multiple>
                                                <?php if ((is_array($categories) || ($categories) instanceof Traversable)) { ?>
                                                    <?php foreach ($categories as $category) { ?>
                                                        <option
                                                                <?php if (isset($segmentRule['transactionsRules']['category_purchased']['ids']) && (is_array($segmentRule['transactionsRules']['category_purchased']['ids']) || ($segmentRule['transactionsRules']['category_purchased']['ids']) instanceof Traversable)) { ?>
                                                                    <?php foreach ($segmentRule['transactionsRules']['category_purchased']['ids'] as $id) { ?>
                                                                        <?php if ($id == $category->id) { ?>
                                                                            selected
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                value="<?php echo $category->id; ?>"><?php echo $category->category; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('SegmentRule[transactionsRules][category_purchased][operator]', 'class' => 'icheck', 'value' => 'and', ((((isset($segmentRule['transactionsRules']['category_purchased']['operator'])) && (($segmentRule['transactionsRules']['category_purchased']['operator'] == 'and') || ($segmentRule['transactionsRules']['category_purchased']['operator'] == ''))) ? 'checked' : '')))); ?> <?php echo Lang::translate('And'); ?></label>
                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('SegmentRule[transactionsRules][category_purchased][operator]', 'class' => 'icheck', 'value' => 'or', ((((isset($segmentRule['transactionsRules']['category_purchased']['operator'])) && ($segmentRule['transactionsRules']['category_purchased']['operator'] == 'or')) ? 'checked' : '')))); ?> <?php echo Lang::translate('Or'); ?></label>

                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="SegmentRule[transactionsRules][products_not_purchased][ids][]"><?php echo Lang::translate('CategoriesNotPurchased'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <select class="select2"
                                                name="SegmentRule[transactionsRules][category_not_purchased][ids][]"
                                                multiple>
                                            <?php if ((is_array($categories) || ($categories) instanceof Traversable)) { ?>
                                                <?php foreach ($categories as $category) { ?>
                                                    <option
                                                            <?php if (isset($segmentRule['transactionsRules']['category_not_purchased']['ids']) && (is_array($segmentRule['transactionsRules']['category_not_purchased']['ids']) || ($segmentRule['transactionsRules']['category_not_purchased']['ids']) instanceof Traversable)) { ?>
                                                                <?php foreach ($segmentRule['transactionsRules']['category_not_purchased']['ids'] as $id) { ?>
                                                                    <?php if ($id == $category->id) { ?>
                                                                        selected
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            value="<?php echo $category->id; ?>"><?php echo $category->category; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('SegmentRule[transactionsRules][category_not_purchased][operator]', 'class' => 'icheck', 'value' => 'and', ((((isset($segmentRule['transactionsRules']['category_not_purchased']['operator'])) && (($segmentRule['transactionsRules']['category_not_purchased']['operator'] == 'and') || ($segmentRule['transactionsRules']['category_not_purchased']['operator'] == ''))) ? 'checked' : '')))); ?> <?php echo Lang::translate('And'); ?></label>
                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('SegmentRule[transactionsRules][category_not_purchased][operator]', 'class' => 'icheck', 'value' => 'or', ((((isset($segmentRule['transactionsRules']['category_not_purchased']['operator'])) && ($segmentRule['transactionsRules']['category_not_purchased']['operator'] == 'or')) ? 'checked' : '')))); ?>  <?php echo Lang::translate('Or'); ?></label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?php echo Lang::translate('AvgPurchasesMonth'); ?></label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <?php echo $this->tag->textField(array('SegmentRule[transactionsRules][last_month][min]', 'class' => 'form-control minVal', 'value' => $segmentRule['transactionsRules']['last_month']['min'])); ?>
                                <label for="" class="sr-only"></label>
                                <span class="formSpan fr"><?php echo Lang::translate('To'); ?></span>
                                <label for="" class="sr-only"></label>
                                <?php echo $this->tag->textField(array('SegmentRule[transactionsRules][last_month][max]', 'class' => 'form-control maxVal', 'value' => $segmentRule['transactionsRules']['last_month']['max'])); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?php echo Lang::translate('AvgPurchasesYear'); ?></label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <?php echo $this->tag->textField(array('SegmentRule[transactionsRules][last_year][min]', 'class' => 'form-control minVal', 'value' => $segmentRule['transactionsRules']['last_year']['min'])); ?>
                                <label for="" class="sr-only"></label>
                                <span class="formSpan fr"><?php echo Lang::translate('To'); ?></span>
                                <label for="" class="sr-only"></label>
                                <?php echo $this->tag->textField(array('SegmentRule[transactionsRules][last_year][max]', 'class' => 'form-control maxVal', 'value' => $segmentRule['transactionsRules']['last_year']['max'])); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php if ($this->router->getActionName() == 'view') { ?>
        <a href="<?php echo $baseUrl; ?>segment" class="btn btn-primary fl backToList"><?php echo Lang::translate('BackToList'); ?></a>
    <?php } else { ?>
        <input type="submit" value="<?php echo Lang::translate('Save'); ?>" class="btn btn-primary fl">
        <a href="<?php echo $baseUrl; ?>segment" class="btn btn-primary backToList"><?php echo Lang::translate('Abort'); ?></a>
    <?php } ?>
</form>
