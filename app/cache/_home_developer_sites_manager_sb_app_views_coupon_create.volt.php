<?php echo $this->partial('coupon/macroses', array()); ?>
<?php $updateVar = ''; ?>
<?php if ($this->router->getActionName() != 'update') { ?><?php $updateVar = ', "unique"'; ?><?php } ?>

<form action="" method="post" enctype="multipart/form-data" class="crudForm" role="form">
    <div class="panel panel-default <?php if ($this->router->getActionName() == 'view') { ?> readOnly<?php } ?>">
        <div class="panel-heading"><i class="fa fa-tag marginLeft"></i> Header</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="Coupon[name]"><?php echo Lang::translate('Name'); ?>*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('Coupon[name]', 'class' => 'required form-control', 'value' => $coupon->name, 'id' => 'couponName')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="Coupon[pos_coupon_id]"><?php echo Lang::translate('CouponNumber'); ?>*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('Coupon[pos_coupon_id]', 'class' => 'required form-control', 'value' => $coupon->pos_coupon_id, 'id' => 'couponID')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="Coupon[max_coupons]"><?php echo Lang::translate('TotalUnits'); ?>*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('Coupon[max_coupons]', 'class' => 'required form-control', 'value' => $coupon->max_coupons, 'id' => 'couponTotalUnits')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="Coupon[start_date]"><?php echo Lang::translate('StartDate'); ?>*</label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('Coupon[start_date]', 'class' => 'form-control minVal required datepicker', 'value' => $coupon->start_date, 'id' => 'couponTotalUnits')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="Coupon[expiry_date]"><?php echo Lang::translate('EndDate'); ?>*</label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('Coupon[expiry_date]', 'class' => 'form-control maxVal required datepicker', 'value' => $coupon->expiry_date, 'id' => 'couponTotalUnits')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                            <div class="form-group">
                                <label for="CouponSegment[]"><?php echo Lang::translate('Audience'); ?></label>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <select multiple class="select2" name="CouponSegment[]">
                                            <?php if ((is_array($segments) || ($segments) instanceof Traversable)) { ?>
                                                <?php foreach ($segments as $segment) { ?>
                                                <option
                                                        <?php if ((is_array($couponSegments) || ($couponSegments) instanceof Traversable)) { ?>
                                                            <?php foreach ($couponSegments as $couponSegment) { ?>
                                                            <?php if ($segment->id == $couponSegment->segment_id) { ?>selected<?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        value="<?php echo $segment->id; ?>"><?php echo $segment->name; ?>
                                                </option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="Coupon[coupon_type]"><?php echo Lang::translate('Type'); ?></label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <select class="select2 select2NoSearch" name="Coupon[coupon_type]">
                                    <?php $types = array('Discount', 'BuyAndGet', 'Gift'); ?>
                                    <?php $v16448846382686667331iterator = $types; $v16448846382686667331incr = 0; $v16448846382686667331loop = new stdClass(); $v16448846382686667331loop->length = count($v16448846382686667331iterator); $v16448846382686667331loop->index = 1; $v16448846382686667331loop->index0 = 1; $v16448846382686667331loop->revindex = $v16448846382686667331loop->length; $v16448846382686667331loop->revindex0 = $v16448846382686667331loop->length - 1; ?><?php foreach ($v16448846382686667331iterator as $i) { ?><?php $v16448846382686667331loop->first = ($v16448846382686667331incr == 0); $v16448846382686667331loop->index = $v16448846382686667331incr + 1; $v16448846382686667331loop->index0 = $v16448846382686667331incr; $v16448846382686667331loop->revindex = $v16448846382686667331loop->length - $v16448846382686667331incr; $v16448846382686667331loop->revindex0 = $v16448846382686667331loop->length - ($v16448846382686667331incr + 1); $v16448846382686667331loop->last = ($v16448846382686667331incr == ($v16448846382686667331loop->length - 1)); ?>
                                    <option <?php if ($coupon->coupon_type == $v16448846382686667331loop->index) { ?>selected<?php } ?>
                                            value="<?php echo $v16448846382686667331loop->index; ?>"><?php echo Lang::translate($i); ?>
                                    </option>
                                    <?php $v16448846382686667331incr++; } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="Coupon[couponImage]"><?php echo Lang::translate('Image'); ?></label>

                        <div class="form-group">
                            <img class="img-thumbnail previewImage" <?php if (!empty($img)) { ?>src="<?php echo $img; ?>"<?php } ?>>
                        </div>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <?php echo $this->tag->fileField(array('couponImage', 'class' => 'imageUpload', 'id' => 'couponImageUpload', 'data-text' => ((!empty($img) ? (Lang::translate('ChangeImage')) : (Lang::translate('ChooseImage')))))); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label><?php echo Lang::translate('Barcode'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <?php echo $this->tag->textField(array('Product[barcode]', 'class' => 'form-control required', 'value' => ((isset($barcode->barcode) ? $barcode->barcode : '')))); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Coupon[category_id]"><?php echo Lang::translate('Category'); ?></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <textarea rows="1" name="" class="form-control c-control"><?php if (isset($rootCategories)) { ?><?php echo trim($rootCategories->category); ?><?php } ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="checkbox-inline">
                        <input type="hidden" value="0" name="Coupon[active]">
                        <label>
                            <?php echo $this->tag->checkField(array('Coupon[active]', 'value' => '1', 'class' => 'icheck', (($coupon->active == '1' ? 'checked' : '')))); ?> <?php echo Lang::translate('Enabled'); ?></label>
                    </div>
                    <div class="checkbox-inline">
                        <input type="hidden" value="0" name="Coupon[promoted]">
                        <label>
                            <?php echo $this->tag->checkField(array('Coupon[promoted]', 'value' => '1', 'class' => 'icheck', (($coupon->promoted == '1' ? 'checked' : '')))); ?> <?php echo Lang::translate('Promoted'); ?></label>
                    </div>
                    <?php if ($allowShare) { ?>
                    <div class="checkbox-inline">
                        <input type="hidden" value="0" name="Coupon[shared]">
                        <label>
                            <?php echo $this->tag->checkField(array('Coupon[shared]', 'value' => '1', 'class' => 'icheck', (($coupon->shared == '1' ? 'checked' : '')))); ?> <?php echo Lang::translate('Shareable'); ?></label>
                    </div>
                    <?php } ?>
                    <div class="checkbox-inline">
                        <input type="hidden" value="0" name="Coupon[public]">
                        <label>
                            <?php echo $this->tag->checkField(array('Coupon[public]', 'value' => '1', 'class' => 'icheck', (($coupon->public == '1' ? 'checked' : '')))); ?>  <?php echo Lang::translate('Public'); ?></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-tags marginLeft"></i> Header</div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist">
                <?php $v16448846382686667331iterator = $langs; $v16448846382686667331incr = 0; $v16448846382686667331loop = new stdClass(); $v16448846382686667331loop->length = count($v16448846382686667331iterator); $v16448846382686667331loop->index = 1; $v16448846382686667331loop->index0 = 1; $v16448846382686667331loop->revindex = $v16448846382686667331loop->length; $v16448846382686667331loop->revindex0 = $v16448846382686667331loop->length - 1; ?><?php foreach ($v16448846382686667331iterator as $label => $lang) { ?><?php $v16448846382686667331loop->first = ($v16448846382686667331incr == 0); $v16448846382686667331loop->index = $v16448846382686667331incr + 1; $v16448846382686667331loop->index0 = $v16448846382686667331incr; $v16448846382686667331loop->revindex = $v16448846382686667331loop->length - $v16448846382686667331incr; $v16448846382686667331loop->revindex0 = $v16448846382686667331loop->length - ($v16448846382686667331incr + 1); $v16448846382686667331loop->last = ($v16448846382686667331incr == ($v16448846382686667331loop->length - 1)); ?>
                <li role="presentation" class="langTab fr <?php if ($v16448846382686667331loop->index0 == 0) { ?>active<?php } ?>" data-lang="<?php echo $lang; ?>">
                    <a href="#coupon_<?php echo $lang; ?>" aria-controls="coupon_<?php echo $lang; ?>" role="tab" data-toggle="tab"><?php echo Lang::translate($label); ?></a></li>
                <?php $v16448846382686667331incr++; } ?>
            </ul>

            <div class="tab-content">
                <?php $v16448846382686667331iterator = $langs; $v16448846382686667331incr = 0; $v16448846382686667331loop = new stdClass(); $v16448846382686667331loop->length = count($v16448846382686667331iterator); $v16448846382686667331loop->index = 1; $v16448846382686667331loop->index0 = 1; $v16448846382686667331loop->revindex = $v16448846382686667331loop->length; $v16448846382686667331loop->revindex0 = $v16448846382686667331loop->length - 1; ?><?php foreach ($v16448846382686667331iterator as $i) { ?><?php $v16448846382686667331loop->first = ($v16448846382686667331incr == 0); $v16448846382686667331loop->index = $v16448846382686667331incr + 1; $v16448846382686667331loop->index0 = $v16448846382686667331incr; $v16448846382686667331loop->revindex = $v16448846382686667331loop->length - $v16448846382686667331incr; $v16448846382686667331loop->revindex0 = $v16448846382686667331loop->length - ($v16448846382686667331incr + 1); $v16448846382686667331loop->last = ($v16448846382686667331incr == ($v16448846382686667331loop->length - 1)); ?>
                    <?php echo $this->callMacro('tabpanel', array($i, $couponLang)); ?>
                <?php $v16448846382686667331incr++; } ?>
            </div>
        </div>
    </div>
    <?php echo $this->callMacro('buttons', array($baseUrl)); ?>
</form>
<?php echo $this->callMacro('modal', array($coupon, $img, $baseUrl)); ?>