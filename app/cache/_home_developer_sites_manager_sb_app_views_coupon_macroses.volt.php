<?php $this->_macros['tabpanel'] = function($__p = null) { if (isset($__p[0])) { $i = $__p[0]; } else { if (isset($__p["i"])) { $i = $__p["i"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'tabpanel' was called without parameter: i");  } } if (isset($__p[1])) { $couponLang = $__p[1]; } else { if (isset($__p["couponLang"])) { $couponLang = $__p["couponLang"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'tabpanel' was called without parameter: couponLang");  } }  ?>
    <div role="tabpanel" id="coupon_<?php echo $i; ?>" class="tab-pane langTabData
        <?php if ($i == 'he') { ?>active<?php } ?>
        <?php if ($this->router->getActionName() == 'view') { ?> readOnly<?php } ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-group"></div>
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="CouponLang[<?php echo $i; ?>][title]"><?php echo Lang::translate('Title'); ?>*</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <input class="form-control couponTitle"
                                           <?php if (isset($couponLang[$i])) { ?>value="<?php echo $couponLang[$i]['title']; ?>"
                                            <?php } ?>
                                           type="text"
                                           name="CouponLang[<?php echo $i; ?>][title]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group">
                            <label for="CouponLang[<?php echo $i; ?>][sub_title]"><?php echo Lang::translate('SubTitle'); ?></label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <input class="couponSubTitle form-control" type="text"
                                           name="CouponLang[<?php echo $i; ?>][sub_title]"
                                           <?php if (isset($couponLang[$i])) { ?>value="<?php echo $couponLang[$i]['sub_title']; ?>" <?php } ?>>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="CouponLang[<?php echo $i; ?>][details]"><?php echo Lang::translate('Details'); ?></label>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                                    <textarea class="form-control couponDetails"
                                                              name="CouponLang[<?php echo $i; ?>][details]">
                                                        <?php if (isset($couponLang[$i])) { ?><?php echo $couponLang[$i]['details']; ?><?php } ?>
                                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group">
                            <label for="CouponLang[<?php echo $i; ?>][disclaimer]"><?php echo Lang::translate('Disclaimer'); ?></label>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                                    <textarea class="form-control couponDisclaimer"
                                                              name="CouponLang[<?php echo $i; ?>][disclaimer]">
                                                        <?php if (isset($couponLang[$i])) { ?><?php echo $couponLang[$i]['disclaimer']; ?><?php } ?>
                                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><?php }; $this->_macros['tabpanel'] = \Closure::bind($this->_macros['tabpanel'], $this); ?><?php $this->_macros['modal'] = function($__p = null) { if (isset($__p[0])) { $coupon = $__p[0]; } else { if (isset($__p["coupon"])) { $coupon = $__p["coupon"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: coupon");  } } if (isset($__p[1])) { $img = $__p[1]; } else { if (isset($__p["img"])) { $img = $__p["img"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: img");  } } if (isset($__p[2])) { $baseUrl = $__p[2]; } else { if (isset($__p["baseUrl"])) { $baseUrl = $__p["baseUrl"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: baseUrl");  } }  ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header previewHeader">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img class="modal-title" id="myModalLabel" src="<?php echo $baseUrl; ?>img/MHLogo.png">
            </div>
            <div class="modal-body">
                <div id="couponPreview" class="preview">


                    <h1 id="previewTitle"><?php echo $coupon->title; ?></h1>
                    <h3 id="previewSubTitle"><?php echo $coupon->sub_title; ?></h3>
                    <figure>
                        <img class="img-responsive" id="couponPreviewImage" <?php if (isset($img)) { ?>src="<?php if (!empty($img)) { ?><?php echo $img; ?><?php } ?>"<?php } ?>>
                    </figure>
                    <h3 id="previewDetails"><?php echo $coupon->details; ?></h3>
                    <h4 id="previewDisclaimer"><?php echo $coupon->disclaimer; ?></h4>


                </div>
            </div>
            <div class="modal-footer previewFooter">
                <img src="<?php echo $baseUrl; ?>img/couponPreviewFooter.png">
            </div>
        </div>
    </div>
</div><?php }; $this->_macros['modal'] = \Closure::bind($this->_macros['modal'], $this); ?><?php $this->_macros['buttons'] = function($__p = null) { if (isset($__p[0])) { $baseUrl = $__p[0]; } else { if (isset($__p["baseUrl"])) { $baseUrl = $__p["baseUrl"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'buttons' was called without parameter: baseUrl");  } }  ?>
<?php if ($this->router->getActionName() == 'view') { ?>
    <a href="<?php echo $baseUrl; ?>coupon" class="btn btn-primary fr backToList"><?php echo Lang::translate('BackToList'); ?></a>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="couponPreviewButton"><?php echo Lang::translate('Preview'); ?></button>
<?php } else { ?>
    <input class="btn btn-primary fl" type="submit" value="<?php echo Lang::translate('Save'); ?>" >
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="couponPreviewButton"><?php echo Lang::translate('Preview'); ?></button>
    <a href="<?php echo $baseUrl; ?>coupon" class="btn btn-primary fr backToList"><?php echo Lang::translate('Abort'); ?></a>
<?php } ?><?php }; $this->_macros['buttons'] = \Closure::bind($this->_macros['buttons'], $this); ?>