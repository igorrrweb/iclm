<?php $this->_macros['buttons'] = function($__p = null) { if (isset($__p[0])) { $baseUrl = $__p[0]; } else { if (isset($__p["baseUrl"])) { $baseUrl = $__p["baseUrl"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'buttons' was called without parameter: baseUrl");  } }  ?>
    <div class="form-group">
    <?php if ($this->router->getActionName() == 'view') { ?>
        <a href="<?php echo $baseUrl; ?>customer" class="btn btn-primary backToList fl"><?php echo Lang::translate('BackToList'); ?></a>
    <?php } else { ?>
        <input type="submit" value="<?php echo Lang::translate('Save'); ?>" class="btn btn-primary fl">
        <a href="<?php echo $baseUrl; ?>customer" class="btn btn-primary backToList"><?php echo Lang::translate('Abort'); ?></a>
    <?php } ?>
    </div><?php }; $this->_macros['buttons'] = \Closure::bind($this->_macros['buttons'], $this); ?>