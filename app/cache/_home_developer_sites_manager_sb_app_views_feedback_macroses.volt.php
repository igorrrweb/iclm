<?php $this->_macros['questions'] = function($__p = null) { if (isset($__p[0])) { $a = $__p[0]; } else { if (isset($__p["a"])) { $a = $__p["a"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'questions' was called without parameter: a");  } } if (isset($__p[1])) { $questions = $__p[1]; } else { if (isset($__p["questions"])) { $questions = $__p["questions"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'questions' was called without parameter: questions");  } } if (isset($__p[2])) { $ratings = $__p[2]; } else { if (isset($__p["ratings"])) { $ratings = $__p["ratings"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'questions' was called without parameter: ratings");  } }  ?>

    <div role="tabpanel" id="<?php echo $a; ?>" class="tab-pane langTabData<?php if ($a == 'he') { ?> active<?php } ?>">
        <div class="form-group"></div>

        <?php echo $this->callMacro('questions_row', array($a, $questions, $ratings)); ?>

        <button type="button" class="btn btn-link addMoreQuestions"><?php echo Lang::translate('AddMoreQuestions'); ?></button>
    </div><?php }; $this->_macros['questions'] = \Closure::bind($this->_macros['questions'], $this); ?><?php $this->_macros['questions_row'] = function($__p = null) { if (isset($__p[0])) { $a = $__p[0]; } else { if (isset($__p["a"])) { $a = $__p["a"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'questions_row' was called without parameter: a");  } } if (isset($__p[1])) { $questions = $__p[1]; } else { if (isset($__p["questions"])) { $questions = $__p["questions"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'questions_row' was called without parameter: questions");  } } if (isset($__p[2])) { $ratings = $__p[2]; } else { if (isset($__p["ratings"])) { $ratings = $__p["ratings"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'questions_row' was called without parameter: ratings");  } }  ?>
    <?php $counter = 0; ?>
    <?php if (isset($questions[$a])) : ?>
    <div class="form-group feedbackQuestions" data-lang="<?php echo $a; ?>">
        <?php $v145080660383992557191iterator = $questions[$a]; $v145080660383992557191incr = 0; $v145080660383992557191loop = new stdClass(); $v145080660383992557191loop->length = count($v145080660383992557191iterator); $v145080660383992557191loop->index = 1; $v145080660383992557191loop->index0 = 1; $v145080660383992557191loop->revindex = $v145080660383992557191loop->length; $v145080660383992557191loop->revindex0 = $v145080660383992557191loop->length - 1; ?><?php foreach ($v145080660383992557191iterator as $key => $question) { ?><?php $v145080660383992557191loop->first = ($v145080660383992557191incr == 0); $v145080660383992557191loop->index = $v145080660383992557191incr + 1; $v145080660383992557191loop->index0 = $v145080660383992557191incr; $v145080660383992557191loop->revindex = $v145080660383992557191loop->length - $v145080660383992557191incr; $v145080660383992557191loop->revindex0 = $v145080660383992557191loop->length - ($v145080660383992557191incr + 1); $v145080660383992557191loop->last = ($v145080660383992557191incr == ($v145080660383992557191loop->length - 1)); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for=""><span class="icon fr"><?php echo $v145080660383992557191loop->index; ?></span></label>
                                <input type="text" value="<?php echo $question['title']; ?>"
                                       name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][title]"
                                       class="form-control questionTitle">
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group">
                                <label><?php echo Lang::translate('QuestionType'); ?></label>
                                <select class="select2 questionType"
                                        name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][type]">
                                    <option value="free_text" <?php if ($question['type'] == 'free_text') { ?>selected<?php } ?>><?php echo Lang::translate('FreeText'); ?></option>
                                    <option value="yes_no" <?php if ($question['type'] == 'yes_no') { ?>selected<?php } ?>><?php echo Lang::translate('YesNo'); ?></option>
                                    <option value="rating" <?php if ($question['type'] == 'rating') { ?>selected<?php } ?>><?php echo Lang::translate('Rating'); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group rating <?php if ($question['type'] != 'rating') { ?>hidden<?php } ?>">
                        <div class="form-inline">
                            <div class="form-group">
                                <ul>
                                    <label><?php echo Lang::translate('RatingValues'); ?></label>
                                    <?php foreach (range(1, 5) as $i) { ?>
                                        <li class="feedbackRating fr">
                                            <div class="form-group">
                                                <label for="" class="sr-only"></label>
                                                <input type="text" class="form-control" <?php if (isset($ratings[$question['id']])) { ?>
                                                    value="<?php echo $ratings[$question['id']]['value' . $i]; ?>"<?php } ?>
                                                       name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][rating][value<?php echo $i; ?>]"
                                                       placeholder="<?php echo $i; ?> <?php echo Lang::translate('Stars'); ?>"></div>
                                        </li>
                                    <?php } ?>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][lang]" value="<?php echo $a; ?>">
            <input type="hidden" name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][id]" value="<?php echo $question['id']; ?>">

            <?php $counter += 1; ?>
        <?php $v145080660383992557191incr++; } ?>
    </div>
    <?php else : ?>
    <?php $v145080660383992557191iterator = range(1, 2); $v145080660383992557191incr = 0; $v145080660383992557191loop = new stdClass(); $v145080660383992557191loop->length = count($v145080660383992557191iterator); $v145080660383992557191loop->index = 1; $v145080660383992557191loop->index0 = 1; $v145080660383992557191loop->revindex = $v145080660383992557191loop->length; $v145080660383992557191loop->revindex0 = $v145080660383992557191loop->length - 1; ?><?php foreach ($v145080660383992557191iterator as $num) { ?><?php $v145080660383992557191loop->first = ($v145080660383992557191incr == 0); $v145080660383992557191loop->index = $v145080660383992557191incr + 1; $v145080660383992557191loop->index0 = $v145080660383992557191incr; $v145080660383992557191loop->revindex = $v145080660383992557191loop->length - $v145080660383992557191incr; $v145080660383992557191loop->revindex0 = $v145080660383992557191loop->length - ($v145080660383992557191incr + 1); $v145080660383992557191loop->last = ($v145080660383992557191incr == ($v145080660383992557191loop->length - 1)); ?>
        <div class="form-group feedbackQuestions" data-lang="<?php echo $a; ?>">
            <?php echo $this->callMacro('clear_row', array($a, $num, $counter)); ?>
        </div>
        <?php $counter += 1; ?>
    <?php $v145080660383992557191incr++; } ?>
    <?php endif ?><?php }; $this->_macros['questions_row'] = \Closure::bind($this->_macros['questions_row'], $this); ?><?php $this->_macros['clear_row'] = function($__p = null) { if (isset($__p[0])) { $a = $__p[0]; } else { if (isset($__p["a"])) { $a = $__p["a"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'clear_row' was called without parameter: a");  } } if (isset($__p[1])) { $num = $__p[1]; } else { if (isset($__p["num"])) { $num = $__p["num"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'clear_row' was called without parameter: num");  } } if (isset($__p[2])) { $counter = $__p[2]; } else { if (isset($__p["counter"])) { $counter = $__p["counter"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'clear_row' was called without parameter: counter");  } }  ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-inline">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for=""><span class="icon fr"><?php echo $num; ?></span></label>
                                <input type="text"
                                       class="form-control questionTitle"
                                       name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][title]">

                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group">
                        <label><?php echo Lang::translate('QuestionType'); ?></label>
                        <select class="select2 questionType"
                                name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][type]">
                            <option value="free_text"><?php echo Lang::translate('FreeText'); ?></option>
                            <option value="yes_no"><?php echo Lang::translate('YesNo'); ?></option>
                            <option value="rating"><?php echo Lang::translate('Rating'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group rating hidden">
                <div class="form-inline">
                    <div class="form-group">
                        <ul>
                            <label><?php echo Lang::translate('RatingValues'); ?></label>
                            <?php $v145080660383992557191iterator = range(1, 5); $v145080660383992557191incr = 0; $v145080660383992557191loop = new stdClass(); $v145080660383992557191loop->length = count($v145080660383992557191iterator); $v145080660383992557191loop->index = 1; $v145080660383992557191loop->index0 = 1; $v145080660383992557191loop->revindex = $v145080660383992557191loop->length; $v145080660383992557191loop->revindex0 = $v145080660383992557191loop->length - 1; ?><?php foreach ($v145080660383992557191iterator as $i) { ?><?php $v145080660383992557191loop->first = ($v145080660383992557191incr == 0); $v145080660383992557191loop->index = $v145080660383992557191incr + 1; $v145080660383992557191loop->index0 = $v145080660383992557191incr; $v145080660383992557191loop->revindex = $v145080660383992557191loop->length - $v145080660383992557191incr; $v145080660383992557191loop->revindex0 = $v145080660383992557191loop->length - ($v145080660383992557191incr + 1); $v145080660383992557191loop->last = ($v145080660383992557191incr == ($v145080660383992557191loop->length - 1)); ?>
                                <li class="feedbackRating fr">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <input type="text" class="form-control"
                                               name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][rating][value<?php echo $i; ?>]"
                                               placeholder="<?php echo $i; ?> <?php echo Lang::translate('Stars'); ?>">
                                    </div>
                                </li>
                            <?php $v145080660383992557191incr++; } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][lang]" value="<?php echo $a; ?>">
    <input type="hidden" name="FeedbackQuestion[<?php echo $a; ?>][<?php echo $counter; ?>][id]" value=""><?php }; $this->_macros['clear_row'] = \Closure::bind($this->_macros['clear_row'], $this); ?>