<?php echo $this->partial('feedback/macroses', array()); ?>
<form action="" method="post" class="crudForm <?php if ($this->router->getActionName() == 'view') { ?> readOnly<?php } ?>">
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-edit marginLeft"></i> <?php echo Lang::translate('CreateNewFeedbackForm'); ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="Feedback[title]"><?php echo Lang::translate('Title'); ?>*</label>
                                <input id="feedbackTitle" class="form-control required" value="<?php echo $feedback->title; ?>"
                                       type="text"
                                       name="Feedback[title]">
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group">
                                <label for="Feedback[sub_title]"><?php echo Lang::translate('SubTitle'); ?>*</label>
                                <input id="feedbackSubTitle" class="form-control required"
                                       value="<?php echo $feedback->sub_title; ?>"
                                       type="text"
                                       name="Feedback[sub_title]">
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group">
                                <label><input type="radio" class="icheck" name="Feedback[active]" value="1" <?php if (($feedback->active == '1') || !isset($feedback->active)) { ?> checked<?php } ?>> <?php echo Lang::translate('Yes'); ?></label>
                            </div>
                            <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                            <div class="form-group">
                                <label><input type="radio" class="icheck" name="Feedback[active]" value="0" <?php if (($feedback->active == '0')) { ?> checked<?php } ?>> <?php echo Lang::translate('No'); ?></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?php echo Lang::translate('Trigger'); ?></label>
                        <div class="radio">
                            <label><input type="radio" class="icheck" name="Feedback[trigger]" value="on_branch_enter"
                                        <?php if (($feedback->trigger == 'on_branch_enter') || $feedback->trigger == '') { ?>
                                checked<?php } ?>> <?php echo Lang::translate('OnBranchEntry'); ?></label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" class="icheck" name="Feedback[trigger]" value="not_in_branch" <?php if (($feedback->trigger == 'not_in_branch')) { ?> checked<?php } ?>> <?php echo Lang::translate('WhenNotInBranch'); ?></label>
                        </div>


                        <div class="radio">
                            <label><input type="radio" class="icheck" name="Feedback[trigger]"
                                          value="on_branch_leave"
                                        <?php if (($feedback->trigger == 'on_branch_leave')) { ?> checked<?php } ?>> <?php echo Lang::translate('OnBranchExitAfterPurchase'); ?></label>
                        </div>


                        <div class="radio">
                            <label><input type="radio" class="icheck" name="Feedback[trigger]"
                                          value="on_branch_leave_empty"
                                        <?php if (($feedback->trigger == 'on_branch_leave_empty')) { ?>
                                checked<?php } ?>> <?php echo Lang::translate('OnBranchExitNoPurchase'); ?></label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-question marginLeft"></i> <?php echo Lang::translate('Questions'); ?></div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist">
                <?php $v127723633073137679171iterator = $langs; $v127723633073137679171incr = 0; $v127723633073137679171loop = new stdClass(); $v127723633073137679171loop->length = count($v127723633073137679171iterator); $v127723633073137679171loop->index = 1; $v127723633073137679171loop->index0 = 1; $v127723633073137679171loop->revindex = $v127723633073137679171loop->length; $v127723633073137679171loop->revindex0 = $v127723633073137679171loop->length - 1; ?><?php foreach ($v127723633073137679171iterator as $label => $lang) { ?><?php $v127723633073137679171loop->first = ($v127723633073137679171incr == 0); $v127723633073137679171loop->index = $v127723633073137679171incr + 1; $v127723633073137679171loop->index0 = $v127723633073137679171incr; $v127723633073137679171loop->revindex = $v127723633073137679171loop->length - $v127723633073137679171incr; $v127723633073137679171loop->revindex0 = $v127723633073137679171loop->length - ($v127723633073137679171incr + 1); $v127723633073137679171loop->last = ($v127723633073137679171incr == ($v127723633073137679171loop->length - 1)); ?>
                    <li role="presentation"  class="langTab fr <?php if ($v127723633073137679171loop->index0 == 0) { ?>active<?php } ?>" data-lang="<?php echo $lang; ?>">
                        <a href="#<?php echo $lang; ?>" aria-controls="offered" role="tab" data-toggle="tab"><?php echo Lang::translate($label); ?></a></li>
                <?php $v127723633073137679171incr++; } ?>
            </ul>

            <div class="tab-content">
                <?php $v127723633073137679171iterator = $langs; $v127723633073137679171incr = 0; $v127723633073137679171loop = new stdClass(); $v127723633073137679171loop->length = count($v127723633073137679171iterator); $v127723633073137679171loop->index = 1; $v127723633073137679171loop->index0 = 1; $v127723633073137679171loop->revindex = $v127723633073137679171loop->length; $v127723633073137679171loop->revindex0 = $v127723633073137679171loop->length - 1; ?><?php foreach ($v127723633073137679171iterator as $a) { ?><?php $v127723633073137679171loop->first = ($v127723633073137679171incr == 0); $v127723633073137679171loop->index = $v127723633073137679171incr + 1; $v127723633073137679171loop->index0 = $v127723633073137679171incr; $v127723633073137679171loop->revindex = $v127723633073137679171loop->length - $v127723633073137679171incr; $v127723633073137679171loop->revindex0 = $v127723633073137679171loop->length - ($v127723633073137679171incr + 1); $v127723633073137679171loop->last = ($v127723633073137679171incr == ($v127723633073137679171loop->length - 1)); ?>
                    <?php echo $this->callMacro('questions', array($a, $questions, $ratings)); ?>
                <?php $v127723633073137679171incr++; } ?>
            </div>
        </div>
    </div>
    <div class="audience panel panel-default">
        <div class="panel-heading"><i class="fa fa-users marginLeft"></i> <?php echo Lang::translate('Audience'); ?></div>
        <div class="panel-body">
            <table id="segments"  width="100%" class="table table-striped table-bordered table-hover " <?php if ($this->router->getActionName() == 'view') { ?>readOnly<?php } ?>">
            <thead>
            <tr>
                <th></th>
                <th><?php echo Lang::translate('SegmentName'); ?></th>
                <th><?php echo Lang::translate('Audience'); ?></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
            <p class="hide jsTranslation"><?php echo Lang::translate('NoMatches'); ?></p>
            <div class="clearfix"></div>
            <p class="hide error" id="segmentError"> - <?php echo Lang::translate('PleaseChooseASegment'); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($this->router->getActionName() == 'view') { ?>
                    <a href="<?php echo $baseUrl; ?>feedback" class="btn btn-primary fl backToList" ><?php echo Lang::translate('BackToList'); ?></a>
                <?php } else { ?>
                    <input type="submit" value="<?php echo Lang::translate('Save'); ?>" class="btn btn-primary fl">
                    <a href="<?php echo $baseUrl; ?>feedback" class="btn btn-primary backToList"><?php echo Lang::translate('Abort'); ?></a>
                <?php } ?>
                <button type="button" class="btn btn-primary feedback-preview" data-toggle="modal" data-target="#feedbackPreview"><?php echo Lang::translate('Preview'); ?></button>
                
            </div>
        </div>
    </div>

</form>
<!-- Modal -->
<div class="modal fade" id="feedbackPreview" tabindex="-1" role="dialog" aria-labelledby="feedbackPreviewTitle">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="feedbackPreviewTitle"></h4>
            </div>
            <div class="modal-body">
                <p class="h4" id="feedbackPreviewSubTitle"></p>
                <ul id="feedbackPreviewQuestions"></ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <span class="hide" id="jsTranslate">["<?php echo Lang::translate('FreeText'); ?>","<?php echo Lang::translate('Yes'); ?>","<?php echo Lang::translate('No'); ?>"]</span>
            </div>
        </div>
    </div>
</div>

<div id="questionPrototype" class="hidden">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-inline">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for=""><span class="icon fr"></span></label>
                                <input type="text"
                                       class="form-control questionTitle"
                                       name="FeedbackQuestion[][title]">

                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                    <div class="form-group">
                        <label><?php echo Lang::translate('QuestionType'); ?></label>
                        <select class="questionType"
                                name="FeedbackQuestion[][type]">
                            <option selected value="free_text"><?php echo Lang::translate('FreeText'); ?></option>
                            <option value="yes_no"><?php echo Lang::translate('YesNo'); ?></option>
                            <option value="rating"><?php echo Lang::translate('Rating'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group rating hidden">
                <div class="form-inline">
                    <div class="form-group">
                        <ul>
                            <label><?php echo Lang::translate('RatingValues'); ?></label>
                            <?php $v127723633073137679171iterator = range(1, 5); $v127723633073137679171incr = 0; $v127723633073137679171loop = new stdClass(); $v127723633073137679171loop->length = count($v127723633073137679171iterator); $v127723633073137679171loop->index = 1; $v127723633073137679171loop->index0 = 1; $v127723633073137679171loop->revindex = $v127723633073137679171loop->length; $v127723633073137679171loop->revindex0 = $v127723633073137679171loop->length - 1; ?><?php foreach ($v127723633073137679171iterator as $i) { ?><?php $v127723633073137679171loop->first = ($v127723633073137679171incr == 0); $v127723633073137679171loop->index = $v127723633073137679171incr + 1; $v127723633073137679171loop->index0 = $v127723633073137679171incr; $v127723633073137679171loop->revindex = $v127723633073137679171loop->length - $v127723633073137679171incr; $v127723633073137679171loop->revindex0 = $v127723633073137679171loop->length - ($v127723633073137679171incr + 1); $v127723633073137679171loop->last = ($v127723633073137679171incr == ($v127723633073137679171loop->length - 1)); ?>
                                <li class="feedbackRating fr">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <input type="text" class="form-control"
                                               name="FeedbackQuestion[][rating][value<?php echo $i; ?>]"
                                               placeholder="<?php echo $i; ?> <?php echo Lang::translate('Stars'); ?>">
                                    </div>
                                </li>
                            <?php $v127723633073137679171incr++; } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="FeedbackQuestion[][lang]" value="">
    </div>

</div>