<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo Lang::translate((ucwords($this->router->getControllerName()))); ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $baseUrl; ?>dashboard">
                    <i class="fa fa-dashboard"></i> <?php echo Lang::translate('Dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo $baseUrl; ?><?php echo $this->router->getControllerName(); ?>">
                    <i class="fa fa-exclamation-circle fr"></i> <?php echo Lang::translate((ucwords($this->router->getControllerName()))); ?>
                </a>
            </li>
            <li class="active">
                <?php $params = $this->router->getParams(); ?>
                <?php $paramStr = ''; ?>
                <?php if (!empty($params)) { ?>
                    <?php $paramStr = implode('/', $params); ?>
                <?php } ?>
                <a href="<?php echo $baseUrl; ?><?php echo $this->router->getControllerName(); ?>/<?php echo $this->router->getActionName(); ?><?php if (!empty($paramStr)) { ?>/<?php echo $paramStr; ?><?php } ?>">
                    <?php echo Lang::translate(('Breadcrumbs' . ucwords($this->router->getActionName()))); ?>
                </a>
            </li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group flash-message">
            <?php echo $this->flashSession->output(); ?>
        </div>
    </div>
</div>
<?php echo $this->getContent(); ?>
<span class="hidden" id="jsTranslate">["<?php echo Lang::translate('Confirm'); ?>","<?php echo Lang::translate('Cancel'); ?>"]</span>