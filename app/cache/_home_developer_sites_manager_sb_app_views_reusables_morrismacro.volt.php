<?php $this->_macros['info_panel'] = function($__p = null) { if (isset($__p[0])) { $panelClass = $__p[0]; } else { if (isset($__p["panelClass"])) { $panelClass = $__p["panelClass"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'info_panel' was called without parameter: panelClass");  } } if (isset($__p[1])) { $faClass = $__p[1]; } else { if (isset($__p["faClass"])) { $faClass = $__p["faClass"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'info_panel' was called without parameter: faClass");  } } if (isset($__p[2])) { $i = $__p[2]; } else { if (isset($__p["i"])) { $i = $__p["i"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'info_panel' was called without parameter: i");  } } if (isset($__p[3])) { $title = $__p[3]; } else { if (isset($__p["title"])) { $title = $__p["title"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'info_panel' was called without parameter: title");  } } if (isset($__p[4])) { $uri = $__p[4]; } else { if (isset($__p["uri"])) { $uri = $__p["uri"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'info_panel' was called without parameter: uri");  } }  ?>
    <div class="col-lg-3 col-md-6">
        <div class="panel <?php echo $panelClass; ?>">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3 hbr">
                        <i class="fa <?php echo $faClass; ?> fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo $i; ?></div>
                        <div><?php echo Lang::translate($title); ?></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo $uri; ?>">
                <div class="panel-footer">
                    <span class="pull-left"><?php echo Lang::translate('ViewDetails'); ?></span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div><?php }; $this->_macros['info_panel'] = \Closure::bind($this->_macros['info_panel'], $this); ?><?php $this->_macros['bar_chart'] = function($__p = null) { if (isset($__p[0])) { $MorrisBarChartId = $__p[0]; } else { if (isset($__p["MorrisBarChartId"])) { $MorrisBarChartId = $__p["MorrisBarChartId"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'bar_chart' was called without parameter: MorrisBarChartId");  } } if (isset($__p[1])) { $BarChartTitle = $__p[1]; } else { if (isset($__p["BarChartTitle"])) { $BarChartTitle = $__p["BarChartTitle"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'bar_chart' was called without parameter: BarChartTitle");  } } if (isset($__p[2])) { $dataValues = $__p[2]; } else { if (isset($__p["dataValues"])) { $dataValues = $__p["dataValues"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'bar_chart' was called without parameter: dataValues");  } } if (isset($__p[3])) { $dataTitles = $__p[3]; } else { if (isset($__p["dataTitles"])) { $dataTitles = $__p["dataTitles"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'bar_chart' was called without parameter: dataTitles");  } }  ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo Lang::translate($BarChartTitle); ?>
        </div>
        <div class="panel-body">
            <div class="barChart" id="<?php echo $MorrisBarChartId; ?>" data-values='<?php echo $dataValues; ?>' data-title='<?php echo $dataTitles; ?>'></div>
        </div>
    </div><?php }; $this->_macros['bar_chart'] = \Closure::bind($this->_macros['bar_chart'], $this); ?>