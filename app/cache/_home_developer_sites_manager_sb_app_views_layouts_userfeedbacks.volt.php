<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo Lang::translate((ucwords($this->router->getControllerName()))); ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $baseUrl; ?>dashboard">
                    <i class="fa fa-dashboard"></i> <?php echo Lang::translate('Dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo $baseUrl; ?><?php echo $this->router->getControllerName(); ?>">
                    <i class="fa fa-users marginLeft"></i> <?php echo Lang::translate((ucwords($this->router->getControllerName()))); ?>
                </a>
            </li>
            <li class="active">
                <?php $params = $this->router->getParams(); ?>
                <?php $paramStr = ''; ?>
                <?php if (!empty($params)) { ?>
                <?php $paramStr = implode('/', $params); ?>
                <?php } ?>
                <a href="<?php echo $baseUrl; ?><?php echo $this->router->getControllerName(); ?>/<?php echo $this->router->getActionName(); ?><?php if (!empty($paramStr)) { ?>/<?php echo $paramStr; ?><?php } ?>">
                    <?php echo Lang::translate(('Breadcrumbs' . ucwords($this->router->getActionName()))); ?>
                </a>
            </li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php echo $this->getContent(); ?>