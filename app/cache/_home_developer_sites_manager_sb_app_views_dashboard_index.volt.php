<?php echo $this->partial('reusables/morrisMacro', array()); ?>

<div class="row">
    <?php echo $this->callMacro('info_panel', array('panel-primary', 'fa-comments', ((isset($feedbacks) ? $feedbacks : 0)), 'FeedBack', $baseUrl . 'userfeedbacks')); ?>
    <?php echo $this->callMacro('info_panel', array('panel-green', 'fa-tasks', ((isset($currentVisitors) ? $currentVisitors : 0)), 'CurrentVisitors', '')); ?>
    <?php echo $this->callMacro('info_panel', array('panel-yellow', 'fa-shopping-cart', ((isset($aboutExpireCampaigns) ? $aboutExpireCampaigns : 0)), 'AboutExpireCampaigns', $baseUrl . 'campaigns')); ?>
    <?php echo $this->callMacro('info_panel', array('panel-red', 'fa-support', ((isset($churn) ? $churn : 0)), 'ChurnAlert', $baseUrl . 'churnalert')); ?>
</div>
<div class="row">
    <div class="col-md-8">
        <?php echo $this->callMacro('bar_chart', array('morris-bar-chart', 'WeeklyStoreVisitors', $visitorsToday, '')); ?>
    </div>
    <div class="col-md-4">
        <?php echo $this->callMacro('bar_chart', array('morris-bar-chart1', 'CouponsRedeemedToday', $couponsRedeemedToday, '')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?php echo $this->callMacro('bar_chart', array('morris-bar-chart-2', 'AvailableCoupons', $availableCoupons, '')); ?>
    </div>
    <div class="col-md-4">
        <?php echo $this->callMacro('bar_chart', array('morris-bar-chart-3', 'AvailableEngagements', $availableEngagements, '')); ?>
    </div>
    <div class="col-md-4">
        <?php echo $this->callMacro('bar_chart', array('morris-bar-chart-4', 'NewLCMembers', $weeklyNewMembers, '')); ?>
    </div>
</div>