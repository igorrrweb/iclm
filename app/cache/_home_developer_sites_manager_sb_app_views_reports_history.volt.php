<?php echo $this->partial('reusables/macroses', array()); ?>
<div id="reportsHistory">
    <form id="indexForm" action="<?php echo $baseUrl; ?>reports/history" method="post">
        <div class="row">
            <div class="col-md-12">
                <h3><?php echo 'Customer\'s Purchase History'; ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3 class="pull-left"><?php echo Lang::translate('CustomerName'); ?> <?php if (!empty($customerData)) { ?><?php echo $customerData['firstname']; ?> <?php echo $customerData['lastname']; ?><?php } ?></h3>
                <h3 class="pull-right"><?php echo Lang::translate('CustomerId'); ?> <?php if (!empty($customerData)) { ?><?php echo $customerData['id']; ?><?php } ?></h3></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="" class=""><?php echo 'LC NUMBER'; ?>: </label>
                            <div class="form-inline">
                                <div class="form-group autocomplete-parent">
                                    <label for="" class="sr-only"></label>
                                    <input type="number" class="form-control"
                                           name="filterValueLcNumber"
                                           value="<?php if (!empty($filterValueLcNumber)) { ?><?php echo $filterValueLcNumber; ?><?php } ?>">
                                    <ul class="autoComplete" data-message="<?php echo Lang::translate('NoMatches'); ?>"></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                        <div class="form-group">
                            <label for="reportFilter2" class=""><?php echo 'USER ID'; ?>: </label>
                            <div class="form-inline">
                                <div class="form-group autocomplete-parent">
                                    <label for="" class="sr-only"></label>
                                    <input class="form-control" type="number" name="filterValueUserId"
                                           value="<?php if (!empty($filterValueUserId)) { ?><?php echo $filterValueUserId; ?><?php } ?>">
                                    <ul class="autoComplete" data-message="<?php echo Lang::translate('NoMatches'); ?>"></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                        <div class="form-group">
                            <label for=""> </label>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <button type="submit" class="btn btn-primary get-report"><?php echo 'Apply'; ?></button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <?php echo $this->callMacro('datatable', array($attributes)); ?>
            </div>
        </div>

        
    </form>

</div>