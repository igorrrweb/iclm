<div class="row">
    <div class="col-md-12">
        <div class="well tile"><a href="<?php echo $baseUrl; ?>systemuser"><?php echo Lang::translate('ManageUsers'); ?></a></div>
        <div class="well tile"><a href="<?php echo $baseUrl; ?>branch"><?php echo Lang::translate('Branches'); ?></a></div>
        <div class="well tile"><a href="<?php echo $baseUrl; ?>admin/settings"><?php echo Lang::translate('Settings'); ?></a></div>
        <div class="well tile"><a href="<?php echo $baseUrl; ?>admin/termsofservice"><?php echo Lang::translate('TermsOfService'); ?></a></div>
    </div>
</div>