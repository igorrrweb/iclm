<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo Lang::translate('Dashboard'); ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo $baseUrl; ?>dashboard">
                    <i class="fa fa-dashboard"></i> <?php echo Lang::translate('Dashboard'); ?>
                </a>
            </li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php echo $this->getContent(); ?>