<?php echo $this->partial('customer/macroses', array()); ?>
<form role="form" action="" method="post" class="crudForm <?php if ($this->router->getActionName() == 'view') { ?> readOnly<?php } ?>" >
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i> <?php echo Lang::translate('PersonalInfo'); ?></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="Customer[firstname]"><?php echo Lang::translate('FirstName'); ?>*</label>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[firstname]', 'class' => 'form-control required', 'value' => $customer->firstname)); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[lastname]"><?php echo Lang::translate('LastName'); ?>*</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[lastname]', 'class' => 'form-control required', 'value' => $customer->lastname)); ?>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group fr">
                            <label for="Customer[id]"><?php echo Lang::translate('IDNumber'); ?>*</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[id]', 'class' => 'form-control required', 'value' => $customer->id)); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group">
                            <label for="Customer[birth_date]"><?php echo Lang::translate('Birthday'); ?></label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[birth_date]', 'class' => 'form-control datepicker birth-date', 'value' => $customer->birth_date)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i> <?php echo Lang::translate('GeneralInfo'); ?></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group">
                            <label><?php echo Lang::translate('Gender'); ?></label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('Customer[gender]', 'class' => 'icheck', 'value' => 'male', (((($customer->gender == 'male') || (!isset($customer->gender))) ? 'checked' : '')))); ?> <?php echo Lang::translate('Male'); ?></label>


                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('Customer[gender]', 'class' => 'icheck', 'value' => 'female', ((($customer->gender == 'female') ? 'checked' : '')))); ?> <?php echo Lang::translate('Female'); ?></label></div>
                            </div>


                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group">
                            <label><?php echo Lang::translate('Married'); ?></label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('Customer[marital_status]', 'class' => 'icheck', 'value' => 'married', ((($customer->gender == 'married') ? 'checked' : '')))); ?> <?php echo Lang::translate('Yes'); ?></label>


                                    <label class="radio-inline">
                                        <?php echo $this->tag->radioField(array('Customer[marital_status]', 'class' => 'icheck', 'value' => 'single', (((($customer->gender == 'single') || (!isset($customer->marital_status))) ? 'checked' : '')))); ?> <?php echo Lang::translate('No'); ?></label></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="Customer[wedding_anniversary_date]"><?php echo Lang::translate('WeddingDay'); ?></label>

                    <div class="form-inline">
                        <div class="form-group">
                            <label for="" class="sr-only"></label>
                            <?php echo $this->tag->textField(array('Customer[wedding_anniversary_date]', 'class' => 'datepicker form-control wedding', 'value' => $customer->wedding_anniversary_date)); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i> <?php echo Lang::translate('ContactInfo'); ?></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group fr">
                            <label for="Customer[phone]"><?php echo Lang::translate('HomeNumber'); ?></label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <?php echo $this->tag->textField(array('Customer[phone]', 'class' => 'form-control', 'value' => $customer->phone)); ?>
                                </div>
                            </div>

                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[mobile]"><?php echo Lang::translate('Mobile'); ?>*</label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[mobile]', 'class' => 'form-control required', 'value' => $customer->mobile)); ?>
                                </div>
                            </div>

                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[email]"><?php echo Lang::translate('EmailAddress'); ?></label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[email]', 'class' => 'form-control', 'value' => $customer->email)); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group fr">
                            <label for="Customer[address]"><?php echo Lang::translate('Address'); ?></label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[address]', 'class' => 'form-control', 'value' => $customer->address)); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[zip_code]"><?php echo Lang::translate('Zipcode'); ?></label>

                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[zip_code]', 'class' => 'form-control', 'value' => $customer->zip_code)); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[city_id]"><?php echo Lang::translate('City'); ?></label>
                            <div class="form-inline">
                                <div class="form-group"><label for="" class="sr-only"></label>
                                    <select class="select2" name="Customer[city_id]">
                                        <option value="0">-------</option>
                                        <?php foreach ($cities as $city) { ?>
                                        <option <?php if ($customer->city_id == $city->id) { ?>selected<?php } ?>
                                                value="<?php echo $city->id; ?>"><?php echo $city->city; ?>
                                        </option>
                                        <?php } ?>
                                    </select></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt marginLeft"></i><?php echo Lang::translate('LCRelatedInfo'); ?></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="form-inline">
                        <div class="form-group fr">
                            <label for="Customer[lc_number]"><?php echo Lang::translate('LCNumber'); ?>*</label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[lc_number]', 'class' => 'form-control required', 'value' => $customer->lc_number)); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[last_digits_cc]"><?php echo Lang::translate('CCLastDigits'); ?></label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[last_digits_cc]', 'class' => 'form-control', 'value' => $customer->last_digits_cc)); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><div class="hidden-xs col-md-1"></div></div>
                        <div class="form-group fr">
                            <label for="Customer[join_date]"><?php echo Lang::translate('JoinDate'); ?></label>

                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="" class="sr-only"></label>
                                    <?php echo $this->tag->textField(array('Customer[join_date]', 'class' => 'form-control datepicker join-date', 'value' => $customer->join_date)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
    <?php echo $this->callMacro('buttons', array($baseUrl)); ?>
</form>

