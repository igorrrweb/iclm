<?php $this->_macros['step'] = function($__p = null) { if (isset($__p[0])) { $step_class = $__p[0]; } else { if (isset($__p["step_class"])) { $step_class = $__p["step_class"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'step' was called without parameter: step_class");  } } if (isset($__p[1])) { $step_href = $__p[1]; } else { if (isset($__p["step_href"])) { $step_href = $__p["step_href"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'step' was called without parameter: step_href");  } } if (isset($__p[2])) { $aria_controls = $__p[2]; } else { if (isset($__p["aria_controls"])) { $aria_controls = $__p["aria_controls"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'step' was called without parameter: aria_controls");  } } if (isset($__p[3])) { $i_class = $__p[3]; } else { if (isset($__p["i_class"])) { $i_class = $__p["i_class"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'step' was called without parameter: i_class");  } } if (isset($__p[4])) { $step_title = $__p[4]; } else { if (isset($__p["step_title"])) { $step_title = $__p["step_title"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'step' was called without parameter: step_title");  } } if (isset($__p[5])) { $step_number = $__p[5]; } else { if (isset($__p["step_number"])) { $step_number = $__p["step_number"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'step' was called without parameter: step_number");  } } if (isset($__p[6])) { $step_body = $__p[6]; } else { if (isset($__p["step_body"])) { $step_body = $__p["step_body"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'step' was called without parameter: step_body");  } }  ?>
<li role="presentation" class="fr <?php echo $step_class; ?>">
    <a href="#<?php echo $step_href; ?>" aria-controls="<?php echo $aria_controls; ?>" role="tab" data-toggle="tab">
        <i class="fr fa <?php echo $i_class; ?>"></i> <?php echo Lang::translate($step_title); ?> <?php echo $step_number; ?>
        <p><?php echo Lang::translate($step_body); ?></p>
    </a>
</li><?php }; $this->_macros['step'] = \Closure::bind($this->_macros['step'], $this); ?><?php $this->_macros['modal'] = function($__p = null) { if (isset($__p[0])) { $id = $__p[0]; } else { if (isset($__p["id"])) { $id = $__p["id"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: id");  } } if (isset($__p[1])) { $labelledby = $__p[1]; } else { if (isset($__p["labelledby"])) { $labelledby = $__p["labelledby"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: labelledby");  } } if (isset($__p[2])) { $baseUrl = $__p[2]; } else { if (isset($__p["baseUrl"])) { $baseUrl = $__p["baseUrl"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: baseUrl");  } } if (isset($__p[3])) { $img_id = $__p[3]; } else { if (isset($__p["img_id"])) { $img_id = $__p["img_id"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: img_id");  } } if (isset($__p[4])) { $previewTitle = $__p[4]; } else { if (isset($__p["previewTitle"])) { $previewTitle = $__p["previewTitle"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: previewTitle");  } } if (isset($__p[5])) { $previewSubTitle = $__p[5]; } else { if (isset($__p["previewSubTitle"])) { $previewSubTitle = $__p["previewSubTitle"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: previewSubTitle");  } } if (isset($__p[6])) { $couponPreviewImage_id = $__p[6]; } else { if (isset($__p["couponPreviewImage_id"])) { $couponPreviewImage_id = $__p["couponPreviewImage_id"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: couponPreviewImage_id");  } } if (isset($__p[7])) { $img = $__p[7]; } else { if (isset($__p["img"])) { $img = $__p["img"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: img");  } } if (isset($__p[8])) { $previewDetails = $__p[8]; } else { if (isset($__p["previewDetails"])) { $previewDetails = $__p["previewDetails"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: previewDetails");  } } if (isset($__p[9])) { $previewDisclaimer = $__p[9]; } else { if (isset($__p["previewDisclaimer"])) { $previewDisclaimer = $__p["previewDisclaimer"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'modal' was called without parameter: previewDisclaimer");  } }  ?>
<div id="<?php echo $id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="<?php echo $labelledby; ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img id="<?php echo $img_id; ?>" src="<?php echo $baseUrl; ?>img/MHLogo.png">
            </div>
            <div class="modal-body">

                <h1 id="previewTitle"><?php echo $previewTitle; ?></h1>
                <h3 id="previewSubTitle"><?php echo $previewSubTitle; ?></h3>
                <figure>
                    <img class="img img-responsive img-thumbnail" id="<?php echo $couponPreviewImage_id; ?>"
                         <?php if (isset($img)) { ?>src="<?php if (!empty($img)) { ?><?php echo $img; ?><?php } ?>"<?php } ?>>
                </figure>
                <h3 id="previewDetails"><?php echo $previewDetails; ?></h3>
                <h4 id="previewDisclaimer"><?php echo $previewDisclaimer; ?></h4>

            </div>
            <div class="modal-footer previewFooter">
                <img src="<?php echo $baseUrl; ?>img/couponPreviewFooter.png">
            </div>
        </div>
    </div>
</div><?php }; $this->_macros['modal'] = \Closure::bind($this->_macros['modal'], $this); ?><?php $this->_macros['table'] = function($__p = null) { if (isset($__p[0])) { $table_id = $__p[0]; } else { if (isset($__p["table_id"])) { $table_id = $__p["table_id"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'table' was called without parameter: table_id");  } } if (isset($__p[1])) { $table_heads = $__p[1]; } else { if (isset($__p["table_heads"])) { $table_heads = $__p["table_heads"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'table' was called without parameter: table_heads");  } }  ?>
<table id="<?php echo $table_id; ?>" width="100%" class="table table-striped table-bordered table-hover "
<?php if ($this->router->getActionName() == 'view') { ?>readOnly<?php } ?>">
    <thead>
    <tr>
        <th></th>
        <?php foreach ($table_heads as $th) { ?>
        <th><?php echo Lang::translate($th); ?></th>
        <?php } ?>
    </tr>
    </thead>
    <tbody></tbody>
</table><?php }; $this->_macros['table'] = \Closure::bind($this->_macros['table'], $this); ?><?php $this->_macros['engagementGeneralDetails'] = function($__p = null) { if (isset($__p[0])) { $engagement = $__p[0]; } else { if (isset($__p["engagement"])) { $engagement = $__p["engagement"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementGeneralDetails' was called without parameter: engagement");  } } if (isset($__p[1])) { $img = $__p[1]; } else { if (isset($__p["img"])) { $img = $__p["img"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementGeneralDetails' was called without parameter: img");  } } if (isset($__p[2])) { $baseUrl = $__p[2]; } else { if (isset($__p["baseUrl"])) { $baseUrl = $__p["baseUrl"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementGeneralDetails' was called without parameter: baseUrl");  } } if (isset($__p[3])) { $langs = $__p[3]; } else { if (isset($__p["langs"])) { $langs = $__p["langs"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementGeneralDetails' was called without parameter: langs");  } } if (isset($__p[4])) { $engagementLang = $__p[4]; } else { if (isset($__p["engagementLang"])) { $engagementLang = $__p["engagementLang"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementGeneralDetails' was called without parameter: engagementLang");  } }  ?>
<div id="engagementGeneralDetails" role="tabpanel" class="tab-pane active">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2">
                <h4><?php echo Lang::translate('GeneralDetails'); ?></h4>
                <p><?php echo Lang::translate('EngagementGeneralInfoText'); ?></p>
            </div>
            <div class="col-md-10">
                <div class="form-group"></div>
                <div class="form-group <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                    <label for="Engagement[name]"><?php echo Lang::translate('Name'); ?>*</label>
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="" class="sr-only"></label>
                            <input class="form-control required"
                                   id="engagementName"
                                   value="<?php echo $engagement->name; ?>" type="text"
                                   name="Engagement[name]" data-column="name">
                        </div>
                    </div>
                </div>
                <div class="form-group <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                    <label for="engagementImage"><?php echo Lang::translate('Image'); ?>*</label>
                    <div class="form-inline">
                        <div class="form-group">
                            <img class="previewImage thumbnail" <?php if (!empty($img)) { ?> src="<?php echo $img; ?>" <?php } ?> >
                            <input class="<?php if ($img == '') { ?>required<?php } ?> input-lg imageUpload"
                                   type="file" id="engagementImageUpload"
                                   name="engagementImage" data-text="<?php echo Lang::translate(' UploadFile'); ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                    <div class="inputGroup fr">
                        <label for="Engagement[start_date]"><?php echo Lang::translate('StartDate'); ?>*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input
                                        class="form-control minVal datepicker required" type="text"
                                        name="Engagement[start_date]"
                                        value="<?php echo $engagement->start_date; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="inputGroup fr">
                        <label for="Engagement[end_date]"><?php echo Lang::translate('EndDate'); ?>*</label>
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="" class="sr-only"></label>
                                <input
                                        class="form-control maxVal datepicker required" type="text"
                                        name="Engagement[end_date]" value="<?php echo $engagement->end_date; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php $v94100686406452418821iterator = $langs; $v94100686406452418821incr = 0; $v94100686406452418821loop = new stdClass(); $v94100686406452418821loop->length = count($v94100686406452418821iterator); $v94100686406452418821loop->index = 1; $v94100686406452418821loop->index0 = 1; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - 1; ?><?php foreach ($v94100686406452418821iterator as $label => $lang) { ?><?php $v94100686406452418821loop->first = ($v94100686406452418821incr == 0); $v94100686406452418821loop->index = $v94100686406452418821incr + 1; $v94100686406452418821loop->index0 = $v94100686406452418821incr; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length - $v94100686406452418821incr; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - ($v94100686406452418821incr + 1); $v94100686406452418821loop->last = ($v94100686406452418821incr == ($v94100686406452418821loop->length - 1)); ?>
                            <li role="presentation"
                                class="langTab fr <?php if ($v94100686406452418821loop->index0 == 0) { ?>active<?php } ?>"
                                data-lang="<?php echo $lang; ?>">
                                <a href="#engagementGeneralDetails_<?php echo $lang; ?>" aria-controls="engagementGeneralDetails_<?php echo $lang; ?>" role="tab" data-toggle="tab"><?php echo Lang::translate($label); ?></a></li>
                        <?php $v94100686406452418821incr++; } ?>
                    </ul>
                    <div class="tab-content">
                        <?php $v94100686406452418821iterator = $langs; $v94100686406452418821incr = 0; $v94100686406452418821loop = new stdClass(); $v94100686406452418821loop->length = count($v94100686406452418821iterator); $v94100686406452418821loop->index = 1; $v94100686406452418821loop->index0 = 1; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - 1; ?><?php foreach ($v94100686406452418821iterator as $i) { ?><?php $v94100686406452418821loop->first = ($v94100686406452418821incr == 0); $v94100686406452418821loop->index = $v94100686406452418821incr + 1; $v94100686406452418821loop->index0 = $v94100686406452418821incr; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length - $v94100686406452418821incr; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - ($v94100686406452418821incr + 1); $v94100686406452418821loop->last = ($v94100686406452418821incr == ($v94100686406452418821loop->length - 1)); ?>
                            <div role="tabpanel" id="engagementGeneralDetails_<?php echo $i; ?>" class="tab-pane langTabData clr <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?> <?php if ($i == 'he') { ?>active<?php } ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group"></div>
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <div class="form-group fr">
                                                        <label for="Engagement[welcome_msg]"><?php echo Lang::translate('WelcomeMessage'); ?><?php if ($i == 'he') { ?>*<?php } ?></label>
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <label for="" class="sr-only"></label>
                                                                <textarea
                                                                        rows="1"
                                                                        class="form-control <?php if ($i == 'he') { ?>required<?php } ?>"
                                                                        name="EngagementLang[<?php echo $i; ?>][welcome_msg]"><?php if (isset($engagementLang[$i])) { ?><?php echo $engagementLang[$i]['welcome_msg']; ?><?php } ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group hidden-xs">
                                                        <div class="col-md-1"></div>
                                                    </div>
                                                    <div class="form-group fr">
                                                        <label for="Engagement[thankyou_msg]"><?php echo Lang::translate('ThankYouMessage'); ?><?php if ($i == 'he') { ?>*<?php } ?></label>
                                                        <div class="form-inline">
                                                            <div class="form-group">
                                                                <label for="" class="sr-only"></label>
                                                                <textarea
                                                                        rows="1"
                                                                        class="form-control <?php if ($i == 'he') { ?>required<?php } ?>"
                                                                        name="EngagementLang[<?php echo $i; ?>][thankyou_msg]"><?php if (isset($engagementLang[$i])) { ?><?php echo $engagementLang[$i]['thankyou_msg']; ?><?php } ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $v94100686406452418821incr++; } ?>
                    </div>
                </div>
                <div class="form-group <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                    <label><?php echo Lang::translate('Trigger'); ?></label>
                    <div class="radio">
                        <label><input type="radio" class="icheck" name="Engagement[trigger]"
                                      value="on_branch_enter"
                                    <?php if (($engagement->trigger == 'on_branch_enter') || $engagement->trigger == '') { ?> checked<?php } ?>> <?php echo Lang::translate('OnBranchEntry'); ?></label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="icheck"
                                      name="Engagement[trigger]"
                                      value="not_in_branch"
                                    <?php if (($engagement->trigger == 'not_in_branch')) { ?>
                            checked<?php } ?>> <?php echo Lang::translate('WhenNotInBranch'); ?></label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="icheck"
                                      name="Engagement[trigger]"
                                      value="on_branch_leave"
                                    <?php if (($engagement->trigger == 'on_branch_leave')) { ?>
                            checked<?php } ?>> <?php echo Lang::translate('OnBranchExitAfterPurchase'); ?></label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="icheck"
                                      name="Engagement[trigger]"
                                      value="on_branch_leave_empty"
                                    <?php if (($engagement->trigger == 'on_branch_leave_empty')) { ?> checked<?php } ?>> <?php echo Lang::translate('OnBranchExitNoPurchase'); ?></label>
                    </div>
                    <div class="checkbox fr <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                        <label><input type="checkbox" class="icheck" name="Engagement[never_expires]" value="1"
                                    <?php if (($engagement->never_expires == '1')) { ?> checked<?php } ?>> <?php echo Lang::translate('NeverExpires'); ?></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }; $this->_macros['engagementGeneralDetails'] = \Closure::bind($this->_macros['engagementGeneralDetails'], $this); ?><?php $this->_macros['engagementAudience'] = function() { ?>
<div id="engagementAudience" class="audience tab-pane <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group"></div>
            <div class="col-md-2">
                <div class="form-group">
                    <p class="errorText hide" id="segmentError"> - <?php echo Lang::translate('PleaseChooseASegment'); ?></p>
                    <div class="sectionInfo">
                        <h4><?php echo Lang::translate('Audience'); ?></h4>
                        <p><?php echo Lang::translate('AudienceInfoText'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="form-group">
                    <?php echo $this->callMacro('table', array('segments', array('SegmentName', 'Audience'))); ?>
                </div>
            </div>
        </div>
    </div>
</div><?php }; $this->_macros['engagementAudience'] = \Closure::bind($this->_macros['engagementAudience'], $this); ?><?php $this->_macros['engagementReward'] = function($__p = null) { if (isset($__p[0])) { $coupon = $__p[0]; } else { if (isset($__p["coupon"])) { $coupon = $__p["coupon"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementReward' was called without parameter: coupon");  } } if (isset($__p[1])) { $allowShare = $__p[1]; } else { if (isset($__p["allowShare"])) { $allowShare = $__p["allowShare"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementReward' was called without parameter: allowShare");  } } if (isset($__p[2])) { $couponImg = $__p[2]; } else { if (isset($__p["couponImg"])) { $couponImg = $__p["couponImg"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementReward' was called without parameter: couponImg");  } } if (isset($__p[3])) { $baseUrl = $__p[3]; } else { if (isset($__p["baseUrl"])) { $baseUrl = $__p["baseUrl"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementReward' was called without parameter: baseUrl");  } } if (isset($__p[4])) { $langs = $__p[4]; } else { if (isset($__p["langs"])) { $langs = $__p["langs"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementReward' was called without parameter: langs");  } } if (isset($__p[5])) { $couponLang = $__p[5]; } else { if (isset($__p["couponLang"])) { $couponLang = $__p["couponLang"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementReward' was called without parameter: couponLang");  } } if (isset($__p[6])) { $engagement = $__p[6]; } else { if (isset($__p["engagement"])) { $engagement = $__p["engagement"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementReward' was called without parameter: engagement");  } }  ?>
<div id="engagementReward" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group"></div>
            <div class="col-md-2">
                <div class="sectionInfo">
                    <p class="h4"><?php echo Lang::translate('ChallengeReward'); ?></p>
                    <p><?php echo Lang::translate('EngagementRewardText'); ?></p>
                </div>
            </div>
            <div class="col-md-10">
                <div class="mainForm">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="errorText hide" id="couponError"> - <?php echo Lang::translate('IncompleteCouponText'); ?></p>

                            <div id="rewardOnCompletion"
                                 class="checkbox <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                                <label><input type="checkbox" class="icheck" name="Engagement[reward]" id="EngagementReward"
                                              value="1" <?php if ($engagement->reward == '1') { ?> checked="checked" <?php } ?> > <?php echo Lang::translate('RewardOnCompletion'); ?></label>
                            </div>
                        </div>
                    </div>
                    <div id="engagementCoupon">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-inline">
                                        <div class="form-group fr <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                                            <label><?php echo Lang::translate('Name'); ?>*</label>
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label for="" class="sr-only"></label>
                                                    <input
                                                            class="couponName form-control tabRequired"
                                                            <?php if (isset($coupon->name)) { ?>
                                                                value="<?php echo $coupon->name; ?>" <?php } ?>
                                                            type="text" name="Coupon[name]"
                                                            data-column="name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                        <div class="form-group fr <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                                            <label for="couponID"><?php echo Lang::translate('CouponNumber'); ?>*</label>

                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label for="" class="sr-only"></label>
                                                    <input id="couponID"
                                                           class="form-control tabRequired"
                                                           value="<?php echo $coupon->pos_coupon_id; ?>"
                                                           type="text"
                                                           name="Coupon[pos_coupon_id]">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group hidden-xs"><div class="col-md-1"></div></div>
                                        <div class="form-group fr <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                                            <label for="couponTotalUnits"><?php echo Lang::translate('TotalUnits'); ?>*</label>

                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label for="" class="sr-only"></label>
                                                    <input id="couponTotalUnits"
                                                           class="form-control tabRequired"
                                                           value="<?php echo $coupon->max_coupons; ?>"
                                                           type="text"
                                                           name="Coupon[max_coupons]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="checkbox-inline fr <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                                        <label><input type="checkbox" value="1" class="icheck"
                                                      name="Coupon[active]" <?php if ($coupon->active == '1') { ?> checked<?php } ?>> <?php echo Lang::translate('Enabled'); ?></label>
                                    </div>
                                    <?php if ($allowShare) { ?>
                                        <div class="checkbox-inline fr <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                                            <label><input type="checkbox" value="1" class="icheck"
                                                          name="Coupon[promoted]" <?php if ($coupon->promoted == '1') { ?> checked<?php } ?>> <?php echo Lang::translate('Promoted'); ?></label>
                                        </div>
                                    <?php } ?>
                                    <div class="checkbox-inline fr <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                                        <label><input type="checkbox" value="1" class="icheck"
                                                      name="Coupon[shared]" <?php if ($coupon->shared == '1') { ?> checked<?php } ?>> <?php echo Lang::translate('Shareable'); ?></label>
                                    </div>
                                    <div class="checkbox-inline fr <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                                        <label><input type="checkbox" value="1" class="icheck"
                                                      name="Coupon[public]" <?php if ($coupon->public == '1') { ?> checked<?php } ?>> <?php echo Lang::translate('Public'); ?></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?>">
                                    <label for="couponImage"><?php echo Lang::translate('Image'); ?>*</label>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="" class="sr-only"></label>
                                            <img class="previewImage thumbnail"
                                                 src="<?php if (!empty($couponImg)) { ?><?php echo $couponImg; ?><?php } ?>">
                                            <input id="couponImageUpload" class="imageUpload"
                                                   type="file"
                                                   name="couponImage"
                                                   data-text="<?php echo Lang::translate(' ChooseFile'); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <ul class="nav nav-tabs" role="tablist">
                                <?php $v94100686406452418821iterator = $langs; $v94100686406452418821incr = 0; $v94100686406452418821loop = new stdClass(); $v94100686406452418821loop->length = count($v94100686406452418821iterator); $v94100686406452418821loop->index = 1; $v94100686406452418821loop->index0 = 1; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - 1; ?><?php foreach ($v94100686406452418821iterator as $label => $lang) { ?><?php $v94100686406452418821loop->first = ($v94100686406452418821incr == 0); $v94100686406452418821loop->index = $v94100686406452418821incr + 1; $v94100686406452418821loop->index0 = $v94100686406452418821incr; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length - $v94100686406452418821incr; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - ($v94100686406452418821incr + 1); $v94100686406452418821loop->last = ($v94100686406452418821incr == ($v94100686406452418821loop->length - 1)); ?>
                                    <li class="langTab fr <?php if ($v94100686406452418821loop->index0 == 0) { ?>active<?php } ?>" data-lang="<?php echo $lang; ?>">
                                        <a href="#engagementReward_<?php echo $lang; ?>" aria-controls="engagementReward_<?php echo $lang; ?>" role="tab" data-toggle="tab"><?php echo Lang::translate($label); ?></a>
                                    </li>
                                <?php $v94100686406452418821incr++; } ?>
                            </ul>
                            <div class="tab-content">
                                <?php $v94100686406452418821iterator = $langs; $v94100686406452418821incr = 0; $v94100686406452418821loop = new stdClass(); $v94100686406452418821loop->length = count($v94100686406452418821iterator); $v94100686406452418821loop->index = 1; $v94100686406452418821loop->index0 = 1; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - 1; ?><?php foreach ($v94100686406452418821iterator as $i) { ?><?php $v94100686406452418821loop->first = ($v94100686406452418821incr == 0); $v94100686406452418821loop->index = $v94100686406452418821incr + 1; $v94100686406452418821loop->index0 = $v94100686406452418821incr; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length - $v94100686406452418821incr; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - ($v94100686406452418821incr + 1); $v94100686406452418821loop->last = ($v94100686406452418821incr == ($v94100686406452418821loop->length - 1)); ?>
                                    <div role="tabpanel" id="engagementReward_<?php echo $i; ?>"  class="tab-pane langTabData <?php if ($this->router->getActionName() == 'view') { ?> readOnly <?php } ?> <?php if ($i == 'he') { ?> active <?php } ?>">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group"></div>
                                                    <div class="form-group">
                                                        <div class="form-inline">
                                                            <div class="form-group fr">
                                                                <label><?php echo Lang::translate('Title'); ?><?php if ($i == 'he') { ?>*<?php } ?></label>

                                                                <div class="form-inline">
                                                                    <div class="form-group">
                                                                        <label for="" class="sr-only"></label>
                                                                        <input
                                                                                class="form-control <?php if ($i == 'he') { ?> tabRequired<?php } ?> couponTitle"
                                                                                <?php if (isset($couponLang[$i]['title'])) { ?>
                                                                                    value="<?php echo $couponLang[$i]['title']; ?>" <?php } ?>
                                                                                type="text"
                                                                                name="CouponLang[<?php echo $i; ?>][title]">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group hidden-xs">
                                                                <div class="col-md-1"></div>
                                                            </div>
                                                            <div class="form-group fr">
                                                                <label><?php echo Lang::translate('SubTitle'); ?></label>

                                                                <div class="form-inline">
                                                                    <div class="form-group">
                                                                        <label for="" class="sr-only"></label>
                                                                        <input
                                                                                class="form-control couponSubTitle"
                                                                                <?php if (isset($couponLang[$i]['sub_title'])) { ?> value="<?php echo $couponLang[$i]['sub_title']; ?>" <?php } ?>
                                                                                type="text"
                                                                                name="CouponLang[<?php echo $i; ?>][sub_title]">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="form-inline">
                                                            <div class="form-group fr ">
                                                                <label><?php echo Lang::translate('Details'); ?></label>

                                                                <div class="form-inline">
                                                                    <div class="form-group">
                                                                        <label for="" class="sr-only"></label>
                                                                                <textarea
                                                                                        rows="1"
                                                                                        class="form-control couponDetails"
                                                                                        name="CouponLang[<?php echo $i; ?>][details]"><?php if (isset($couponLang[$i]['details'])) { ?><?php echo $couponLang[$i]['details']; ?><?php } ?></textarea>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group hidden-xs">
                                                                <div class="col-md-1"></div>
                                                            </div>
                                                            <div class="form-group fr">
                                                                <label><?php echo Lang::translate('Disclaimer'); ?></label>
                                                                <div class="form-inline">
                                                                    <div class="form-group">
                                                                        <label for="" class="sr-only"></label>
                                                                                <textarea
                                                                                        rows="1"
                                                                                        class="form-control couponDisclaimer"
                                                                                        name="CouponLang[<?php echo $i; ?>][disclaimer]"><?php if (isset($couponLang[$i]['disclaimer'])) { ?><?php echo $couponLang[$i]['disclaimer']; ?><?php } ?></textarea>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $v94100686406452418821incr++; } ?>
                            </div>
                            <button id="couponPreviewButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#couponPreview"><?php echo Lang::translate('Preview'); ?></button>
                            <?php echo $this->callMacro('modal', array('couponPreview', 'couponPreviewLabel', $baseUrl, 'couponPreviewLabel', ((isset($coupon->title) ? $coupon->title : '')), ((isset($coupon->sub_title) ? $coupon->sub_title : '')), 'couponPreviewImage', ((isset($img) ? $img : '')), ((isset($coupon->details) ? $coupon->details : '')), (($coupon->disclaimer ? $coupon->disclaimer : '')))); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }; $this->_macros['engagementReward'] = \Closure::bind($this->_macros['engagementReward'], $this); ?><?php $this->_macros['punchcardData'] = function($__p = null) { if (isset($__p[0])) { $punchcard = $__p[0]; } else { if (isset($__p["punchcard"])) { $punchcard = $__p["punchcard"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'punchcardData' was called without parameter: punchcard");  } }  ?>
    <div id="punchcardData" class="tab-pane">
        <p class="h3"><?php echo Lang::translate('PunchcardCriteria'); ?></p>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="Punchcard[duration]"><?php echo Lang::translate('EngagementDuration'); ?></label>

                    <div class="form-inline">
                        <div class="form-group">
                            <input type="text" id="punchcardDuration"
                                   class="form-control required"
                                   name="Punchcard[duration]"
                                   value="<?php echo $punchcard->duration; ?>">
                            <span><?php echo Lang::translate('Days'); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label><?php echo Lang::translate('IncreaseVisitsBy'); ?></label>
                    <div class="form-group">
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label><?php echo Lang::translate('AverageOf'); ?>
                                        1-2 <?php echo Lang::translate('Visits'); ?> <?php echo Lang::translate('IncreaseBy'); ?></label>
                                    <input type="text" class="form-control required"
                                           name="Punchcard[punch1_2]"
                                           value="<?php echo $punchcard->punch1_2; ?>">
                                    <span><?php echo Lang::translate('Visits'); ?></span>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label><?php echo Lang::translate('AverageOf'); ?>
                                        3-5 <?php echo Lang::translate('Visits'); ?> <?php echo Lang::translate('IncreaseBy'); ?></label>
                                    <input type="text"
                                           class="form-control required"
                                           name="Punchcard[punch3_5]"
                                           value="<?php echo $punchcard->punch3_5; ?>">
                                    <span><?php echo Lang::translate('Visits'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label><?php echo Lang::translate('AverageOf'); ?>
                                        6-8 <?php echo Lang::translate('Visits'); ?> <?php echo Lang::translate('IncreaseBy'); ?></label>
                                    <input type="text" class="form-control required"
                                           name="Punchcard[punch6_8]"
                                           value="<?php echo $punchcard->punch6_8; ?>">
                                    <span><?php echo Lang::translate('Visits'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label><?php echo Lang::translate('AverageOf'); ?> <?php echo Lang::translate('MoreThan'); ?>
                                        8 <?php echo Lang::translate('Visits'); ?> <?php echo Lang::translate('IncreaseBy'); ?></label>
                                    <input type="text" class="form-control required"
                                           name="Punchcard[punch8]"
                                           value="<?php echo $punchcard->punch8; ?>">
                                    <span><?php echo Lang::translate('Visits'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><?php }; $this->_macros['punchcardData'] = \Closure::bind($this->_macros['punchcardData'], $this); ?><?php $this->_macros['engagementPreview'] = function($__p = null) { if (isset($__p[0])) { $engagement = $__p[0]; } else { if (isset($__p["engagement"])) { $engagement = $__p["engagement"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementPreview' was called without parameter: engagement");  } } if (isset($__p[1])) { $punchcard = $__p[1]; } else { if (isset($__p["punchcard"])) { $punchcard = $__p["punchcard"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'engagementPreview' was called without parameter: punchcard");  } }  ?>
    <div id="engagementPreview" class="tab-pane">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group"></div>
                <div class="form-group">
                    <div class="panel panel-warning">
                        <div id="punchcardPreviewHeader" class="panel-heading"><?php echo Lang::translate('Punchcard'); ?></div>
                        <div id="punchcardPreviewBody" class="panel-body">
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1">
                                    <p id="punchcardPreviewText"><?php echo Lang::translate('PunchcardPreviewText'); ?></p>
                                </div>
                                <div class="clearfix"></div>
                                <h3 id="punchcardPreviewName"><?php echo $engagement->name; ?></h3>
                                <div class="h1" id="punchcardPreviewDuration">
                                    <span><?php echo $punchcard->duration; ?></span>
                                    <p><?php echo Lang::translate('Days'); ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group topHr"><hr></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $v94100686406452418821iterator = range(1, 4); $v94100686406452418821incr = 0; $v94100686406452418821loop = new stdClass(); $v94100686406452418821loop->length = count($v94100686406452418821iterator); $v94100686406452418821loop->index = 1; $v94100686406452418821loop->index0 = 1; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - 1; ?><?php foreach ($v94100686406452418821iterator as $i) { ?><?php $v94100686406452418821loop->first = ($v94100686406452418821incr == 0); $v94100686406452418821loop->index = $v94100686406452418821incr + 1; $v94100686406452418821loop->index0 = $v94100686406452418821incr; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length - $v94100686406452418821incr; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - ($v94100686406452418821incr + 1); $v94100686406452418821loop->last = ($v94100686406452418821incr == ($v94100686406452418821loop->length - 1)); ?>
                                        <div class="punchcardPreviewNumbersFormGroup fr col-xs-6  col-md-3 ">
                                            <span class="circle"><?php echo $i; ?></span>
                                        </div>
                                    <?php $v94100686406452418821incr++; } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $v94100686406452418821iterator = range(5, 8); $v94100686406452418821incr = 0; $v94100686406452418821loop = new stdClass(); $v94100686406452418821loop->length = count($v94100686406452418821iterator); $v94100686406452418821loop->index = 1; $v94100686406452418821loop->index0 = 1; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - 1; ?><?php foreach ($v94100686406452418821iterator as $i) { ?><?php $v94100686406452418821loop->first = ($v94100686406452418821incr == 0); $v94100686406452418821loop->index = $v94100686406452418821incr + 1; $v94100686406452418821loop->index0 = $v94100686406452418821incr; $v94100686406452418821loop->revindex = $v94100686406452418821loop->length - $v94100686406452418821incr; $v94100686406452418821loop->revindex0 = $v94100686406452418821loop->length - ($v94100686406452418821incr + 1); $v94100686406452418821loop->last = ($v94100686406452418821incr == ($v94100686406452418821loop->length - 1)); ?>
                                        <div class="punchcardPreviewNumbersFormGroup fr col-xs-6  col-md-3 ">
                                            <span class="circle"><?php echo $i; ?></span>
                                        </div>
                                    <?php $v94100686406452418821incr++; } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group"><hr></div>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </div>
                        <div id="punchcardPreviewFooter" class="panel-footer">
                            <a class="btn btn-success">
                                <i class="fa fa-clock-o fr marginLeft"></i> <?php echo Lang::translate('RemindMe'); ?></a>
                            <p><?php echo Lang::translate('Regulations'); ?></p>
                        </div>
                    </div>
                    <div class="btns">
                        <?php if ($this->router->getActionName() != 'view') { ?>
                            <input id="save" class="btn btn-primary" type="submit" value="<?php echo Lang::translate('Save'); ?>">
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div><?php }; $this->_macros['engagementPreview'] = \Closure::bind($this->_macros['engagementPreview'], $this); ?><?php $this->_macros['btns'] = function($__p = null) { if (isset($__p[0])) { $target = $__p[0]; } else { if (isset($__p["target"])) { $target = $__p["target"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'btns' was called without parameter: target");  } }  ?>
<div class="btns">
    <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#<?php echo $target; ?>"><?php echo Lang::translate('Preview'); ?></button>
    <?php if ($this->router->getActionName() != 'view') { ?>
        <input id="save" class="btn btn-primary" type="submit" value="<?php echo Lang::translate('Save'); ?>">
    <?php } ?>
</div><?php }; $this->_macros['btns'] = \Closure::bind($this->_macros['btns'], $this); ?><?php $this->_macros['pagerwizard'] = function($__p = null) { if (isset($__p[0])) { $baseUrl = $__p[0]; } else { if (isset($__p["baseUrl"])) { $baseUrl = $__p["baseUrl"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'pagerwizard' was called without parameter: baseUrl");  } } if (isset($__p[1])) { $path = $__p[1]; } else { if (isset($__p["path"])) { $path = $__p["path"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'pagerwizard' was called without parameter: path");  } }  ?>
<ul class="pager wizard">
    <li class="previous fr">
        <a href="#"><?php echo Lang::translate('Previous'); ?></a>
    </li>
    <li class="next fl">
        <a href="#"><?php echo Lang::translate('Next'); ?></a>
    </li>
    <li>
        <p>
            <a href="<?php echo $baseUrl; ?><?php echo $path; ?>" class="btn btn-primary backToList"><?php echo Lang::translate('Abort'); ?></a>
        </p>
    </li>
</ul><?php }; $this->_macros['pagerwizard'] = \Closure::bind($this->_macros['pagerwizard'], $this); ?><?php $this->_macros['addMoreAnswersMockUp'] = function() { ?>
    <div class="addMoreAnswersMockUp hidden">
        <div class="answersListUnit form-group">
            <div class="form-group">
                <img class="thumbnail previewImage">
                <input class="imageUpload" type="file" name="answerImage[]"
                       data-text='<?php echo Lang::translate('ChooseFile'); ?>'>
            </div>
            <div class="form-inline">
                <div class="form-group">
                    <label class="icon fr"></label>
                    <input type="text" class="answersListUnitTitle form-control" name='PollAnswer[][]'>
                </div>
            </div>
        </div>
    </div><?php }; $this->_macros['addMoreAnswersMockUp'] = \Closure::bind($this->_macros['addMoreAnswersMockUp'], $this); ?>
