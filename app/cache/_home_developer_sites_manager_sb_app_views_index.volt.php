<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $this->settings['projectName']; ?> - <?php echo Lang::translate((ucwords($this->router->getControllerName()))); ?></title>

    <?php echo $this->assets->outputCss('headerCss'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?php echo $this->assets->outputJs('headerJs'); ?>
    <![endif]-->

</head>

<body class="<?php if ($lang == 'he') { ?>rtl<?php } else { ?>ltr<?php } ?>" id="<?php echo $this->router->getControllerName(); ?>" data-lang="<?php echo $lang; ?>">

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $baseUrl; ?>"><?php echo $this->settings['projectName']; ?> - <?php echo Lang::translate((ucwords($this->router->getControllerName()))); ?></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                            </div>
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                            </div>
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                            </div>
                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>Read All Messages</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-tasks">
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <span class="pull-right text-muted">40% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <span class="pull-right text-muted">20% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <span class="pull-right text-muted">60% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <span class="pull-right text-muted">80% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Tasks</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-tasks -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-comment fa-fw"></i> New Comment
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                <span class="pull-right text-muted small">12 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-envelope fa-fw"></i> Message Sent
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-tasks fa-fw"></i> New Task
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-alerts -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $baseUrl; ?>index/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown langOptions fl">
                
                <a class="dropdown-toggle langToggle" data-toggle="dropdown">
                    <img src="<?php echo $baseUrl; ?>img/<?php echo $lang; ?>.png">
                </a>
                <ul class="dropdown-menu langMenu">
                    <li <?php if ($lang == 'en') { ?>class="active"<?php } ?>>
                        <a class="changeLang" data-lang="en">
                            <img src="<?php echo $baseUrl; ?>img/en.png">
                            <span class="marginRight"><?php echo Lang::translate('English'); ?></span>
                        </a>
                    </li>
                    <li <?php if ($lang == 'he') { ?>class="active"<?php } ?>>
                        <a class="changeLang" data-lang="he">
                            <img src="<?php echo $baseUrl; ?>img/he.png">
                            <span class="marginRight"><?php echo Lang::translate('Hebrew'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li <?php if ($this->dispatcher->getControllerName() == 'index') { ?>class='active'<?php } ?>>
                        <a href="<?php echo $baseUrl; ?>dashboard" <?php if ($this->dispatcher->getControllerName() == 'index') { ?>class='active'<?php } ?>><i class="fa fa-dashboard fa-fw"></i><span> <?php echo Lang::translate('Dashboard'); ?></span></a>
                    </li>
                    <li <?php if ($this->dispatcher->getControllerName() == 'segment') { ?>class='active'<?php } ?>>
                        <a href="<?php echo $baseUrl; ?>segment" <?php if ($this->dispatcher->getControllerName() == 'segment') { ?>class='active'<?php } ?>><i class="fa fa-sort-amount-asc fr"></i><span> <?php echo Lang::translate('Segments'); ?></span></a>
                    </li>
                    <li <?php if ($this->dispatcher->getControllerName() == 'coupon') { ?>class='active'<?php } ?>>
                        <a href="<?php echo $baseUrl; ?>coupon" <?php if ($this->dispatcher->getControllerName() == 'coupon') { ?>class='active'<?php } ?>><i class="fa fa-tag fr"></i><span> <?php echo Lang::translate('Coupons'); ?></span></a>
                    </li>
                    <li <?php if ($this->dispatcher->getControllerName() == 'customer') { ?>class='active'<?php } ?>>
                        <a href="<?php echo $baseUrl; ?>customer" <?php if ($this->dispatcher->getControllerName() == 'customer') { ?>class='active'<?php } ?>><i class="fa fa-users fr"></i><span> <?php echo Lang::translate('ClubMembers'); ?></span></a>
                    </li>
                    <li id="engagementsTab">
                        <a id="engagementsHeader"><i class="fa fa-rocket fr"></i><span> <?php echo Lang::translate('UserEngagements'); ?></span></a>
                        <ul class="nav nav-second-level" id="engagementsListTabs">
                            <li <?php if ($this->dispatcher->getControllerName() == 'poll') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>poll" <?php if ($this->dispatcher->getControllerName() == 'poll') { ?>class='active'<?php } ?>><i class="fa fa-check-square-o marginLeft"></i> <?php echo Lang::translate('Polls'); ?></a>
                            </li>
                            <li <?php if ($this->dispatcher->getControllerName() == 'punchcard') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>punchcard" <?php if ($this->dispatcher->getControllerName() == 'punchcard') { ?>class='active'<?php } ?>><i class="fa fa-cubes marginLeft"></i> <?php echo Lang::translate('PunchCards'); ?></a>
                            </li>
                            <li <?php if ($this->dispatcher->getControllerName() == 'fliptile') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>fliptile" <?php if ($this->dispatcher->getControllerName() == 'fliptile') { ?>class='active'<?php } ?>><i class="fa fa-th marginLeft"></i> <?php echo Lang::translate('Fliptiles'); ?></a>
                            </li>
                            <li <?php if ($this->dispatcher->getControllerName() == 'message') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>message" <?php if ($this->dispatcher->getControllerName() == 'message') { ?>class='active'<?php } ?>><i class="fa fa-envelope-o marginLeft"></i> <?php echo Lang::translate('Messages'); ?></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a id="reportsHeader"><i class="fa fa-list-alt fr"></i><span> <?php echo Lang::translate('Reports'); ?></span></a>
                        <ul class="nav nav-second-level" id="reportTabs">
                            <li <?php if ($this->dispatcher->getControllerName() == 'reports' && $this->dispatcher->getActionName() == 'coupons') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>reports/coupons" <?php if ($this->dispatcher->getControllerName() == 'reports' && $this->dispatcher->getActionName() == 'coupons') { ?>class='active'<?php } ?>><i class="fa fa-tags marginLeft"></i> <?php echo Lang::translate('Coupons'); ?></a>
                            </li>
                            <li <?php if ($this->dispatcher->getControllerName() == 'reports' && $this->dispatcher->getActionName() == 'segments') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>reports/segments" <?php if ($this->dispatcher->getControllerName() == 'reports' && $this->dispatcher->getActionName() == 'segments') { ?>class='active'<?php } ?>><i class="fa fa-list-alt marginLeft"></i> <?php echo Lang::translate('Segments'); ?></a>
                            </li>
                            <li <?php if ($this->dispatcher->getControllerName() == 'reports' && $this->dispatcher->getActionName() == 'usersreports') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>reports/usersreports" <?php if ($this->dispatcher->getControllerName() == 'reports' && $this->dispatcher->getActionName() == 'usersreports') { ?>class='active'<?php } ?>><i class="fa fa-tags marginLeft"></i> <?php echo Lang::translate('UsersReports'); ?></a>
                            </li>
                            <li <?php if ($this->dispatcher->getControllerName() == 'reports' && $this->dispatcher->getActionName() == 'history') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>reports/history" <?php if ($this->dispatcher->getControllerName() == 'reports' && $this->dispatcher->getActionName() == 'history') { ?>class='active'<?php } ?>><i class="fa fa-tags marginLeft"></i> <?php echo Lang::translate('PurchaseHistory'); ?></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a id="feedbackHeader"><i class="fa fa-rss fr"></i><span> <?php echo Lang::translate('Feedback'); ?></span></a>
                        <ul class="nav nav-second-level" id="feedbackTabs">
                            <li <?php if ($this->dispatcher->getControllerName() == 'feedback') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>feedback" <?php if ($this->dispatcher->getControllerName() == 'feedback') { ?>class='active'<?php } ?>><i class="fa fa-edit marginLeft"></i> <?php echo Lang::translate('FeedbackForms'); ?></a>
                            </li>
                            <li <?php if ($this->dispatcher->getControllerName() == 'userfeedbacks') { ?>class='active'<?php } ?>>
                                <a href="<?php echo $baseUrl; ?>userfeedbacks" <?php if ($this->dispatcher->getControllerName() == 'userfeedbacks') { ?>class='active'<?php } ?>><i class="fa fa-users marginLeft"></i> <?php echo Lang::translate('UserFeedbacks'); ?></a>
                            </li>
                        </ul>
                    <li <?php if ($this->dispatcher->getControllerName() == 'beacon') { ?>class='active'<?php } ?>>
                        <a href="<?php echo $baseUrl; ?>beacon" <?php if ($this->dispatcher->getControllerName() == 'beacon') { ?>class='active'<?php } ?>><i class="fa fa-wifi fr"></i><span> <?php echo Lang::translate('Beacons'); ?></span></a>
                    </li>
                    <li <?php if ($this->dispatcher->getControllerName() == 'admin') { ?>class='active'<?php } ?>>
                        <a href="<?php echo $baseUrl; ?>admin" <?php if ($this->dispatcher->getControllerName() == 'admin') { ?>class='active'<?php } ?>><i class="fa fa-users fr"></i><span> <?php echo Lang::translate('Admin'); ?></span></a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <?php echo $this->getContent(); ?>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
<input type="hidden" readonly value="<?php echo $baseUrl; ?>" class="baseUrl">
<!-- /#wrapper -->
<?php echo $this->assets->outputJs('footer'); ?>
</body>

</html>

