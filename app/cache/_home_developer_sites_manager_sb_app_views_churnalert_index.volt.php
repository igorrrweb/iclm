<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <select class="select2 campaign-type" name="Search[campaignType]">
                <option value=""><?php echo Lang::translate('CampaignType'); ?>: <?php echo Lang::translate('All'); ?></option>
                <?php foreach ($campaigns as $campaign) { ?>
                    <option  value="<?php echo $campaign['type']; ?>" <?php if (isset($search['campaignType']) && $search['campaignType'] == $campaign['type']) { ?>
                    selected<?php } ?>><?php echo Lang::translate($campaign['type']); ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
            <thead>
            <tr>
                <?php foreach ($attributes as $attribute) { ?>
                    <th><?php echo Lang::translate($attribute); ?></th>
                <?php } ?>

            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        <!-- /.panel -->
    </div>
</div>