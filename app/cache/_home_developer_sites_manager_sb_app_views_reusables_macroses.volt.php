<?php $this->_macros['datatable'] = function($__p = null) { if (isset($__p[0])) { $attributes = $__p[0]; } else { if (isset($__p["attributes"])) { $attributes = $__p["attributes"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'datatable' was called without parameter: attributes");  } }  ?>
    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
        <thead>
        <tr>
            <?php foreach ($attributes as $attribute) { ?>
                <th><?php echo Lang::translate($attribute); ?></th>
            <?php } ?>

        </tr>
        </thead>
        <tbody>
        </tbody>
    </table><?php }; $this->_macros['datatable'] = \Closure::bind($this->_macros['datatable'], $this); ?>

