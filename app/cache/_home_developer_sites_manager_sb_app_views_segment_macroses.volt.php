<?php $this->_macros['gender'] = function($__p = null) { if (isset($__p[0])) { $segmentRule = $__p[0]; } else { if (isset($__p["segmentRule"])) { $segmentRule = $__p["segmentRule"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'gender' was called without parameter: segmentRule");  } }  ?>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][gender]" class="icheck", value="any"
                <?php if ((($segmentRule['profileRules']['gender'] == 'any') || (empty($segmentRule['profileRules']['gender'])))) { ?>
               checked <?php } ?>> <?php echo Lang::translate('Both'); ?></label>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][gender]" class="icheck", value="male"
                <?php if (($segmentRule['profileRules']['gender'] == 'male')) { ?>
        checked <?php } ?>> <?php echo Lang::translate('Male'); ?></label>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][gender]" class="icheck", value="female"
                <?php if (($segmentRule['profileRules']['gender'] == 'female')) { ?>
        checked <?php } ?>> <?php echo Lang::translate('Female'); ?></label><?php }; $this->_macros['gender'] = \Closure::bind($this->_macros['gender'], $this); ?><?php $this->_macros['married'] = function($__p = null) { if (isset($__p[0])) { $segmentRule = $__p[0]; } else { if (isset($__p["segmentRule"])) { $segmentRule = $__p["segmentRule"]; } else {  throw new \Phalcon\Mvc\View\Exception("Macro 'married' was called without parameter: segmentRule");  } }  ?>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][marital_status]" class="icheck", value="any"
                <?php if ((($segmentRule['profileRules']['marital_status'] == 'any') || (empty($segmentRule['profileRules']['marital_status'])))) { ?>
        checked <?php } ?>> <?php echo Lang::translate('Both'); ?></label>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][marital_status]" class="icheck", value="single"
                <?php if (($segmentRule['profileRules']['marital_status'] == 'single')) { ?>
        checked <?php } ?>> <?php echo Lang::translate('Single'); ?></label>
    <label class="radio-inline">
        <input type="radio" name="SegmentRule[profileRules][marital_status]" class="icheck", value="married"
                <?php if (($segmentRule['profileRules']['marital_status'] == 'married')) { ?>
        checked <?php } ?>> <?php echo Lang::translate('Married'); ?></label><?php }; $this->_macros['married'] = \Closure::bind($this->_macros['married'], $this); ?>

