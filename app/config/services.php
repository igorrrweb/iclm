<?php
/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config $config
 */

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionalManager;
use Phalcon\Http\Response\Cookies;
use Phalcon\Crypt;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir(APP_PATH . $config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => APP_PATH . $config->application->cacheDir,
                'compiledSeparator' => '_'
            ));
            $volt->getCompiler()->addFilter("round","round");
            $volt->getCompiler()->addFilter("roundShort",function($float){
                return 'round(' . $float . ', 2);';
            });
            $volt->getCompiler()->addFilter("date", function($date){
                return 'substr(' . $date . ',0,10)';
            });
            $volt->getCompiler()->addFilter("t", function($arg){
                return 'Lang::translate(' . $arg . ')';
            });
            $volt->getCompiler()->addFunction("implode", 'implode');


            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () use ($config) {
    $dbConfig = $config->database->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    return new Flash(array(
        'error'   => 'col-xs-12 col-md-12 alert alert-danger',
        'success' => 'col-xs-12 col-md-12 alert alert-success',
        'notice'  => 'col-xs-12 col-md-12 alert alert-info',
        'warning' => 'col-xs-12 col-md-12 alert alert-warning'
    ));
});
/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flashSession', function(){
    return new FlashSession(array(
        'error'   => 'col-xs-12 col-md-12 alert alert-danger',
        'success' => 'col-xs-12 col-md-12 alert alert-success',
        'notice'  => 'col-xs-12 col-md-12 alert alert-info',
        'warning' => 'col-xs-12 col-md-12 alert alert-warning'
    ));
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});
/**
 * Setup crypt
 */
$di->setShared('crypt', function() use($di) {
    $crypt = new \Phalcon\Crypt();
    $crypt->setMode(MCRYPT_MODE_CFB);
    $crypt->setKey('#WZ$SpH=dp?.ak//AsNp$');
    return $crypt;
});
/**
 * Setup cookies
 */
$di->set('cookies', function() {
    $cookies = new Cookies();
    $cookies->useEncryption(false);
    return $cookies;
});

/**
 * Setup settings
 */
$di->set("settings", function(){

    $settings = "";
    foreach (SystemSettings::find()->toArray() as $row)
    {
        $settings[$row['setting']] = $row['value'];
    }

    return [
        "dateFormat" => isset($settings['time_format']) ? $settings['time_format'] : "d/m/Y",
        "allowShares" => isset($settings['coupon_share']) ? (bool)$settings['coupon_share'] : true,
        "defaultLang" => isset($settings['default_lang']) ? $settings['default_lang'] : "en",
        "availableLangs" => ["Hebrew" => "he","Arabic" => "ar","Russian"=>"ru","English" => "en"],
        "projectName" => "LOGO"
    ];

});

/**
 * Setup modelsCache
 */
$di->set("modelsCache",function(){

    $frontCache =new Phalcon\Cache\Frontend\Data([
        "lifetime" => 86400
    ]);

    $cache = new Phalcon\Cache\Backend\Redis($frontCache);
    $keys = $cache->queryKeys();

    foreach ($keys as $key) {
        $cache->delete($key);
    }
    return $cache;
});

/**
 * Setup routes
 */
$di->set('router', function () {
    return require __DIR__ . '/routes.php';
}, true);

/**
 * Setup transactions
 */
$di->setShared('transactions', function(){
    return new TransactionalManager();
});

