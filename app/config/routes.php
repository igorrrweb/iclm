<?php

$router = new Phalcon\Mvc\Router(false);

//Remove trailing slashes automatically
$router->removeExtraSlashes(true);

$router->add('/:controller/:action/:params', [
    'controller' => 1,
    'action' => 2,
    'params' => 3

]);

$router->add('/:controller/:action', [
    'controller' => 1,
    'action' => 2,
]);

$router->add('/:controller', [
    'controller' => 1,
    'action' => 'index'
]);

$router->add('/404', [
   'controller' => 'index',
    'action' =>'notFound'
]);



return $router;