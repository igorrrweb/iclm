<?php

error_reporting(E_ALL);

define('APP_PATH', realpath('..') . '/');
use Phalcon\Config\Adapter\Ini as ConfigIni;

try {

    /**
     * Read the configuration
     */
    $config = new ConfigIni(APP_PATH . "app/config/config.ini");

    /**
     * Read auto-loader
     */
    include APP_PATH . "app/config/loader.php";
    include APP_PATH . "vendor/autoload.php";

    /**
     * Read services
     */
    include APP_PATH . "app/config/services.php";

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
