$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn">>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,

        ajax: {
            url: baseUrl+"poll/getDataTable",
            method: "POST"
        },
        columns: [
            { data:'id'},
            { data:'name'},
            { data:'duration'},
            { data:'end_date'},
            { data:'redeemed'},
            { data:'status',
                searchable: false,
                render: function(data, type, row){
                    if (data == '1') {
                        return '<input type="checkbox" data-item="'+row.id+'" checked="checked" class="activate switcher">'
                    } else {
                        return '<input type="checkbox" data-item="'+row.id+'" class="activate switcher">'
                    }
                }
            },
            { data:"actions",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a href="'+baseUrl+'poll/update/'+data+'"><i class=\"fa fa-edit\"></i></a> '+
                        '<a class="confirm" data-item="'+data+'" data-msg="Are you sure you want to delete '+row.name+'?">'+
                        '<i class="fa fa-times"></i></a> '+
                        '<a href="'+baseUrl+'poll/view/'+data+'">'+
                        '<i class="fa fa-list-alt"></i></a>';
                }
            }
        ],
        "fnDrawCallback": function() {
            switcherInit();
        }

    });

    $('.createNewBtn').prepend('<a href="'+baseUrl+'poll/create"><button type="button" class="btn btn-primary btn-block createNew">New Poll</button></a>');

    $('.select2').select2({
        language: lang,
    });

    $('.datepicker').datetimepicker({
        timepicker: true,
        format: "d/m/Y H:i:00"
    });

    var segmentTable = $('#segments').DataTable({
        "processing": true,
        "serverSide": true,
        ajax: {
            url: baseUrl+"poll/getSegment",
            method: "POST",
            "data": function ( d ) {
                var params = window.location.pathname.split('/');
                var getParam = params.length > 0 ? parseInt(params.pop()) : '';
                d.getParam = getParam;
            }
        },
        columns: [
            { data:'active_segment',
                searchable: false,
                render: function(data, type, row){
                    if(data == '1'){
                        return '<input type="checkbox" class="icheck" name="Segments[]" checked="checked" value="'+row.id+'">'
                    } else {
                        return '<input type="checkbox" class="icheck" name="Segments[]" value="'+row.id+'">'
                    }
                }
            },
            { data:'name'},
            { data:'audience'}
        ],
        "fnDrawCallback": function() {
            $('form.readOnly input[type="checkbox"]').attr('disabled','disabled');
        }
    });
    if(!$("form").hasClass('readOnly')){
        var $validator = $("form").validate({
            errorClass:'myErrorClass',
            errorPlacement: function (error, element) {
                var elem = $(element);
                error.insertAfter(element);
            },
            rules:{
                "Engagement[name]":{
                    required: true,
                    remote:{
                        url:baseUrl+'poll/validateName',
                        type:"post",
                        data:{
                            id_param: function(){
                                var path = window.location.pathname.split('/');
                                return parseInt(path[path.length-1]);
                            },
                            name: function(){
                                return $('input[name="Engagement[name]"]').val();
                            }
                        }
                    }
                },
                "engagementImage":{
                    required: {
                        depends: function(element){
                            return $('#engagementGeneralDetails .previewImage').attr('src') ? false : true;
                        }
                    },
                    image:true
                },
                "Engagement[start_date]":{
                    required: true
                },
                "Engagement[end_date]":{
                    required: true,
                    endDate:true
                },
                "EngagementLang[he][welcome_msg]":{
                    required: true
                },
                "EngagementLang[he][thankyou_msg]":{
                    required: true
                },
                "Coupon[name]":{
                    required: {
                        depends: function(element){
                            return $(document).find('input[name="Engagement[reward]"]').is(":checked");
                        }
                    },
                    remote:{
                        url:baseUrl+'poll/validateCouponName',
                        type:"post",
                        data:{
                            id_param: function(){
                                var path = window.location.pathname.split('/');
                                return parseInt(path[path.length-1]);
                            },
                            name: function(){
                                return $('input[name="Coupon[name]"]').val();
                            }
                        }
                    }
                },
                "Coupon[pos_coupon_id]":{
                    required: true,
                    number: true
                },
                "Coupon[max_coupons]":{
                    required: true,
                    number: true
                },
                "couponImage":{
                    image:true
                },
                "CouponLang[he][title]":{
                    required: true
                },
                "Poll[he][question]":{
                    required: true
                },
                "answerImage[]":{
                    image:true
                },
                "PollAnswer[he][]":{
                    required: true
                }
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.addClass(errorClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.removeClass(errorClass);
            },
            submitHandler: function(form){
                form.submit();
            }
        });
    }

    $("#engagementImageUpload").on("change",function(){
        var input = $(this);
        readURL(input[0], "#engagementGeneralDetails .previewImage");
    });

    $("#couponImageUpload").on("change",function(){
        var input = $(this);
        readURL(input[0], "#engagementCoupon .previewImage");
    });

    $('#pollData .imageUpload').on("change",function(){
        var input = $(this);
        if (input[0].files && input[0].files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                input.siblings('img').attr('src', e.target.result);
            };
            reader.readAsDataURL(input[0].files[0]);
        }
    });

    $(document).on('click','#couponPreviewButton',function (){
        $("#couponPreview #couponPreviewImage").attr('src',$("#engagementCoupon .previewImage").attr('src'));
        $("#couponPreview #previewTitle").html($("#engagementReward .tab-pane.langTabData.active  .couponTitle").val());
        $("#couponPreview #previewSubTitle").html($("#engagementReward .tab-pane.langTabData.active .couponSubTitle").val());
        $("#couponPreview #previewDetails").html($("#engagementReward .tab-pane.langTabData.active .couponDetails").val());
        $("#couponPreview #previewDisclaimer").html($("#engagementReward .tab-pane.langTabData.active .couponDisclaimer").val());
    });

    $(document).on("click",".addMoreAnswers", function(){
        var html = $('.addMoreAnswersMockUp .answersListUnit').clone();
        html.find('.icon').text(parseInt($('#pollData .tab-pane.langTabData.active .pollAnswersList')
                .children('.answersListUnit').last().find('.icon').text())+1);
        html.find('.answersListUnitTitle').attr('name',$('#pollData .tab-pane.langTabData.active .pollAnswersList')
            .children('.answersListUnit').last().find('.answersListUnitTitle').attr('name'));
        $('#pollData .tab-pane.langTabData.active .pollAnswersList').append(html);
    });

    $('#userEngagements #rootwizard').bootstrapWizard({
        onNext: function(tab,navigation,index){
            var $valid = $("form").valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            $("body").scrollTop(0);
            if (index === 4){
                setPreview();
            }
        },
        onPrevious: function(tab,navigation,index){
            $("body").scrollTop(0);
        },
        onTabClick: function(tab,navigation,index){
            var $valid = $("form").valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            $("body").scrollTop(0);
            setPreview();
        }
    });

    if ($('#engagementReward input[name="Engagement[reward]"]').prop('checked') == true ) {
        $("#engagementCoupon").css("display","block");
    }

    $(document).on("click", '#engagementReward input[name="Engagement[reward]"]', function(event){
        if($(this).prop('checked')==true){
            $("#engagementCoupon").slideDown("slow");
        } else {
            $("#engagementCoupon").slideUp("slow");
        }
    });

    function setPreview(){
        $("#pollPreviewQuestion").text($("#pollData .langTabData.active .pollQuestion").val());
        $("#pollPreviewQuestionFancy").text($("#pollData .langTabData.active .pollQuestion").val());
        var answers = "";
        $.each($("#pollData .langTabData.active .pollAnswers input[type=text]:not([disabled])"), function (key, value) {
            var iPreview = $(value).parent().parent().siblings().find('img.previewImage').attr('src');
            if ($(value).val() != ""){
                answers += "<li class='col-md-6 col-xs-12'><img class='thumbnail previewImage'  src='"+iPreview+"' /><br>" +
                    "<label><input type='radio' class='fr marginLeft'> "+($(value).val())+"</label></li>";
            }
        });
        $("#pollPreviewAnswers").html(answers);
        $("#pollPreviewAnswersFancy").html(answers);
        $('#pollPreviewAnswersFancyModalLabel').text($("#pollData .langTabData.active .pollQuestion").val());
        $('.engagementPreviewImage img').attr('src',$('#engagementGeneralDetails .previewImage').attr('src'));
    }

    $(document).on("click",".confirm",function(){
        var langs = JSON.parse($("#jsTranslate").text());
        var id = $(this).data("item");
        $.confirm({
            title: langs[0],
            content: $(this).data("msg"),
            icon: "fa fa-bullseye",
            confirmButton: langs[0],
            cancelButton: langs[1],
            theme: 'hololight',
            confirm: function(){
                $.post(baseUrl+"poll/delete",{id:id},function(response){
                    if(response.status){
                        table.row(id).remove().draw();
                    } else {
                        console.log(response.error);
                    }
                },"json");
            }
        })
    });

    if($('.flash-message').has('.alert').length>0){
        setTimeout(function(){
            $('.flash-message').slideUp('400');
            $('.flash-message').empty();
        },2000);
    }

    $(document).on('switchChange.bootstrapSwitch','.switcher', function(event, state) {
        var path = location.pathname;
        $.post(path + "/activate", "active=" + (!state ? 0 : 1 ) + "&item-id=" +  $(this).data("item"));
    });

    $('form.readOnly input[type="checkbox"], form.readOnly input[type="radio"], form.readOnly input[type="file"], form.readOnly .select2').attr('disabled','disabled');
    $('form.readOnly textarea, form.readOnly input[type!="checkbox"]').attr('readonly','readonly');

});
