$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");

    function setDatesFields() {
        $('#select_start_date').val($('.startDate').val() + ' - ' + $('.endDate').val());
    }
    setDatesFields();

    $('.select2').select2({
        language: lang,
    });

    $('#select_start_date').daterangepicker({
        "startDate": $('.startDate').val(),
        "endDate": $('.endDate').val(),
        "cancelClass": "btn-sm",
        "applyClass": "btn-sm btn-primary get-chart",
        "opens": "left",
        locale: {
            format: 'DD/MM/YYYY'
        },
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    $('#select_start_date').on('apply.daterangepicker', function(ev, picker){
        var startDate = picker.startDate.format('DD/MM/YYYY');
        var endDate = picker.endDate.format('D/MM/YYYY');
        $('.startDate').val(startDate);
        $('.endDate').val(endDate);
        $('#indexForm').submit();
    });

    $(document).on('keyup','input[name="filterValueUserId"]',function(){
        var autoComplete =  $(this).siblings(".autoComplete");
        $.post(baseUrl+'reports/autoComplete',
            {
                filterValueUserId:$(this).val(),
                extracolumn:'LcNumber',
                extravalue:$('input[name="filterValueLcNumber"]').val()
            },
            function(response){
                fillupAutocomplete(response, autoComplete)
            },'json');
    });

    $(document).on('keyup','input[name="filterValueLcNumber"]',function(){
        var autoComplete =  $(this).siblings(".autoComplete");
        $.post(baseUrl+'reports/autoComplete',
            {
                filterValueLcNumber:$(this).val(),
                extracolumn:'UserId',
                extravalue:$('input[name="filterValueUserId"]').val()
            },
            function(response){
                fillupAutocomplete(response, autoComplete)
            },'json');
    });

    $(document).on('click','.autoComplete li',function(){
        $(this).parent().siblings("input").val($(this).text());
        $(this).parent().slideUp();
    });

    $(document).on('blur','.autocomplete-parent input',function(){
        $(this).siblings('.autoComplete').slideUp();
    });




});