$(function() {
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");

    $(".changeLang").on("click", function () {
        $.post(baseUrl + "index/switchLang", "lang=" + $(this).data("lang"), function (response) {
            location.reload();
        });
    });

    $(".langToggle").on("click", function () {
        $(".langMenu").slideToggle()
    });

    $("#forgotPassword").on("click", function () {
        $("#resetPassword").slideToggle();
    });

    $("#submitReset").on("click", function () {
        $("#resetPassword p").fadeOut();
        $.post(baseUrl + "index/forgotPassword", "userEmail=" + $("#userEmail").val(), function (response) {
            if (response == "true") {
                $("#resetPassword p.hide").eq(1).fadeIn();
            } else {
                $("#resetPassword p.hide").eq(0).fadeIn();
            }
        })
    });

    $("#passwordResetForm").on("submit", function () {
        if ($("#newPassword").val() != $("#confirmPassword").val() || $("#newPassword").val() == "") {
            $("#passwordError").fadeIn();
            return false;
        }
    });

    $("#form_login").validate({
        errorClass:'error text-danger',
        errorPlacement: function (error, element) {
            var elem = $(element);
            error.insertAfter(element);
        },
        rules:{
            username:{
                required:true,
            },
            password:{
                required:true,
            },
        },
        submitHandler: function(form){
            var data = $(form).serialize();
            $.ajax({
                url: baseUrl+'index',
                method: 'POST',
                data: data,
                error: function(){},
                success: function(response){
                    if(response == baseUrl+"dashboard"){

                        window.location.href = response;
                    }
                }
            });
        }
    });

});

