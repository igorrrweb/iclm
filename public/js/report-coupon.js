$(function() {
    var baseUrl = $('.baseUrl').val();

    var lang = $("body").data("lang");

    $('body.rtl .panel-body').css({
        direction: 'ltr'
    });

    cb(moment($('#select_start_date').data('startdate'),'DD/MM/YYYY'),moment($('#select_start_date').data('enddate'),'DD/MM/YYYY'));

    $('#select_start_date').daterangepicker({
        startDate: moment($('#select_start_date').data('startdate'),'DD/MM/YYYY').format('DD/MM/YYYY'),
        endDate: moment($('#select_start_date').data('enddate'),'DD/MM/YYYY').format('DD/MM/YYYY'),
        "cancelClass": "btn-sm",
        "applyClass": "btn-sm btn-primary get-chart",
        "opens": "left",
        locale: {
            format: 'DD/MM/YYYY'
        },
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    },cb);

    $('#select_start_date').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate.format('DD/MM/YYYY');
        var endDate = picker.endDate.format('DD/MM/YYYY');
        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        var dates = [startDate,endDate];

        $.post(baseUrl+"reports/getCouponData/"+id, "dates="+JSON.stringify(dates), function(response){
            var data = JSON.parse(response);
            switch (true)
            {
                case data['redeemed']['dates'] == undefined && data['presented']['dates'] == undefined:
                    var labels = dates;
                    var data = ['presented','redeemed'];
                    data['presented'] = [0,0];
                    data['redeemed'] = [0,0];
                    break;
                case data['redeemed']['dates'] == undefined && data['presented']['dates']:
                    var labels = data['presented']['dates'];
                    break;
                case data['presented']['dates'] == undefined && data['redeemed']['dates']:
                    var labels = data['redeemed']['dates'];
                default:
                    var labels = [];
                    $.each(unique(data['presented']['dates'].concat(data['redeemed']['dates'])), function(i,value){
                        labels.push(value);
                    });
                    break;
            }
            $('.chartContainer').data('values',data);
            $('.chartContainer').data('labels',labels);

            setChart();
        });
    });


    setChart();

    function setChart() {
        var dataValues = JSON.parse( JSON.stringify( $('.chartContainer').data('values') ) );
        var dataLabels = JSON.parse( JSON.stringify( $('.chartContainer').data('labels') ) );
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'flot-line-chart',
                type: 'line'
            },
            title: {
                text: null
            },
            xAxis: {
                categories: dataLabels,
                labels: {
                    rotation: -90,
                    style: {
                        fontSize: '13px',
                    }
                },
                reversed: lang == 'en' ? false : true,
            },
            yAxis: {
                title: null,
                opposite: lang == 'en' ? false : true,
            },
            series: [
                {
                    name: 'presented',
                    data: dataValues.presented.values,
                },
                {
                    name: 'redeemed',
                    data: dataValues.redeemed.values,
                }
            ],
            credits: {
                enabled: false
            },
            legend:{
                enabled:true
            },
            tooltip:{
                enabled:true
            },
        });
    }

    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"B><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn"B>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTable').DataTable({
        "dom": dom,
        buttons:['csv','excel'],
        "paging":   false,
        "info":     false,
        "searching": false,
        'ordering':false,
    });

});