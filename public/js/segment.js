$(function() {
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn">>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        ajax: {
            url: baseUrl + "segment/getDataTable",
            method: "POST"
        },
        columns: [
            {data: "id"},
            {data: "name"},
            {data: "members"},
            {
                data: "active",
                searchable: false,
                render: function (data, type, row) {
                    if (data == '1') {
                        return '<input type="checkbox" data-item="' + row.id + '" checked="checked" class="activate switcher">'
                    } else {
                        return '<input type="checkbox" data-item="' + row.id + '" class="activate switcher">'
                    }
                }
            },
            {data: "created_at"},
            {
                data: "actions",
                searchable: false,
                orderable: false,
                render: function (data, type, row) {
                    return '<a href="' + baseUrl + 'segment/update/' + data + '"><i class=\"fa fa-edit\"></i></a> ' +
                        '<a class="confirm" data-item="' + data + '" data-msg="Are you sure you want to delete ' + row.name + '?">' +
                        '<i class="fa fa-times"></i></a> ' +
                        '<a href="' + baseUrl + 'segment/view/' + data + '">' +
                        '<i class="fa fa-list-alt"></i></a>';
                }
            }
        ],
        "fnDrawCallback": function () {
            switcherInit();
        }

    });


    $('.createNewBtn').prepend('<a href="' + baseUrl + 'segment/create"><button type="button" class="btn btn-primary btn-block createNew">New Segment</button></a>');


    $('.select2').select2({
        language: lang,
    });

    $(document).on("click", ".confirm", function () {
        var langs = JSON.parse($("#jsTranslate").text());
        var id = $(this).data("item");
        $.confirm({
            title: langs[0],
            content: $(this).data("msg"),
            icon: "fa fa-bullseye",
            confirmButton: langs[0],
            cancelButton: langs[1],
            theme: 'hololight',
            confirm: function () {
                $.post(baseUrl + "segment/delete", {id: id}, function (response) {
                    if (response.status) {
                        table.row(id).remove().draw();
                    } else {
                        console.log(response.error);
                    }
                }, "json");
            }
        })
    });

    if ($('.flash-message').has('.alert').length > 0) {
        setTimeout(function () {
            $('.flash-message').slideUp('400');
            $('.flash-message').empty();
        }, 2000);
    }

    if(!$("form").hasClass('readOnly')){
        var validobj = $("form").validate({
            errorClass: 'myErrorClass',
            errorPlacement: function (error, element) {
                var elem = $(element);
                error.insertAfter(element);
            },
            rules: {
                "Segment[name]": {
                    required: true,
                    remote: {
                        url: baseUrl + 'segment/validateName',
                        type: "post",
                        data: {
                            id_param: function () {
                                var path = window.location.pathname.split('/');
                                return parseInt(path[path.length - 1]);
                            },
                            name: function () {
                                return $('input[name="Segment[name]"]').val();
                            }
                        }
                    }
                },
                "SegmentRule[profileRules][gender]": {
                    required: true
                },
                "SegmentRule[profileRules][marital_status]": {
                    required: true
                },
                "SegmentRule[profileRules][birth_date]": {
                    number: true
                },
                "SegmentRule[profileRules][wedding_anniversary_date]": {
                    number: true
                },
                "SegmentRule[profileRules][join_date]": {
                    number: true
                },
                "SegmentRule[profileRules][age][min]": {
                    number: true
                },
                "SegmentRule[profileRules][age][max]": {
                    number: true
                },
                "SegmentRule[branchVisitsRules][freq][min]": {
                    number: true
                },
                "SegmentRule[branchVisitsRules][freq][max]": {
                    number: true
                },
                "SegmentRule[transactionsRules][last_month][min]": {
                    number: true
                },
                "SegmentRule[transactionsRules][last_month][max]": {
                    number: true
                },
                "SegmentRule[transactionsRules][last_year][min]": {
                    number: true
                },
                "SegmentRule[transactionsRules][last_year][max]": {
                    number: true
                },
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.addClass(errorClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.removeClass(errorClass);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    $(document).on('switchChange.bootstrapSwitch','.switcher', function(event, state) {
        var path = location.pathname;
        $.post(path + "/activate", "active=" + (!state ? 0 : 1 ) + "&item-id=" +  $(this).data("item"));
    });

    $('form.readOnly input[type="checkbox"], form.readOnly input[type="radio"], form.readOnly input[type="file"], form.readOnly .select2').attr('disabled','disabled');
    $('form.readOnly textarea, form.readOnly input[type!="checkbox"]').attr('readonly','readonly');

});

