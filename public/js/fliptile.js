$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn">>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,

        ajax: {
            url: baseUrl+"fliptile/getDataTable",
            method: "POST"
        },
        columns: [
            { data:'id'},
            { data:'name'},
            { data:'duration'},
            { data:'end_date'},
            { data:'redeemed'},
            { data:'status',
                searchable: false,
                render: function(data, type, row){
                    if (data == '1') {
                        return '<input type="checkbox" data-item="'+row.id+'" checked="checked" class="activate switcher">'
                    } else {
                        return '<input type="checkbox" data-item="'+row.id+'" class="activate switcher">'
                    }
                }
            },
            { data:"actions",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a href="'+baseUrl+'fliptile/update/'+data+'"><i class=\"fa fa-edit\"></i></a> '+
                        '<a class="confirm" data-item="'+data+'" data-msg="Are you sure you want to delete '+row.name+'?">'+
                        '<i class="fa fa-times"></i></a> '+
                        '<a href="'+baseUrl+'fliptile/view/'+data+'">'+
                        '<i class="fa fa-list-alt"></i></a>';
                }
            }
        ],
        "fnDrawCallback": function() {
            switcherInit();
        }
    });

    $('.createNewBtn').prepend('<a href="'+baseUrl+'fliptile/create"><button type="button" class="btn btn-primary btn-block createNew">New Fliptile</button></a>');

    $('.datepicker').datetimepicker({
        timepicker: true,
        format: "d/m/Y H:i:00"
    });

    var segmentTable = $('#segments').DataTable({
        "processing": true,
        "serverSide": true,
        ajax: {
            url: baseUrl+"fliptile/getSegment",
            method: "POST",
            "data": function ( d ) {
                var params = window.location.pathname.split('/');
                var getParam = params.length > 0 ? parseInt(params.pop()) : '';
                d.getParam = getParam;
            }
        },
        columns: [
            { data:'active_segment',
                searchable: false,
                render: function(data, type, row){
                    if(data == '1'){
                        return '<input type="checkbox" class="icheck" name="Segments[]" checked="checked" value="'+row.id+'">'
                    } else {
                        return '<input type="checkbox" class="icheck" name="Segments[]" value="'+row.id+'">'
                    }
                }
            },
            { data:'name'},
            { data:'audience'}
        ],
        "fnDrawCallback": function() {
            $('form.readOnly input[type="checkbox"]').attr('disabled','disabled');
        }
    });

    $("#engagementImageUpload").on("change",function(){
        var input = $(this);
        readURL(input[0], "#engagementGeneralDetails .previewImage");
    });

    $("#couponImageUpload").on("change",function(){
        var input = $(this);
        readURL(input[0], "#engagementCoupon .previewImage");
    });

    $('#fliptileData .fliptileImageUpload').on('change', function(){
        var input = $(this);
        if (input[0].files && input[0].files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                input.closest('.row').find('img').first().attr('src', e.target.result);
            };
            reader.readAsDataURL(input[0].files[0]);
        }
    });
    if(!$("form").hasClass('readOnly')) {
        var $validator = $("form").validate({
            errorClass: 'myErrorClass',
            errorPlacement: function (error, element) {
                var elem = $(element);
                error.insertAfter(element);
            },
            rules: {
                "Engagement[name]": {
                    required: true,
                    remote: {
                        url: baseUrl + 'fliptile/validateName',
                        type: "post",
                        data: {
                            id_param: function () {
                                var path = window.location.pathname.split('/');
                                return parseInt(path[path.length - 1]);
                            },
                            name: function () {
                                return $('input[name="Engagement[name]"]').val();
                            }
                        }
                    }
                },
                "engagementImage": {
                    required: {
                        depends: function (element) {
                            return $('#engagementGeneralDetails .previewImage').attr('src') ? false : true;
                        }
                    },
                    image: true
                },
                "Engagement[start_date]": {
                    required: true
                },
                "Engagement[end_date]": {
                    required: true,
                    endDate: true
                },
                "EngagementLang[he][welcome_msg]": {
                    required: true,
                },
                "EngagementLang[he][thankyou_msg]": {
                    required: true,
                },
                "Coupon[name]": {
                    required: {
                        depends: function (element) {
                            return $(document).find('input[name="Engagement[reward]"]').is(":checked");
                        }
                    },
                    remote: {
                        url: baseUrl + 'fliptile/validateCouponName',
                        type: "post",
                        data: {
                            id_param: function () {
                                var path = window.location.pathname.split('/');
                                return parseInt(path[path.length - 1]);
                            },
                            name: function () {
                                return $('input[name="Coupon[name]"]').val();
                            }
                        }
                    }
                },
                "Coupon[pos_coupon_id]": {
                    required: true,
                    number: true
                },
                "Coupon[max_coupons]": {
                    required: true,
                    number: true
                },
                "couponImage": {
                    image: true
                },
                "CouponLang[he][title]": {
                    required: true,
                },
                "image1": {
                    required: {
                        depends: function (element) {
                            var res = true;
                            if ($('.fliptileDataFlag1 img.previewImage').attr('src')) {
                                res = false;
                            }
                            if (!$('input[name="Fliptile[1][price_before]"]').val()) {
                                res = false;
                            }
                            return res;
                        }
                    },
                    image: true
                },
                "image2": {
                    required: {
                        depends: function (element) {
                            var res = true;
                            if ($('.fliptileDataFlag2 img.previewImage').attr('src')) {
                                res = false;
                            }
                            if (!$('input[name="Fliptile[2][price_before]"]').val()) {
                                res = false;
                            }
                            return res;
                        }
                    },
                    image: true
                },
                "image3": {
                    required: {
                        depends: function (element) {
                            var res = true;
                            if ($('.fliptileDataFlag3 img.previewImage').attr('src')) {
                                res = false;
                            }
                            if (!$('input[name="Fliptile[3][price_before]"]').val()) {
                                res = false;
                            }
                            return res;
                        }
                    },
                    image: true
                },
                "image4": {
                    required: {
                        depends: function (element) {
                            var res = true;
                            if ($('.fliptileDataFlag4 img.previewImage').attr('src')) {
                                res = false;
                            }
                            if (!$('input[name="Fliptile[4][price_before]"]').val()) {
                                res = false;
                            }
                            return res;
                        }
                    },
                    image: true
                },
                "image5": {
                    required: {
                        depends: function (element) {
                            var res = true;
                            if ($('.fliptileDataFlag5 img.previewImage').attr('src')) {
                                res = false;
                            }
                            if (!$('input[name="Fliptile[5][price_before]"]').val()) {
                                res = false;
                            }
                            return res;
                        }
                    },
                    image: true
                },
                "image6": {
                    required: {
                        depends: function (element) {
                            var res = true;
                            if ($('.fliptileDataFlag6 img.previewImage').attr('src')) {
                                res = false;
                            }
                            if (!$('input[name="Fliptile[6][price_before]"]').val()) {
                                res = false;
                            }
                            return res;
                        }
                    },
                    image: true
                },
                "Fliptile[1][price_before]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag1 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[2][price_before]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag2 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[3][price_before]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag3 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[4][price_before]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag4 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[5][price_before]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag5 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[6][price_before]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag6 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[1][price_after]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag1 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[2][price_after]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag2 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[3][price_after]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag3 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[4][price_after]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag4 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[5][price_after]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag5 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                },
                "Fliptile[6][price_after]": {
                    required: {
                        depends: function (element) {
                            return typeof $('.fliptileDataFlag6 img.previewImage').attr('src') == "undefined" ? false : true;
                        }
                    },
                    custom_float: true
                }
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.addClass(errorClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.removeClass(errorClass);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    $('#userEngagements #rootwizard').bootstrapWizard({
        onNext: function(tab,navigation,index){
            var $valid = $("form").valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            $("body").scrollTop(0);
            if (index === 4){
                setPreview();
            }
        },
        onPrevious: function(tab,navigation,index){
            $("body").scrollTop(0);
        },
        onTabClick: function(tab,navigation,index){
            var $valid = $("form").valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            $("body").scrollTop(0);
            setPreview();

        }
    });

    if ($('#engagementReward input[name="Engagement[reward]"]').prop('checked') == true ) {
        $("#engagementCoupon").css("display","block");
    }

    $(document).on("click", '#engagementReward input[name="Engagement[reward]"]', function(event){
        if($(this).prop('checked')==true){
            $("#engagementCoupon").slideDown("slow");
        } else {
            $("#engagementCoupon").slideUp("slow");
        }
    });

    function setPreview(){
        $("#engagementPreview .engagementPreviewImage img").attr('src',$("#engagementGeneralDetails .previewImage").attr('src'));
        $('#fliptilePuzzle').empty();

        $.each($("#fliptileData").find('img'), function (key, value) {
            if ($(value).attr('src') != undefined ){
                $('#fliptilePuzzle').append('<div class="col-md-4 img-responsive-wrapper"><img class="img-thumbnail img-responsive fliptile-pazzle" src="'+$(value).attr('src')+'" /></div>');
            }
        });
        setTimeout(function(){
            $('.fliptile-pazzle').height($('.img-responsive-wrapper').width());
            $('.general-image').height($('.engagementPreviewImage').width());
        },100);

    }
    $(window).resize(function(){
        $('.fliptile-pazzle').height($('.img-responsive-wrapper').width());
        $('.general-image').height($('.engagementPreviewImage').width());
    });

    $(document).on("click",".confirm",function(){
        var langs = JSON.parse($("#jsTranslate").text());
        var id = $(this).data("item");
        $.confirm({
            title: langs[0],
            content: $(this).data("msg"),
            icon: "fa fa-bullseye",
            confirmButton: langs[0],
            cancelButton: langs[1],
            theme: 'hololight',
            confirm: function(){
                $.post(baseUrl+"fliptile/delete",{id:id},function(response){
                    if(response.status){
                        table.row(id).remove().draw();
                    } else {
                        console.log(response.error);
                    }
                },"json");
            }
        })
    });

    if($('.flash-message').has('.alert').length>0){
        setTimeout(function(){
            $('.flash-message').slideUp('400');
            $('.flash-message').empty();
        },2000);
    }

    $(document).on('switchChange.bootstrapSwitch','.switcher', function(event, state) {
        var path = location.pathname;
        $.post(path + "/activate", "active=" + (!state ? 0 : 1 ) + "&item-id=" +  $(this).data("item"));
    });

    $('form.readOnly input[type="checkbox"], form.readOnly input[type="radio"], form.readOnly input[type="file"], form.readOnly .select2').attr('disabled','disabled');
    $('form.readOnly textarea, form.readOnly input[type!="checkbox"]').attr('readonly','readonly');

});