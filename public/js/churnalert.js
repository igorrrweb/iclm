$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    $('.select2').select2({
        language: lang,
    });

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"B><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">',
        buttons:['csv','excel'],
        ajax: {
            url: baseUrl+"churnalert/getDataTable",
            method: "POST",
            "data": function ( d ) {
                d.campaign_type = $('.select2').select2("val");
            }
        },
        columns: [
            {data:"customer_name"},
            {data:"phone_number"},
            {data:"join_date"},
            {data:"churn_alert_date"},
            {data:"retention"},
            {data:"status",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a href="'+baseUrl+'reports/history/'+data+'" >View</a>'
                }
            },
            {data: "actions",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a href="'+baseUrl+'churnalert/edit/'+data+'"><i class=\"fa fa-edit\"></i></a> '+
                        '<a class="confirm" data-item="'+data+'" data-msg="Are you sure you want to delete '+row.name+'?">'+
                        '<i class="fa fa-times"></i></a> '+
                        '<a href="'+baseUrl+'reports/history/'+data+'" >'+
                        '<i class="fa fa-list-alt"></i></a>';
                }
            }
        ],
        "fnDrawCallback": function() {}


    });


});
