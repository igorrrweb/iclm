$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-xs-12 col-sm-6 col-md-3 col-lg-2"l><".col-xs-12 col-sm-6 col-md-9 col-lg-10"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-xs-12 col-sm-6 col-md-9 col-lg-10"f><".col-xs-12 col-sm-6 col-md-3 col-lg-2"l>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        ajax: {
            url: baseUrl+"userfeedbacks/getDataTable",
            method: "POST"
        },
        columns: [
            {data: "question_id"},
            {data: "feedback_id"},
            {data: "customer_id"},
            {data: "title"},
            {data: "created_at"},
            {data: "actions",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a class="action" href="'+baseUrl+'userfeedbacks/viewsubmitted/'+row.actions+'/'+row.customer_id+'">View </a>'+
                        '<button data-member_id="'+row.customer_id+'" data-question_id="'+row.question_id+'" class="action archiveBtn btn btn-sm btn-link">Archive</button>'+
                        '<button data-member_id="'+row.customer_id+'" data-question_id="'+row.question_id+'" class="action readBtn btn btn-sm btn-link">Read</button>'+
                        '<button data-member_id="'+row.customer_id+'" data-question_id="'+row.question_id+'" class="action unreadBtn btn btn-sm btn-link">Unread</button>';
                }
            }
        ],
        "createdRow": function( row, data, dataIndex ) {
            if ( data.is_read == "1" ) {
                $(row).addClass( 'read' );
            } else {
                $(row).addClass( 'unread' );
            }
        },
        "fnDrawCallback": function() {}

    });

    $(document).on("click",".action",function(event){
        var params = $(this).data('member_id') + "/" + $(this).data('question_id');
        switch (true)
        {
            case $(event.target).hasClass("archiveBtn"):
                $.get(baseUrl+"userfeedbacks/archive/" + params);
                break;
            case $(event.target).hasClass("readBtn"):
                $.get(baseUrl+"userfeedbacks/read/" + params);
                break;
            case $(event.target).hasClass("unreadBtn"):
                $.get(baseUrl+"userfeedbacks/unread/" + params);
                break;
        }

        location.reload();
    });

});

