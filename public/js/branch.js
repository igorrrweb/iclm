$(function() {
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn">>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        ajax: {
            url: baseUrl+"branch/getDataTable",
            method: "POST"
        },
        columns: [
            { data:'branch_number'},
            { data:'branch_name',
                render: function(data, type, row){
                    return row.branch_name_lang ? row.branch_name_lang : row.branch_name ;
                }
            },
            { data:'city'},
            { data:'manager'},
            { data:'phone_number'},
            { data:'active',
                searchable: false,
                render: function(data, type, row){
                    if (data == '1') {
                        return '<input type="checkbox" data-item="'+row.actions+'" checked="checked" class="activate switcher">'
                    } else {
                        return '<input type="checkbox" data-item="'+row.actions+'" class="activate switcher">'
                    }
                }
            },
            { data:"actions",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a href="'+baseUrl+'branch/update/'+data+'"><i class=\"fa fa-edit\"></i></a> '+
                        '<a class="confirm" data-item="'+data+'" data-msg="Are you sure you want to delete '+row.branch_name+'?">'+
                        '<i class="fa fa-times"></i></a> '+
                        '<a href="'+baseUrl+'branch/view/'+data+'">'+
                        '<i class="fa fa-list-alt"></i></a>';
                }
            },
        ],
        "fnDrawCallback": function() {
            switcherInit();
        }

    });

    $('.createNewBtn').prepend('<a href="'+baseUrl+'branch/create"><button type="button" class="btn btn-primary btn-block createNew">New Branch</button></a>');

    $('.select2').select2({
        language: lang,
    });

    $(document).on("click",".confirm",function(){
        var langs = JSON.parse($("#jsTranslate").text());
        var id = $(this).data("item");
        $.confirm({
            title: langs[0],
            content: $(this).data("msg"),
            icon: "fa fa-bullseye",
            confirmButton: langs[0],
            cancelButton: langs[1],
            theme: 'hololight',
            confirm: function(){
                $.post(baseUrl+"branch/delete",{id:id},function(response){
                    if(response.status){
                        table.row(id).remove().draw();
                    } else {
                        console.log(response.error);
                    }
                },"json");
            }
        })
    });

    var $validator = $("form").validate({
        errorClass:'myErrorClass',
        errorPlacement: function (error, element) {
            var elem = $(element);
            error.insertAfter(element);
        },
        rules:{
            "Branch[branch_no]":{
                required:true,
                remote:{
                    url:baseUrl+'branch/validateNo',
                    type:"post",
                    data:{
                        id_param: function(){
                            var path = window.location.pathname.split('/');
                            return parseInt(path[path.length-1]);
                        },
                        name: function(){
                            return $('input[name="Branch[branch_no]"]').val();
                        }
                    }
                }
            },
            "branchLang[he][branch_name]":{
                require_from_group: [1, ".branch_name"]
            },
            "branchLang[ar][branch_name]":{
                require_from_group: [1, ".branch_name"]
            },
            "branchLang[ru][branch_name]":{
                require_from_group: [1, ".branch_name"]
            },
            "branchLang[en][branch_name]":{
                require_from_group: [1, ".branch_name"]
            },
            "branchLang[he][street_address]":{
                require_from_group: [1, ".street_address"]
            },
            "branchLang[ar][street_address]":{
                require_from_group: [1, ".street_address"]
            },
            "branchLang[ru][street_address]":{
                require_from_group: [1, ".street_address"]
            },
            "branchLang[en][street_address]":{
                require_from_group: [1, ".street_address"]
            },
            "Branch[zip_code]":{
                zipcode:true
            },
            "Branch[branch_phone]":{
                required:true,
                custom_phone:true
            },
            "branchLang[he][manager]":{
                require_from_group: [1, ".manager-name"],
                custom_text:true
            },
            "branchLang[ar][manager]":{
                require_from_group: [1, ".manager-name"],
                custom_text:true
            },
            "branchLang[ru][manager]":{
                require_from_group: [1, ".manager-name"],
                custom_text:true
            },
            "branchLang[en][manager]":{
                require_from_group: [1, ".manager-name"],
                custom_text:true
            },
            "Branch[mobile_manager]":{
                custom_mobile:true
            },
            "Branch[latitude]":{
                required:true,
                custom_float:true
            },
            "Branch[longitude]":{
                required:true,
                custom_float:true
            },
            "Opening[sunday][open_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[sunday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[sunday][close_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[sunday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[monday][open_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[monday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[monday][close_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[monday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[tuesday][open_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[tuesday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[tuesday][close_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[tuesday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[wednesday][open_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[wednesday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[wednesday][close_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[wednesday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[thursday][open_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[thursday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[thursday][close_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[thursday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[friday][open_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[friday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[friday][close_hour]":{
                required: {
                    depends: function(element){
                        return !$(document).find('input[name="Opening[friday][is_closed]"]').is(":checked");
                    }
                },
            },
            "Opening[saturday][open_hour]":{
                required: {
                    depends: function(element){
                        return  !$(document).find('input[name="Opening[saturday][is_closed]"]').is(":checked") ?
                            ($(document).find('input[name="Opening[saturday][shabat_radio]"]').is(":checked") ? false : true ) : false;
                    }
                }
            },
            "Opening[saturday][close_hour]":{
                required: {
                    depends: function(element){
                        return  !$(document).find('input[name="Opening[saturday][is_closed]"]').is(":checked") ?
                            ($(document).find('input[name="Opening[saturday][shabat_radio]"]').is(":checked") ? false : true ) : false;
                    }
                }
            },
            "Opening[saturday][shabat_grace]":{
                required: {
                    depends: function(element){
                        var res = false;
                        if($(document).find('input[name="Opening[saturday][shabat_radio]"]').is(":checked")){
                            res = true;
                        }
                        return res;
                    }
                },
                custom_positive:true
            },
            "Opening[saturday][shabat_close_hour]":{
                required: {
                    depends: function(element){
                        var res = false;
                        if($(document).find('input[name="Opening[saturday][shabat_radio]"]').is(":checked")){
                            res = true;
                        }
                        return res;
                    }
                },
            }
        },
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.removeClass(errorClass);
        },
        submitHandler: function(form){
            form.submit();
        }
    });

    $('input[type="checkbox"].closed').click(function(){
        var el1 = $(this).closest('.branch-hours').find('.week-day:first');
        var el2 = $(this).closest('.branch-hours').find('.week-day:last');
        var el3 = $(this).closest('.branch-hours').find('.normal:first');
        var el4 = $(this).closest('.branch-hours').find('.normal:last');
        var validator = $("form").validate();
        el1.length > 0 ? validator.element( el1 ) : false;
        el2.length > 0 ? validator.element( el2 ) : false;
        el3.length > 0 ? validator.element( el3 ) : false;
        el4.length > 0 ? validator.element( el4 ) : false;
    });

    var booRadio = $("#branch").find('.radio-check').prop('checked');
    $("#branch").on('click', '.radio-check', function(event){
        if(booRadio == false){
            $(this).prop('checked',true);
            booRadio = true;
        } else {
            $(this).prop('checked',false);
            booRadio = false;
        }
        var el1 = $(this).closest('.branch-hours').find('.normal:first');
        var el2 = $(this).closest('.branch-hours').find('.normal:last');
        var validator = $("form").validate();
        el1.length > 0 ? validator.element( el1 ) : false;
        el2.length > 0 ? validator.element( el2 ) : false;
    });

    $(document).on('switchChange.bootstrapSwitch','.switcher', function(event, state) {
        var path = location.pathname;
        $.post(path + "/activate", "active=" + (!state ? 0 : 1 ) + "&item-id=" +  $(this).data("item"));
    });

    if($('.flash-message').has('.alert').length>0){
        setTimeout(function(){
            $('.flash-message').slideUp('400');
            $('.flash-message').empty();
        },2000);
    }

    $('[name="Branch[branch_no]"]').on('change',function(e){
        $('[name="Branch[branch_no]"]').val($(this).val());
    });

    $('[name="Branch[zip_code]"]').on('change',function(e){
        $('[name="Branch[zip_code]"]').val($(this).val());
    });

    $('[name="Branch[branch_phone]"]').on('change',function(e){
        $('[name="Branch[branch_phone]"]').val($(this).val());
    });

    $('[name="Branch[latitude]"]').on('change',function(e){
        $('[name="Branch[latitude]"]').val($(this).val());
    });

    $('[name="Branch[longitude]"]').on('change',function(e){
        $('[name="Branch[longitude]"]').val($(this).val());
    });

    $('form.readOnly input[type="checkbox"], form.readOnly input[type="radio"], form.readOnly input[type="file"], form.readOnly .select2').attr('disabled','disabled');
    $('form.readOnly textarea, form.readOnly input[type!="checkbox"]').attr('readonly','readonly');
});