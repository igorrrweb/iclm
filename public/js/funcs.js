function readURL(input, previewElement) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(previewElement).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function unique(array){
    return array.filter(function(el, index, arr){
        return index == arr.indexOf(el);
    });
}

function cb(start, end) {
    $('#select_start_date').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    $('#select_start_date').attr('data-startDate',start.format('DD/MM/YYYY'));
    $('#select_start_date').attr('data-endDate',end.format('DD/MM/YYYY'));

}

function ucfirst(str){
    var f = str.charAt(0).toUpperCase();
    return f+str.substr(1,str.length-1);
}

function switcherInit(){
    $(".switcher").bootstrapSwitch({
        size:'mini'
    });
}

function fillupAutocomplete(data, element){
    element.empty();
    if (data.length === 0)
    {
        element.empty();
        element.append("<li>"+element.data('message')+"</li>");
        element.slideDown();
        setTimeout(function(){
            element.slideUp();
        },1500);


    } else {
        element.empty();
        for (var i = 0; i < data.length; i++)
        {
            var elementClass = i % 2 ? "event" : "odd";
            element.append("<li class='"+elementClass+"'>" + data[i].user + "</li>");
        }
        element.slideDown();
    }

}



