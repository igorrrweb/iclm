$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    $('body.rtl .panel-body').css({
        direction: 'ltr'
    });
    cb(moment($('#select_start_date').data('startdate'),'DD/MM/YYYY'),moment($('#select_start_date').data('enddate'),'DD/MM/YYYY'));

    function setLabel(legend){
        $('#select_start_date').attr('data-legend',legend);
    }


    function getLabel(){
        return $('#select_start_date').attr('data-legend');
    }

    function setTitle(title,start,finish){
        $('#start-date').html(start);
        $('.chart-title-span').html(title);
        $('#end-date').html(finish);
    }

    function setPage(){
        var startDate = $('#select_start_date').data('startdate');
        var endDate = $('#select_start_date').data('enddate');
        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        var dates = [startDate,endDate];
        var requestUrl = baseUrl+"reports/getSegmentData/" + id;



        $.post(requestUrl, {dates: JSON.stringify(dates), flag:$('.segment-tab li.active a').attr('aria-controls') }, function (response) {
            var res = JSON.parse(response);
            $('.chartContainer').attr('data-values',res.data);
            $('.chartContainer').attr('data-labels',res.labels);
            setLabel($('.segment-tab li.active a').text());
            setTitle($('.segment-tab li.active a').text(),startDate,endDate);
            setChart(res.labels,res.data);
        });
    }
    var _readyLib = setInterval(function(){
        if ($.fn.daterangepicker){
            setPage();
            clearInterval(_readyLib);
        }
    },100);


    $('#select_start_date').daterangepicker({
        startDate: moment($('#select_start_date').data('startdate'),'DD/MM/YYYY').format('DD/MM/YYYY'),
        endDate: moment($('#select_start_date').data('enddate'),'DD/MM/YYYY').format('DD/MM/YYYY'),
        "cancelClass": "btn-sm",
        "applyClass": "btn-sm btn-primary get-chart",
        "opens": "left",
        locale: {
            format: 'DD/MM/YYYY'
        },
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#select_start_date').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate.format('DD/MM/YYYY');
        var endDate = picker.endDate.format('DD/MM/YYYY');
        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        var dates = [startDate,endDate];
        var requestUrl = baseUrl+"reports/getSegmentData/" + id;

        $.post(requestUrl, {dates: JSON.stringify(dates), flag:$('.segment-tab li.active a').attr('aria-controls') }, function (response) {
            var res = JSON.parse(response);
            $('.chartContainer').attr('data-values',res.data);
            $('.chartContainer').attr('data-labels',res.labels);


            setLabel($('.segment-tab li.active a').text());
            setTitle($('.segment-tab li.active a').text(),startDate,endDate);
            setChart(res.labels,res.data);

        });

    });

    function setChart(arrLabels, arrValues) {
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'flot-line-chart',
                type: 'line'
            },
            title: {
                text: null
            },
            xAxis: {
                categories: arrLabels,
                labels: {
                    rotation: -90,
                    style: {
                        fontSize: '13px',
                    }
                },
                reversed: lang == 'en' ? false : true,
            },
            yAxis: {
                title: null,
                opposite: lang == 'en' ? false : true,
            },
            series: [
                {
                    name: getLabel(),
                    data: arrValues,
                }
            ],
            credits: {
                enabled: false
            },
            legend:{
                enabled:true
            },
            tooltip:{
                enabled:true
            },
        });
    }

    $(document).on('click','.segment-tab li',function(){
        var startDate = $('#select_start_date').data('startdate');
        var endDate = $('#select_start_date').data('enddate');
        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        var dates = [startDate,endDate];
        var requestUrl = baseUrl+"reports/getSegmentData/" + id;
        var flag = $(this).find('a:first').attr('aria-controls');
        $.post(requestUrl, {dates: JSON.stringify(dates), flag:flag }, function (response) {
            var res = JSON.parse(response);
            $('.chartContainer').attr('data-values',res.data);
            $('.chartContainer').attr('data-labels',res.labels);
            setLabel(ucfirst(flag));
            setTitle($('.segment-tab li.active a').text(),startDate,endDate);
            setChart(res.labels,res.data);
        });
    });

});