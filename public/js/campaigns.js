$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    $('.select2').select2({
        language: lang,
    });

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": '<".panel panel-default"<".panel-heading"<".row"<".col-xs-12 col-sm-6 col-md-3 col-lg-2"l><".col-xs-12 col-sm-6 col-md-9 col-lg-10"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">',
        ajax: {
            url: baseUrl+"campaigns/getDataTable",
            method: "POST",
            "data": function ( d ) {
                d.campaign_type = $('.select2').select2("val");
            }
        },
        columns: [
            {data: "name"},
            {data: "audience"},
            {data: "campaign_type"},
            {data: "expiry_date"},
            {data: "redeemed_completed"},
            {data: "status",
                render: function(data, type, row){
                    if (data == '1') {
                        return '<input type="checkbox" checked="checked" class="activate switcher">'
                    } else {
                        return '<input type="checkbox" class="activate switcher">'
                    }
                }
            },
            {data: "actions",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a href="'+baseUrl+row.campaign_type.toLowerCase()+'/update/'+data+'"><i class=\"fa fa-edit\"></i></a> '+
                        '<a class="confirm" data-item="'+data+'" data-msg="Are you sure you want to delete '+row.name+'?">'+
                        '<i class="fa fa-times"></i></a> '+
                        '<a href="'+baseUrl+row.campaign_type.toLowerCase()+'/view/'+data+'" data-type="'+row.campaign_type.toLowerCase()+'">'+
                        '<i class="fa fa-list-alt"></i></a>';
                }
            }

        ],
        "fnDrawCallback": function() {
            switcherInit();
        }

    });

    $(document).on('change', '.select2', function(){
        table.draw();
    });

    $(document).on("click",".confirm",function(){
        var langs = JSON.parse($("#jsTranslate").text());
        var id = $(this).data("item");
        var type = $(this).data('type');
        $.confirm({
            title: langs[0],
            content: $(this).data("msg"),
            icon: "fa fa-bullseye",
            confirmButton: langs[0],
            cancelButton: langs[1],
            theme: 'hololight',
            confirm: function(){
                $.post(baseUrl+type+"/delete",{id:id},function(response){
                    if(response.status){
                        table.row(id).remove().draw();
                    } else {
                        console.log(response.error);
                    }
                },"json");
            }
        })
    });


});
