$(function() {
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn">>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        "search": {
            "caseInsensitive": false
        },
        ajax: {
            url: baseUrl + "customer/getDataTable",
            method: "POST"
        },
        columns: [
            {data: "id"},
            {data: "name"},
            {data: "join_date"},
            {data: "last_visit"},
            {data: "status"},
            {
                data: "actions",
                searchable: false,
                orderable: false,
                render: function (data, type, row) {
                    return '<a href="' + baseUrl + 'customer/update/' + data + '"><i class=\"fa fa-edit\"></i></a> ' +
                        '<a class="confirm" data-item="' + data + '" data-msg="Are you sure you want to delete ' + row.name + '?">' +
                        '<i class="fa fa-times"></i></a> ' +
                        '<a href="' + baseUrl + 'customer/view/' + data + '">' +
                        '<i class="fa fa-list-alt"></i></a>';
                }
            }
        ]

    });

    $('.createNewBtn').prepend('<a href="' + baseUrl + 'customer/create"><button type="button" class="btn btn-primary btn-block createNew">New Customer</button></a>');

    $('.select2').select2({
        language: lang,
    });

    $('.datepicker').datetimepicker({
        timepicker: false,
        format: "d/m/Y",
        maxDate: 0
    });

    $(document).on("click", ".confirm", function () {
        var langs = JSON.parse($("#jsTranslate").text());
        var id = $(this).data("item");
        $.confirm({
            title: langs[0],
            content: $(this).data("msg"),
            icon: "fa fa-bullseye",
            confirmButton: langs[0],
            cancelButton: langs[1],
            theme: 'hololight',
            confirm: function () {
                $.post(baseUrl + "customer/delete", {id: id}, function (response) {
                    if (response.status) {
                        table.row(id).remove().draw();
                    } else {
                        console.log(response.error);
                    }
                }, "json");
            }
        })
    });

    if ($('.flash-message').has('.alert').length > 0) {
        setTimeout(function () {
            $('.flash-message').slideUp('400');
            $('.flash-message').empty();
        }, 2000);
    }

    if(!$("form").hasClass('readOnly')){
        var validobj = $("form").validate({
            errorClass: 'myErrorClass',
            errorPlacement: function (error, element) {
                var elem = $(element);
                error.insertAfter(element);
            },
            rules: {
                "Customer[firstname]": {
                    required: true,
                    custom_text: true
                },
                "Customer[lastname]": {
                    required: true,
                    custom_text: true
                },
                "Customer[id]": {
                    required: true,
                    custom_id: true,
                    remote: {
                        url: baseUrl + 'customer/validateId',
                        type: "post",
                        data: {
                            id_param: function () {
                                var path = window.location.pathname.split('/');
                                return parseInt(path[path.length - 1]);
                            },
                            name: function () {
                                return $('input[name="Customer[id]"]').val();
                            }
                        }
                    }
                },
                "Customer[phone]": {
                    required: true,
                    custom_phone: true,
                    remote: {
                        url: baseUrl + 'customer/validatePhone',
                        type: "post",
                        data: {
                            id_param: function () {
                                var path = window.location.pathname.split('/');
                                return parseInt(path[path.length - 1]);
                            },
                            name: function () {
                                return $('input[name="Customer[phone]"]').val();
                            }
                        }
                    }
                },
                "Customer[mobile]": {
                    required: true,
                    custom_mobile: true,
                    remote: {
                        url: baseUrl + 'customer/validateMobile',
                        type: "post",
                        data: {
                            id_param: function () {
                                var path = window.location.pathname.split('/');
                                return parseInt(path[path.length - 1]);
                            },
                            name: function () {
                                return $('input[name="Customer[mobile]"]').val();
                            }
                        }
                    }
                },
                "Customer[email]": {
                    email: true,
                },
                "Customer[zip_code]": {
                    zipcode: true
                },
                "Customer[lc_number]": {
                    lc_number: true,
                    required: true
                },
                "Customer[last_digits_cc]": {
                    number: true
                }
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.addClass(errorClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.removeClass(errorClass);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    $('form.readOnly input[type="checkbox"], form.readOnly input[type="radio"], form.readOnly input[type="file"], form.readOnly .select2').attr('disabled','disabled');
    $('form.readOnly textarea, form.readOnly input[type!="checkbox"]').attr('readonly','readonly');


});
