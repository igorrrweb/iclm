$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
            '<".panel panel-default"<".panel-heading"<".row"<".col-xs-12 col-sm-6 col-md-3 col-lg-2"l><".col-xs-12 col-sm-6 col-md-9 col-lg-10"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-xs-12 col-sm-6 col-md-9 col-lg-10"f><".col-xs-12 col-sm-6 col-md-3 col-lg-2"l>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        ajax: {
            url: baseUrl+"reports/getSegmentsDataTable",
            method: "POST"
        },
        columns: [
            {data: "name",
                render: function(data, type, row){
                    return '<a href="'+baseUrl+'reports/segment/'+row.id+'">'+data+'</a>';
                }
            },
            {data: "audience"},
            {data: "redeemed" },
            {data: "available_coupons"},
            {data: "available_engagements"}
        ],
    });
});