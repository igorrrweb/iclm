$(function() {
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn">>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        ajax: {
            url: baseUrl + "message/getDataTable",
            method: "POST"
        },
        columns: [
            {data: 'id'},
            {data: 'title'},
            {data: 'duration'},
            {data: 'end_date'},
            {data: 'audience'},
            {
                data: 'active',
                searchable: false,
                render: function (data, type, row) {
                    if (data == '1') {
                        return '<input type="checkbox" data-item="' + row.id + '" checked="checked" class="activate switcher">'
                    } else {
                        return '<input type="checkbox" data-item="' + row.id + '" class="activate switcher">'
                    }
                }
            },
            {
                data: "actions",
                searchable: false,
                orderable: false,
                render: function (data, type, row) {
                    return '<a href="' + baseUrl + 'message/update/' + data + '"><i class=\"fa fa-edit\"></i></a> ' +
                        '<a class="confirm" data-item="' + data + '" data-msg="Are you sure you want to delete ' + row.title + '?">' +
                        '<i class="fa fa-times"></i></a> ' +
                        '<a href="' + baseUrl + 'message/view/' + data + '">' +
                        '<i class="fa fa-list-alt"></i></a>';
                }
            },
        ],
        "fnDrawCallback": function () {
            switcherInit();
        }

    });

    $('.createNewBtn').prepend('<a href="' + baseUrl + 'message/create"><button type="button" class="btn btn-primary btn-block createNew">New Message</button></a>');

    $('.datepicker').datetimepicker(
        {
            timepicker: false,
            format: "d/m/Y"
        }
    );

    var segmentTable = $('#segments').DataTable({
        "processing": true,
        "serverSide": true,
        ajax: {
            url: baseUrl + "message/getSegment",
            method: "POST",
            "data": function (d) {
                var params = window.location.pathname.split('/');
                var getParam = params.length > 0 ? parseInt(params.pop()) : '';
                d.getParam = getParam;
            }
        },
        columns: [
            {
                data: 'active_segment',
                searchable: false,
                render: function (data, type, row) {
                    if (data == '1') {
                        return '<input type="checkbox" class="icheck" name="Segments[]" checked="checked" value="' + row.id + '">'
                    } else {
                        return '<input type="checkbox" class="icheck" name="Segments[]" value="' + row.id + '">'
                    }
                }
            },
            {data: 'name'},
            {data: 'audience'}
        ],
        "fnDrawCallback": function () {
            $('form.readOnly input[type="checkbox"]').attr('disabled','disabled');
        }
    });
    if(!$("form").hasClass('readOnly')){
        var $validator = $("form").validate({
            errorClass: 'myErrorClass',
            errorPlacement: function (error, element) {
                var elem = $(element);
                error.insertAfter(element);
            },
            rules: {
                "MessageTranslation[he][title]": {
                    required: true,
                    remote: {
                        url: baseUrl + 'message/validateTitle',
                        type: "post",
                        data: {
                            id_param: function () {
                                var path = window.location.pathname.split('/');
                                return parseInt(path[path.length - 1]);
                            },
                            lang: function () {
                                return 'he';
                            },
                            name: function () {
                                return $('input[name="MessageTranslation[he][title]"]').val();
                            }
                        }
                    }
                },
                "MessageTranslation[he][content]": {
                    required: true
                },
                "messageImage": {
                    required: {
                        depends: function (element) {
                            return $('#messageGeneralDetails .previewImage').attr('src') ? false : true;
                        }
                    },
                    image: true
                },
                "Message[start_date]": {
                    required: true
                },
                "Message[end_date]": {
                    required: true,
                    endDate: true
                },

            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.addClass(errorClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                elem.removeClass(errorClass);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }


    $('#rootwizard').bootstrapWizard({
        onTabClick: function(tab,navigation,index)
        {
            var $valid = $("form").valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            $(".previewTitle").text($("#messageTitle").val());
            $(".previewContent").text($("#messageContent").val());

        },
        onNext: function(tab,navigation,index)
        {
            var $valid = $("form").valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            $("body").scrollTop(0);
            $(".previewTitle").text($("#messageTitle").val());
            $(".previewContent").text($("#messageContent").val());

        },
        onPrevious: function(tab,navigation,index)
        {
            $("body").scrollTop(0);
        }
    });


    $(document).on("click",".confirm",function(){
        var langs = JSON.parse($("#jsTranslate").text());
        var id = $(this).data("item");
        $.confirm({
            title: langs[0],
            content: $(this).data("msg"),
            icon: "fa fa-bullseye",
            confirmButton: langs[0],
            cancelButton: langs[1],
            theme: 'hololight',
            confirm: function(){
                $.post(baseUrl+"message/delete",{id:id},function(response){
                    if(response.status){
                        table.row(id).remove().draw();
                    } else {
                        console.log(response.error);
                    }
                },"json");
            }
        })
    });

    if($('.flash-message').has('.alert').length>0){
        setTimeout(function(){
            $('.flash-message').slideUp('400');
            $('.flash-message').empty();
        },2000);
    }

    $("#imageUpload").on("change",function(){
        var input = $(this);
        readURL(input[0], "#messageGeneralDetails .previewImage");
    });

    $(document).on('switchChange.bootstrapSwitch','.switcher', function(event, state) {
        var path = location.pathname;
        $.post(path + "/activate", "active=" + (!state ? 0 : 1 ) + "&item-id=" +  $(this).data("item"));
    });

    $('form.readOnly input[type="checkbox"], form.readOnly input[type="radio"], form.readOnly input[type="file"], form.readOnly .select2').attr('disabled','disabled');
    $('form.readOnly textarea, form.readOnly input[type!="checkbox"]').attr('readonly','readonly');

});