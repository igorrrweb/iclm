$(function() {
    $('body.rtl .panel-body').css({
        direction: 'ltr'
    });
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"B><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn"B>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    $('.select2').select2({
        language: lang,
    });

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        buttons:['csv','excel'],
        ajax: {
            url: baseUrl+"reports/getCouponsDataTable",
            method: "POST",
            "data": function ( d ) {
                d.category = $('.select2.category').select2("val");
                d.product = $('.select2.product').select2("val");
            }
        },
        columns: [
            { data:'id'},
            { data:'name',
                render: function(data,type,row){
                    return '<a href="'+baseUrl+'reports/coupon/'+row.id+'">'+data+'</a>';
                }
            },
            { data:'category'},
            { data:'audience'},
            { data:'coupon_type'},
            { data:'redeemed'},
            { data:"percentage_of_goal_reached"}
        ]
    });

    Highcharts.setOptions({
        colors: ['#0B62A4']

    });

    $.each($(".barChart"),function(index,value){
        var titleText = $(value).parent().siblings('.panel-heading').text();
        var dataValues = JSON.parse( JSON.stringify( $(value).data("values") ) );
        var dataTitle = JSON.parse( JSON.stringify( $(value).data('title') ) );

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: value,
                type: 'column'
            },
            title: {
                text: null
            },
            xAxis: {
                categories: dataTitle,
                labels: {
                    rotation: -90,
                    style: {
                        fontSize: '13px',
                    }
                },
                reversed: lang == 'en' ? false : true,
            },
            yAxis: {
                title: null,
                opposite: lang == 'en' ? false : true,
            },
            series: [
                {
                    name: titleText,
                    data: dataValues,
                    dataLabels: {
                        enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                            fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }
            ],
            credits: {
                enabled: false
            },
            legend:{
                enabled:false
            },
            tooltip:{
                enabled:false
            }
        });
    });

    $.each($("#morris-donut-chart"),function(index,value){

        Morris.Donut({
            element: 'morris-donut-chart',
            data: [{
                label: "Active Coupons",
                value: $("#morris-donut-chart").data('activecoupons')
            },
                {
                    label: "Total Coupons",
                    value: $("#morris-donut-chart").data('totalcoupons')
                }],
            resize: true
        });
        $("#morris-donut-chart").height($("#CouponsRedeemedWeek").height());
    });

    $(document).on('change', '.select2', function(){
        table.draw();
    });

    $(window).resize(function(){
        $("#morris-donut-chart svg").height($("#CouponsRedeemedWeek svg").height());
    });

});