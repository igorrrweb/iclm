$(function(){
    var baseUrl = $('.baseUrl').val();

    jQuery.validator.addMethod("custom_mobile", function(value, element) {
        if(value){
            return /^\d{2,3}-?\d{3}-?\d{3,4}$/.test(value);
        } else {
            return true;
        }
    }, 'Mobile number is invalid.');

    jQuery.validator.addMethod("lc_number", function(value, element) {
        return /^\d+$/.test(value) && value.length <= 10;
    }, 'Insert a number of 10 digits max.');

    jQuery.validator.addMethod("custom_id", function(value, element) {
        return /^\d+$/.test(value) && value.length == 9;
    }, 'ID Number is invalid.');

    jQuery.validator.addMethod("custom_text", function(value, element) {
        return /^([^0-9]*)$/.test(value);
    }, 'Please use letters only.');

    jQuery.validator.addMethod("custom_phone", function(value, element) {
        if(value){
            return /^\d{2,3}-?\d{3}-?\d{3,4}$/.test(value);
        } else {
            return true;
        }
    }, 'Phone number is invalid.');

    jQuery.validator.addMethod("zipcode", function(value, element) {
        if(value){
            return /^\d+$/.test(value) && value.length >= 5;
        } else {
            return true;
        }
    }, 'Zipcode is invalid.');

    jQuery.validator.addMethod("lastCC", function(value, element) {
        return /^\d+$/.test(value) && value.length === 4;
    }, 'lastCC is invalid.');

    jQuery.validator.addMethod("image", function(value, element) {
        if(value){
            var ext = value.indexOf(".", value.length-5);
            var imageExts = [".gif", ".png", ".jpg", ".jpeg"];
            return (imageExts.indexOf(value.substr(ext).toLowerCase()) == -1) ? false : true;
        } else {
            return true;
        }
    }, 'Please upload only jpg,jpef,png,gif images.');

    jQuery.validator.addMethod("endDate", function(value, element) {
        var date = value.substr(0,10).split("/");
        var reformattedDate = date[1] + "/" + date[0]  + "/" + date[2] + " " + value.substr(10,9);
        return Date.now() < Date.parse(reformattedDate);
    }, 'Date must end in the future.');

    jQuery.validator.addMethod("barcode", function(value, element) {
        var bool = false;
        $.ajax({
            url: baseUrl+"coupon/getCategory",
            async: false,
            type: "post",
            data: {
                barcode:value
            },
            success: function(response){
                response = JSON.parse(response);
                if(response.id){
                    $('.c-control').val(response.category);
                    bool = true;
                } else {
                    $('.c-control').val('');
                }
            }
        });
        return bool;
    }, "We don't have any product that matches that barcode, please make sure you're using the right barcode.");

    jQuery.validator.addMethod("custom_float", function(value, element) {
        if(value){
            return !isNaN(parseFloat(value));
        }else{
            return true;
        }
    }, 'The number inserted is incorrect.');

    jQuery.validator.addMethod("custom_positive", function(value, element) {
        if(value){
            return parseInt(value) > 0;
        } else {
            return true;
        }
    }, 'This field is required.');

});