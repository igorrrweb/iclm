$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"B><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn"B>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        buttons:['csv','excel'],
        ajax: {
            url: baseUrl+"reports/getHistoryDataTable",
            method: "POST",
            "data": function ( d ) {
                d.lc = $('input[name="filterValueLcNumber"]').val();
                d.uid = $('input[name="filterValueUserId"]').val();
            }
        },
        columns: [
            { data:"date"},
            { data:"duration"},
            { data:"amount"},
            { data:"redeemed"}
        ]
    });

    $(document).on('keyup','input[name="filterValueUserId"]',function(){
        var autoComplete =  $(this).siblings(".autoComplete");
        $.post(baseUrl+'reports/autoComplete',
            {
                filterValueUserId:$(this).val(),
                extracolumn:'LcNumber',
                extravalue:$('input[name="filterValueLcNumber"]').val()
            },
            function(response){
                fillupAutocomplete(response, autoComplete)
        },'json');
    });

    $(document).on('keyup','input[name="filterValueLcNumber"]',function(){
        var autoComplete =  $(this).siblings(".autoComplete");
        $.post(baseUrl+'reports/autoComplete',
            {
                filterValueLcNumber:$(this).val(),
                extracolumn:'UserId',
                extravalue:$('input[name="filterValueUserId"]').val()
            },
            function(response){
                fillupAutocomplete(response, autoComplete)
            },'json');
    });

    $(document).on('click','.autoComplete li',function(){
        $(this).parent().siblings("input").val($(this).text());
        $(this).parent().slideUp();
    });

    $(document).on('blur','.autocomplete-parent input',function(){
        $(this).siblings('.autoComplete').slideUp();
    });




});