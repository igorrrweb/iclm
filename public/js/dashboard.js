$(function() {
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    $('body.rtl .panel-body').css({
        direction: 'ltr'
    });

    $.each($(".barChart"),function(index,value){
        var titleText = $(value).parent().siblings('.panel-heading').text();
        var dataTitle = [];
        var dataValues = [];
        JSON.parse( JSON.stringify( $(value).data("values") ), function(key, value){
            dataTitle.push(key);
            dataValues.push(value);
        } );
        dataValues.pop();
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: value,
                type: 'column'
            },
            title: {
                text: null
            },
            xAxis: {
                categories: dataTitle,
                labels: {
                    rotation: -90,
                    style: {
                        fontSize: '13px',
                    }
                },
                reversed: lang == 'en' ? false : true,
            },
            yAxis: {
                title: null,
                opposite: lang == 'en' ? false : true,
            },
            series: [
                {
                    name: titleText,
                    data: dataValues,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }
            ],
            credits: {
                enabled: false
            },
            legend:{
                enabled:false
            },
            tooltip:{
                enabled:false
            }
        });
    });

});
