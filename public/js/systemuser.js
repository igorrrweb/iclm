$(function() {
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn">>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        ajax: {
            url: baseUrl+"systemuser/getDataTable",
            method: "POST"
        },
        columns: [
            { data:'id'},
            { data:'username'},
            { data:'role'},
            { data:'added'},
            { data:'last_login'},
            { data:'status',
                searchable: false,
                render: function(data, type, row){
                    if (data == '1') {
                        return '<input type="checkbox" data-item="'+row.id+'" checked="checked" class="activate switcher">'
                    } else {
                        return '<input type="checkbox" data-item="'+row.id+'" class="activate switcher">'
                    }
                }
            },
            { data:"actions",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a href="'+baseUrl+'systemuser/update/'+data+'"><i class=\"fa fa-edit\"></i></a> '+
                        '<a class="confirm" data-item="'+data+'" data-msg="Are you sure you want to delete '+row.username+'?">'+
                        '<i class="fa fa-times"></i></a> '+
                        '<a href="'+baseUrl+'systemuser/view/'+data+'">'+
                        '<i class="fa fa-list-alt"></i></a>';
                }
            },
        ],
        "fnDrawCallback": function() {
            switcherInit();
        }

    });

    $('.createNewBtn').prepend('<a href="'+baseUrl+'systemuser/create"><button type="button" class="btn btn-primary btn-block createNew">New System User</button></a>');

    function generatePassword() {
        var charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var retVal = "";
        for (var i = 0; i < 8; i++) {
            retVal += charset.charAt(Math.floor(Math.random() * charset.length));
        }
        return retVal;
    }

    $("#generatePassword").on("click", function () {
        var gen = generatePassword();
        $("#userPassword").val(gen);
        $("#confirmPassword").val(gen);
    });

    $(document).on("click",".confirm",function(){
        var langs = JSON.parse($("#jsTranslate").text());
        var id = $(this).data("item");
        $.confirm({
            title: langs[0],
            content: $(this).data("msg"),
            icon: "fa fa-bullseye",
            confirmButton: langs[0],
            cancelButton: langs[1],
            theme: 'hololight',
            confirm: function(){
                $.post(baseUrl+"systemuser/delete",{id:id},function(response){
                    if(response.status){
                        table.row(id).remove().draw();
                    } else {
                        console.log(response.error);
                    }
                },"json");
            }
        })
    });

    var $validator = $("form.crudForm").validate({
        errorClass:'myErrorClass',
        errorPlacement: function (error, element) {
            var elem = $(element);
            error.insertAfter(element);
        },
        rules:{
            "SystemUser[first_name]":{
                required: true,
                custom_text:true
            },
            "SystemUser[last_name]":{
                required: true,
                custom_text:true
            },
            "SystemUser[username]":{
                required: true,
                remote:{
                    url:baseUrl+'systemuser/validateName',
                    type:"post",
                    data:{
                        id_param: function(){
                            var path = window.location.pathname.split('/');
                            return parseInt(path[path.length-1]);
                        },
                        name: function(){
                            return $('input[name="SystemUser[username]"]').val();
                        }
                    }
                }
            },
            "SystemUser[password]":{
                required: true,
            },
            "confirmPassword" : {
                equalTo : "#userPassword"
            },
            "SystemUser[email]":{
                required: true,
                email:true
            },
            "SystemUser[phone]":{
                custom_phone:true
            },
        },
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.removeClass(errorClass);
        },
        submitHandler: function(form){
            form.submit();
        }
    });

    $(document).on('switchChange.bootstrapSwitch','.switcher', function(event, state) {
        var path = location.pathname;
        $.post(path + "/activate", "active=" + (!state ? 0 : 1 ) + "&item-id=" +  $(this).data("item"));
    });

    if($('.flash-message').has('.alert').length>0){
        setTimeout(function(){
            $('.flash-message').slideUp('400');
            $('.flash-message').empty();
        },2000);
    }


    var formChangePassword = $("form.changePassword").validate({
        errorClass:'myErrorClass',
        errorPlacement: function (error, element) {
            var elem = $(element);
            error.insertAfter(element);
        },
        rules:{
            password:{
                required: true,
            },
            confirmPassword : {
                equalTo : "#password"
            }
        },
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.removeClass(errorClass);
        },
        submitHandler: function(form){

        }
    });



    $(document).on('click','.save-changed-password', function(e){
        e.preventDefault();
        var $valid = $("form.changePassword").valid();
        if(!$valid) {
            $validator.focusInvalid();
            return false;
        } else {
            var param = $("form.changePassword").serialize();
            $.post(baseUrl+"systemuser/changePassword",param,function(response){
                if(response == "true"){
                    $('.change-response').html('<div class="form-group"><div class="col-xs-12 col-md-12 alert alert-success">Password has been changed successfully</div></div>');
                    setTimeout(function(){
                        $('.change-response').slideUp(400);
                        $('.change-response').empty();
                    },2000);
                }
            },'json');
        }
    });

});