$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    
    $('.select2').select2({
        language: lang,
    });


    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": '<".panel panel-default"<".panel-heading"<".row"<".col-xs-12 col-sm-6 col-md-3 col-lg-2"l><".col-xs-12 col-sm-6 col-md-9 col-lg-10"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">',
        ajax: {
            url: baseUrl+"churnalert/getEditDataTable",
            method: "POST",
            "data": function ( d ) {
                //d.campaign_type = $('.select2').select2("val");
            }
        },
        columns: [
            {data:"id"},
            {data:"date"},
            {data:"activity",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return "<select class='select-2'>"+
                        "<option value='' selected>"+row.action+"</option>"+
                        "<option value='1'>"+row.feedback+"</option>"+
                        "<option value='2'>"+row.coupon+"</option>"+
                        "<option value='3'>"+row.flip_tiles+"</option>"+
                        "<option value='4'>"+row.phone_call+"</option>"+
                        "</select>";
                }
            },
        ],
        "fnDrawCallback": function() {
            $('.select-2').select2({
                language: lang,
            });
        }

    });

    $('.save-btn').click(function(){
        event.preventDefault();
        var activity = [];
        $('#indexForm table').find('tr[data-add-activity="true"][date-saved="false"]').each(function(){
            activity.push([$(this).find('td:eq(1)').text(), $(this).find('.select2').select2("val")]);
            $(this).attr('date-saved','true');
        });
    });



});