$(function() {
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    /*-----TOS-----*/
    $(".previewBtn").on("click", function () {
        $(".previewBody").text($(".langTabData.active .tosText").val());
    });

    $("#systemuser .crudForm").on("submit", function () {
        if ($("#userPassword").val() != $("#confirmPassword").val()) {
            $("#passwordsNotMatching").fadeIn();
            return false;
        }
    });

    $("#userPassword, #confirmPassword").on("blur", function () {
        $("#passwordsNotMatching").fadeOut();
    });

    $('.select2').select2({
        language: lang,
    });

    var validobj = $("form").validate({
        errorClass:'myErrorClass',
        errorPlacement: function (error, element) {
            var elem = $(element);
            error.insertAfter(element);
        },
        rules:{
            "Settings[max_allow_share]":{
                required:true
            },
            "Settings[max_allow_push]":{
                number:true
            },
            "Settings[contact_us]":{
                email:true
            },
            "Settings[closing_reminder]":{
                number:true
            }
        },
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.removeClass(errorClass);
        },
        submitHandler: function(form){
            form.submit();
        }
    });

    if($('.flash-message').has('.alert').length>0){
        setTimeout(function(){
            $('.flash-message').slideUp('400');
            $('.flash-message').empty();
        },2000);
    }

});