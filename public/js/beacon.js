$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-xs-12 col-sm-6 col-md-3 col-lg-2"l><".col-xs-12 col-sm-6 col-md-9 col-lg-10"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-xs-12 col-sm-6 col-md-9 col-lg-10"f><".col-xs-12 col-sm-6 col-md-3 col-lg-2"l>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,

        ajax: {
            url: baseUrl+"beacon/getDataTable",
            method: "POST"
        },
        columns: [
            { data:"id"},
            { data:"name"},
            { data:"branch"},
            { data:"in_store_location"},
            { data:"last_health_check"},
            { data:"actions",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a href="'+baseUrl+'beacon/view/'+data+'">[Details]</a>';
                }
            }
        ]

    });



    $('.select2').select2({
        language: lang,
    });

    $('.datepicker').datetimepicker({
        timepicker: false,
        format: "d/m/Y",
        maxDate:0
    });


});
