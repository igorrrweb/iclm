$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");
    var dom = lang == 'en' ?
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-12 col-md-3 col-lg-2 createNewBtn"><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-7 col-md-6 col-lg-8"f>>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">'
        :
        '<".panel panel-default"<".panel-heading"<".row"<".col-sm-7 col-md-6 col-lg-8"f><".col-sm-5 col-md-3 col-lg-2"l><".col-sm-12 col-md-3 col-lg-2 createNewBtn">>><".panel-body"<".dataTable_wrapper table-responsive"rtp>>><"clearfix">';

    var table = $('#dataTables').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": dom,
        ajax: {
            url: baseUrl+"feedback/getDataTable",
            method: "POST"
        },
        columns: [
            { data:'id'},
            { data:'title'},
            { data:'audience'},
            { data:'status',
                searchable: false,
                render: function(data, type, row){
                    if (data == '1') {
                        return '<input type="checkbox" checked="checked" data-item="'+row.id+'" class="activate switcher">'
                    } else {
                        return '<input type="checkbox" data-item="'+row.id+'" class="activate  switcher">'
                    }
                }
            },
            { data:"actions",
                searchable: false,
                orderable:false,
                render: function(data, type, row){
                    return '<a href="'+baseUrl+'feedback/update/'+data+'"><i class=\"fa fa-edit\"></i></a> '+
                        '<a class="confirm" data-item="' + data + '" data-msg="Are you sure you want to delete '+row.title+'?">'+
                        '<i class="fa fa-times"></i></a> '+
                        '<a href="'+baseUrl+'feedback/view/'+data+'">'+
                        '<i class="fa fa-list-alt"></i></a>';
                }
            },
        ],
        "fnDrawCallback": function() {
            switcherInit();
        }

    });

    $('.createNewBtn').prepend('<a href="'+baseUrl+'feedback/create"><button type="button" class="btn btn-primary btn-block createNew">New Feedback</button></a>');

    $('.select2').select2({
        language: lang,
    });

    var segmentTable = $('#segments').DataTable({
        "processing": true,
        "serverSide": true,
        ajax: {
            url: baseUrl+"feedback/getSegment",
            method: "POST",
            "data": function ( d ) {
                var params = window.location.pathname.split('/');
                var getParam = params.length > 0 ? params.pop() : '';
                d.getParam = getParam;
            }
        },
        columns: [
            { data:'active',
                searchable: false,
                render: function(data, type, row){
                    if(row.active_segment == '1'){
                        return '<input type="checkbox" class="icheck" name="Segments[]" checked="checked" value="'+row.id+'">'
                    } else {
                        return '<input type="checkbox" class="icheck" name="Segments[]" value="'+row.id+'">'
                    }
                }
            },
            { data:'name'},
            { data:'audience'}
        ],
        "fnDrawCallback": function() {
            $('form.readOnly input[type="checkbox"]').attr('disabled','disabled');
        }
    });

    $(document).on("click",".confirm",function(){
        var langs = JSON.parse($("#jsTranslate").text());
        var id = $(this).data("item");
        $.confirm({
            title: langs[0],
            content: $(this).data("msg"),
            icon: "fa fa-bullseye",
            confirmButton: langs[0],
            cancelButton: langs[1],
            theme: 'hololight',
            confirm: function(){
                $.post(baseUrl+"feedback/delete",{id:id},function(response){
                    if(response.status){
                        table.row(id).remove().draw();
                    } else {
                        console.log(response.error);
                    }
                },"json");
            }
        })
    });

    var $validator = $("form").validate({
        errorClass:'myErrorClass',
        errorPlacement: function (error, element) {
            var elem = $(element);
            error.insertAfter(element);
        },
        rules:{
            "Feedback[title]":{
                required: true
            },
            "Feedback[sub_title]":{
                required: true
            },
            "FeedbackQuestion[0][title]":{
                require_from_group: [1, ".questionTitle"]
            },
            "FeedbackQuestion[1][title]":{
                require_from_group: [1, ".questionTitle"]
            },
            "FeedbackQuestion[2][title]":{
                require_from_group: [1, ".questionTitle"]
            },
            "FeedbackQuestion[3][title]":{
                require_from_group: [1, ".questionTitle"]
            },
            "FeedbackQuestion[4][title]":{
                require_from_group: [1, ".questionTitle"]
            },
            "FeedbackQuestion[5][title]":{
                require_from_group: [1, ".questionTitle"]
            },
            "FeedbackQuestion[6][title]":{
                require_from_group: [1, ".questionTitle"]
            },
            "FeedbackQuestion[7][title]":{
                require_from_group: [1, ".questionTitle"]
            },
        },
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.removeClass(errorClass);
        },
        submitHandler: function(form){
            form.submit();
        }
    });

    $(document).on('switchChange.bootstrapSwitch','.switcher', function(event, state) {
        var path = location.pathname;
        $.post(path + "/activate", "active=" + (!state ? 0 : 1 ) + "&item-id=" +  $(this).data("item"));
    });

    if($('.flash-message').has('.alert').length>0){
        setTimeout(function(){
            $('.flash-message').slideUp('400');
            $('.flash-message').empty();
        },2000);
    }

    $('form.readOnly input[type="checkbox"], form.readOnly input[type="radio"], form.readOnly input[type="file"], form.readOnly .select2').attr('disabled','disabled');
    $('form.readOnly textarea, form.readOnly input[type!="checkbox"]').attr('readonly','readonly');
    $('form.readOnly .addMoreQuestions').attr('disabled','disabled');

    $(document).on("change","#feedback .questionType",function(){

        if ($(this).select2('val') == "rating")
        {
            $(this).closest(".row").find(".rating").removeClass('hidden').slideDown();
        } else {
            $(this).closest(".row").find(".rating").addClass('hidden').slideUp();
        }
    });

    $(document).on('click','button.feedback-preview',function(){
        $("#feedbackPreviewTitle").text($("#feedbackTitle").val());
        $("#feedbackPreviewSubTitle").text($("#feedbackSubTitle").val());
        $("#feedbackPreviewQuestions").empty();

        $(document).find(".langTabData.active .feedbackQuestions .questionTitle").each(function(index,value){
            var answer = "<div class='feedbackPreviewAnswer'>";
            var text = JSON.parse($("#jsTranslate").text());
            switch ($(value).closest('.row').find(".select2").select2("val"))
            {
                case "free_text":
                    answer += text[0] + "...";
                    break;
                case "yes_no":
                    answer += '<p class="yesNo">' + text[1] + '<img src="'+baseUrl+'img/yes_no.png">' + text[2] + '</p>';
                    break;
                case "rating":
                    var li = "";
                    $.each($(value).closest('.row').find('.feedbackRating'),function(num,rating){
                        li += "<li class='previewRating fr'><p>|</p>"+$(rating).find('input').val()+"</li>";
                    });
                    answer += '<span class="ratingDelimiter fl">+</span><span class="ratingDelimiter fr">-</span>' +
                        '<img class="cartImg" src="'+baseUrl+'img/cart.png"><ul class="previewRatings">' + li + '</ul>';
                    break;
            }

            answer += "</div><div class='clearfix'></div>";

            $("#feedbackPreviewQuestions").append(
                "<li class='previewQuestion'>" +
                "<p class='h3'>" +
                "<span class='previewQuestionNumber'>"+(index + 1)+"" + "</span>"+$(value).val() +
                "</p>" + answer + "</li>"
            );
        });
    });

    $("#feedback .addMoreQuestions").on("click",function(e){
        e.preventDefault();
        var index = $(".tab-pane.langTabData.active .feedbackQuestions .row").size();
        var lang = $('.tab-pane.langTabData.active .feedbackQuestions').data('lang');
        var html = $("#questionPrototype .row").clone();
        console.log(JSON.stringify(html));
        html.find(".icon").text(index+1);
        html.find(".questionTitle").attr("name","FeedbackQuestion["+lang+"][" + index + "][title]");
        html.find("select").addClass('select2').attr("name","FeedbackQuestion["+lang+"][" + index + "][type]");
        $(html).find('.select2').select2("destroy").select2();
        html.find("input[type='hidden']").val(lang).attr("name","FeedbackQuestion["+lang+"][" + index + "][lang]");
        console.log(html);
        $.each(html.find(".rating ul li.feedbackRating input"), function(index2,value){
            $(value).attr("name","FeedbackQuestion["+lang+"]["+index+"][rating][value" + (index2 + 1) +"]");
        });

        $(".tab-pane.langTabData.active .feedbackQuestions:last").append(html);
    });

});
