$(function(){
    var baseUrl = $('.baseUrl').val();
    var lang = $("body").data("lang");

    $(".changeLang").on("click", function(){
        $.post(baseUrl+"index/switchLang", "lang=" + $(this).data("lang"), function(response){
            location.reload();
        });
    });

});