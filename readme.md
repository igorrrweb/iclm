Это администраторская часть проекта.

выполнена с использованием
phalcon -v  2.0.10
php -v 5.6.4
php-redis -v 2.2.5
mysql -v 5.6.28


Controllers:
-------------
there are two kind of controllers, ControllerBase that extends Controller
and a controller that extends ControllerBase class.

Models:
-------
there are two kind of models, BaseModel that extends Phalcon\Model
and a model that extends BaseModel class.


Views
-------------
Для каждой страницы for exampe страница '/segment' вызывается
layout:  /app/views/layouts/segment.volt
template:  /app/views/segment/index.volt
Также используются macroses /app/views/segment/macroses.volt


Авторизация выполняется путем отправки аякс запроса to /index
В случае удачной авторизации происходит редирект на страницу /dashboard

Access control list реализован в классе /app/library/ACL.php

Перечень страниц хранится в ДБ табл system_resource

Права на доступ к страницам хранятся в ДБ табл system_role_resources

Для отображеня информации на страницах ControllerName/index используется плагин datatable https://datatables.net/

В ходе построения страницы for exampe '/segment'
используется controller:SegmentController.php metod:index.
1) Определяется набор колонок для таблицы, который описан в getAttributesAction().
2) Отрабатывает /public/js/segment.js. А именно плагин datatable (https://datatables.net/) "var table = $('#dataTables').DataTable({})",
который обращается через аякс на /segment/getDataTable и получает данные для формирования таблицы.

Для построения елементов типа селект используется плагин select2.js (http://select2.github.io/)

Валидация форм /segment/create, /segment/update/id реализована с помощью jquery-validate.js (https://jqueryvalidation.org)

Другие страницы работают аналогично.
